package com.android.print;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Set;
import java.util.UUID;
import android.R.bool;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.util.Log; 
 
 /**
 * @author dzq
 *
 */
/**
 * @author dzq
 *
 */
public class Printer {
	
	 private static BluetoothAdapter mBluetoothAdapter   = null;
	 private static BluetoothDevice  mBluetoothDevice    = null; 
	 private static Set<BluetoothDevice> pairedDevices   = null; 
	 private static OutputStream     outStream           = null;  
	 private static BluetoothSocket  mBluetoothtSocket   = null; 
	 private static boolean openflg  = false; 
	 
	 final public static int printString(String content)  
	 {  

		  byte[] print_byte= null;       
		     
		  if (!openflg)  
		  {  
			  return -1;              
		  }     

		  if (content.equals(""))  
		  {
			  return -2;   
		  }      
		  
		  try {  

			  print_byte = content.getBytes("GBK");
			  
			
		} catch (UnsupportedEncodingException e) { 
			// TODO Auto-generated catch block  
			e.printStackTrace();  
		}    

		try { 
//           ���ܰ汾�������й�ȼ��
//			 for(int i=0;i<print_byte.length;i++)
//			 {
//				 outStream.write((print_byte[i])^(0x55));  
//			 }
//			 outStream.write(new byte[]{0x5f});
			
			 outStream.write(print_byte);
			 outStream.write(new byte[]{0x0a}); 
			 outStream.flush();                   
             
		} catch (IOException e) {
			// TODO Auto-generated catch block          
			e.printStackTrace();                     
		}    
		   
		
		return 0;            
		  
	 }    
	  
	 
	 final public static int init() 
	 {
		  if (!openflg) 
		  {
			  return -1;          
		  } 
		  
		  try {
			    
			   outStream.write(new byte[]{0x1B,'@'}); 
			   outStream.flush();     
			    
		     } catch (IOException e) {     
			// TODO Auto-generated catch block    
			e.printStackTrace();       
		   }  
		 
		  return 0;            	 	    
	 }  	 
	   
	 
	 final public static int setZoonIn(int widthZoonIn,int heightZoonIn)
	 {
		 
		  if (!openflg) 
		  {
			  return -1;          
		  } 
		  
		  if(widthZoonIn>4||widthZoonIn<1||heightZoonIn>4||heightZoonIn<1)
		  {
			 return -2;           
		  }        
		  
		  try {
			  
			outStream.write(new byte[]{0x1B,0x55,(byte)widthZoonIn});
			outStream.flush();     
			outStream.write(new byte[]{0x1B,0x56,(byte)heightZoonIn});
			outStream.flush(); 
			
		  } catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(); 
		 }
		  
		  return 0;            		    
	 }   
	 
	 
	 final public static int setAlignType(int alignType)
	 {
		  if (!openflg) 
		  {
			  return -1;          
		  } 
		  
		  if(alignType>2||alignType<0)
		  {
			  return -2;    
		  }
		   
		  try { 
			  
			outStream.write(new byte[]{0x1B,0x61,(byte)alignType});
			outStream.flush();  
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		return 0;  
		
	 }
	 
	 
	 
	 final public static int setLeftMargin(int n)
	 {
		 
		  if (!openflg) 
		  {
			  return -1;          
		  } 
		  
		  if (n>255||n<0)
		  {
			  return -2;        
		  }            
		   
		  try {
				outStream.write(new byte[]{0x1B,0x6c,(byte)n});
				outStream.flush();  
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		  

		 return 0;    
		 
	 } 
	 
	 
	 final public static int setRightMargin(int n)
	 {
		  if (!openflg) 
		  {
			  return -1;          
		  } 
		  
		  if (n>255||n<0)
		  {
			  return -2;        
		  }  
		  
		  
		  try {
				outStream.write(new byte[]{0x1B,0x51,(byte)n});
				outStream.flush();  
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();   
		}  
		  
		 
		 return 0;     
		  
	 } 
	 
	 
	 final public static int setLineSpacingByDotPitch (int n) 
	 {
		 
		  if (!openflg) 
		  {
			  return -1;          
		  } 
		  
		  if (n>255||n<0)
		  {
			  return -2;        
		  }   		      
		  
		  try {
				outStream.write(new byte[]{0x1B,0x33,(byte)n});
				outStream.flush();  
			
		  } catch (IOException e) {   
			// TODO Auto-generated catch block   
			 e.printStackTrace();     
		  }  		 
		   
		  return 0;        
	 }
	 
	 
	 final public static int setWordSpacingByDotPitch(int n)
	 {
		 
		  if (!openflg) 
		  {
			  return -1;          
		  } 
		  
		  if (n>255||n<0)
		  {
			  return -2;        
		  }   		      
		  
		  try {
				outStream.write(new byte[]{0x1B,0x20,(byte)n}); 
				outStream.flush();   
			
		  } catch (IOException e) {    
			// TODO Auto-generated catch block    
			 e.printStackTrace();      
		  }  		 
		 
		  return 0;        
	 }
	 
	 
	 
	 final public static int setPrintOrientation (int printOrientation)
	 {
		 
		  if (!openflg) 
		  {
			  return -1;          
		  } 
		  
		  if (printOrientation>1||printOrientation<0)
		  {
			  return -2;            
		  }   		      
		  
		  try {
				outStream.write(new byte[]{0x1B,0x7b,(byte)printOrientation}); 
				outStream.flush();    
			
		  } catch (IOException e) {    
			// TODO Auto-generated catch block     
			 e.printStackTrace();       
		  }  		 
		 
		 return 0;      
	 }
	 
	 
	 final public static int setBold(int n)
	 {
		 
		  if (!openflg) 
		  {
			  return -1;          
		  } 
		  
		  if (n>1||n<0)
		  {
			  return -2;        
		  }   		      
		  
		  try {
				outStream.write(new byte[]{0x1B,0x45,(byte)n}); 
				outStream.flush();   
			
		  } catch (IOException e) {    
			// TODO Auto-generated catch block    
			 e.printStackTrace();       
		  }   		 
		 
		 return 0;        
	 }
	 
	 
	 final public static int setUnderLine(int n)
	 {
		 
		  if (!openflg) 
		  {
			  return -1;          
		  } 
		  
		  if (n>1||n<0)
		  {
			  return -2;        
		  }   		      
		  
		  try {
				outStream.write(new byte[]{0x1B,0x2d,(byte)n}); 
				outStream.flush();   
			
		  } catch (IOException e) {    
			// TODO Auto-generated catch block    
			 e.printStackTrace();       
		  }   			 
		 
		 return 0;     
	 }
	 
	 
	 final public static int setInverse(int n)
	 {
		 
		  if (!openflg) 
		  {
			  return -1;          
		  } 
		  
		  if (n>1||n<0)
		  {
			  return -2;        
		  }   		      
		  
		  try {
				outStream.write(new byte[]{0x1B,0x42,(byte)n}); 
				outStream.flush();    
			
		  } catch (IOException e) {    
			// TODO Auto-generated catch block    
			 e.printStackTrace();       
		  } 		 
		 
		 return 0;   
	 }
	 
	 
	 final public static int printLF()
	 {
		  if (!openflg) 
		  {
			  return -1;          
		  } 		 
		  
		  try {
				outStream.write(new byte[]{0x0a}); 
				outStream.flush();    
			
		  } catch (IOException e) {     
			// TODO Auto-generated catch block    
			 e.printStackTrace();        
		  } 			 
		 
		 return 0;    
	 }
	 
	 
	 final public static int feedPaper(int n)
	 {
		 
		  if (!openflg) 
		  {
			  return -1;          
		  } 
		  
		  if (n>255||n<0)
		  {
			  return -2;        
		  }   		      
		  
		  try {
				outStream.write(new byte[]{0x1B,0x64,(byte)n}); 
				outStream.flush();    
			
		  } catch (IOException e) {     
			// TODO Auto-generated catch block    
			 e.printStackTrace();             
		  }      	  		 

		 return 0;     
		 
	 }	 
	 
	 

	final public static int printTwoBar(String content,int size) 
	 {
		
		byte[] byte_content = null; 
		
		  if (!openflg)  
		  {
			  return -1;           
		  } 
		  
		  if (size>7||size<1||content.equals(""))
		  {
			  return -2;                   
		  }      
		  
		  try {
			  
			byte_content = content.getBytes("GBK");
			
		} catch (UnsupportedEncodingException e1) {     
			// TODO Auto-generated catch block    
			e1.printStackTrace();            
		}       
		 
		   
		byte sent_byte[] = new byte[byte_content.length+7];  
		
		sent_byte[0] = 0x1b; 
		sent_byte[1] = 0x5a; 
		sent_byte[2] = 0x00;  
		sent_byte[3] = 0x00;   
		sent_byte[4] = (byte)(size+1);   
		sent_byte[5] = (byte) ((byte_content.length>>0)&0xff);       
		sent_byte[6] = (byte) ((byte_content.length>>8)&0xff);  
		 
		
		System.arraycopy(byte_content, 0, sent_byte, 7, byte_content.length);  
	
		  try { 
			  
			  
				outStream.write(sent_byte);   
				outStream.flush();     
			
		  } catch (IOException e) {          
			// TODO Auto-generated catch block           
			 e.printStackTrace();                 
		  }         		  
		    
		 return 0;                      
	}       
	 
	 
	
	
	
	/**
	 * @param bartyp 
	 * @param content 
	 * @return 
	 */
	final public static int printOneBar(String bartyp,int leftsp,String content)
	 {
		 byte typecode;
		 byte[] byte_content = null;
		 
		  if (!openflg)  
		  {
			  return -1;                 
		  } 
		  
		  if(content.equals("")) 
		  {    
			  return -2;                                            
		  }                         		  
		 
		 if(bartyp.equals("EAN8")) 
		 {  
			typecode =  0x44;    
		 }
		 else if(bartyp.equals("EAN13")) 
		 {
			 typecode =  0x43;         
		 }
		 else if(bartyp.equals("CODE39")) 
		 {
			 typecode =  0x45;          
		 }		 
		 else if(bartyp.equals("CODE128"))  
		 { 
			 typecode =  0x49;             
		 }    
		 else
		 {  
			 return -3;          
		 } 
		 
		 try {
			 
			byte_content = content.getBytes("GBK");
			
		} catch (UnsupportedEncodingException e) { 
			// TODO Auto-generated catch block  
			e.printStackTrace();     
		}
		 
		 byte sent_byte[] = new byte[byte_content.length+4];  
		 
		sent_byte[0] = 0x1D; 
		sent_byte[1] = 0x6B; 
		sent_byte[2] = typecode;  
		sent_byte[3] = (byte) byte_content.length; 
		
		System.arraycopy(byte_content, 0, sent_byte, 4, byte_content.length);
		
		  try { 
			   
			    outStream.write(new byte[]{0x1d,0x78,(byte)leftsp});
			    outStream.flush(); 
				outStream.write(sent_byte);      
				outStream.flush();      
			
		  } catch (IOException e) {            
			// TODO Auto-generated catch block             
			 e.printStackTrace();                          
		  }     	 			 

		 return 0;      

	 } 
	 
	 
	
	 final public static int open() 
     {
		 
		 if (openflg)
		 {
			 return 0;   	        
		 }   

    	 mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter(); //����豸�Լ������������� 
    	 
    	 if (mBluetoothAdapter == null) {    
    		 
    		 return -1;    //��֧������      
		 }
    	 else 
    	 {
    		 
    		 boolean  btflg = mBluetoothAdapter.isEnabled(); 
    		  
    		 if (btflg) {      
    		  	    
    			 Log.d("edasion","btflg  is open");            
			 }      
    		 else
    		 { 
    			 mBluetoothAdapter.enable();    
    			 while(mBluetoothAdapter.getState() == BluetoothAdapter.STATE_TURNING_ON );

    		 }          

 	    	pairedDevices = mBluetoothAdapter.getBondedDevices();  
  
 	    	
 			if ( pairedDevices.size() > 0 )         
 			{ 
 				  
 				String adder = null;  
 				
 				for (BluetoothDevice device : pairedDevices) {    
 					 
 					String  btnameString = device.getName();        
 					
 					String  subString = btnameString.substring(0, 8);       
 					
 					if (subString.equals("chinagas")) {          
						
 						adder = device.getAddress();       
 						
 						Log.d("edasion","adder=="+adder);  
 						
 						mBluetoothDevice = mBluetoothAdapter.getRemoteDevice(adder);
 						 
 						 if (mBluetoothDevice==null)    
				    	 {     
 							 return -3;                                       
				    	 }                  
 						
 						 try { 
							mBluetoothtSocket = mBluetoothDevice.createRfcommSocketToServiceRecord(UUID.fromString("00001101-0000-1000-8000-00805f9b34fb"));
						} catch (IOException e) {     
							// TODO Auto-generated catch block  
							e.printStackTrace();     
							return -2;     
						}
 						
 						 try { 
 							 
							mBluetoothtSocket.connect();   
							
						} catch (IOException e) {   
							// TODO Auto-generated catch block   
							e.printStackTrace();
							return -2;  
						} 
 				  
 						 try {
							outStream = mBluetoothtSocket.getOutputStream();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							return -2;  
						}    
 
 						 break;                  
					}     
 					
 				}
 				
 				if ( adder == null )
 				{
 					 return -4;	 
 				}
 				
	           		    	  
 			}
 			else 
 			{ 	 
 			    return -4; //����δ��ƥ��    
 			}     

		}
    	 
    	openflg = true;  
    	return 0;
     }
	
	
	 final public static int close() 
	 {
		 
		  if (openflg) 
		  {
			  try {
				  
				 outStream.close();
				 outStream = null; 
			} catch (IOException e) {   
				// TODO Auto-generated catch block   
				e.printStackTrace();   
				return -1;   
			}   
			  
			  try {
				mBluetoothtSocket.close(); 
				mBluetoothtSocket = null; 
			} catch (IOException e1) {  
				// TODO Auto-generated catch block 
				e1.printStackTrace();   
				return -1;   
			}
			
			pairedDevices   = null;  
			mBluetoothDevice= null; 
			mBluetoothAdapter.disable(); 
			mBluetoothAdapter = null;  
			openflg = false;   
		  }  
		 
		 return 0;     
	 }
	 

}
 
 
 
 
