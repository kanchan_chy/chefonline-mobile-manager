package com.dashboard.dashboardhome;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.chefonline.managermobile.R;
import com.dashboard.activity.DashBoardActivity;
import com.dashboard.adapter.DashBoardDataAdapter;
import com.dashboard.datamodel.OrderData;
import com.dashboard.datamodel.PaymentData;
import com.dashboard.datamodel.ReservationDataUnconfirmed;
import com.dashboard.datamodel.ScheduleData;
import com.dashboard.presenter.DashBoardPresenter;
import com.dashboard.services.DashBoardService;
import com.dashboard.utility.ConstantValues;
import com.dashboard.utility.DateFormatter;
import com.dashboard.utility.PreferenceUtil;
import com.dashboard.view.DashBoardView;

import java.util.ArrayList;

public class DashBoardTodayActivityOld extends AppCompatActivity implements DashBoardView {
    public static String fromDate = "";
    public static String toDate = "";
    TextView textViewFilter;
    TextView textViewPaypal;
    TextView textViewTotal;
    TextView textViewCash;
    TextView textViewMessage;
    RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    public static DashBoardPresenter dashBoardPresenter;

    PreferenceUtil preferenceUtil;
    DashBoardDataAdapter myAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board_today);
        initView();
        dashBoardPresenter = new DashBoardPresenter(this, new DashBoardService());
        fromDate = DateFormatter.getCurrentDate();
        toDate = DateFormatter.getCurrentDate();

        try {
            String showingText1 = "" + DateFormatter.doFormat(fromDate, DateFormatter.DD_MM_YYYY, DateFormatter.MMM_dd_yyyy);
            String showingText2 = "" + DateFormatter.doFormat(toDate, DateFormatter.DD_MM_YYYY, DateFormatter.MMM_dd_yyyy);
            //textViewFilter.setText(showingText1 + " - " + showingText2);
            textViewFilter.setText("Today's Order");
            dashBoardPresenter.onClickGetOrder(ConstantValues.ORDER_ONLINE_LIST_API_ID, true);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    Handler timerHandler = new Handler();
    Runnable timerRunnable = new Runnable() {
        @Override
        public void run() {
            myAdapter.notifyDataSetChanged();
            timerHandler.postDelayed(this, 1000); //run every minute
        }
    };

    private void initView() {
        preferenceUtil = new PreferenceUtil(this);

        recyclerView= (RecyclerView) findViewById(R.id.recycleViewOrderList);
        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(DashBoardTodayActivityOld.this);
        recyclerView.setLayoutManager(mLayoutManager);

        Toolbar toolbar =(Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle(preferenceUtil.getRestaurantName());

        textViewFilter= (TextView) findViewById(R.id.textViewFilter);
        textViewPaypal = (TextView) findViewById(R.id.textViewPaypal);
        textViewTotal = (TextView) findViewById(R.id.textViewTotal);
        textViewCash = (TextView) findViewById(R.id.textViewCash);
        textViewMessage = (TextView) findViewById(R.id.textViewMessage);

    }
/*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_dash_board_today, menu);
        return true;
    }  */

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_all_orders) {
            startActivity(new Intent(this, DashBoardActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public String getRestaurantId() {
        // Log.i("Rest")
        return preferenceUtil.getResID();
    }

    @Override
    public String getStartDate() {
        return fromDate;
    }

    @Override
    public String getEndDate() {
        return toDate;
    }

    @Override
    public ProgressDialog showProgressDialog() {
        ProgressDialog progress = new ProgressDialog(DashBoardTodayActivityOld.this);
        progress.setMessage("Please wait....");
        progress.show();
        return progress;
    }

    @Override
    public void checkConnectivity() {

    }

    @Override
    public void showInternetDialog(String message, String okButton, String cancelButton) {

    }

    @Override
    public Context getClassContext() {
        return DashBoardTodayActivityOld.this;
    }

    @Override
    public void setScheduleData(ArrayList<ScheduleData> scheduleData) {

    }

    @Override
    public void setOrderData(ArrayList<OrderData> orderDatas) {
        myAdapter = new DashBoardDataAdapter(orderDatas, getClassContext(), true);
        recyclerView.setAdapter(myAdapter);
        timerHandler.postDelayed(timerRunnable, 500);
    }

    @Override
    public void setReservationData(ArrayList<ReservationDataUnconfirmed> reservationDataUnconfirmed) {

    }

    @Override
    public void setPaymentData(ArrayList<PaymentData> paymentData) {

    }

    @Override
    public void setCash(String text) {
        textViewCash.setText(text);
    }

    @Override
    public void setPayPal(String text) {
        textViewPaypal.setText(text);
    }

    @Override
    public void setTotal(String text) {
        textViewTotal.setText(text);
    }

    @Override
    public void setTotalOrder(String text) {

    }

    @Override
    public void setDeliveryOrder(String text) {

    }

    @Override
    public void setCollectionOrder(String text) {

    }

    @Override
    public void setTotalOrderReservation(String text) {

    }

    @Override
    public void showNotPrintedOrder() {

    }

    @Override
    public void showEmptyView(boolean isVisible) {
        if (isVisible) {
            textViewMessage.setVisibility(View.VISIBLE);
        } else {
            textViewMessage.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onPause() {
        //timerHandler.removeCallbacks(timerRunnable);
        super.onPause();
    }

    @Override
    protected void onResume() {

        super.onResume();
    }

}
