package com.dashboard.adapter;

import android.content.Context;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.chefonline.managermobile.R;
import com.dashboard.datamodel.OffersData;
import com.dashboard.utility.UnsafeOkHttpClient;
import com.dashboard.utility.UtilityMethod;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import okhttp3.OkHttpClient;


/**
 * Created by user on 1/18/2016.
 */
public class OffersAdapter extends RecyclerView.Adapter<OffersAdapter.OffersViewHolder>{

    private ArrayList<OffersData> mDataset = new ArrayList<>();
    private Context context;
    private Picasso.Builder builder;
    private Picasso picasso;

    // Provide a suitable constructor (depends on the kind of dataset)
    public OffersAdapter(Context context, ArrayList<OffersData> myDataset) {
        this.context = context;
        mDataset = myDataset;
        try {
            builder = new Picasso.Builder(context);
            OkHttpClient okHttpClient = UnsafeOkHttpClient.getUnsafeOkHttpClient(context);
            builder.downloader(new OkHttp3Downloader(okHttpClient));
            picasso = builder.build();
        } catch (Exception e) {

        }
    }

    @Override
    public OffersViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_offers, parent, false);
        // set the view's size, margins, paddings and layout parameters
        OffersViewHolder vh = new OffersViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(OffersViewHolder holder, int position) {
        holder.txtTitle.setText(mDataset.get(position).getTitle());
        holder.txtOfferFor.setText("Offer For: "+mDataset.get(position).getOfferFor());
        holder.txtPosition.setText("Position: "+mDataset.get(position).getPosition());
        holder.txtSpent.setText("Spent £"+ UtilityMethod.priceFormatter(mDataset.get(position).getSpentAmount())+" or more");
        holder.txtEligible.setText("Eligible Amount: £"+UtilityMethod.priceFormatter(mDataset.get(position).getEligibleAmount()));

        try {
            picasso.load(mDataset.get(position).getImageUrl().trim()).placeholder(R.drawable.placeholder).error(R.drawable.image_placeholder).noFade().fit().into(holder.imgOffer);
        } catch (Exception e) {
            // new CustomToast(mContext, "" + e, "", false);
        }

    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class OffersViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        // each data item is just a string in this case
        public TextView txtTitle;
        public TextView txtSpent;
        public TextView txtEligible;
        public TextView txtPosition;
        public TextView txtOfferFor;
        public ImageView imgOffer;
        public ImageButton imgBtnPopup;

        public OffersViewHolder(View v) {
            super(v);
            txtTitle = (TextView) v.findViewById(R.id.txtTitle);
            txtSpent = (TextView) v.findViewById(R.id.txtSpent);
            txtEligible = (TextView) v.findViewById(R.id.txtEligible);
            txtPosition = (TextView) v.findViewById(R.id.txtPosition);
            txtOfferFor = (TextView) v.findViewById(R.id.txtOfferFor);
            imgOffer = (ImageView) v.findViewById(R.id.imgOffer);
            imgBtnPopup=(ImageButton) v.findViewById(R.id.imgBtnPopup);
            imgBtnPopup.setOnClickListener(OffersViewHolder.this);
        }

        @Override
        public void onClick(View v) {
            int pos = getAdapterPosition();
            if(v.getId() == R.id.imgBtnPopup) {
                settingsMenu(v, pos);
            }
        }


        public void settingsMenu(View menuItemView, int pos) {
          // menuItemView = findViewById(R.id.action_settings);
            final PopupMenu popup = new PopupMenu(context, menuItemView);

            popup.getMenuInflater().inflate(R.menu.menu_offers, popup.getMenu());
            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                public boolean onMenuItemClick(MenuItem item) {
                    int i = item.getItemId();
                    if (i == R.id.action_delete) {
                       // Toast.makeText(context, "Delete selected", Toast.LENGTH_SHORT).show();
                        return true;

                    } else if (i == R.id.action_edit) {
                      //  Toast.makeText(context, "Edit selected", Toast.LENGTH_SHORT).show();
                        return true;

                    } else {
                        return false;
                    }
                }
            });

            popup.show();
        }

    }



}
