package com.dashboard.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.chefonline.managermobile.R;
import com.dashboard.datamodel.EndOfDayPayment;
import com.dashboard.utility.UtilityMethod;

import java.util.ArrayList;

/**
 * Created by user on 1/18/2016.
 */
public class EndOfDayPaymentAdapter extends BaseAdapter{

    private ArrayList<EndOfDayPayment> mDataset = new ArrayList<>();
    private Context context;
    LayoutInflater inflater;

    // Provide a suitable constructor (depends on the kind of dataset)
    public EndOfDayPaymentAdapter(Context context, ArrayList<EndOfDayPayment> myDataset) {
        this.context = context;
        mDataset = myDataset;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return mDataset.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(convertView == null) {
            convertView = inflater.inflate(R.layout.row_end_of_day_payment, null);
            holder = new ViewHolder();
            holder.txtGrandTotal = (TextView) convertView.findViewById(R.id.txtGrandTotal);
            holder.txtPaymentType = (TextView) convertView.findViewById(R.id.txtPaymentType);
            convertView.setTag(holder);
        }
        else holder = (ViewHolder) convertView.getTag();

        holder.txtPaymentType.setText(mDataset.get(position).getPaymentMethod());
        holder.txtGrandTotal.setText("£" + UtilityMethod.priceFormatter(mDataset.get(position).getGrandTotal()));

        return convertView;
    }

    static class ViewHolder{
        // each data item is just a string in this case
        public TextView txtGrandTotal;
        public TextView txtPaymentType;
    }



}
