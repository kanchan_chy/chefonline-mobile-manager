package com.dashboard.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.chefonline.managermobile.R;
import com.dashboard.datamodel.EndOfDayUser;

import java.util.ArrayList;

/**
 * Created by user on 1/18/2016.
 */
public class EndOfDayUserAdapter extends BaseAdapter{

    private ArrayList<EndOfDayUser> mDataset = new ArrayList<>();
    private Context context;
    LayoutInflater inflater;

    // Provide a suitable constructor (depends on the kind of dataset)
    public EndOfDayUserAdapter(Context context, ArrayList<EndOfDayUser> myDataset) {
        this.context = context;
        mDataset = myDataset;
        inflater = LayoutInflater.from(context);
    }


    @Override
    public int getCount() {
        return mDataset.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(convertView == null) {
            convertView = inflater.inflate(R.layout.row_end_of_day_user, null);
            holder = new ViewHolder();
            holder.cardView = (CardView) convertView.findViewById(R.id.cv);
            holder.textViewSerialNo = (TextView) convertView.findViewById(R.id.textViewSerialNo);
            holder.txtUserName = (TextView) convertView.findViewById(R.id.txtUserName);
            holder.txtOrderedDish = (TextView) convertView.findViewById(R.id.txtOrderedDish);
            convertView.setTag(holder);
        }
        else holder = (ViewHolder) convertView.getTag();

        holder.textViewSerialNo.setText(String.valueOf(position + 1));
        holder.txtUserName.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS);
        holder.txtUserName.setText(mDataset.get(position).getUserName());
        holder.txtOrderedDish.setText("£" + mDataset.get(position).getOrderPrice());

        return convertView;
    }

    static class ViewHolder{
        // each data item is just a string in this case
        public CardView cardView;
        public TextView txtUserName;
        public TextView txtOrderedDish;
        public TextView textViewSerialNo;
    }

}
