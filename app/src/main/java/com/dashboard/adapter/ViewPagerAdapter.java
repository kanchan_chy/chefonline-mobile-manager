package com.dashboard.adapter;


import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.LayoutInflater;

import com.dashboard.fragments.DashBoardTodayFragment;
import com.dashboard.fragments.ReservationFragment;

import java.util.List;

/**
 * Created by Edwin on 15/02/2015.
 */
public class ViewPagerAdapter extends FragmentPagerAdapter {
    LayoutInflater inflater;
    Context mContext;
    List<String> strings;
    CharSequence Titles[]; // This will Store the Titles of the Tabs which are Going to be passed when ViewPagerAdapter is created
    int NumbOfTabs; // Store the number of tabs, this will also be passed when the ViewPagerAdapter is created


    // Build a Constructor and assign the passed Values to appropriate values in the class
    public ViewPagerAdapter(Context context, FragmentManager fm, CharSequence mTitles[]) {
        super(fm);
        this.Titles = mTitles;
        this.mContext = context;

    }

    //This method return the fragment for the every position in the View Pager
    @Override
    public Fragment getItem(int position) {

        if(position == 0) // if the position is 0 we are returning the First tab
        {
            DashBoardTodayFragment tab1 = new DashBoardTodayFragment();
            return tab1;

        } else             // As we are having 2 tabs if the position is now 0 it must be 1 so we are returning second tab
        {
            ReservationFragment tab2 = new ReservationFragment();
            return tab2;
        }

    }

    @Override
    public CharSequence getPageTitle(int position) {
       return Titles[position];
        //return MainActivity.relativeSizeSpan(Titles[position], position, mContext);
    }

    // This method return the Number of tabs for the tabs Strip

    @Override
    public int getCount() {
        return Titles.length;
    }


}