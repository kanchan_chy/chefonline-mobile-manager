package com.dashboard.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.chefonline.managermobile.R;
import com.dashboard.datamodel.ReservationDatas;
import com.dashboard.utility.PreferenceUtil;

import java.util.ArrayList;

/**
 * Created by user on 1/18/2016.
 */
public class ReservationGroupListAdapter extends RecyclerView.Adapter<ReservationGroupListAdapter.ReservationViewHolder>{

    private ArrayList<ReservationDatas> mDataset = new ArrayList<>();
    private Context context;
    String totalValue;
    boolean isTimerVisible;
    PreferenceUtil preferenceUtil;
    private ArrayList<String> reservationNewIds = new ArrayList<>();

    // Provide a suitable constructor (depends on the kind of dataset)
    public ReservationGroupListAdapter(Context context, ArrayList<ReservationDatas> myDataset, String totalValue, boolean isTimerVisible) {
        this.context = context;
        this.isTimerVisible = isTimerVisible;
        this.totalValue = totalValue;
        mDataset = myDataset;
        preferenceUtil = new PreferenceUtil(context);
    }

    @Override
    public ReservationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_group, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ReservationViewHolder vh = new ReservationViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ReservationViewHolder holder, int position) {
        holder.textViewTitle.setText(mDataset.get(position).getTitle());
        holder.textViewTitle.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS);

        holder.recyclerViewList.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(context);
        holder.recyclerViewList.setLayoutManager(mLayoutManager);

        ReservationAdapter adapter = new ReservationAdapter(context, preferenceUtil.getResID(), preferenceUtil.getRestaurantName(), mDataset.get(position).getOrderDatas(), false);
        holder.recyclerViewList.setAdapter(adapter);

    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class ReservationViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        // each data item is just a string in this case
        public TextView textViewTitle;
        public RecyclerView recyclerViewList;
        CardView cardView;

        public ReservationViewHolder(View v) {
            super(v);
            textViewTitle = (TextView) v.findViewById(R.id.textView);
            recyclerViewList = (RecyclerView) v.findViewById(R.id.recycleView);

        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();

        }
    }



}
