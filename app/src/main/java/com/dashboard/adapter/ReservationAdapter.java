package com.dashboard.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chefonline.managermobile.R;
import com.dashboard.activity.ReservationDetailsActivity;
import com.dashboard.datamodel.ReservationDataSerialize;
import com.dashboard.datamodel.ReservationDataUnconfirmed;
import com.dashboard.utility.ConstantValues;
import com.dashboard.utility.DateFormatter;

import java.util.ArrayList;

/**
 * Created by user on 1/18/2016.
 */
public class ReservationAdapter extends RecyclerView.Adapter<ReservationAdapter.ReservationViewHolder>{

    private ArrayList<ReservationDataUnconfirmed> mDataset = new ArrayList<>();
    String restaurantId;
    String restaurantName;
    boolean fromUnconfirmed;
    private Context context;
    private long mLastClickTime = 0;
    private ArrayList<String>reservationNewIds = new ArrayList<>();

    // Provide a suitable constructor (depends on the kind of dataset)
    public ReservationAdapter(Context context, String restaurantId, String restaurantName, ArrayList<ReservationDataUnconfirmed> myDataset, boolean fromUnconfirmed) {
        this.context = context;
        this.restaurantId = restaurantId;
        this.restaurantName = restaurantName;
        this.fromUnconfirmed = fromUnconfirmed;
        mDataset = myDataset;
        reservationNewIds = ConstantValues.reservationNewIds;
    }

    @Override
    public ReservationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_reservation, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ReservationViewHolder vh = new ReservationViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ReservationViewHolder holder, int position) {
        String customerName = "";
        if(mDataset.get(position).getCustomerTitle() != null && !mDataset.get(position).getCustomerTitle().trim().equalsIgnoreCase("") && !mDataset.get(position).getCustomerTitle().trim().equalsIgnoreCase("null")) {
            customerName = customerName + mDataset.get(position).getCustomerTitle().trim() + " ";
        }
        if(mDataset.get(position).getCustomerFirstName() != null && !mDataset.get(position).getCustomerFirstName().trim().equalsIgnoreCase("") && !mDataset.get(position).getCustomerFirstName().trim().equalsIgnoreCase("null")) {
            customerName += mDataset.get(position).getCustomerFirstName().trim();
        }
        if(mDataset.get(position).getCustomerLastName() != null && !mDataset.get(position).getCustomerLastName().trim().equalsIgnoreCase("") && !mDataset.get(position).getCustomerLastName().trim().equalsIgnoreCase("null")) {
            customerName += " " + mDataset.get(position).getCustomerLastName().trim();
        }
        holder.textViewTitle.setText(customerName);
        holder.textViewTitle.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS);
        holder.textViewGuestNo.setText(mDataset.get(position).getGuestNumber());
        holder.textViewReservedDate.setText(mDataset.get(position).getReservedDate());
        holder.textViewReservedTime.setText(mDataset.get(position).getReservedTime());
        holder.textViewCreatedDate.setText("Created at " + mDataset.get(position).getCreatedDate());
        holder.textViewCreatedTime.setText(mDataset.get(position).getCreatedTime());

        if(mDataset.get(position).getCustomerMObileNo() != null && !"".equalsIgnoreCase(mDataset.get(position).getCustomerMObileNo()) && !"null".equalsIgnoreCase(mDataset.get(position).getCustomerMObileNo())) {
            holder.textViewMobile.setText(mDataset.get(position).getCustomerMObileNo().trim());
        } else {
            holder.textViewMobile.setText("Mobile number not available");
        }

        if(mDataset.get(position).getCustomerEmail() != null && !"".equalsIgnoreCase(mDataset.get(position).getCustomerEmail()) && !"null".equalsIgnoreCase(mDataset.get(position).getCustomerEmail())) {
            holder.textViewEmail.setText(mDataset.get(position).getCustomerEmail().trim());
        } else {
            holder.textViewEmail.setText("Email not available");
        }

        if(reservationNewIds.contains(mDataset.get(position).getReservationId())) {
            holder.textViewNew.setVisibility(View.VISIBLE);
        } else {
            holder.textViewNew.setVisibility(View.GONE);
        }

        if("1".equalsIgnoreCase(mDataset.get(position).getReservationStatus())) {
            holder.textViewStatus.setVisibility(View.VISIBLE);
        } else {
            //holder.textViewStatus.setVisibility(View.GONE);
            holder.textViewStatus.setText("Pending");
            holder.textViewStatus.setTextColor(Color.parseColor("#ffa03b"));
            holder.textViewStatus.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }

        DateFormatter dateFormatter = new DateFormatter();
        if(dateFormatter.isToday(mDataset.get(position).getReservedDate())) {

            // handle today
            holder.imgViewText.setImageResource(R.drawable.image_text);

        } else if(dateFormatter.isTomorrow(mDataset.get(position).getReservedDate())) {

            // handle tomorrow
            holder.imgViewText.setImageResource(R.drawable.image_text);

        } else {

            // handle upcoming
            holder.imgViewText.setImageResource(R.drawable.image_text);

        }


    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class ReservationViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        // each data item is just a string in this case
        public TextView textViewTitle;
        public TextView textViewEmail;
        public TextView textViewMobile;
        public TextView textViewReservedDate;
        public TextView textViewReservedTime;
        public TextView textViewCreatedDate;
        public TextView textViewCreatedTime;
        public TextView textViewStatus;
        public TextView textViewNew;
        public TextView textViewGuestNo;
        public ImageView imgViewText;
        CardView cardView;

        public ReservationViewHolder(View v) {
            super(v);
            textViewTitle = (TextView) v.findViewById(R.id.textViewTitle);
            textViewEmail = (TextView) v.findViewById(R.id.textViewEmail);
            textViewMobile = (TextView) v.findViewById(R.id.textViewMobile);
            textViewReservedDate = (TextView) v.findViewById(R.id.textViewReservationDate);
            textViewReservedTime = (TextView) v.findViewById(R.id.textViewReservationTime);
            textViewCreatedDate = (TextView) v.findViewById(R.id.textViewCreatedDate);
            textViewCreatedTime = (TextView) v.findViewById(R.id.textViewCreatedTime);
            textViewStatus = (TextView) v.findViewById(R.id.textViewStatus);
            textViewNew = (TextView) v.findViewById(R.id.textViewNew);
            textViewGuestNo = (TextView) v.findViewById(R.id.textViewGuestNo);
            imgViewText = (ImageView) v.findViewById(R.id.imgViewText);
            cardView = (CardView) v.findViewById(R.id.cv);
            cardView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            if(ConstantValues.reservationNewIds.contains(mDataset.get(position).getReservationId())) {
                ConstantValues.reservationNewIds.remove((String)mDataset.get(position).getReservationId());
                reservationNewIds = ConstantValues.reservationNewIds;
                notifyDataSetChanged();
            }
            ReservationDataSerialize serializeData = new ReservationDataSerialize(restaurantId, restaurantName, mDataset.get(position));
            Intent intent = new Intent(context, ReservationDetailsActivity.class);
            intent.putExtra("fromUnconfirmed", fromUnconfirmed);
            intent.putExtra("sampleObject", serializeData);
            context.startActivity(intent);
        }
    }



}
