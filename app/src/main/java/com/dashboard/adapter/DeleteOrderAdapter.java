package com.dashboard.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.drawable.Drawable;
import android.os.SystemClock;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chefonline.managermobile.R;
import com.dashboard.activity.OrderDetailsActivity;
import com.dashboard.datamodel.CustomerDataSerialize;
import com.dashboard.datamodel.OrderData;
import com.dashboard.services.DashBoardOrderDeleteApiCallBack;
import com.dashboard.utility.ConstantValues;
import com.dashboard.utility.DateFormatter;
import com.dashboard.utility.UtilityMethod;
import com.dashboard.view.DashBoardDeleteInterface;

import java.util.ArrayList;

public class DeleteOrderAdapter extends RecyclerView.Adapter<DeleteOrderAdapter.ViewHolder>{
    private ArrayList<OrderData> mDataset = new ArrayList<>();
    private Context context;
    boolean isTimerVisible = false;
    private int lastPosition = -1;
    private long mLastClickTime = 0;

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        // each data item is just a string in this case
        public TextView textViewName;
        public TextView textViewPostcode;
        public TextView textViewAddress;
        public TextView textViewPaymentType;
        public TextView textViewOrderType;
        public TextView textViewOrderTime;
        public TextView textViewTotal;
     // public TextView textViewCountDownTimer;
        public ImageView imageViewOrderPolicy;
        public ImageButton btnMenu;
        public CardView cardView;
        public RelativeLayout relative_container;

        public ViewHolder(View v) {
            super(v);
            textViewName = (TextView) v.findViewById(R.id.textViewName);
            textViewPostcode = (TextView) v.findViewById(R.id.textViewPostcode);
            textViewAddress = (TextView) v.findViewById(R.id.textViewAddress);
            textViewPaymentType = (TextView) v.findViewById(R.id.textViewPaymentType);
            textViewOrderType = (TextView) v.findViewById(R.id.textViewOrderType);
            textViewOrderTime = (TextView) v.findViewById(R.id.textViewOrderTime);
            textViewTotal = (TextView) v.findViewById(R.id.textViewTotal);
            //textViewCountDownTimer = (TextView) v.findViewById(R.id.textViewCountDownTimer);
            imageViewOrderPolicy = (ImageView) v.findViewById(R.id.imageViewOrderPolicy);
            cardView = (CardView) v.findViewById(R.id.cv);
            relative_container = (RelativeLayout) v.findViewById(R.id.relative_container);
            btnMenu = (ImageButton) v.findViewById(R.id.btnMenu);
            cardView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(v.getId() == R.id.cv) {
                int position = getAdapterPosition();
                CustomerDataSerialize dene = new CustomerDataSerialize(4, "Chef Online", mDataset.get(position));
                Intent intent = new Intent(context, OrderDetailsActivity.class);
                intent.putExtra("order_no", mDataset.get(position).getOrderNo());
                intent.putExtra("sampleObject", dene);
                context.startActivity(intent);
            }
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public DeleteOrderAdapter(ArrayList<OrderData> myDataset, Context context, boolean isTimerVisible) {
        mDataset = myDataset;
        this.context = context;
        this.isTimerVisible = isTimerVisible;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public DeleteOrderAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_delete_order, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    int count = 0;
    @Override
    public void onBindViewHolder(final ViewHolder holder,final int position) {
        holder.textViewName.setText(mDataset.get(position).getCustomerName());
        holder.textViewName.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS);
        String postCode = mDataset.get(position).getPostCode();
        if(postCode != null && !postCode.equalsIgnoreCase("") && !postCode.equalsIgnoreCase("null")) {
            holder.textViewPostcode.setText(postCode);
        } else {
            holder.textViewPostcode.setText("Post code unavailable");
        }
        String address = mDataset.get(position).getCustomerAddress();
        if(address != null && !address.equalsIgnoreCase("") && !address.equalsIgnoreCase("null")) {
            holder.textViewAddress.setText(address);
        } else {
            holder.textViewAddress.setText("Address unavailable");
        }
      /*  holder.textViewOrderTime.setText("IN: " +
                DateFormatter.doFormat(mDataset.get(position).getOrderIn(), "hh:mm a", "h:mm a")
                + " | OUT: " + DateFormatter.doFormat(mDataset.get(position).getOrderOut(), "hh:mm a", "h:mm a"));   */
        holder.textViewOrderTime.setText("IN: " + mDataset.get(position).getOrderIn().trim() + "  |  OUT: " + mDataset.get(position).getOrderOut().trim());
        holder.textViewOrderType.setText(mDataset.get(position).getOrderType());
        holder.textViewPaymentType.setText(mDataset.get(position).getPaymentMethod());
        holder.textViewTotal.setText("£" + UtilityMethod.priceFormatterRound(Double.valueOf(mDataset.get(position).getGrandTotal())));

        /*long outputTime = Math.abs(1524485218 - System.currentTimeMillis());*/
      //  holder.textViewCountDownTimer.setText(UtilityMethod.countDownTimer(mDataset.get(position).getOrderOut()));

        if (mDataset.get(position).getOrderType().equalsIgnoreCase("delivery")) {
            ColorFilter filter = new LightingColorFilter(ContextCompat.getColor(context, R.color.colorAccent), ContextCompat.getColor(context, R.color.colorAccent));
            holder.imageViewOrderPolicy.setImageResource(R.drawable.delivery);
            holder.imageViewOrderPolicy.setColorFilter(filter);
        } else {
            ColorFilter filter = new LightingColorFilter(ContextCompat.getColor(context, R.color.colorAccent), ContextCompat.getColor(context, R.color.colorAccent));
            holder.imageViewOrderPolicy.setImageResource(R.drawable.collection);
            holder.imageViewOrderPolicy.setColorFilter(filter);
        }


        holder.btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog("Are you sure want to delete now?", "YES", "NO", position);
            }
        });

    /*    if(!isTimerVisible) {
            holder.textViewCountDownTimer.setVisibility(View.GONE);
        } else {
            setAnimation(holder.cardView, holder.relative_container, 0);
            //holder.cardView.setCardBackgroundColor(R.color.colorAccent);
        }   */


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public void deleteItem(int position) {
        mDataset.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mDataset.size());

    }

    private void openDialog(String message, String okButton, String cancelButton, final int position) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_warning);
        TextView txtViewPopupMessage = (TextView) dialog.findViewById(R.id.txtMessage);
        txtViewPopupMessage.setText(message);

        final TextView txtAccept = (TextView) dialog.findViewById(R.id.txtAccept);
        txtAccept.setText(okButton);

        // if button is clicked, close the custom dialog
        txtAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000){
                    mLastClickTime = SystemClock.elapsedRealtime();
                    return;
                }
                else {
                    mLastClickTime = SystemClock.elapsedRealtime();
                }
                new DashBoardOrderDeleteApiCallBack(context, mDataset.get(position).getOrderNo(), new DashBoardDeleteInterface() {
                    @Override
                    public void onRequestComplete(String response) {
                        if ("1".equalsIgnoreCase(response.toString().trim())) {
                            deleteItem(position);
                            Toast.makeText(context, "Order Deleted.", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, "Order can not delete.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                dialog.dismiss();
            }

        });

        final TextView txtCancel = (TextView) dialog.findViewById(R.id.txtCancel);
        txtCancel.setText(cancelButton);
        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        dialog.show();
    }

    private void setAnimation(final CardView viewToAnimate, final View view, int position) {
        if (!ConstantValues.IS_ACTIVITY_RUNNING) {
            return;
        }
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            //Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            Animation animation = AnimationUtils.loadAnimation(context, R.anim.animation_left_to_right);
            viewToAnimate.startAnimation(animation);

            if (ConstantValues.IS_NEW_ORDER) {
                ConstantValues.IS_NEW_ORDER = false;
                final android.os.Handler timerHandler = new android.os.Handler();
                final Runnable timerRunnable = new Runnable() {
                    @Override
                    public void run() {
                        if (count > 30) {
                            timerHandler.removeCallbacks(this);
                            view.setBackgroundColor(context.getResources().getColor(R.color.white));
                        } else {
                            timerHandler.postDelayed(this, 1000); //run every minute
                            if(count%2 == 0) {
                                view.setBackgroundColor(context.getResources().getColor(R.color.brand_red));
                            } else {
                                view.setBackgroundColor(context.getResources().getColor(R.color.white));
                            }

                            count = count + 1;
                        }


                    }
                };

                timerHandler.postDelayed(timerRunnable, 1000); //run every minute
            }

            lastPosition = position;
        }
    }


}