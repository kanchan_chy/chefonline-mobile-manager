package com.dashboard.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chefonline.managermobile.R;
import com.dashboard.activity.DashBoardTodayActivityMain;
import com.dashboard.datamodel.RestaurantListData;
import com.dashboard.db.DataBaseUtil;
import com.dashboard.utility.PreferenceUtil;
import com.dashboard.utility.UnsafeOkHttpClient;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import okhttp3.OkHttpClient;

/**
 * Created by user on 5/9/2016.
 */
public class RestaurantListAdapter extends RecyclerView.Adapter<RestaurantListAdapter.MyViewHlder> {

    Activity mContext;
    ArrayList<RestaurantListData>restaurantListDatas;
    int selectedPos;
    private Picasso.Builder builder;
    private Picasso picasso;


    public RestaurantListAdapter(Activity mContext, ArrayList<RestaurantListData>restaurantListDatas) {
        this.mContext = mContext;
        this.restaurantListDatas = restaurantListDatas;

        try {
            builder = new Picasso.Builder(mContext);
            OkHttpClient okHttpClient = UnsafeOkHttpClient.getUnsafeOkHttpClient(mContext);
            builder.downloader(new OkHttp3Downloader(okHttpClient));
            picasso = builder.build();
        } catch (Exception e) {

        }
    }

    @Override
    public MyViewHlder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.row_restaurant_list, parent, false);
        return new MyViewHlder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHlder holder, int position) {
        holder.textViewRestaurant.setText(restaurantListDatas.get(position).getRestaurantName());
        holder.textViewCuisine.setText(restaurantListDatas.get(position).getCuisineName());
        if(position == (restaurantListDatas.size()-1)) {
            holder.viewSeparator.setVisibility(View.GONE);
        }
        if(restaurantListDatas.get(position).getLogoUrl() != null && !"".equalsIgnoreCase(restaurantListDatas.get(position).getLogoUrl().trim()) && !"null".equalsIgnoreCase(restaurantListDatas.get(position).getLogoUrl().trim())) {
            try {
                picasso.load(restaurantListDatas.get(position).getLogoUrl().trim()).placeholder(R.drawable.placeholder).error(R.drawable.placeholder).noFade().fit().into(holder.imgViewLogo);
            } catch (Exception e) {
               // new CustomToast(mContext, "" + e, "", false);
            }
        }
    }

    @Override
    public int getItemCount() {
        return restaurantListDatas.size();
    }

    public class MyViewHlder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public ImageView imgViewLogo;
        TextView textViewRestaurant;
        TextView textViewCuisine;
        RelativeLayout relativeContainer;
        View viewSeparator;
        private ProgressDialog progressDialog;

        public MyViewHlder(View itemView) {
            super(itemView);
            imgViewLogo = (ImageView) itemView.findViewById(R.id.imgViewLogo);
            textViewRestaurant = (TextView) itemView.findViewById(R.id.txtViewRestaurantName);
            textViewCuisine = (TextView) itemView.findViewById(R.id.txtViewCuisineName);
            relativeContainer = (RelativeLayout) itemView.findViewById(R.id.relativeContainer);
            viewSeparator = (View) itemView.findViewById(R.id.viewSeparator);
            relativeContainer.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int pos = getAdapterPosition();
            selectedPos = pos;
            if(v.getId() == R.id.relativeContainer) {
                PreferenceUtil preferenceUtil = new PreferenceUtil(mContext);
                preferenceUtil.setResID(restaurantListDatas.get(pos).getRestaurantId());
                preferenceUtil.setRestaurantName(restaurantListDatas.get(pos).getRestaurantName());
                // get reservation settings data
              /*  RestaurantListPresenter restaurantListPresenter = new RestaurantListPresenter(MyViewHlder.this);
                restaurantListPresenter.getReservationSettings();  */
                insertOrUpdateRecentData();
                Intent intent = new Intent(mContext, DashBoardTodayActivityMain.class);
                mContext.startActivity(intent);
            }
        }

    }


    private void insertOrUpdateRecentData() {
        try {

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String viewDate = sdf.format(new Date());

            DataBaseUtil dataBaseUtil = new DataBaseUtil(mContext);
            dataBaseUtil.open();
            ArrayList<String>recentRestIds = dataBaseUtil.fetchRecentRestIds();
            if(recentRestIds != null && recentRestIds.contains(restaurantListDatas.get(selectedPos).getRestaurantId())) {
                dataBaseUtil.updateRecentData(restaurantListDatas.get(selectedPos).getRestaurantId(), restaurantListDatas.get(selectedPos).getRestaurantName(), restaurantListDatas.get(selectedPos).getLogoUrl(), viewDate);
            } else {
                dataBaseUtil.insertRecentData(restaurantListDatas.get(selectedPos).getRestaurantId(), restaurantListDatas.get(selectedPos).getRestaurantName(), restaurantListDatas.get(selectedPos).getLogoUrl(), viewDate);
            }
            dataBaseUtil.close();
        } catch (Exception e) {
            Log.e("Db_error", "" + e);
            Toast.makeText(mContext, "" + e, Toast.LENGTH_LONG).show();
        }
    }


    class CircleTransform implements Transformation {
        @Override
        public Bitmap transform(Bitmap source) {
            int size = Math.min(source.getWidth(), source.getHeight());

            int x = (source.getWidth() - size) / 2;
            int y = (source.getHeight() - size) / 2;

            Bitmap squaredBitmap = Bitmap.createBitmap(source, x, y, size, size);
            if (squaredBitmap != source) {
                source.recycle();
            }

            Bitmap bitmap = Bitmap.createBitmap(size, size, source.getConfig());

            Canvas canvas = new Canvas(bitmap);
            Paint paint = new Paint();
            BitmapShader shader = new BitmapShader(squaredBitmap, BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP);
            paint.setShader(shader);
            paint.setAntiAlias(true);

            float r = size/2f;
            canvas.drawCircle(r, r, r, paint);

            squaredBitmap.recycle();
            return bitmap;
        }

        @Override
        public String key() {
            return "circle";
        }
    }



    public class CircleTransform2 implements Transformation {

        float scale = mContext.getResources().getDisplayMetrics().density;
        int pxValue = (int) (2 * scale + 0.5f);
        private final int BORDER_COLOR = mContext.getResources().getColor(R.color.gray_dark);
        private final int BORDER_RADIUS = pxValue;

        @Override
        public Bitmap transform(Bitmap source) {
            int size = Math.min(source.getWidth(), source.getHeight());

            int x = (source.getWidth() - size) / 2;
            int y = (source.getHeight() - size) / 2;

            Bitmap squaredBitmap = Bitmap.createBitmap(source, x, y, size, size);
            if (squaredBitmap != source) {
                source.recycle();
            }

            Bitmap bitmap = Bitmap.createBitmap(size, size, source.getConfig());

            Canvas canvas = new Canvas(bitmap);
            Paint paint = new Paint();
            BitmapShader shader = new BitmapShader(squaredBitmap, BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP);
            paint.setShader(shader);
            paint.setAntiAlias(true);

            float r = size / 2f;

            // Prepare the background
            Paint paintBg = new Paint();
            paintBg.setColor(BORDER_COLOR);
            paintBg.setAntiAlias(true);

            // Draw the background circle
            canvas.drawCircle(r, r, r, paintBg);

            // Draw the image smaller than the background so a little border will be seen
            canvas.drawCircle(r, r, r - BORDER_RADIUS, paint);

            squaredBitmap.recycle();
            return bitmap;
        }

        @Override
        public String key() {
            return "circle";
        }
    }



    Bitmap getCircularBitmap() {
        float scale = mContext.getResources().getDisplayMetrics().density;
        int pxValue = (int) (2 * scale + 0.5f);
        int BORDER_COLOR = mContext.getResources().getColor(R.color.gray_dark);
        int BORDER_RADIUS = pxValue;

        Bitmap source = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.placeholder);
        int size = Math.min(source.getWidth(), source.getHeight());

        int x = (source.getWidth() - size) / 2;
        int y = (source.getHeight() - size) / 2;

        Bitmap squaredBitmap = Bitmap.createBitmap(source, x, y, size, size);
        if (squaredBitmap != source) {
            source.recycle();
        }

        Bitmap bitmap = Bitmap.createBitmap(size, size, source.getConfig());

        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint();
        BitmapShader shader = new BitmapShader(squaredBitmap, BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP);
        paint.setShader(shader);
        paint.setAntiAlias(true);

        float r = size / 2f;

        // Prepare the background
        Paint paintBg = new Paint();
        paintBg.setColor(BORDER_COLOR);
        paintBg.setAntiAlias(true);

        // Draw the background circle
        canvas.drawCircle(r, r, r, paintBg);

        // Draw the image smaller than the background so a little border will be seen
        canvas.drawCircle(r, r, r - BORDER_RADIUS, paint);

        squaredBitmap.recycle();
        return bitmap;
    }




}
