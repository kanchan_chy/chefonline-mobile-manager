package com.dashboard.datamodel;

/**
 * Created by masum on 22/08/2015.
 */
public class OffersData {
    private String title;
    private String position;
    private String offerFor;
    private String imageUrl;
    private Double spentAmount;
    private Double eligibleAmount;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl=imageUrl;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getOfferFor() {
        return offerFor;
    }

    public void setOfferFor(String offerFor) {
        this.offerFor = offerFor;
    }

    public Double getSpentAmount() {
        return spentAmount;
    }

    public void setSpentAmount(Double spentAmount) {
        this.spentAmount = spentAmount;
    }

    public Double getEligibleAmount() {
        return eligibleAmount;
    }

    public void setEligibleAmount(Double eligibleAmount) {
        this.eligibleAmount = eligibleAmount;
    }
}
