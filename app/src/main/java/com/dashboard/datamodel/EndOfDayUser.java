package com.dashboard.datamodel;

import java.io.Serializable;

/**
 * Created by masum on 22/08/2015.
 */
public class EndOfDayUser implements Serializable{

    private String userId;
    private String userName;
    private String orderPrice;

    private String orderedDish;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getOrderPrice() {
        return orderPrice;
    }

    public void setOrderPrice(String orderPrice) {
        this.orderPrice = orderPrice;
    }

    public String getOrderedDish() {
        return orderedDish;
    }

    public void setOrderedDish(String orderedDish) {
        this.orderedDish = orderedDish;
    }

}
