package com.dashboard.datamodel;

import java.io.Serializable;

/**
 * Created by user on 5/31/2016.
 */
public class PaymentData implements Serializable{

    private String paymentMethod;
    private String paymentAmount;

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(String paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

}
