package com.dashboard.datamodel;

import java.io.Serializable;

/**
 * Created by masum on 22/08/2015.
 */
public class EndOfDayPayment implements Serializable{
    private String paymentMethod;
    private Double grandTotal;

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public Double getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(Double grandTotal) {
        this.grandTotal = grandTotal;
    }
}
