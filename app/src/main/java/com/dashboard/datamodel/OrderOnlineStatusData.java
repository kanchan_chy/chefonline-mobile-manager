package com.dashboard.datamodel;

import java.io.Serializable;

/**
 * Created by masum on 24/01/2016.
 */
public class OrderOnlineStatusData implements Serializable{
    private String success;
    private String isComingSoon;
    private String message;

    public String getIsComingSoon() {
        return isComingSoon;
    }

    public void setIsComingSoon(String isComingSoon) {
        this.isComingSoon = isComingSoon;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }
}
