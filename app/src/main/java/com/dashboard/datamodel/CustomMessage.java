package com.dashboard.datamodel;

public class CustomMessage {
    public String UserName;
    public String DeviceUniqueID;
    public String Message;
    public String ConnectionId;
    public String orderType;
    public String reservationId;
    public String orderId;
    public String isReservationAuto;
}
