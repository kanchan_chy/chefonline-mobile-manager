package com.dashboard.datamodel;

import java.io.Serializable;

/**
 * Created by masum on 24/08/2015.
 */
public class CustomerDataSerialize implements Serializable {

    private double id;
    private String name;

    private String customerName;
    private String customerAddress;
    private String customerCity;
    private String customerMobile;
    private String orderType;
    private String postCode;
    private String grandTotal;
    private String orderDate;
    private String orderIn;
    private String orderOut;
    private String paymentMethod;
    private String paymentStatus;
    private String printStatus;
    private String comments;
    private String paymentTransactionId;

    public CustomerDataSerialize(double id, String name, OrderData orderData){
        this.id = id;
        this.name = name;
        this.customerName = orderData.getCustomerName();
        this.customerAddress = orderData.getCustomerAddress();
        this.customerCity = orderData.getCustomerCity();
        this.customerMobile = orderData.getCustomerMobile();
        this.comments = orderData.getComments();

        this.orderType = orderData.getOrderType();
        this.orderDate = orderData.getOrderDate();
        this.postCode = orderData.getPostCode();
        this.orderOut = orderData.getOrderOut();
        this.orderIn = orderData.getOrderIn();
        this.paymentMethod = orderData.getPaymentMethod();
        this.printStatus = orderData.getPrintStatus();
    }

    public double getId() {
        return id;
    }

    public void setId(double id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getComments() {
        return comments;
    }


    public String getCustomerName() {
        return this.customerName;
    }

    public String getOrderType() {
        return orderType;
    }

    public String getPostCode() {
        return postCode;
    }

    public String getGrandTotal() {
        return grandTotal;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public String getOrderIn() {
        return orderIn;
    }

    public String getOrderOut() {
        return orderOut;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public String getPaymentTransactionId() {
        return paymentTransactionId;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }


    public String getCustomerCity() {
        return customerCity;
    }

    public String getCustomerMobile() {
        return customerMobile;
    }

    public String getPrintStatus() {
        return printStatus;
    }

}
