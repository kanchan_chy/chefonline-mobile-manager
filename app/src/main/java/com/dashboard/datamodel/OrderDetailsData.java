package com.dashboard.datamodel;

import com.dashboard.datamodel.DishData;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by masum on 23/08/2015.
 */
public class OrderDetailsData implements Serializable{
    private int orderItemCount;
    private String totalAmount;
    private String discountAmount;
    private String grandTotal;
    private String deliveryCharge;
    private String offer;
    private ArrayList<DishData> dishDatas;
    private String restaurantName;
    private String restLatitude;
    private String restLongitude;
    private String paymentMethod;
    private String orderNo;
    private String customerName;
    private String customerAddress;
    private String customerCity;
    private String customerMobile;
    private String customerEmail;
    private String customerLatitude;
    private String customerLongitude;
    private String orderType;
    private String postCode;
    private String orderIn;
    private String orderOut;
    private String comments;
    private ArrayList<OrderAmountData> orderAmountDatas;

    public ArrayList<OrderAmountData> getOrderAmountDatas() {
        return orderAmountDatas;
    }

    public void setOrderAmountDatas(ArrayList<OrderAmountData> orderAmountDatas) {
        this.orderAmountDatas = orderAmountDatas;
    }

    public String getRestLongitude() {
        return restLongitude;
    }

    public void setRestLongitude(String restLongitude) {
        this.restLongitude = restLongitude;
    }

    public String getRestLatitude() {
        return restLatitude;
    }

    public void setRestLatitude(String restLatitude) {
        this.restLatitude = restLatitude;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getCustomerLatitude() {
        return customerLatitude;
    }

    public void setCustomerLatitude(String customerLatitude) {
        this.customerLatitude = customerLatitude;
    }

    public String getCustomerLongitude() {
        return customerLongitude;
    }

    public void setCustomerLongitude(String customerLongitude) {
        this.customerLongitude = customerLongitude;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getCustomerCity() {
        return customerCity;
    }

    public void setCustomerCity(String customerCity) {
        this.customerCity = customerCity;
    }

    public String getCustomerMobile() {
        return customerMobile;
    }

    public void setCustomerMobile(String customerMobile) {
        this.customerMobile = customerMobile;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getOrderIn() {
        return orderIn;
    }

    public void setOrderIn(String orderIn) {
        this.orderIn = orderIn;
    }

    public String getOrderOut() {
        return orderOut;
    }

    public void setOrderOut(String orderOut) {
        this.orderOut = orderOut;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public ArrayList<DishData> getDishDatas() {
        return dishDatas;
    }

    public void setDishDatas(ArrayList<DishData> dishDatas) {
        this.dishDatas = dishDatas;
    }

    public String getRestaurantName() {
        return restaurantName;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }

    public int getOrderItemCount() {
        return orderItemCount;
    }

    public void setOrderItemCount(int orderItemCount) {
        this.orderItemCount = orderItemCount;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(String discountAmount) {
        this.discountAmount = discountAmount;
    }

    public String getOffer() {
        return offer;
    }

    public void setOffer(String offer) {
        this.offer = offer;
    }

    public String getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        this.grandTotal = grandTotal;
    }

    public String getDeliveryCharge() {
        return deliveryCharge;
    }

    public void setDeliveryCharge(String deliveryCharge) {
        this.deliveryCharge = deliveryCharge;
    }
}
