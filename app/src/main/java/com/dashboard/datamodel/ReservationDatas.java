package com.dashboard.datamodel;

import java.util.ArrayList;

/**
 * Created by apple on 11/2/16.
 */

public class ReservationDatas {
    private String title;

    public ArrayList<ReservationDataUnconfirmed> getOrderDatas() {
        return orderDatas;
    }

    public void setOrderDatas(ArrayList<ReservationDataUnconfirmed> orderDatas) {
        this.orderDatas = orderDatas;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    private ArrayList<ReservationDataUnconfirmed> orderDatas;
}
