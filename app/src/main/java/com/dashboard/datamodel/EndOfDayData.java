package com.dashboard.datamodel;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by masum on 22/08/2015.
 */
public class EndOfDayData implements Serializable{

    private Double totalAmount = 0.00;
    private String totalItem = "0";
    private ArrayList<EndOfDayPayment> endOfDayPayments = new ArrayList<>();
    private ArrayList<EndOfDayUser> endOfDayUsers = new ArrayList<>();

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getTotalItem() {
        return totalItem;
    }

    public void setTotalItem(String totalItem) {
        this.totalItem = totalItem;
    }

    public ArrayList<EndOfDayPayment> getEndOfDayPayments() {
        return endOfDayPayments;
    }

    public void setEndOfDayPayments(ArrayList<EndOfDayPayment> endOfDayPayments) {
        this.endOfDayPayments = endOfDayPayments;
    }

    public ArrayList<EndOfDayUser> getEndOfDayUsers() {
        return endOfDayUsers;
    }

    public void setEndOfDayUsers(ArrayList<EndOfDayUser> endOfDayUsers) {
        this.endOfDayUsers = endOfDayUsers;
    }

}
