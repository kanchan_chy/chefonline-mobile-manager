package com.dashboard.datamodel;

import java.io.Serializable;

/**
 * Created by user on 6/23/2016.
 */
public class OrderAmountData implements Serializable{
    private String name;
    private String amount;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
