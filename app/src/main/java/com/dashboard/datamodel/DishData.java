package com.dashboard.datamodel;


import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by masum on 13/04/2015.
 */
public class DishData implements Serializable{
    private String dishId;
    private String dishName;
    private String dishDescription;
    private String dishRating;
    private String dishTotalRating;
    private String dishSpiceLevel;
    private String dishAllergens;
    private String dishPrice;
    private String dishQty;
    private String dishUnitPrice;
    private boolean isPizzaMenu;
    private ArrayList<PizzaItemData> pizzaItemList = new ArrayList<>();

    public DishData() {
    }

    public String getDishDescription() {
        return dishDescription;
    }

    public void setDishDescription(String dishDescription) {
        this.dishDescription = dishDescription;
    }

    public String getDishId() {
        return dishId;
    }

    public void setDishId(String dishId) {
        this.dishId = dishId;
    }

    public String getDishName() {
        return dishName;
    }

    public void setDishName(String dishName) {
        this.dishName = dishName;
    }

    public String getDishRating() {
        return dishRating;
    }

    public void setDishRating(String dishRating) {
        this.dishRating = dishRating;
    }

    public String getDishTotalRating() {
        return dishTotalRating;
    }

    public void setDishTotalRating(String dishTotalRating) {
        this.dishTotalRating = dishTotalRating;
    }

    public String getDishSpiceLevel() {
        return dishSpiceLevel;
    }

    public void setDishSpiceLevel(String dishSpiceLevel) {
        this.dishSpiceLevel = dishSpiceLevel;
    }

    public String getDishAllergens() {
        return dishAllergens;
    }

    public void setDishAllergens(String dishAllergens) {
        this.dishAllergens = dishAllergens;
    }

    public String getDishPrice() {
        return dishPrice;
    }

    public void setDishPrice(String dishPrice) {
        this.dishPrice = dishPrice;
    }

    public String getDishQty() {
        return dishQty;
    }

    public void setDishQty(String dishQty) {
        this.dishQty = dishQty;
    }


    public String getDishUnitPrice() {
        return dishUnitPrice;
    }

    public void setDishUnitPrice(String dishUnitPrice) {
        this.dishUnitPrice = dishUnitPrice;
    }

    public boolean isPizzaMenu() {
        return isPizzaMenu;
    }

    public void setPizzaMenu(boolean pizzaMenu) {
        isPizzaMenu = pizzaMenu;
    }

    public ArrayList<PizzaItemData> getPizzaItemList() {
        return pizzaItemList;
    }

    public void setPizzaItemList(ArrayList<PizzaItemData> pizzaItemList) {
        this.pizzaItemList = pizzaItemList;
    }

}
