package com.dashboard.datamodel;

import java.io.Serializable;

/**
 * Created by masum on 22/08/2015.
 */
public class ReservationDataSerialize implements Serializable {
    private String customerTitle;
    private String customerFirstName;
    private String customerLastName;
    private String customerEmail;
    private String customerMObileNo;
    private String customerTelephone;
    private String guestNumber;
    private String createdTime;
    private String createdDate;
    private String reservedTime;
    private String reservedDate;
    private String reservationId;
    private String restaurantId;
    private String restaurantName;
    private String specialRequest;

    public ReservationDataSerialize(String restaurantId, String restaurantName, ReservationDataUnconfirmed reservationDataUnconfirmed) {
        this.restaurantId = restaurantId;
        this.restaurantName = restaurantName;
        this.customerTitle = reservationDataUnconfirmed.getCustomerTitle();
        this.customerFirstName = reservationDataUnconfirmed.getCustomerFirstName();
        this.customerLastName = reservationDataUnconfirmed.getCustomerLastName();
        this.customerMObileNo = reservationDataUnconfirmed.getCustomerMObileNo();
        this.customerTelephone = reservationDataUnconfirmed.getCustomerTelephone();
        this.customerEmail = reservationDataUnconfirmed.getCustomerEmail();
        this.guestNumber = reservationDataUnconfirmed.getGuestNumber();
        this.reservationId = reservationDataUnconfirmed.getReservationId();
        this.createdDate = reservationDataUnconfirmed.getCreatedDate();
        this.createdTime = reservationDataUnconfirmed.getCreatedTime();
        this.reservedDate = reservationDataUnconfirmed.getReservedDate();
        this.reservedTime = reservationDataUnconfirmed.getReservedTime();
        this.specialRequest = reservationDataUnconfirmed.getSpecialRequest();
    }

    public String getRestaurantName() {
        return restaurantName;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }

    public String getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
    }

    public String getCustomerTelephone() {
        return customerTelephone;
    }

    public void setCustomerTelephone(String customerTelephone) {
        this.customerTelephone = customerTelephone;
    }

    public String getCustomerTitle() {
        return customerTitle;
    }

    public void setCustomerTitle(String customerTitle) {
        this.customerTitle = customerTitle;
    }

    public String getCustomerLastName() {
        return customerLastName;
    }

    public void setCustomerLastName(String customerLastName) {
        this.customerLastName = customerLastName;
    }

    public String getCustomerFirstName() {
        return customerFirstName;
    }

    public void setCustomerFirstName(String customerFirstName) {
        this.customerFirstName = customerFirstName;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getCustomerMObileNo() {
        return customerMObileNo;
    }

    public void setCustomerMObileNo(String customerMObileNo) {
        this.customerMObileNo = customerMObileNo;
    }

    public String getGuestNumber() {
        return guestNumber;
    }

    public void setGuestNumber(String guestNumber) {
        this.guestNumber = guestNumber;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getReservedTime() {
        return reservedTime;
    }

    public void setReservedTime(String reservedTime) {
        this.reservedTime = reservedTime;
    }

    public String getReservedDate() {
        return reservedDate;
    }

    public void setReservedDate(String reservedDate) {
        this.reservedDate = reservedDate;
    }

    public String getReservationId() {
        return reservationId;
    }

    public void setReservationId(String reservationId) {
        this.reservationId = reservationId;
    }

    public String getSpecialRequest() {
        return specialRequest;
    }

    public void setSpecialRequest(String specialRequest) {
        this.specialRequest = specialRequest;
    }

}
