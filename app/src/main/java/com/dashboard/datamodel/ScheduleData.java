package com.dashboard.datamodel;

import java.io.Serializable;

/**
 * Created by masum on 04/05/2015.
 */
public class ScheduleData implements Serializable{
    private String weekDayId;
    private String dayName;
    private String openingTime;
    private String closingTime;

    public String getClosingTime() {
        return closingTime;
    }

    public void setClosingTime(String closingTime) {
        this.closingTime = closingTime;
    }

    public String getOpeningTime() {
        return openingTime;
    }

    public void setOpeningTime(String openingTime) {
        this.openingTime = openingTime;
    }

    public String getWeekDayId() {
        return weekDayId;
    }

    public void setWeekDayId(String weekDayId) {
        this.weekDayId = weekDayId;
    }

    public String getDayName() {
        return dayName;
    }

    public void setDayName(String dayName) {
        this.dayName = dayName;
    }


}
