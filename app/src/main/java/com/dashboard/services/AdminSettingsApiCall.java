package com.dashboard.services;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dashboard.singleton.HttpRequestQueue;
import com.dashboard.utility.ConstantValues;
import com.dashboard.view.OnRequestComplete;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by masum on 12/01/2016.
 */

public class AdminSettingsApiCall {
    OnRequestComplete dashBoardInterface;

    /*public AdminSettingsApiCall(Context context, String restaurantId, OnRequestComplete dashBoardInterface) {
        this.dashBoardInterface = dashBoardInterface;
        //callRestaurantOrderOnlineStatusAPI(context, restaurantId);
    }*/

    public void callRestaurantOrderOnlineStatusAPI(Context context, final String restaurantId, OnRequestComplete dashBoardInterface) {
        this.dashBoardInterface = dashBoardInterface;
        StringRequest myReq = new StringRequest(Request.Method.POST, ConstantValues.BASE_API_URL, createMyReqSuccessListener(), createMyReqErrorListener()) {
            protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("funId", "49");
                params.put("restaurant_id",restaurantId);
                return params;
            }
        };

        int socketTimeout = 30000; //30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        myReq.setRetryPolicy(policy);
        HttpRequestQueue.getInstance().addToRequestQueue(myReq);

    }


    public void callChangeOrderOnlineStatusAPI(Context context, final String restaurantId, final String status, OnRequestComplete dashBoardInterface) {
        this.dashBoardInterface = dashBoardInterface;
        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest myReq = new StringRequest(Request.Method.POST, ConstantValues.BASE_API_URL, createMyReqSuccessListener(), createMyReqErrorListener()) {
            protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("funId", "50");
                params.put("restaurant_id", restaurantId);
                params.put("coming_soon", status);
                Log.i("params post", "" + params.toString());
                return params;
            }
        };

        int socketTimeout = 30000; //30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        myReq.setRetryPolicy(policy);
        queue.add(myReq);

    }

    /** Response listener for login api */
    private Response.Listener<String> createMyReqSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                dashBoardInterface.onRequestComplete(response);
            }

        };
    }

    private Response.ErrorListener createMyReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    dashBoardInterface.onRequestComplete(error.getMessage().toString());
                } catch (Exception e) {
                    Log.e("ERROR:OD", "" + e.getMessage());
                }

            }
        };
    }
}
