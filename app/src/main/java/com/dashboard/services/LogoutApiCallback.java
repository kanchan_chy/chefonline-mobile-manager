package com.dashboard.services;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.dashboard.singleton.HttpRequestQueue;
import com.dashboard.utility.ConstantValues;
import com.dashboard.view.VolleyApiInterface;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by masum on 22/08/2015.
 */
public class LogoutApiCallback {

    VolleyApiInterface volleyApiInterface;

    public LogoutApiCallback(Context context, String userId, String deviceId, VolleyApiInterface volleyApiInterface) {
        this.volleyApiInterface = volleyApiInterface;
        callLogoutApi(context, userId, deviceId);
    }

    /** api for forgot password */
    private void callLogoutApi(Context context,final String userId, final String deviceId) {
        StringRequest myReq = new StringRequest(Request.Method.POST, ConstantValues.BASE_API_URL, createMyReqSuccessListener(), createMyReqErrorListener()) {
            protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("funId", "73");
                params.put("user_id", userId);
                params.put("device_id", deviceId);
                Log.e("Logout params", params.toString());
                return params;
            }
        };

        int socketTimeout = 30000; //30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        myReq.setRetryPolicy(policy);
        HttpRequestQueue.getInstance().addToRequestQueue(myReq);


    }

    /** Response listener for login api */
    private Response.Listener<String> createMyReqSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                volleyApiInterface.onRequestSuccess(response);
            }

        };
    }

    private Response.ErrorListener createMyReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                volleyApiInterface.onRequestFailed(error);
            }
        };
    }

}
