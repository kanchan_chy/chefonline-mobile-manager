package com.dashboard.services;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.dashboard.jsonparser.JsonParser;
import com.dashboard.singleton.HttpRequestQueue;
import com.dashboard.utility.ConstantValues;
import com.dashboard.view.DashBoardDeleteInterface;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by masum on 22/08/2015.
 */
public class DashBoardOrderDeleteApiCallBack {
    DashBoardDeleteInterface dashBoardDeleteInterface;
    Context context;

    public DashBoardOrderDeleteApiCallBack(Context context, String orderId, DashBoardDeleteInterface dashBoardDeleteInterface) {
        this.dashBoardDeleteInterface = dashBoardDeleteInterface;
        this.context = context;
        getHotRestaurant(context, orderId);
    }

    ProgressDialog progress;

    private void getHotRestaurant(Context context, final String orderId) {
        StringRequest myReq = new StringRequest(Request.Method.POST, ConstantValues.BASE_API_URL, createMyReqSuccessListener(), createMyReqErrorListener()) {
            protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("funId", "35");
                params.put("order_id", orderId);
                return params;
            }
        };

        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        myReq.setRetryPolicy(policy);
        HttpRequestQueue.getInstance().addToRequestQueue(myReq);
        progress = new ProgressDialog(context);
        progress.setMessage("Please wait....");
        progress.show();
    }

    private Response.ErrorListener createMyReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    progress.dismiss();
                    dashBoardDeleteInterface.onRequestComplete("0");
                    Toast.makeText(context, "Server Response Error Splash" + error.getMessage(), Toast.LENGTH_SHORT).show();

                } catch (Exception e) {

                }
            }
        };
    }

    private Response.Listener<String> createMyReqSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    progress.dismiss();
                    Log.i("resp", "" + response);
                    //  Toast.makeText(context, "Result" + response, Toast.LENGTH_SHORT).show();
                    String status = JsonParser.parseDeleteOrder(response);
                    dashBoardDeleteInterface.onRequestComplete(status);
                }
                catch (Exception e2) {
                }

            }

        };
    }
}
