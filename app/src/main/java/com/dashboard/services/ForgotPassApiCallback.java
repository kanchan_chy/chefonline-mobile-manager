package com.dashboard.services;

import android.content.Context;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.dashboard.singleton.HttpRequestQueue;
import com.dashboard.utility.ConstantValues;
import com.dashboard.view.LoginApiInterface;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by masum on 22/08/2015.
 */
public class ForgotPassApiCallback {

    LoginApiInterface loginApiInterface;

    public ForgotPassApiCallback(Context context, String email, LoginApiInterface loginApiInterface) {
        this.loginApiInterface = loginApiInterface;
        callForgotPasswordApi(context, email);
    }

    /** api for forgot password */
    private void callForgotPasswordApi(Context context,final String email) {
        StringRequest myReq = new StringRequest(Request.Method.POST, ConstantValues.BASE_API_URL, createMyReqSuccessListener(), createMyReqErrorListener()) {
            protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("funId", "4");
                params.put("email", email);
                return params;
            }
        };

        int socketTimeout = 30000; //30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        myReq.setRetryPolicy(policy);
        HttpRequestQueue.getInstance().addToRequestQueue(myReq);


    }

    /** Response listener for login api */
    private Response.Listener<String> createMyReqSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                loginApiInterface.onRequestComplete(response);
            }

        };
    }

    private Response.ErrorListener createMyReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    loginApiInterface.onRequestComplete(error.getMessage().toString());
                } catch (Exception e) {

                }
            }
        };
    }

}
