package com.dashboard.services;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dashboard.singleton.HttpRequestQueue;
import com.dashboard.utility.ConstantValues;
import com.dashboard.view.OnRequestComplete;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by masum on 12/01/2016.
 */

public class RestaurantDeviceStatusApiCall {
    OnRequestComplete onRequestComplete;

    public RestaurantDeviceStatusApiCall() {

    }

    public void callOpenStatusAPI(Context context, final String restaurantId, OnRequestComplete dashBoardInterface) {
        this.onRequestComplete = dashBoardInterface;
        StringRequest myReq = new StringRequest(Request.Method.POST, ConstantValues.BASE_API_URL, createMyReqSuccessListener(), createMyReqErrorListener()) {
            protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("funId", "");
                params.put("rest_id", restaurantId);
                params.put("weekday", "");
                return params;
            }
        };

        int socketTimeout = 30000; //30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        myReq.setRetryPolicy(policy);
        HttpRequestQueue.getInstance().addToRequestQueue(myReq);

    }

    public void callPrintStatusAPI(Context context, final String orderID, OnRequestComplete dashBoardInterface) {
        this.onRequestComplete = dashBoardInterface;
        StringRequest myReq = new StringRequest(Request.Method.POST, ConstantValues.BASE_API_URL, createMyReqSuccessListener(), createMyReqErrorListener()) {
            protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("funId", "52");
                params.put("order_id", orderID);
                params.put("order_printing_status", "1");
                return params;
            }
        };

        int socketTimeout = 30000; //30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        myReq.setRetryPolicy(policy);
        HttpRequestQueue.getInstance().addToRequestQueue(myReq);

    }

    public void callReservationStatusChangeAPI(Context context, final String orderID, OnRequestComplete dashBoardInterface) {
        this.onRequestComplete = dashBoardInterface;
        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest myReq = new StringRequest(Request.Method.POST, ConstantValues.BASE_API_URL, createMyReqSuccessListener(), createMyReqErrorListener()) {
            protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("funId", "52");
                params.put("order_id", orderID);
                params.put("order_printing_status", "1");
                return params;
            }
        };

        int socketTimeout = 30000; //30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        myReq.setRetryPolicy(policy);
        queue.add(myReq);

    }

    /** Response listener for printing status api */
    private Response.Listener<String> createMyReqSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Print response", "***" + response);
                onRequestComplete.onRequestComplete(response);
            }

        };
    }

    private Response.ErrorListener createMyReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    onRequestComplete.onRequestComplete("Exception in device status");
                } catch (Exception e) {
                    Log.e("ERROR:OD", "" + e.getMessage());
                }

            }
        };
    }
}
