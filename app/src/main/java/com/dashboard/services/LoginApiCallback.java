package com.dashboard.services;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.dashboard.singleton.HttpRequestQueue;
import com.dashboard.utility.ConstantValues;
import com.dashboard.view.LoginApiInterface;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by masum on 22/08/2015.
 */
public class LoginApiCallback {

    LoginApiInterface loginApiInterface;

    public LoginApiCallback(Context context, String userId, String password, String deviceId, String regKey, String platformNo, LoginApiInterface loginApiInterface) {
        this.loginApiInterface = loginApiInterface;
        callLoginApi(context, userId, password, deviceId, regKey, platformNo);
    }

    private void callLoginApi(Context context, final String userId, final String password, final String device_id, final String regKey, final String platformNo) {
        StringRequest myReq = new StringRequest(Request.Method.POST, ConstantValues.BASE_API_URL, createMyReqSuccessListener(), createMyReqErrorListener()) {
            protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("funId", "37");
                params.put("rest_id", "");
                params.put("username", userId);
                params.put("password", password);
                params.put("device_id", device_id);
                params.put("platform", platformNo);
                params.put("token", regKey);
                Log.e("LOGIN PARAMS", "" + params.toString());
                return params;
            }
        };

        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        myReq.setRetryPolicy(policy);
        HttpRequestQueue.getInstance().addToRequestQueue(myReq);

    }

    /** Response listener for login api */
    private Response.Listener<String> createMyReqSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    loginApiInterface.onRequestComplete(response);
                }
                catch (Exception e2) {

                }
            }

        };
    }

    private Response.ErrorListener createMyReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    loginApiInterface.onRequestComplete("Something went wrong");
                }
                catch (Exception e2) {

                }
            }
        };
    }

}
