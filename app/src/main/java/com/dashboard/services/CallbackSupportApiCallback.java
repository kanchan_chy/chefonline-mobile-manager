package com.dashboard.services;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.dashboard.singleton.HttpRequestQueue;
import com.dashboard.utility.ConstantValues;
import com.dashboard.view.VolleyApiInterface;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by masum on 22/08/2015.
 */
public class CallbackSupportApiCallback {

    VolleyApiInterface volleyApiInterface;

    public CallbackSupportApiCallback(Context context, String deviceId, String deviceOsSerial, String batterySattus, String batteryLevel, String ipAddress,String upTime, String wifiMac, String restId, String restName, String email, String query, VolleyApiInterface volleyApiInterface) {
        this.volleyApiInterface = volleyApiInterface;
        callSubmitQuery(context, deviceId, deviceOsSerial, batterySattus, batteryLevel, ipAddress, upTime, wifiMac, restId, restName, email, query);
    }

    /** api for forgot password */
    private void callSubmitQuery(Context context, final String deviceId, final String deviceOsSerial, final String batterySattus, final String batteryLevel, final String ipAddress, final String upTime, final String wifiMac, final String restId, final String restName, final String email, final String query) {
        StringRequest myReq = new StringRequest(Request.Method.POST, ConstantValues.BASE_API_URL, createMyReqSuccessListener(), createMyReqErrorListener()) {
            protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("funId", "54");
                params.put("device_id", deviceId);
                params.put("device_os_serial_no", deviceOsSerial);
                params.put("battery_status", batterySattus);
                params.put("battery_level", batteryLevel);
                params.put("ip_address", ipAddress);
                params.put("up_time", upTime);
                params.put("wifi_mac_address", wifiMac);
                params.put("restaurant_id", restId);
                params.put("restaurant_name", restName);
                params.put("email", email);
                params.put("message", query);
                Log.i("Callback PARAMS", "" + params.toString());
                return params;
            }
        };

        int socketTimeout = 30000; //30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        myReq.setRetryPolicy(policy);
        HttpRequestQueue.getInstance().addToRequestQueue(myReq);


    }

    /** Response listener for login api */
    private Response.Listener<String> createMyReqSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                volleyApiInterface.onRequestSuccess(response);
            }

        };
    }

    private Response.ErrorListener createMyReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                volleyApiInterface.onRequestFailed(error);
            }
        };
    }

}
