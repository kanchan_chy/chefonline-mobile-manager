package com.dashboard.services;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.dashboard.singleton.HttpRequestQueue;
import com.dashboard.utility.ConstantValues;
import com.dashboard.view.OnRequestComplete;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Masum on 22/08/2015.
 */
public class OrderDetailsApiCallBack {
    OnRequestComplete dashBoardInterface;

    public OrderDetailsApiCallBack(Context context, String orderId, OnRequestComplete dashBoardInterface) {
        this.dashBoardInterface = dashBoardInterface;
        callOrderDetailsApi(context, orderId);
    }

    private void callOrderDetailsApi(Context context, final String orderId) {
        StringRequest myReq = new StringRequest(Request.Method.POST, ConstantValues.BASE_API_URL, createMyReqSuccessListener(), createMyReqErrorListener()) {
            protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("funId", "34");
                params.put("order_id", orderId);
                return params;
            }
        };

        int socketTimeout = 30000; //30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        myReq.setRetryPolicy(policy);
        HttpRequestQueue.getInstance().addToRequestQueue(myReq);

    }

    /** Response listener for login api */
    private Response.Listener<String> createMyReqSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    dashBoardInterface.onRequestComplete(response);
                } catch (Exception e) {

                }
            }

        };
    }

    private Response.ErrorListener createMyReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    dashBoardInterface.onRequestComplete(error.getMessage().toString());
                } catch (Exception e) {
                    Log.e("ERROR:OD", "" + e.getMessage());
                }

            }
        };
    }
}
