package com.dashboard.services;

import android.content.Context;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.dashboard.singleton.HttpRequestQueue;
import com.dashboard.utility.ConstantValues;
import com.dashboard.view.VolleyApiInterface;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by masum on 22/08/2015.
 */
public class SplashApiCallback {

    VolleyApiInterface volleyApiInterface;

    public SplashApiCallback(Context context, String platform, VolleyApiInterface volleyApiInterface) {
        this.volleyApiInterface = volleyApiInterface;
        callForgotPincodeApi(context, platform);
    }

    /** api for forgot password */
    private void callForgotPincodeApi(Context context,final String platform) {
        StringRequest myReq = new StringRequest(Request.Method.POST, ConstantValues.BASE_API_URL, createMyReqSuccessListener(), createMyReqErrorListener()) {
            protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("funId", "67");
                params.put("platform_id", platform);
                return params;
            }
        };

        int socketTimeout = 30000; //30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        myReq.setRetryPolicy(policy);
        HttpRequestQueue.getInstance().addToRequestQueue(myReq);


    }

    /** Response listener for login api */
    private Response.Listener<String> createMyReqSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                volleyApiInterface.onRequestSuccess(response);
            }

        };
    }

    private Response.ErrorListener createMyReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                volleyApiInterface.onRequestFailed(error);
            }
        };
    }

}
