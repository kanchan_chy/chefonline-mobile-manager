package com.dashboard.services;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.dashboard.view.AddOffersResponseInterface;

/**
 * Created by masum on 22/08/2015.
 */
public class AddOfferService {

    AddOffersResponseInterface responseInterface;

    public void callOfferSubmitApi(Context context, final String title, final String desc, final String spentAmount, final String imgUrl, final String offerFor, final String offerPosition) {
        RequestQueue queue = Volley.newRequestQueue(context);

     /*   StringRequest myReq = new StringRequest(Request.Method.POST, ConstantValues.BASE_API_URL, createSubmitSuccessListener(), createSubmitErrorListener()) {
            protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("funId", "37");
                params.put("rest_id", "");
                params.put("username", title);
                params.put("password", desc);
                params.put("device_id", spentAmount);
                params.put("platform", imgUrl);
                params.put("token", offerFor);
                Log.i("LOGIN PARAMS", "" + params.toString());
                return params;
            }
        };

        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        myReq.setRetryPolicy(policy);
        queue.add(myReq);  */

    }

    public void callOfferGalleryApi(Context context, final String title, final String desc, final String spentAmount, final String imgUrl, final String offerFor, final String offerPosition, final String firstOrderOffer, final boolean defaultStatus, final String orderDay) {
        RequestQueue queue = Volley.newRequestQueue(context);

    /*    StringRequest myReq = new StringRequest(Request.Method.POST, ConstantValues.BASE_API_URL, createGallerySuccessListener(), createGalleryErrorListener()) {
            protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("funId", "37");
                params.put("rest_id", "");
                params.put("username", title);
                params.put("password", desc);
                params.put("device_id", spentAmount);
                params.put("platform", imgUrl);
                params.put("token", offerFor);
                Log.i("LOGIN PARAMS", "" + params.toString());
                return params;
            }
        };

        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        myReq.setRetryPolicy(policy);
        queue.add(myReq);  */

    }

    /** Response listener for login api */
    private Response.Listener<String> createSubmitSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                responseInterface.onSubmitRequestSuccess(response);
            }

        };
    }

    private Response.ErrorListener createSubmitErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                responseInterface.onSubmitRequestError(error);
            }
        };
    }

    private Response.Listener<String> createGallerySuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                responseInterface.onGalleryRequestSuccess(response);
            }

        };
    }

    private Response.ErrorListener createGalleryErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                responseInterface.onGalleryRequestError(error);
            }
        };
    }


}
