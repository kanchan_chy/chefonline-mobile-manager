package com.dashboard.gcm;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.chefonline.managermobile.R;
import com.dashboard.activity.DashBoardTodayActivityMain;
import com.dashboard.activity.LoginActivity;
import com.dashboard.fragments.DashBoardTodayFragment;
import com.dashboard.services.OrderDetailsApiCallBack;
import com.dashboard.singleton.ApplicationVisibleState;
import com.dashboard.utility.ConstantValues;
import com.dashboard.utility.DateFormatter;
import com.dashboard.utility.PreferenceUtil;
import com.dashboard.view.OnRequestComplete;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.util.Random;

public class GcmIntentService extends IntentService {

	public static final int NOTIFICATION_ID = 1;
	private static final String TAG = "GcmIntentService";
	private NotificationManager mNotificationManager;
	NotificationCompat.Builder builder;

	public GcmIntentService() {
		super("GcmIntentServive");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		//final PowerManager.WakeLock wakeLock;

		Log.e("Activity state", "***" + ApplicationVisibleState.isActivityVisible());

		/*if(ApplicationVisibleState.isActivityVisible()) {
			return;
		}*/

		Bundle extras = intent.getExtras();
		GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
		String messageType = gcm.getMessageType(intent);

		try {
			if (!extras.getString("Message").equalsIgnoreCase("")) {
				if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
					sendNotification("Send error: " + extras.toString(), "0", "");
				} else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
					sendNotification("Deleted messages on server: " + extras.toString(), "0","");
					// If it's a regular GCM message, do some work.
				} else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
					// This loop represents the service doing some work.
					for (int i = 0; i < 5; i++) {
						Log.i("", "Working... " + (i + 1) + "/5 @ " + SystemClock.elapsedRealtime());
						try {
							 	Thread.sleep(100);
						} catch (InterruptedException e) {

						}
					}
				}

				//Post notification of received message.
				Log.i("", "Completed work @ " + SystemClock.elapsedRealtime());
				Log.i("PUSH DATA", "Received: " + extras.toString());


				final PreferenceUtil preferenceUtil = new PreferenceUtil(this);
				System.out.println("My Key" + preferenceUtil.getGCMRegId());
				if (preferenceUtil.getGCMRegId().equals(extras.getString("Reg_key"))
						&& preferenceUtil.getResID().equals(extras.getString("restaurant_id"))) {

				/*	PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
					wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "ForceWakeLockNotification");
					wakeLock.acquire(); */
				/*if (items.contains(preferenceUtil.getGCMRegId()) && preferenceUtil.getResID().equals(extras.getString("restaurant_id"))) {*/

					DashBoardTodayFragment.fromDate = DateFormatter.getCurrentDate();
					DashBoardTodayFragment.toDate = DateFormatter.getCurrentDate();

					if ("Reservation".equalsIgnoreCase(extras.getString("type"))) {
						sendNotification(extras.getString("Message"), "Reservation type", "CHEF ONLINE");
						ConstantValues.reservationNewIds.add(extras.getString("reservation_id"));

						if ("1".equalsIgnoreCase(extras.getString("is_auto_reservation"))) {
							Log.i(TAG, "Auto confirm true");
							Intent updateIntent = new Intent("on-update-completed");
							LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(updateIntent);
						} else {
							Intent intentUpdateReservationBubble = new Intent("on-update-auto-reservation");
							LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intentUpdateReservationBubble);
						}

						return;
					}

					sendNotification(extras.getString("Message"), "Test type", "CHEF ONLINE");
					ConstantValues.IS_NEW_ORDER = true;
					final String orderId = extras.getString("order_id");

					new OrderDetailsApiCallBack(getApplicationContext(), extras.getString("order_id"), new OnRequestComplete() {
						@Override
						public void onRequestComplete(String response) {
							Log.i("*** NEW ORDER ***", "Response" + response);
							try {
								//OrderDetailsData orderDetailsData = JsonParser.parseOrderDetailsData(response);
								DashBoardTodayFragment.dashBoardPresenter.onClickGetOrder(ConstantValues.ORDER_ONLINE_LIST_API_ID, false);

							} catch (Exception e) {

							}
						}
					});

				}

			}

		} catch (Exception e) {
			Log.i("Exception Push", "" + e.getMessage() + e.getStackTrace());
		}

		//WakeLocker.release();
		GcmBroadcastReceiver.completeWakefulIntent(intent);

	}

	private void sendNotification(String msg, String pushType, String user_or_conversation_id) {
		if(msg != null) {
			Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
			Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
			r.play();
			mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
			//Log.e("msg, type & identity is", "" + msg+"," + pushType + "," + user_or_conversation_id);
			PreferenceUtil preferenceUtil = new PreferenceUtil(this);
			PendingIntent contentIntent;

			if (preferenceUtil.getLogInStatus() == 0) {
				contentIntent = PendingIntent.getActivity(this, 0, new Intent(this, LoginActivity.class), PendingIntent.FLAG_UPDATE_CURRENT);
			} else {
				contentIntent = PendingIntent.getActivity(this, 0, new Intent(this, DashBoardTodayActivityMain.class), PendingIntent.FLAG_UPDATE_CURRENT);
			}

			Bitmap notificationLargeIconBitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.app_icon);

			NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
					.setSmallIcon(R.drawable.notification_icon)
					.setLargeIcon(notificationLargeIconBitmap)
					.setContentTitle("ChefOnline Manager")
					.setStyle(new NotificationCompat.BigTextStyle().bigText(msg))
					.setContentText(msg)
					.setAutoCancel(true);

			mBuilder.setContentIntent(contentIntent);
			mBuilder.setContentIntent(contentIntent);
			/*Random r = new Random();
			int notify_id = (r.nextInt(80) + 65);*/

			mNotificationManager.notify(new Random().nextInt(), mBuilder.build());

		}
	}

}
