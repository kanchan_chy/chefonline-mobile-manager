package com.dashboard.activity;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.chefonline.managermobile.R;
import com.dashboard.customview.CustomToast;
import com.dashboard.singleton.ApplicationVisibleState;
import com.dashboard.utility.DirectionsJSONParser;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by user on 5/29/2016.
 */
public class MapActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMapLoadedCallback{

    GoogleMap map;
    boolean isMapLoaded;
    Double customerLat;
    Double customerLon;
    Double restLat;
    Double restLon;
    Marker markerCustomer;
    Marker markerRest;
    String customerName = "";
    String restName = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        initView();
        initData();
        initMap();
    }

    @Override
    protected void onPause() {
        ApplicationVisibleState.activityPaused();
        super.onPause();
    }

    @Override
    protected void onResume() {
        ApplicationVisibleState.activityResumed();
        super.onResume();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.setOnMapLoadedCallback(this);
    }

    @Override
    public void onMapLoaded() {
        isMapLoaded = true;
        try {
              LatLngBounds bounds = new LatLngBounds.Builder()
                .include(new LatLng(restLat, restLon))
                .include(new LatLng(customerLat, customerLon))
                .build();
              map.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 150));

        /*    CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(restLat, restLon)).zoom(13.0F).build();
            map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));  */
        } catch (Exception e) {
        }

        try {
            markerCustomer = map.addMarker(new MarkerOptions().position(new LatLng(customerLat, customerLon)).title(customerName));
            markerRest = map.addMarker(new MarkerOptions().position(new LatLng(restLat, restLon)).title(restName));
            markerRest.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
            markerCustomer.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
            markerCustomer.showInfoWindow();
            drawPath();
        } catch (Exception e) {
        }

    }



    private void initView() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Google Map");
        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }
    }


    private void initData() {
        isMapLoaded = false;
        try {
            restName = getIntent().getExtras().getString("rest_name");
            customerName = getIntent().getExtras().getString("customer_name");
            String tempCustomerLat = getIntent().getExtras().getString("customer_lat");
            String tempCustomerLon = getIntent().getExtras().getString("customer_lon");
            String tempRestLat = getIntent().getExtras().getString("rest_lat");
            String tempRestLon = getIntent().getExtras().getString("rest_lon");
            if(tempCustomerLat != null && !tempCustomerLat.equalsIgnoreCase("")) {
                customerLat = Double.valueOf(tempCustomerLat);
            }
            if(tempCustomerLon != null && !tempCustomerLon.equalsIgnoreCase("")) {
                customerLon = Double.valueOf(tempCustomerLon);
            }
            if(tempRestLat != null && !tempRestLat.equalsIgnoreCase("")) {
                restLat = Double.valueOf(tempRestLat);
            }
            if(tempRestLon != null && !tempRestLon.equalsIgnoreCase("")) {
                restLon = Double.valueOf(tempRestLon);
            }
        } catch (Exception e) {
        }
    }

    private void initMap() {
        try{
            ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMapAsync(this);
        } catch (Exception e) {
        }
    }





    public void drawPath()
    {
        try
        {
            new CustomToast(this, "Please wait to see the path", "", true);
            String str_origin = "origin="+restLat+","+restLon;
            // Destination of route
            String str_dest = "destination="+customerLat+","+customerLon;
            // Sensor enabled
            String sensor = "sensor=false";
            // Building the parameters to the web service
            String parameters = str_origin+"&"+str_dest+"&"+sensor;
            // Output format
            String output = "json";
            // Building the url to the web service
            String url = "https://maps.googleapis.com/maps/api/directions/"+output+"?"+parameters;
            DownloadTask downloadTask = new DownloadTask();
            // Start downloading json data from Google Directions API
            downloadTask.execute(url);
        }
        catch(Exception e)
        {
            Toast.makeText(getApplicationContext(),"Path can't be shown, please try again",Toast.LENGTH_LONG).show();
        }
    }



    /** A method to download json data from url */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try{
            URL url = new URL(strUrl);
            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();
            // Connecting to url
            urlConnection.connect();
            // Reading data from url
            iStream = urlConnection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
            StringBuffer sb = new StringBuffer();
            String line = "";
            while( ( line = br.readLine()) != null){
                sb.append(line);
            }
            data = sb.toString();
            br.close();

        }catch(Exception e){
        }finally{
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }



    // Fetches data from url passed
    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try{
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            }catch(Exception e){
                //Log.d("Background Task",e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }
    }



    /** A class to parse the Google Places in JSON format */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String,String>>> >{

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try{
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            }catch(Exception e){
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();

            // Traversing through all the routes
            for(int i=0;i<result.size();i++){
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for(int j=0;j<path.size();j++){
                    HashMap<String,String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(8);
                lineOptions.color(Color.RED);
            }

            // Drawing polyline in the Google Map for the i-th route
            map.addPolyline(lineOptions);
        }

    }




}
