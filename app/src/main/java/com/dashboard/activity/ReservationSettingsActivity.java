package com.dashboard.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.chefonline.managermobile.R;
import com.dashboard.customview.CustomToast;
import com.dashboard.presenter.ReservationSettingsPresenter;
import com.dashboard.singleton.ApplicationVisibleState;
import com.dashboard.utility.PreferenceUtil;
import com.dashboard.view.ReservationSettingsView;


/**
 * Created by user on 1/6/2016.
 */
public class ReservationSettingsActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener, ReservationSettingsView {

   // ToggleButton toggleAutoReservation;
   // ToggleButton toggleReservation;
    CheckBox checkAutoReservation;
    CheckBox checkReservation;
    private ProgressDialog progressDialog;
    ReservationSettingsPresenter reservationSettingsPresenter;
    PreferenceUtil preferenceUtil;
    private boolean handleReservationChange, handleAutoReservationChange;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reservation_settings2);
        preferenceUtil = new PreferenceUtil(this);
        reservationSettingsPresenter = new ReservationSettingsPresenter(this);
        initView();
        reservationSettingsPresenter.getReservationSettings();
    }

    @Override
    protected void onPause() {
        ApplicationVisibleState.activityPaused();
        super.onPause();
    }

    @Override
    protected void onResume() {
        ApplicationVisibleState.activityResumed();
        super.onResume();
    }

    private void initView() {
        checkAutoReservation = (CheckBox) findViewById(R.id.checkAutoReservation);
        checkReservation = (CheckBox) findViewById(R.id.checkReservation);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Reservation Settings");
        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

    }

    private void setUiClickHandler() {
        checkAutoReservation.setOnCheckedChangeListener(this);
        checkReservation.setOnCheckedChangeListener(this);
    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(buttonView.getId() == R.id.checkAutoReservation)  {
            if(handleAutoReservationChange) {
                int isAutoReservation;
                if(isChecked) isAutoReservation = 1;
                else isAutoReservation = 0;
                reservationSettingsPresenter.setIsAutoReservationActive(getRestaurantId(), String.valueOf(isAutoReservation));
            } else {
                handleAutoReservationChange = true;
            }
        }
        else if(buttonView.getId() == R.id.checkReservation)  {
            if(handleReservationChange) {
                int isReservation;
                if(isChecked) isReservation = 1;
                else isReservation = 0;
                reservationSettingsPresenter.setIsReservationActive(getRestaurantId(), String.valueOf(isReservation));
            } else {
                handleReservationChange = true;
            }
        }
    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    @Override
    public String getRestaurantId() {
        return preferenceUtil.getResID();
    }

    @Override
    public void loadReservationSettings() {
        checkReservation.setChecked(preferenceUtil.getReservation());
        checkAutoReservation.setChecked(preferenceUtil.getAutoReservation());
        setUiClickHandler();
        handleReservationChange = true;
        handleAutoReservationChange = true;
    }

    @Override
    public void setAutoReservationActive(boolean status) {
        boolean checked = checkAutoReservation.isChecked();
        if(!status) {
            checked = !checked;
            handleAutoReservationChange = false;
            checkAutoReservation.setChecked(checked);
        }
        preferenceUtil.setAutoReservation(checked);
        if(status) {
            Intent intent = new Intent(ReservationSettingsActivity.this, SplashActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
        }
     /*   Intent intent = new Intent("on-update-auto-reservation");
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);  */
    }

    @Override
    public void setReservationActive(boolean status) {
        boolean checked = checkReservation.isChecked();
        if(!status) {
            checked = !checked;
            handleReservationChange = false;
            checkReservation.setChecked(checked);
        }
        preferenceUtil.setReservation(checked);
        if(status) {
            Intent intent = new Intent(ReservationSettingsActivity.this, SplashActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
        }
      /*  Intent intent = new Intent("on-update-reservation");
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);  */
    }

    @Override
    public void showProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait....");
        progressDialog.show();
    }

    @Override
    public void dismissProgressDialog() {
        progressDialog.dismiss();
    }

    @Override
    public void showToast(String message, String subMessage, boolean isButtonOk) {
        new CustomToast(this, message, subMessage, isButtonOk);
    }


}