package com.dashboard.activity;

import android.content.Intent;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.chefonline.managermobile.R;
import com.dashboard.datamodel.CustomerDataSerialize;
import com.dashboard.singleton.ApplicationVisibleState;

public class CustomerInfoActivity extends AppCompatActivity {
    TextView textViewCustomerName;
    TextView textViewAddress;
    TextView textViewPostCode;
    TextView textViewCity;
    TextView textViewOrderType;
    TextView textViewPaymentType;
    TextView textViewTime;
    TextView textViewComments;
    TextView textViewMobile;
    TextView textViewCommentsTitle;

    CustomerDataSerialize customerData;
    private long mLastClickTime = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_info);

        Intent i = getIntent();
        customerData = (CustomerDataSerialize) i.getSerializableExtra("sampleObject");

        intiView();
        setData();

    }

    @Override
    protected void onPause() {
        ApplicationVisibleState.activityPaused();
        super.onPause();
    }

    @Override
    protected void onResume() {
        ApplicationVisibleState.activityResumed();
        super.onResume();
    }

    private void setData() {
        textViewCustomerName.setText(customerData.getCustomerName());
        textViewAddress.setText(customerData.getCustomerAddress());
        textViewMobile.setText("Mobile: " + customerData.getCustomerMobile());
        textViewPostCode.setText("Postcode: " + customerData.getPostCode());
        textViewCity.setText("City: " + customerData.getCustomerCity());

        textViewOrderType.setText("Order type: " + customerData.getOrderType());
        textViewTime.setText("Time: " + customerData.getOrderOut());

        if (customerData.getComments().equalsIgnoreCase("")) {
            textViewCommentsTitle.setVisibility(View.GONE);
            textViewComments.setVisibility(View.GONE);

        } else {
            textViewComments.setText(customerData.getComments());
        }

        textViewPaymentType.setText("Payment: " + customerData.getPaymentMethod());
    }

    private void intiView() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Customer Info");
        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

        textViewCustomerName = (TextView) findViewById(R.id.textViewCustomerName);
        textViewAddress = (TextView) findViewById(R.id.textViewAddress);
        textViewPostCode = (TextView) findViewById(R.id.textViewPostcode);
        textViewAddress = (TextView) findViewById(R.id.textViewAddress);
        textViewCity = (TextView) findViewById(R.id.textViewCity);
        textViewOrderType = (TextView) findViewById(R.id.textViewOrderType);
        textViewMobile = (TextView) findViewById(R.id.textViewMobile);
        textViewPaymentType = (TextView) findViewById(R.id.textViewPaymentType);
        textViewTime = (TextView) findViewById(R.id.textViewTime);
        textViewComments = (TextView) findViewById(R.id.textViewComments);
        textViewMobile = (TextView) findViewById(R.id.textViewMobile);
        textViewCommentsTitle = (TextView) findViewById(R.id.textViewCommentsTitle);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_customer_info, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000){
            mLastClickTime = SystemClock.elapsedRealtime();
            return true;
        }
        else {
            mLastClickTime = SystemClock.elapsedRealtime();
        }
        int id = item.getItemId();

        /*//noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }*/

        if (id == R.id.home) {
            onBackPressed();
            /*if (getParentActivityIntent() == null) {
                //Log.i("Exception ", "You have forgotten to specify the parentActivityName in the AndroidManifest!");
                onBackPressed();
            } else {
                NavUtils.navigateUpFromSameTask(this);
            }*/
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
