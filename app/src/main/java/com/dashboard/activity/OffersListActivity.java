package com.dashboard.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.chefonline.managermobile.R;
import com.dashboard.adapter.OffersAdapter;
import com.dashboard.datamodel.OffersData;
import com.dashboard.singleton.ApplicationVisibleState;

import java.util.ArrayList;

/**
 * Created by user on 1/18/2016.
 */
public class OffersListActivity extends AppCompatActivity implements View.OnClickListener{

    RecyclerView recyclerOffersList;
    FloatingActionButton fabOffers;

    OffersAdapter offersAdapter;
    ArrayList<OffersData> mOfferData=new ArrayList<>();
    private long mLastClickTime = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offers_list);
        initView();
        setUiClick();
        prepareListData();
        loadData();
    }

    @Override
    protected void onPause() {
        ApplicationVisibleState.activityPaused();
        super.onPause();
    }

    @Override
    protected void onResume() {
        ApplicationVisibleState.activityResumed();
        super.onResume();
    }

    private void initView() {
        recyclerOffersList=(RecyclerView)findViewById(R.id.recycleOffersList);
        fabOffers=(FloatingActionButton)findViewById(R.id.fabOffers);

        LinearLayoutManager manager = new LinearLayoutManager(this);
        recyclerOffersList.setLayoutManager(manager);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Offers");
        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }
    }


    private void setUiClick() {
        fabOffers.setOnClickListener(this);
    }

    private void loadData() {
        offersAdapter = new OffersAdapter(OffersListActivity.this, mOfferData);
        recyclerOffersList.setAdapter(offersAdapter);
    }


    private void prepareListData() {
        mOfferData = new ArrayList<>();
        for(int i=1;i<=5;i++) {
            OffersData mData=new OffersData();
            mData.setTitle("My Title "+i);
            mData.setPosition("Left");
            mData.setOfferFor("Collection and Delivery");
            mData.setImageUrl("http://bayleaf.co.uk/takeaway/backoffice/uploads/offer/327864356_1436273602.png");
            mData.setSpentAmount(39.50);
            mData.setEligibleAmount(39.50);
            mOfferData.add(mData);
        }
    }


    @Override
    public void onClick(View v) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000){
            mLastClickTime = SystemClock.elapsedRealtime();
            return;
        }
        else {
            mLastClickTime = SystemClock.elapsedRealtime();
        }
        if(v.getId()==R.id.fabOffers) {
            startActivity(new Intent(OffersListActivity.this,AddChangeOffersActivity.class));
        }
    }


}
