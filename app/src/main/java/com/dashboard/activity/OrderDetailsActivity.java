package com.dashboard.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.chefonline.managermobile.R;
import com.dashboard.adapter.OrderDetailsAdapter;
import com.dashboard.datamodel.CustomerDataSerialize;
import com.dashboard.datamodel.OrderDetailsData;
import com.dashboard.presenter.OrderDetailsPresenter;
import com.dashboard.services.OrderDetailsService;
import com.dashboard.singleton.ApplicationVisibleState;
import com.dashboard.view.OrderDetailsView;

public class OrderDetailsActivity extends AppCompatActivity implements OrderDetailsView, View.OnClickListener {
    RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    OrderDetailsPresenter orderDetailsPresenter;
    //Button btnPrint;
    FloatingActionButton floatBtnInfo;
    CustomerDataSerialize serializeCustomerData;
    String orderNo = "";
    boolean addHeader;

    OrderDetailsData orderDetailsData;
    OrderDetailsAdapter myAdapter;
    private long mLastClickTime = 0;
    private final int MY_PERMISSIONS_REQUEST_CALL_PHONE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Order Details");
        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

        floatBtnInfo = (FloatingActionButton) findViewById(R.id.floatBtnInfo);
        floatBtnInfo.setOnClickListener(this);

        recyclerView= (RecyclerView) findViewById(R.id.recycleViewOrderList);
        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        orderDetailsPresenter  = new OrderDetailsPresenter(this, new OrderDetailsService());

        if(savedInstanceState == null) {
            addHeader = true;
            Intent i = getIntent();
            orderNo = i.getStringExtra("order_no");
            serializeCustomerData = (CustomerDataSerialize) i.getSerializableExtra("sampleObject");
            orderDetailsPresenter.onClickGetOrder();
        } else {
            addHeader = false;
            orderNo = savedInstanceState.getString("order_no");
            serializeCustomerData = (CustomerDataSerialize) savedInstanceState.getSerializable("serializable_data");
            orderDetailsData = (OrderDetailsData) savedInstanceState.getSerializable("order_details");
            setOrderData(orderDetailsData);
        }
    }

    @Override
    protected void onPause() {
        ApplicationVisibleState.activityPaused();
        super.onPause();
    }

    @Override
    protected void onResume() {
        ApplicationVisibleState.activityResumed();
        super.onResume();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("order_no", orderNo);
        outState.putSerializable("order_details", orderDetailsData);
        outState.putSerializable("serializable_data", serializeCustomerData);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL_PHONE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (checkSelfPermission(Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            try{
                                Intent callIntent = new Intent(Intent.ACTION_CALL);
                                callIntent.setData(Uri.parse("tel:" + OrderDetailsAdapter.phNumber));
                                startActivity(callIntent);
                            }
                            catch (Exception e){
                                //  showWarningDialog("Unable to deliver the call. "+ e.getMessage());
                            }
                        }
                    }
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
        }
    }


    /*
    private void sendUpdateToList(int position) {
        Log.d("sender", "Broadcasting data position");
        Intent intent = new Intent("on-print-completed");
        intent.putExtra("position", position);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }


    public void printBill() {
        boolean printStatus = UtilityMethod.printBill(getApplicationContext(), orderDetailsData, serializeCustomerData);
        //print status changes 0 to 1 and make printed
        if ("0".equalsIgnoreCase(serializeCustomerData.getPrintStatus())) {
            if (printStatus) {
                //call printer status change api
                new RestaurantDeviceStatusApiCall().callPrintStatusAPI(getApplicationContext(), getOrderNo(), new OnRequestComplete() {
                    @Override
                    public void onRequestComplete(String response) {
                        Log.i("Print Status", "*** " + response);
                        try {
                            DashBoardTodayFragment.dashBoardPresenter.onClickGetOrder(ConstantValues.ORDER_ONLINE_LIST_API_ID, false);
                            sendUpdateToList(getIntent().getExtras().getInt("position"));
                        } catch (Exception e) {
                            Log.i("Order Details Act", "*** " + e.getMessage());
                        }

                        //DashBoardActivity.dashBoardPresenter.on
                    }
                });
            }
        }

        //UtilityMethod.printBillKitchen(getApplicationContext(), orderDetailsData, serializeCustomerData);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_order_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.home) {
            onBackPressed();
            return true;
        } else if (id == R.id.action_print) {
            printBill();

        } else if (id == R.id.action_print_text) {
            printBill();

        }

        return super.onOptionsItemSelected(item);
    }   */

    @Override
    public Context getClassContext() {
        return OrderDetailsActivity.this;
    }

    @Override
    public void setOrderData(OrderDetailsData dishDatas) {
        myAdapter = new OrderDetailsAdapter(dishDatas, this, serializeCustomerData, addHeader);
        recyclerView.setAdapter(myAdapter);
        orderDetailsData = dishDatas;
    }

    @Override
    public void setTotal(String total) {
        //textViewTotal.setText("" + total);
    }

    @Override
    public void setRestaurantName(String name) {

    }

    @Override
    public void setOrderQuantity(int amount) {

    }

    @Override
    public void setPaymentMethod(String method) {

    }

    @Override
    public String getOrderNo() {
        return orderNo;
    }

    @Override
    public ProgressDialog showProgressDialog() {
        ProgressDialog progress = new ProgressDialog(OrderDetailsActivity.this);
        progress.setMessage("Please wait....");
        progress.show();
        return progress;
    }

    @Override
    public void onClick(View v) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000){
            mLastClickTime = SystemClock.elapsedRealtime();
            return;
        }
        else {
            mLastClickTime = SystemClock.elapsedRealtime();
        }
        switch (v.getId()) {
            case R.id.floatBtnInfo:
                Intent intent = new Intent(this, CustomerInfoActivity.class);
                intent.putExtra("sampleObject", serializeCustomerData);
                startActivity(intent);
                break;
        }
    }

    /*public static void Messagebox(Context context,String info)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("title");
        builder.setMessage(info);
        builder.setPositiveButton("yes", null);
        builder.show();
    }*/

}
