package com.dashboard.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chefonline.managermobile.R;
import com.dashboard.adapter.RestaurantListAdapter;
import com.dashboard.customview.CustomToast;
import com.dashboard.datamodel.RestaurantListData;
import com.dashboard.presenter.RestaurantListPresenter;
import com.dashboard.singleton.ApplicationVisibleState;
import com.dashboard.utility.PreferenceUtil;
import com.dashboard.view.RestaurantListView;

import java.util.ArrayList;

/**
 * Created by user on 5/9/2016.
 */
public class RestaurantListActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, RestaurantListView{

   // SwipeRefreshLayout swipeRefreshLayout;
    LinearLayout linearLoader;
    LinearLayout linearRecentHolder;
    RecyclerView recyclerView;
    TextView textViewMessage;
    RelativeLayout relativeContainer;
    RelativeLayout relativeRecent;
    private ProgressDialog progressDialog;
    LinearLayoutManager linearLayoutManager;

    ArrayList<RestaurantListData>restaurantListDatas;
    RestaurantListAdapter adapter;

    RestaurantListPresenter restaurantListPresenter;
    int pagingIndex;
    boolean isRefreshed, moreAvailable;
    PreferenceUtil preferenceUtil;

    int pastVisiblesItems, visibleItemCount, totalItemCount;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant_list);
        preferenceUtil = new PreferenceUtil(this);
        initView();
        setUiClickHandler();
        restaurantListDatas = new ArrayList<>();
        restaurantListPresenter = new RestaurantListPresenter(this);
        isRefreshed = false;
        moreAvailable = true;
        pagingIndex = 0;
        restaurantListPresenter.addRecentRestaurants(relativeRecent, linearRecentHolder);
        restaurantListPresenter.onClickGetRestaurantList(true, "" + pagingIndex, "10", "");
    }

    @Override
    protected void onPause() {
        ApplicationVisibleState.activityPaused();
        super.onPause();
    }

    @Override
    protected void onResume() {
        ApplicationVisibleState.activityResumed();
        super.onResume();
        restaurantListPresenter.addRecentRestaurants(relativeRecent, linearRecentHolder);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_restaurant_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_search) {
            startActivity(new Intent(RestaurantListActivity.this, RestaurantSearchActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }

    /*
    @Override
    public void onBackPressed() {
        showExitDialog("Are you sure want to exit now?", "EXIT", "CANCEL");
    }  */

    private void initView() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(preferenceUtil.getUserName());
        getSupportActionBar().setSubtitle("Super Admin");
        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

        textViewMessage = (TextView) findViewById(R.id.txtMessage);
       // swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        linearLoader = (LinearLayout) findViewById(R.id.linearLoader);
        linearRecentHolder = (LinearLayout) findViewById(R.id.linearRecentHolder);
        relativeContainer = (RelativeLayout) findViewById(R.id.relativeContainer);
        relativeRecent = (RelativeLayout) findViewById(R.id.relativeRecent);
        recyclerView = (RecyclerView) findViewById(R.id.recycleViewRestaurantList);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);

    }

    private void setUiClickHandler() {
       // swipeRefreshLayout.setOnRefreshListener(this);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                    if (!isRefreshed) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            //Do pagination.. i.e. fetch new data
                            doPagination();
                        }
                    }
                }
            }
        });
    }


    public void doPagination() {
        if(moreAvailable) {
            isRefreshed = true;
            pagingIndex++;
            linearLoader.setVisibility(View.VISIBLE);
            restaurantListPresenter.onClickGetRestaurantList(false, "" + pagingIndex, "10", "");
        }
    }


    @Override
    public void onRefresh() {
        isRefreshed = true;
        pagingIndex++;
        restaurantListPresenter.onClickGetRestaurantList(false, "" + pagingIndex, "10", "");
    }

    @Override
    public Context getClassContext() {
        return this;
    }

    @Override
    public void setRestaurantData(ArrayList<RestaurantListData> restaurantListDatasFound) {
        if(isRefreshed) {
          //  swipeRefreshLayout.setRefreshing(false);
            linearLoader.setVisibility(View.GONE);
        }
        int scrollCount = this.restaurantListDatas.size() - 3;
        this.restaurantListDatas.addAll(restaurantListDatasFound);
        adapter = new RestaurantListAdapter(this, this.restaurantListDatas);
        recyclerView.setAdapter(adapter);
        if(restaurantListDatas.size() > 0) {
            showEmptyView(false);
        } else {
            showEmptyView(true);
        }
        if(scrollCount < 0) scrollCount = 0;
        if(isRefreshed) {
            linearLayoutManager.scrollToPosition(scrollCount);
        }
        if(restaurantListDatasFound == null || restaurantListDatasFound.size() < 1) {
            moreAvailable = false;
        }
        isRefreshed = false;
    }

    @Override
    public void openDashboardActivity() {
    }

    @Override
    public void showEmptyView(boolean show) {
        if(show) {
            relativeContainer.setVisibility(View.GONE);
            textViewMessage.setVisibility(View.VISIBLE);
        } else {
            textViewMessage.setVisibility(View.GONE);
            relativeContainer.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showProgressDialog() {
        progressDialog = new ProgressDialog(RestaurantListActivity.this);
        progressDialog.setMessage("Please wait....");
        progressDialog.show();
    }

    @Override
    public void dismissDialog() {
        progressDialog.dismiss();
    }

    @Override
    public void showToast(String message, String subMessage, boolean isButtonOk) {
        new CustomToast(this, message, subMessage, isButtonOk);
    }


    public void showExitDialog(String message, String okButton, String cancelButton) {
        final Dialog dialog = new Dialog(RestaurantListActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.dialog_exit);
        TextView txtViewPopupMessage = (TextView) dialog.findViewById(R.id.txtViewPopupMessage);
        txtViewPopupMessage.setText(message);
        final TextView txtAccept = (TextView) dialog.findViewById(R.id.txtAccept);
        txtAccept.setText(okButton);

        // if button is clicked, close the custom dialog
        txtAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                finish();
            }

        });

        final TextView txtCancel = (TextView) dialog.findViewById(R.id.txtCancel);
        txtCancel.setText(cancelButton);
        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        dialog.show();
    }

}
