package com.dashboard.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintJob;
import android.print.PrintManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chefonline.managermobile.R;
import com.dashboard.broadcastreceiver.ConnectivityChangeReceiver;
import com.dashboard.customview.CustomToast;
import com.dashboard.datamodel.EndOfDayPayment;
import com.dashboard.datamodel.EndOfDayUser;
import com.dashboard.presenter.EndOfDayPresenter;
import com.dashboard.singleton.ApplicationVisibleState;
import com.dashboard.utility.ConstantValues;
import com.dashboard.utility.DateFormatter;
import com.dashboard.utility.PreferenceUtil;
import com.dashboard.utility.UtilityMethod;
import com.dashboard.view.ConnectivityChangeInterface;
import com.dashboard.view.EndOfDayView;

import java.util.ArrayList;

/**
 * Created by user on 1/28/2016.
 */
public class EndOfDayActivity extends AppCompatActivity implements EndOfDayView, ConnectivityChangeInterface{
    String totalAmount = "";
    String totalItem = "";

    TextView txtTotalAmount, txtTotalItem, txtMessage, txtCustomers, txtPayments;
    TextView textViewConnectivity;
    LinearLayout linearUser, linearPayment;
    RelativeLayout relativeHeader;
    RelativeLayout relativeList;
    private ProgressDialog progressDialog;

    ArrayList<EndOfDayPayment> endDayPayments = new ArrayList<>();
    ArrayList<EndOfDayUser> endOfDayUsers = new ArrayList<>();

    EndOfDayPresenter endOfDayPresenter;
    PreferenceUtil preferenceUtil;

    String printingText = "";
    WebView mWebView;
    ConnectivityChangeReceiver connectivityChangeReceiver;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_end_of_day);
        initView();
        preferenceUtil = new PreferenceUtil(this);
        endOfDayPresenter = new EndOfDayPresenter(this);

        connectivityChangeReceiver = new ConnectivityChangeReceiver(this);
        IntentFilter connectivityChangeFilter = new IntentFilter(ConstantValues.ACTION_CONNECTIVITY_CHANGE);
        registerReceiver(connectivityChangeReceiver, connectivityChangeFilter);

        if(savedInstanceState == null) {
            endOfDayPresenter.loadListData();
        } else {
            txtTotalItem.setText(savedInstanceState.getString("total_item"));
            txtTotalAmount.setText(savedInstanceState.getString("total_amount"));
            endDayPayments = (ArrayList<EndOfDayPayment>)savedInstanceState.getSerializable("payments");
            endOfDayUsers = (ArrayList<EndOfDayUser>)savedInstanceState.getSerializable("users");
            boolean isEmpty = true;
            if(endOfDayUsers != null && endOfDayUsers.size() > 0) {
                loadUserData(endOfDayUsers);
                isEmpty = false;
            }
            if(endDayPayments != null && endDayPayments.size() > 0) {
                loadPaymentData(endDayPayments);
                isEmpty = false;
            }
            if(isEmpty) {
                setEmptyView();
            }
        }
    }

    @Override
    protected void onPause() {
        ApplicationVisibleState.activityPaused();
        super.onPause();
    }

    @Override
    protected void onResume() {
        ApplicationVisibleState.activityResumed();
        super.onResume();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("total_amount", txtTotalAmount.getText().toString());
        outState.putString("total_item", txtTotalItem.getText().toString());
        outState.putSerializable("payments", endDayPayments);
        outState.putSerializable("users", endOfDayUsers);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_order_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.home) {
            onBackPressed();
            return true;
        } else if (id == R.id.action_print) {
            arrangePrintData();
            printEndOfDayData();
        } else if (id == R.id.action_print_text) {
            arrangePrintData();
            printEndOfDayData();
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onDestroy() {
        if(connectivityChangeReceiver != null) {
            unregisterReceiver(connectivityChangeReceiver);
        }
        super.onDestroy();
    }

    private void initView() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("End of Day");
        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

        txtTotalAmount = (TextView) findViewById(R.id.txtTotalAmount);
        txtTotalItem = (TextView) findViewById(R.id.txtTotalItem);
        txtMessage = (TextView) findViewById(R.id.txtMessage);
        textViewConnectivity = (TextView) findViewById(R.id.textViewConnectivity);
        txtCustomers = (TextView) findViewById(R.id.textViewCustomers);
        txtPayments = (TextView) findViewById(R.id.textViewPayments);
        linearUser = (LinearLayout) findViewById(R.id.linearUser);
        linearPayment = (LinearLayout) findViewById(R.id.linearPayment);
        relativeHeader = (RelativeLayout) findViewById(R.id.relativeHeader);
        relativeList = (RelativeLayout) findViewById(R.id.relativeList);
    }


    @Override
    public void loadPaymentData(ArrayList<EndOfDayPayment> endOfDayPayments) {
        this.endDayPayments = endOfDayPayments;
        loadPaymentsRow();
        if(endOfDayPayments.size() > 0) {
            txtPayments.setVisibility(View.VISIBLE);
        }
        else {
            txtPayments.setVisibility(View.GONE);
        }
        if(this.endDayPayments.size() > 0 || endOfDayUsers.size() > 0) {
            relativeHeader.setVisibility(View.VISIBLE);
            relativeList.setVisibility(View.VISIBLE);
        } else {
            relativeHeader.setVisibility(View.GONE);
            relativeList.setVisibility(View.GONE);
        }

    }

    @Override
    public void loadUserData(ArrayList<EndOfDayUser> endOfDayUsers) {
        this.endOfDayUsers = endOfDayUsers;
        loadUsersRow();
        if(endOfDayUsers.size() > 0) {
            txtCustomers.setVisibility(View.VISIBLE);
        }
        else {
            txtCustomers.setVisibility(View.GONE);
        }
    }


    @Override
    public void setEmptyView() {
        txtMessage.setVisibility(View.VISIBLE);
        relativeHeader.setVisibility(View.GONE);
        relativeList.setVisibility(View.GONE);
    }

    @Override
    public Context getClassContext() {
        return EndOfDayActivity.this;
    }

    @Override
    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
        txtTotalAmount.setText("£" + totalAmount);
    }

    @Override
    public void setTotalItem(String totalItem) {
        this.totalItem = totalItem;
        txtTotalItem.setText(totalItem);
    }

    @Override
    public void showProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait....");
        progressDialog.show();
    }

    @Override
    public void dismissProgressDialog() {
        progressDialog.dismiss();
    }

    @Override
    public void showToast(String message, String subMessage, boolean isButtonOk) {
        new CustomToast(this, message, subMessage, isButtonOk);
    }

    @Override
    public String getRestaurantId() {
        return preferenceUtil.getResID();
    }

    @Override
    public String getCurrentDateTime() {
        return DateFormatter.getCurrentDateTime();
    }

    @Override
    public void onConnectivityChanged() {
        if(UtilityMethod.isConnectedToInternet(this)){
            textViewConnectivity.setBackgroundColor(ContextCompat.getColor(this, R.color.green));
            textViewConnectivity.setText("Connected");
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    textViewConnectivity.setVisibility(View.GONE);
                }
            }, 1000);
        } else {
            textViewConnectivity.setBackgroundColor(ContextCompat.getColor(this, R.color.amber_dark));
            textViewConnectivity.setText("Waiting for network");
            textViewConnectivity.setVisibility(View.VISIBLE);
        }
    }

    public static class ListUtils {
        public static void setDynamicHeight(ListView mListView) {
            ListAdapter mListAdapter = mListView.getAdapter();
            if (mListAdapter == null) {
                // when adapter is null
                return;
            }
            int height = 0;
            int desiredWidth = View.MeasureSpec.makeMeasureSpec(mListView.getWidth(), View.MeasureSpec.UNSPECIFIED);
            for (int i = 0; i < mListAdapter.getCount(); i++) {
                View listItem = mListAdapter.getView(i, null, mListView);
                listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                height += listItem.getMeasuredHeight();
            }
            ViewGroup.LayoutParams params = mListView.getLayoutParams();
            params.height = height + (mListView.getDividerHeight() * (mListAdapter.getCount() - 1));
            mListView.setLayoutParams(params);
            mListView.requestLayout();
        }
    }


    private void loadPaymentsRow() {
        linearPayment.removeAllViews();
        int size = endDayPayments.size();
        TextView[] txtGrandTotals = new TextView[size];
        TextView[] txtPaymentTypes = new TextView[size];
        View[] views = new View[size];
        LayoutInflater inflater = LayoutInflater.from(this);
        for (int i = 0; i < size; i++) {
            views[i] = inflater.inflate(R.layout.row_end_of_day_payment, null);
            txtGrandTotals[i] = (TextView) views[i].findViewById(R.id.txtGrandTotal);
            txtPaymentTypes[i] = (TextView) views[i].findViewById(R.id.txtPaymentType);
            txtPaymentTypes[i].setText(endDayPayments.get(i).getPaymentMethod());
            txtGrandTotals[i].setText("£" + UtilityMethod.priceFormatter(endDayPayments.get(i).getGrandTotal()));
            linearPayment.addView(views[i]);
        }
    }


    private void loadUsersRow() {
        linearUser.removeAllViews();
        int size = endOfDayUsers.size();
        TextView[] txtUserNames = new TextView[size];
        TextView[] txtOrderedDishs = new TextView[size];
        TextView[] textViewSerialNos = new TextView[size];
        View[] views = new View[size];
        LayoutInflater inflater = LayoutInflater.from(this);
        for (int i = 0; i < size; i++) {
            views[i] = inflater.inflate(R.layout.row_end_of_day_user, null);
            textViewSerialNos[i] = (TextView) views[i].findViewById(R.id.textViewSerialNo);
            txtUserNames[i] = (TextView) views[i].findViewById(R.id.txtUserName);
            txtOrderedDishs[i] = (TextView) views[i].findViewById(R.id.txtOrderedDish);
            textViewSerialNos[i].setText(String.valueOf(i + 1));
            txtUserNames[i].setText(endOfDayUsers.get(i).getUserName());
            txtOrderedDishs[i].setText("£" + endOfDayUsers.get(i).getOrderPrice());
            linearUser.addView(views[i]);
        }
    }


    private void arrangePrintData() {
        printingText = "End of Day Report<br>==============================<br>";
        printingText += "Total Amount:  £" + totalAmount + "<br>";
        printingText += "Total Order:  " + totalItem + "<br>";
        if(endOfDayUsers != null && endOfDayUsers.size() > 0) {
            printingText += "==============================<br>";
            printingText += "Customer(s)<br><br>";
            for( int i =0; i <endOfDayUsers.size(); i++) {
                printingText += (i+1) + "  " + endOfDayUsers.get(i).getUserName() + "   £" + endOfDayUsers.get(i).getOrderPrice() + "<br>";
            }
        }
        if(endDayPayments != null && endDayPayments.size() > 0) {
            printingText += "<br><br>Payment(s)<br><br>";
            for (int i =0; i < endDayPayments.size(); i++) {
                printingText += endDayPayments.get(i).getPaymentMethod() + "   £" + UtilityMethod.priceFormatter(endDayPayments.get(i).getGrandTotal()) + "<br>";
            }
        }
    }


    private void createWebPrintJob(WebView webView) {

        // Get a PrintManager instance
        PrintManager printManager = (PrintManager) getSystemService(Context.PRINT_SERVICE);

        // Get a print adapter instance
        PrintDocumentAdapter printAdapter = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            printAdapter = webView.createPrintDocumentAdapter();
            // Create a print job with name and adapter instance
            String jobName = getString(R.string.app_name) + " Document";
            PrintJob printJob = printManager.print(jobName, printAdapter, new PrintAttributes.Builder().build());

            // Save the job object for later status checking
            // mPrintJobs.add(printJob);
        } else {
            new CustomToast(this, "This feature is not supported in your device", "", false);
        }

    }


    private void printEndOfDayData() {
        WebView webView = new WebView(this);
        webView.setWebViewClient(new WebViewClient() {

            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                // Log.i(TAG, "page finished loading " + url);
                createWebPrintJob(view);
                mWebView = null;
            }
        });

        // Generate an HTML document on the fly:
        String htmlDocument = "<html><body>" + printingText + "</body></html>";
        webView.loadDataWithBaseURL(null, htmlDocument, "text/HTML", "UTF-8", null);

        // Keep a reference to WebView object until you pass the PrintDocumentAdapter
        // to the PrintManager
        mWebView = webView;
    }



}
