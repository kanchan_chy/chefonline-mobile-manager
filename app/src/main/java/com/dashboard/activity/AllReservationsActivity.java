package com.dashboard.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chefonline.managermobile.R;
import com.dashboard.adapter.ALLReservationAdapter;
import com.dashboard.adapter.ReservationAdapter;
import com.dashboard.broadcastreceiver.ConnectivityChangeReceiver;
import com.dashboard.datamodel.OrderData;
import com.dashboard.datamodel.PaymentData;
import com.dashboard.datamodel.ReservationDataUnconfirmed;
import com.dashboard.datamodel.ScheduleData;
import com.dashboard.presenter.DashBoardPresenter;
import com.dashboard.services.DashBoardService;
import com.dashboard.utility.ConstantValues;
import com.dashboard.utility.DateFormatter;
import com.dashboard.utility.PreferenceUtil;
import com.dashboard.utility.UtilityMethod;
import com.dashboard.view.ConnectivityChangeInterface;
import com.dashboard.view.DashBoardView;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnMenuTabSelectedListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class AllReservationsActivity extends AppCompatActivity implements DashBoardView, View.OnClickListener, ConnectivityChangeInterface{
    public static String TAG = "AllReservationsActivity";
    public static String fromDate = "";
    public static String toDate = "";
    TextView textViewFilter;
    TextView textViewTotalReservation;
    TextView textViewReservationLevel;
    TextView textViewMessage;
    TextView textViewConnectivity;
    ImageButton btnCustomRange;
    RelativeLayout relativeContainer;

    RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    public static DashBoardPresenter dashBoardPresenter;
    PreferenceUtil preferenceUtil;

    ALLReservationAdapter adapter;
    ArrayList<ReservationDataUnconfirmed> reservationDatas;

    private long mLastClickTime = 0;
    CoordinatorLayout coordinatorLayout;
    BottomBar bottomBar;
    FloatingActionButton floatBtnFilter;
    private  int selectedPosition = 4;
    boolean loadData = true;
    private ImageView prevSelectedImgView;
    Toolbar toolbar;

    ConnectivityChangeReceiver connectivityChangeReceiver;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_reservations);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.relative_main_reservation);
        bottomBar = BottomBar.attach(this, savedInstanceState);
        bottomBar.setItemsFromMenu(R.menu.menu_bottom_tab, new OnMenuTabSelectedListener() {
            @Override
            public void onMenuItemSelected(int itemId) {
                switch (itemId) {
                    case R.id.action_search:
                        dashBoardPresenter.onClickGetReservation(ConstantValues.ORDER_RESERVATION_LIST_API_ID, "1", true);
                        break;
                    case R.id.action_calendar:
                        if(loadData) {
                            dashBoardPresenter.onClickGetReservation(ConstantValues.ORDER_RESERVATION_LIST_API_ID, "3", true);
                        } else {
                            loadData = true;
                        }
                        break;

                }
            }
        });

        bottomBar.setActiveTabColor(ContextCompat.getColor(this, R.color.brand_red));
        //bottomBar.getBar().setBackgroundColor(ContextCompat.getColor(this, R.color.brand_red));

        initView();
        dashBoardPresenter = new DashBoardPresenter(this, new DashBoardService());
        preferenceUtil = new PreferenceUtil(this);

        connectivityChangeReceiver = new ConnectivityChangeReceiver(this);
        IntentFilter connectivityChangeFilter = new IntentFilter(ConstantValues.ACTION_CONNECTIVITY_CHANGE);
        registerReceiver(connectivityChangeReceiver, connectivityChangeFilter);

        if(savedInstanceState == null) {
            loadData = true;
            ArrayList<ReservationDataUnconfirmed> mReservationDataConfirmed = new ArrayList<>();
            setReservationData(mReservationDataConfirmed);
            selectedPosition = getIntent().getExtras().getInt("selected_pos");
            filterOptionSelected();
        } else {
            selectedPosition = savedInstanceState.getInt("selected_pos");
            if(selectedPosition == 1) {
                loadData = false;
            } else {
                loadData = true;
            }


            reservationDatas = (ArrayList<ReservationDataUnconfirmed>)savedInstanceState.getSerializable("reservationDatas");
            setReservationData(reservationDatas);
            textViewFilter.setText(savedInstanceState.getString("filter_text"));
            toolbar.setSubtitle(savedInstanceState.getString("sub_title"));
            setTotalOrderReservation(savedInstanceState.getString("total_reservation_text"));
            if(reservationDatas.size() > 0) {
                showEmptyView(false);
            } else {
                showEmptyView(true);
            }
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("current_tab_pos", bottomBar.getCurrentTabPosition());
        outState.putInt("selected_pos", selectedPosition);
        outState.putString("filter_text", textViewFilter.getText().toString());
        String subTitle = "";
        if(toolbar.getSubtitle() != null) {
            subTitle = toolbar.getSubtitle().toString();
        }
        outState.putString("sub_title", subTitle);
        outState.putString("total_reservation_text", textViewTotalReservation.getText().toString());
        outState.putSerializable("reservationDatas", reservationDatas);
    }

    @Override
    protected void onDestroy() {
        if(connectivityChangeReceiver != null) {
            unregisterReceiver(connectivityChangeReceiver);
        }
        super.onDestroy();
    }

    private void initView() {
        recyclerView= (RecyclerView) findViewById(R.id.recycleViewReservationList);
        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(AllReservationsActivity.this);
        recyclerView.setLayoutManager(mLayoutManager);

        textViewFilter= (TextView) findViewById(R.id.textViewFilter);
        textViewTotalReservation = (TextView) findViewById(R.id.textViewTotalReservation);
        textViewReservationLevel = (TextView) findViewById(R.id.textViewReservationLevel);
        textViewMessage = (TextView) findViewById(R.id.textViewMessage);
        textViewConnectivity = (TextView) findViewById(R.id.textViewConnectivity);
        textViewMessage.setVisibility(View.GONE);
        btnCustomRange = (ImageButton) findViewById(R.id.btnCustomRange);
        relativeContainer = (RelativeLayout) findViewById(R.id.relativeLayoutInfo);
        floatBtnFilter = (FloatingActionButton) findViewById(R.id.floatBtnFilter);
        floatBtnFilter.setOnClickListener(this);
        btnCustomRange.setOnClickListener(this);

        toolbar =(Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Reservations");

        floatBtnFilter.setTag(floatBtnFilter.getVisibility());
        floatBtnFilter.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int newVis = floatBtnFilter.getVisibility();
                if ((int) floatBtnFilter.getTag() != newVis) {
                    floatBtnFilter.setTag(floatBtnFilter.getVisibility());
                    //visibility has changed
                    if (floatBtnFilter.getVisibility() == View.VISIBLE) {
                        bottomBar.show();
                    } else {
                        bottomBar.hide();
                    }
                }
            }
        });


    }


    private String getDateToday() {
        Calendar calendar = Calendar.getInstance();
        Date today = calendar.getTime();

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

        String todayAsString = dateFormat.format(today);
        try {
            return DateFormatter.doFormat(todayAsString, DateFormatter.DD_MM_YYYY, DateFormatter.MMM_dd_yyyy);

        } catch (Exception e) {

        }

       return "";
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 100) {
            if(resultCode == RESULT_OK){
                String confirmStatus = "1";
                if(bottomBar.getCurrentTabPosition() == 0) {
                    confirmStatus = "1";
                } else if(bottomBar.getCurrentTabPosition() == 1) {
                    confirmStatus = "3";
                } else if(bottomBar.getCurrentTabPosition() == 2) {
                    confirmStatus = "";
                }
                if("".equalsIgnoreCase(confirmStatus)) {
                    Snackbar.make(coordinatorLayout, "Declined list is not ready yet", Snackbar.LENGTH_LONG).show();
                    return;
                }
                try {
                    fromDate = data.getStringExtra("fromDate");
                    toDate = data.getStringExtra("toDate");
                    String showingText1 = "" + DateFormatter.doFormat(fromDate, DateFormatter.DD_MM_YYYY, DateFormatter.MMM_dd_yyyy);
                    String showingText2 = "" + DateFormatter.doFormat(toDate, DateFormatter.DD_MM_YYYY, DateFormatter.MMM_dd_yyyy);
                    textViewFilter.setText(showingText1 + " - " + showingText2);
                    toolbar.setSubtitle(showingText1 + " - " + showingText2);
                    dashBoardPresenter.onClickGetReservation(ConstantValues.ORDER_RESERVATION_LIST_API_ID, confirmStatus, true);
                } catch (Exception e) {
                    Log.i(TAG, e.getMessage());
                }

            }

            if (resultCode == RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_dash_board, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            /*case R.id.action_calendar:
                Intent intent = new Intent(DashBoardActivity.this, CustomRangeActivity.class);
                startActivityForResult(intent, 100);
                return true;*/
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_settings:
                //openSettings();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

        //return super.onOptionsItemSelected(item);
    }

    @Override
    public String getRestaurantId() {
        return preferenceUtil.getResID();
    }

    @Override
    public String getStartDate() {
        return fromDate;
    }

    @Override
    public String getEndDate() {
        return toDate;
    }

    @Override
    public ProgressDialog showProgressDialog() {
        ProgressDialog progress = new ProgressDialog(AllReservationsActivity.this);
        progress.setMessage("Please wait....");
        progress.show();
        return progress;
    }

    @Override
    public void checkConnectivity() {

    }

    @Override
    public void showInternetDialog(String message, String okButton, String cancelButton) {

    }

    @Override
    public Context getClassContext() {
        return AllReservationsActivity.this;
    }

    @Override
    public void setScheduleData(ArrayList<ScheduleData> scheduleData) {

    }

    @Override
    public void setOrderData(ArrayList<OrderData> orderDatas) {

    }

    @Override
    public void setReservationData(ArrayList<ReservationDataUnconfirmed> reservationDataUnconfirmed) {
        this.reservationDatas = reservationDataUnconfirmed;
        setTotalOrderReservation("" + reservationDatas.size());
        boolean fromUnconfirmed;
        if(bottomBar.getCurrentTabPosition() == 1) {
            fromUnconfirmed = true;
        } else {
            fromUnconfirmed = false;
        }
        adapter = new ALLReservationAdapter(AllReservationsActivity.this, preferenceUtil.getResID(), preferenceUtil.getRestaurantName(), reservationDataUnconfirmed, fromUnconfirmed);
        recyclerView.setAdapter(adapter);

        if(reservationDatas.size() > 0) {
            relativeContainer.setVisibility(View.VISIBLE);
        } else {
            relativeContainer.setVisibility(View.GONE);
        }
    }

    @Override
    public void setPaymentData(ArrayList<PaymentData> paymentData) {

    }

    @Override
    public void setCash(String text) {

    }

    @Override
    public void setPayPal(String text) {

    }

    @Override
    public void setTotal(String text) {

    }

    @Override
    public void setTotalOrder(String text) {

    }

    @Override
    public void setDeliveryOrder(String text) {

    }

    @Override
    public void setCollectionOrder(String text) {

    }

    @Override
    public void setTotalOrderReservation(String text) {
        String level = "Confirmed Reservations";
        if (bottomBar.getCurrentTabPosition() == 0) {
            level = "Confirmed Reservations";
        } else if (bottomBar.getCurrentTabPosition() == 1) {
            level = "Unconfirmed Reservations";
        } else if (bottomBar.getCurrentTabPosition() == 2) {
            level = "Declined Reservations";
        }
        textViewReservationLevel.setText(level);
        textViewTotalReservation.setText(text);
    }

    @Override
    public void showNotPrintedOrder() {

    }

    @Override
    public void showEmptyView(boolean isVisible) {
        if (isVisible) {
            reservationDatas = new ArrayList<>();
            recyclerView.setVisibility(View.GONE);
            relativeContainer.setVisibility(View.GONE);
            textViewMessage.setVisibility(View.VISIBLE);
        } else {
            textViewMessage.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            relativeContainer.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onResume() {
        try {
            adapter.notifyDataSetChanged();
        } catch (Exception e) {

        }
        super.onResume();
    }

    private void filterOptionSelected() {
        try {

            String confirmStatus = "1";
            if (bottomBar.getCurrentTabPosition() == 0) {
                confirmStatus = "1";
            } else if (bottomBar.getCurrentTabPosition() == 1) {
                confirmStatus = "3";
            }

            if (selectedPosition == 0) {
                fromDate = DateFormatter.getCurrentDate();
                toDate = DateFormatter.getCurrentDate();
                String showingText1 = "" + DateFormatter.doFormat(fromDate, DateFormatter.DD_MM_YYYY, DateFormatter.MMM_dd_yyyy);
                String showingText2 = "" + DateFormatter.doFormat(toDate, DateFormatter.DD_MM_YYYY, DateFormatter.MMM_dd_yyyy);
                textViewFilter.setText(showingText1 + " - " + showingText2);
                toolbar.setSubtitle(showingText1 + " - " + showingText2);
                dashBoardPresenter.onClickGetReservation(ConstantValues.ORDER_RESERVATION_LIST_API_ID, confirmStatus, true);
            }

            if (selectedPosition == 1) {
                fromDate = DateFormatter.getCustomDate(-1);
                toDate = DateFormatter.getCustomDate(-1);
                String showingText1 = "" + DateFormatter.doFormat(fromDate, DateFormatter.DD_MM_YYYY, DateFormatter.MMM_dd_yyyy);
                String showingText2 = "" + DateFormatter.doFormat(toDate, DateFormatter.DD_MM_YYYY, DateFormatter.MMM_dd_yyyy);
                textViewFilter.setText(showingText1 + " - " + showingText2);
                toolbar.setSubtitle(showingText1 + " - " + showingText2);
                dashBoardPresenter.onClickGetReservation(ConstantValues.ORDER_RESERVATION_LIST_API_ID, confirmStatus, true);
            }

            if (selectedPosition == 2) {
                fromDate = DateFormatter.getCustomDate(-6);
                toDate = DateFormatter.getCurrentDate();
                String showingText1 = "" + DateFormatter.doFormat(fromDate, DateFormatter.DD_MM_YYYY, DateFormatter.MMM_dd_yyyy);
                String showingText2 = "" + DateFormatter.doFormat(toDate, DateFormatter.DD_MM_YYYY, DateFormatter.MMM_dd_yyyy);
                textViewFilter.setText(showingText1 + " - " + showingText2);
                toolbar.setSubtitle(showingText1 + " - " + showingText2);
                dashBoardPresenter.onClickGetReservation(ConstantValues.ORDER_RESERVATION_LIST_API_ID, confirmStatus, true);
            }

            if (selectedPosition == 3) {
                fromDate = DateFormatter.getCustomDate(-29);
                toDate = DateFormatter.getCurrentDate();
                String showingText1 = "" + DateFormatter.doFormat(fromDate, DateFormatter.DD_MM_YYYY, DateFormatter.MMM_dd_yyyy);
                String showingText2 = "" + DateFormatter.doFormat(toDate, DateFormatter.DD_MM_YYYY, DateFormatter.MMM_dd_yyyy);
                textViewFilter.setText(showingText1 + " - " + showingText2);
                toolbar.setSubtitle(showingText1 + " - " + showingText2);
                dashBoardPresenter.onClickGetReservation(ConstantValues.ORDER_RESERVATION_LIST_API_ID, confirmStatus, true);
            }

            if (selectedPosition == 4) {
                int day = DateFormatter.getDayofMonth();
                fromDate = DateFormatter.getCustomDate(-(day - 1));
                toDate = DateFormatter.getCustomDate(DateFormatter.getTotalDaysOfMonth() - day);
                String showingText1 = "" + DateFormatter.doFormat(fromDate, DateFormatter.DD_MM_YYYY, DateFormatter.MMM_dd_yyyy);
                String showingText2 = "" + DateFormatter.doFormat(toDate, DateFormatter.DD_MM_YYYY, DateFormatter.MMM_dd_yyyy);
                textViewFilter.setText(showingText1 + " - " + showingText2);
                toolbar.setSubtitle(showingText1 + " - " + showingText2);
                dashBoardPresenter.onClickGetReservation(ConstantValues.ORDER_RESERVATION_LIST_API_ID, confirmStatus, true);
            }

            if (selectedPosition == 5) {
                int day = DateFormatter.getDayofMonth();
                fromDate = DateFormatter.getLastMonthFirstDate();
                toDate = DateFormatter.getLastMonthLastDate();
                String showingText1 = "" + DateFormatter.doFormat(fromDate, DateFormatter.DD_MM_YYYY, DateFormatter.MMM_dd_yyyy);
                String showingText2 = "" + DateFormatter.doFormat(toDate, DateFormatter.DD_MM_YYYY, DateFormatter.MMM_dd_yyyy);
                textViewFilter.setText(showingText1 + " - " + showingText2);
                toolbar.setSubtitle(showingText1 + " - " + showingText2);
                dashBoardPresenter.onClickGetReservation(ConstantValues.ORDER_RESERVATION_LIST_API_ID, confirmStatus, true);
            }

            if (selectedPosition == 6) {
                Intent intent = new Intent(AllReservationsActivity.this, CustomRangeActivity.class);
                startActivityForResult(intent, 100);
            }
        } catch (Exception e) {

        }
    }


    View.OnClickListener filterClickListener(final int selectedPos, final ImageView imgViewSelected) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedPosition = selectedPos;
                prevSelectedImgView.setVisibility(View.INVISIBLE);
                imgViewSelected.setVisibility(View.VISIBLE);
                prevSelectedImgView = imgViewSelected;
            }
        };
    }


    private void showFilterOptionDialog() {
        final Dialog dialog = new Dialog(AllReservationsActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.dialog_filter);
        final TextView txtAccept = (TextView) dialog.findViewById(R.id.txtAccept);
        // if button is clicked, close the custom dialog
        txtAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    mLastClickTime = SystemClock.elapsedRealtime();
                    return;
                } else {
                    mLastClickTime = SystemClock.elapsedRealtime();
                }
                dialog.dismiss();
                filterOptionSelected();
            }

        });

        final TextView txtCancel = (TextView) dialog.findViewById(R.id.txtCancel);
        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        RelativeLayout[] relativeOptions = new RelativeLayout[8];
        ImageView[] imgOptions = new ImageView[8];

        int[] relativeIds = {R.id.relativeToday, R.id.relativeYesterday, R.id.relativeLast7, R.id.relativeLast30, R.id.relativeThisMonth, R.id.relativeLastMonth, R.id.relativeCustomRange};
        int[] imgIds = {R.id.imgViewToday, R.id.imgViewYesterday, R.id.imgViewLast7, R.id.imgViewLast30, R.id.imgViewThisMonth, R.id.imgViewLastMonth, R.id.imgViewCustomRange};

        for(int i = 0; i < relativeIds.length; i++) {
            relativeOptions[i] = (RelativeLayout) dialog.findViewById(relativeIds[i]);
            imgOptions[i] = (ImageView) dialog.findViewById(imgIds[i]);
            relativeOptions[i].setOnClickListener(filterClickListener(i, imgOptions[i]));
        }

        prevSelectedImgView = imgOptions[selectedPosition];
        imgOptions[selectedPosition].setVisibility(View.VISIBLE);

        dialog.show();
    }


    @Override
    public void onClick(View v) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000){
            mLastClickTime = SystemClock.elapsedRealtime();
            return;
        }
        else {
            mLastClickTime = SystemClock.elapsedRealtime();
        }
        switch (v.getId()) {
            case R.id.btnCustomRange:
                Intent intent = new Intent(AllReservationsActivity.this, CustomRangeActivity.class);
                startActivityForResult(intent, 100);
                break;
            case R.id.floatBtnFilter:
                showFilterOptionDialog();

        }
    }

    @Override
    public void onConnectivityChanged() {
        if(UtilityMethod.isConnectedToInternet(this)){
            textViewConnectivity.setBackgroundColor(ContextCompat.getColor(this, R.color.green));
            textViewConnectivity.setText("Connected");
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    textViewConnectivity.setVisibility(View.GONE);
                }
            }, 1000);
        } else {
            textViewConnectivity.setBackgroundColor(ContextCompat.getColor(this, R.color.amber_dark));
            textViewConnectivity.setText("Waiting for network");
            textViewConnectivity.setVisibility(View.VISIBLE);
        }
    }
}
