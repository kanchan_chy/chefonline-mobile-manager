package com.dashboard.activity;


import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.chefonline.managermobile.R;
import com.dashboard.customview.CustomToast;
import com.dashboard.customview.MsmEditText;
import com.dashboard.singleton.ApplicationVisibleState;
import com.dashboard.utility.ConstantValues;
import com.dashboard.utility.PreferenceUtil;
import com.dashboard.utility.UtilityMethod;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ResetPasswordActivity extends AppCompatActivity implements View.OnClickListener {
    private static String TAG = "ResetPasswordActivity";
    private EditText editTextEmail;
    private MsmEditText editTextPrevPassword;
    private MsmEditText editTextNewPassword;
    private MsmEditText editTextNewPasswordConfirm;
    private ImageButton imgBtnBack;
    private Button btnSend;
    PreferenceUtil preferenceUtil;
    private long mLastClickTime = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        initView();
        setUiListener();
    }

    @Override
    protected void onPause() {
        ApplicationVisibleState.activityPaused();
        super.onPause();
    }

    @Override
    protected void onResume() {
        ApplicationVisibleState.activityResumed();
        super.onResume();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        if(savedInstanceState != null) {
            editTextPrevPassword.setText(savedInstanceState.getString("prev_pass"));
            editTextNewPassword.setText(savedInstanceState.getString("new_pass"));
            editTextNewPasswordConfirm.setText(savedInstanceState.getString("confirm_pass"));
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString("prev_pass", editTextPrevPassword.getText().toString());
        outState.putString("new_pass", editTextNewPassword.getText().toString());
        outState.putString("confirm_pass", editTextNewPasswordConfirm.getText().toString());
    }


    private void setUiListener() {
        btnSend.setOnClickListener(this);
        imgBtnBack.setOnClickListener(this);
    }

    private void initView() {
        preferenceUtil = new PreferenceUtil(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Reset Password");
        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

        editTextEmail = (EditText) findViewById(R.id.editTextEmail);
        editTextPrevPassword = (MsmEditText) findViewById(R.id.editTextPrevPassword);
        editTextNewPassword = (MsmEditText) findViewById(R.id.editTextNewPassword);
        editTextNewPasswordConfirm = (MsmEditText) findViewById(R.id.editTextNewPasswordConfirm);
        imgBtnBack = (ImageButton) findViewById(R.id.imgBtnBack);
        btnSend = (Button) findViewById(R.id.btnSend);

        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
        editTextEmail.setTypeface(tf);

        if (isLoggedIn()) {
            Log.e("email", preferenceUtil.getUserEmail());
            editTextEmail.setText(preferenceUtil.getUserEmail());
        }

        editTextEmail.setEnabled(false);

    }

    public boolean isLoggedIn() {
        return preferenceUtil.getLogInStatus() == 1;
    }

    ProgressDialog progress;
    private void callResetPassword() {
        RequestQueue queue = Volley.newRequestQueue(ResetPasswordActivity.this);
        StringRequest myReq = new StringRequest(Request.Method.POST, ConstantValues.BASE_API_URL, createMyReqSuccessListener(), createMyReqErrorListener()) {
            protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("funId", "10");
                params.put("email", editTextEmail.getText().toString().trim());
                params.put("previouspassword", editTextPrevPassword.getText().toString().trim());
                params.put("newpassword", editTextNewPassword.getText().toString().trim());

                return params;
            }
        };

        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        myReq.setRetryPolicy(policy);
        queue.add(myReq);
        progress = new ProgressDialog(ResetPasswordActivity.this);
        progress.setMessage("Please wait....");
        progress.show();
    }

    private Response.ErrorListener createMyReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.dismiss();
                String x = error.getMessage();
                new CustomToast(ResetPasswordActivity.this, "Something went wrong, please try again.", "", false);
            }
        };
    }

    private Response.Listener<String> createMyReqSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progress.dismiss();
                Log.i(TAG, "****" + response);
                try {
                    JSONObject jsonObject = new org.json.JSONObject(response);
                    if ("Success".equalsIgnoreCase(jsonObject.getString("status"))) {
                        new CustomToast(ResetPasswordActivity.this, "Your password reset successfully.", "", true);
                        finish();
                    } else {
                        new CustomToast(ResetPasswordActivity.this, jsonObject.getString("msg"), "", true);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    new CustomToast(ResetPasswordActivity.this, "Something went wrong, please try again.", "", true);
                }

            }

        };
    }

    private boolean inputValidation() {

        if (!UtilityMethod.isEmailValid(editTextEmail.getText().toString().trim())
                || editTextEmail.getText().toString().trim().equalsIgnoreCase("")) {
            new CustomToast(this, "Please enter valid email.", "", false);
            return false;
        }

        if (editTextPrevPassword.getText().toString().trim().equalsIgnoreCase("")) {
            editTextPrevPassword.setErrorTextVisible(true);
            editTextPrevPassword.setErrorMsg("Please enter valid password.");
            editTextPrevPassword.setTypingFocus();
            return false;
        }

        if (editTextNewPassword.getText().toString().trim().equalsIgnoreCase("")) {
            editTextNewPassword.setErrorTextVisible(true);
            editTextNewPassword.setErrorMsg("Please enter valid password.");
            editTextNewPassword.setTypingFocus();
            return false;
        }

        if (editTextNewPasswordConfirm.getText().trim().equalsIgnoreCase("")) {
            editTextNewPasswordConfirm.setErrorTextVisible(true);
            editTextNewPasswordConfirm.setErrorMsg("Your confirm password is required.");
            editTextNewPasswordConfirm.setTypingFocus();
            return false;
        }

        if (!editTextNewPasswordConfirm.getText().trim().equalsIgnoreCase(editTextNewPassword.getText().toString().trim())) {
            editTextNewPasswordConfirm.setErrorTextVisible(true);
            editTextNewPasswordConfirm.setErrorMsg("Password mismatch.");
            editTextNewPasswordConfirm.setTypingFocus();
            return false;
        }

        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnSend:
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000){
                    mLastClickTime = SystemClock.elapsedRealtime();
                    return;
                }
                else {
                    mLastClickTime = SystemClock.elapsedRealtime();
                }
                if (inputValidation()) {
                    callResetPassword();
                }

                break;
            case R.id.imgBtnBack:
                onBackPressed();
                break;

        }
    }
}

