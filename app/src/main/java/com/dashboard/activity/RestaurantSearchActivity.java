package com.dashboard.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chefonline.managermobile.R;
import com.dashboard.adapter.RestaurantListAdapter;
import com.dashboard.customview.CustomToast;
import com.dashboard.datamodel.RestaurantListData;
import com.dashboard.presenter.RestaurantListPresenter;
import com.dashboard.singleton.ApplicationVisibleState;
import com.dashboard.view.RestaurantListView;

import java.util.ArrayList;

/**
 * Created by user on 5/9/2016.
 */
public class RestaurantSearchActivity extends AppCompatActivity implements View.OnClickListener, TextView.OnEditorActionListener, SwipeRefreshLayout.OnRefreshListener, RestaurantListView{

    //SwipeRefreshLayout swipeRefreshLayout;
    LinearLayout linearLoader;
    RecyclerView recyclerView;
    TextView textViewMessage;
    EditText editTextSearch;
    ImageButton imgBtnClose;
    private ProgressDialog progressDialog;
    LinearLayoutManager linearLayoutManager;

    ArrayList<RestaurantListData>restaurantListDatas;
    RestaurantListAdapter adapter;

    RestaurantListPresenter restaurantListPresenter;
    int pagingIndex;
    boolean isRefreshed, moreAvailable;
    String searchedText = "";

    int pastVisiblesItems, visibleItemCount, totalItemCount;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant_search);
        initView();
        setUiClickHandler();
        restaurantListDatas = new ArrayList<>();
        restaurantListPresenter = new RestaurantListPresenter(this);
        isRefreshed = false;
        pagingIndex = 0;
        //restaurantListPresenter.onClickGetRestaurantList("" + pagingIndex, "10", "");
    }

    @Override
    protected void onPause() {
        ApplicationVisibleState.activityPaused();
        super.onPause();
    }

    @Override
    protected void onResume() {
        ApplicationVisibleState.activityResumed();
        super.onResume();
    }

    private void initView() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

        textViewMessage = (TextView) findViewById(R.id.txtMessage);
       // swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        linearLoader = (LinearLayout) findViewById(R.id.linearLoader);
        recyclerView = (RecyclerView) findViewById(R.id.recycleViewRestaurantList);
        imgBtnClose = (ImageButton) findViewById(R.id.imgBtnClose);
        editTextSearch = (EditText) findViewById(R.id.editTextSearch);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        imgBtnClose.setVisibility(View.INVISIBLE);

    }

    private void setUiClickHandler() {
       // swipeRefreshLayout.setOnRefreshListener(this);
        imgBtnClose.setOnClickListener(this);
        editTextSearch.setOnEditorActionListener(this);

        editTextSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 1) {
                    imgBtnClose.setVisibility(View.VISIBLE);
                } else if (s.length() == 0) {
                    imgBtnClose.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                    if (!isRefreshed) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            //Do pagination.. i.e. fetch new data
                            doPagination();
                        }
                    }
                }
            }
        });

    }


    public void doPagination() {
        if(searchedText != null && !"".equalsIgnoreCase(searchedText) && moreAvailable) {
            isRefreshed = true;
            pagingIndex++;
            linearLoader.setVisibility(View.VISIBLE);
            restaurantListPresenter.onClickGetRestaurantList(false, "" + pagingIndex, "10", searchedText);
        }
    }


    @Override
    public void onRefresh() {
        if(searchedText != null && !"".equalsIgnoreCase(searchedText)) {
            isRefreshed = true;
            pagingIndex++;
            restaurantListPresenter.onClickGetRestaurantList(false, "" + pagingIndex, "10", searchedText);
        } else {
            //swipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public Context getClassContext() {
        return this.getApplicationContext();
    }

    @Override
    public void setRestaurantData(ArrayList<RestaurantListData> restaurantListDatasFound) {
        if(isRefreshed) {
            //swipeRefreshLayout.setRefreshing(false);
            linearLoader.setVisibility(View.GONE);
        }
        int scrollCount = this.restaurantListDatas.size() - 3;
        this.restaurantListDatas.addAll(restaurantListDatasFound);
        adapter = new RestaurantListAdapter(this, this.restaurantListDatas);
        recyclerView.setAdapter(adapter);
        if(restaurantListDatas.size() > 0) {
            showEmptyView(false);
        } else {
            showEmptyView(true);
        }
        if(scrollCount < 0) scrollCount = 0;
        if(isRefreshed) {
            linearLayoutManager.scrollToPosition(scrollCount);
        }
        if(restaurantListDatasFound == null || restaurantListDatasFound.size() < 1) {
            moreAvailable = false;
        }
        isRefreshed = false;
    }

    @Override
    public void openDashboardActivity() {
    }

    @Override
    public void showEmptyView(boolean show) {
        if(show) {
            textViewMessage.setVisibility(View.VISIBLE);
            textViewMessage.setText("Whoops! We couldn't find anything for \"" + searchedText + "\"");
        } else {
            textViewMessage.setVisibility(View.GONE);
        }
    }

    @Override
    public void showProgressDialog() {
        progressDialog = new ProgressDialog(RestaurantSearchActivity.this);
        progressDialog.setMessage("Please wait....");
        progressDialog.show();
    }

    @Override
    public void dismissDialog() {
        progressDialog.dismiss();
    }

    @Override
    public void showToast(String message, String subMessage, boolean isButtonOk) {
        new CustomToast(this, message, subMessage, isButtonOk);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.imgBtnClose) {
            editTextSearch.setText("");
        }
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if(v.getId() == R.id.editTextSearch) {
            if(actionId == EditorInfo.IME_ACTION_SEARCH) {
                if(!"".equalsIgnoreCase(editTextSearch.getText().toString().trim())) {
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(editTextSearch.getWindowToken(), 0);
                    restaurantListDatas = new ArrayList<>();
                    searchedText = editTextSearch.getText().toString().trim();
                    moreAvailable = true;
                    restaurantListPresenter.onClickGetRestaurantList(true, "" + pagingIndex, "10", searchedText);
                } else {
                    editTextSearch.setFocusable(true);
                    editTextSearch.requestFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(editTextSearch, InputMethodManager.SHOW_IMPLICIT);
                    editTextSearch.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
                }
            }
        }
        return false;
    }
}
