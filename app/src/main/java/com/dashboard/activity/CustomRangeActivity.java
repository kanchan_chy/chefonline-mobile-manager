package com.dashboard.activity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import com.chefonline.managermobile.R;
import com.dashboard.customview.CustomToast;
import com.dashboard.singleton.ApplicationVisibleState;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class CustomRangeActivity extends AppCompatActivity implements View.OnClickListener  {
    EditText editTextFromDate;
    EditText editTextToDate;
    Button btnApply, btnFromDate, btnToDate;
    private long mLastClickTime = 0;
    Date strDate = null, endDate = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_range);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Custom Range");
        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

        btnApply = (Button) findViewById(R.id.btnApply);
        btnFromDate = (Button) findViewById(R.id.btnFromDate);
        btnToDate = (Button) findViewById(R.id.btnToDate);
        editTextFromDate = (EditText) findViewById(R.id.editTextFromDate);
        editTextToDate = (EditText) findViewById(R.id.editTextToDate);
        btnApply.setOnClickListener(this);
        btnFromDate.setOnClickListener(this);
        btnToDate.setOnClickListener(this);
    }

    @Override
    protected void onPause() {
        ApplicationVisibleState.activityPaused();
        super.onPause();
    }

    @Override
    protected void onResume() {
        ApplicationVisibleState.activityResumed();
        super.onResume();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }


    private DatePickerDialog fromDatePickerDialog;
    private SimpleDateFormat dateFormatter;
    private void toDatePicker() {
        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.UK);
        Calendar newCalendar = Calendar.getInstance();
        fromDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);

               /* DateFormatter formatter = new DateFormatter();
                SimpleDateFormat sdf = new SimpleDateFormat(formatter.DD_MM_YYYY);
                String currentDate = sdf.format(new Date());*/

                //String valid_until = "01/07/2013";
                //SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                try {
                    endDate = dateFormatter.parse(dateFormatter.format(newDate.getTime()));
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                editTextToDate.setText(dateFormatter.format(newDate.getTime()));

                /*if (new Date().before(strDate)) {
                    new CustomToast(CustomRangeActivity.this, "Can not be greater than current date.", "", false);
                } else {
                    editTextToDate.setText(dateFormatter.format(newDate.getTime()));
                }*/

            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        fromDatePickerDialog.show();
    }


    private void fromDatePicker() {
        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.UK);
        Calendar newCalendar = Calendar.getInstance();
        fromDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);

               /* DateFormatter formatter = new DateFormatter();
                SimpleDateFormat sdf = new SimpleDateFormat(formatter.DD_MM_YYYY);
                String currentDate = sdf.format(new Date());*/

                //String valid_until = "01/07/2013";
                //SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                try {
                    strDate = dateFormatter.parse(dateFormatter.format(newDate.getTime()));
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                editTextFromDate.setText(dateFormatter.format(newDate.getTime()));
                /*if (new Date().before(strDate)) {
                     new CustomToast(CustomRangeActivity.this, "Can not be greater than current date.", "", false);
                } else {
                    editTextFromDate.setText(dateFormatter.format(newDate.getTime()));
                }*/

            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        fromDatePickerDialog.show();
    }



    private boolean validation() {
        if("".equalsIgnoreCase(editTextFromDate.getText().toString().trim()) && "".equalsIgnoreCase(editTextToDate.getText().toString().trim())) {
            new CustomToast(this, "From Date and To Date required", "", false);
            return false;
        }
        if("".equalsIgnoreCase(editTextFromDate.getText().toString().trim())) {
            new CustomToast(this, "From Date required", "", false);
            return false;
        }
        if("".equalsIgnoreCase(editTextToDate.getText().toString().trim())) {
            new CustomToast(this, "To Date required", "", false);
            return false;
        }
        if(strDate != null && endDate != null) {
            if(strDate.after(endDate)) {
                new CustomToast(this, "Valid To Date required ", "", false);
                return false;
            }
        }
        return true;
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnApply:
                if(validation()) {
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("fromDate", editTextFromDate.getText().toString().trim());
                    returnIntent.putExtra("toDate", editTextToDate.getText().toString().trim());
                    setResult(RESULT_OK, returnIntent);
                    finish();
                }
                break;

            case R.id.btnFromDate:
                    fromDatePicker();
                break;

            case R.id.btnToDate:
                    toDatePicker();
                break;
        }
    }
}
