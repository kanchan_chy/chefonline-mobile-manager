package com.dashboard.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.SystemClock;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chefonline.managermobile.R;
import com.dashboard.adapter.ViewPagerAdapter;
import com.dashboard.customview.CustomToast;
import com.dashboard.presenter.DashboardTodayPresenter;
import com.dashboard.services.SignalRService;
import com.dashboard.singleton.ApplicationVisibleState;
import com.dashboard.utility.ConstantValues;
import com.dashboard.utility.PreferenceUtil;
import com.dashboard.utility.SlidingTabLayout;
import com.dashboard.utility.UtilityMethod;
import com.dashboard.view.DashBoardTodayView;

public class DashBoardTodayActivityMain extends AppCompatActivity implements DashBoardTodayView{
    PreferenceUtil preferenceUtil;
    static ViewPager pager;
    ViewPagerAdapter adapter;
    private ProgressDialog progressDialog;
    static SlidingTabLayout tabs;
    CharSequence Titles[] = {"ONLINE ORDER", "RESERVATION"};
    CharSequence TitlesOne[] = {"ONLINE ORDER"};

    DashboardTodayPresenter dashboardTodayPresenter;

    private static PowerManager.WakeLock wakeLock;
    Handler awakeHandler;
    private long mLastClickTime = 0;

    private final Context mContext = this;
    private SignalRService mService;
    private boolean mBound = false;

    private  int selectedPosition;
    private ImageView prevSelectedImgView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board_today_main);
        initView();
        dashboardTodayPresenter = new DashboardTodayPresenter(this);
        if(savedInstanceState == null) {
            dashboardTodayPresenter.getReservationSettings();
        } else {
            setReservationTab();
        }

        // Start SignalR service
        PreferenceUtil preferenceUtil = new PreferenceUtil(this);
        ConstantValues.UUID = preferenceUtil.getUUID();
        ConstantValues.RESTAURANT_ID = preferenceUtil.getResID();
        /*try {
            Intent intent = new Intent();
            intent.setClass(mContext, SignalRService.class);
            startService(intent);
            bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        } catch (Exception e) {

        }*/

    }

    @Override
    protected void onPause() {
        //timerHandler.removeCallbacks(timerRunnable);
        ConstantValues.IS_ACTIVITY_RUNNING = false;
        ApplicationVisibleState.activityPaused();
        super.onPause();
    }

    @Override
    protected void onResume() {
        ConstantValues.IS_ACTIVITY_RUNNING = true;
        ApplicationVisibleState.activityResumed();
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        if (mBound) {
            unbindService(mConnection);
            mBound = false;
        }
        super.onDestroy();
    }

    private final ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            // We've bound to SignalRService, cast the IBinder and get SignalRService instance
            SignalRService.LocalBinder binder = (SignalRService.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };

    private void initView() {
        preferenceUtil = new PreferenceUtil(this);

        Toolbar toolbar =(Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        //getSupportActionBar().setTitle(preferenceUtil.getRestaurantName());
        TextView txtToolbarTitle = (TextView)findViewById(R.id.txtToolbarTitle);
        txtToolbarTitle.setText(preferenceUtil.getRestaurantName());

        if(preferenceUtil.getUSER_GROUP_ID().equalsIgnoreCase("1")) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }
      //  awakeHandler = new Handler();
      //  awakeHandler.postDelayed(awakeRunnable, 10800000);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON |
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

        // Assigning ViewPager View and setting the adapter
        pager = (ViewPager) findViewById(R.id.pager);

        // Assiging the Sliding Tab Layout View
        tabs = (SlidingTabLayout) findViewById(R.id.tabs);

        //tabs.setDistributeEvenly(true); // To make the Tabs Fixed set this true, This makes the tabs Space Evenly in Available width

        // Setting Custom Color for the Scroll bar indicator of the Tab View
        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.white);
            }

            @Override
            public int getDividerColor(int position) {
                //return getResources().getColor(R.color.yellow);
                return 0;
            }
        });

        Log.e("Serial", UtilityMethod.getDeviceSerialNumber(this));

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home_options, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_all_orders) {
            initialWorkToViewOrders();
            return true;
        } else if (id == R.id.action_all_reservations) {
            initialWorkToViewReservations();
            return true;
        } else if (id == R.id.action_end_of_day) {
            initialWorkToEndOfDayReport();
            return true;
        }else if (id == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
/*
    @Override
    public void onBackPressed() {
        if(preferenceUtil.getUSER_GROUP_ID().equalsIgnoreCase("4")) {
            showExitDialog("Are you sure want to exit now?", "EXIT", "CANCEL");
        } else {
            finish();
        }
    }  */

    public static void changeBubbleText(int pos, int value)
    {
        tabs.changeBubbleText(pos, value);
    }


    public void optionsMenu() {
        View menuItemView = findViewById(R.id.action_settings);
        final PopupMenu popup = new PopupMenu(DashBoardTodayActivityMain.this, menuItemView);

        popup.getMenuInflater().inflate(R.menu.menu_view_orders, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                int i = item.getItemId();
                if (i == R.id.action_all_orders) {
                    initialWorkToViewOrders();
                    return true;
                } else if (i == R.id.action_all_reservations) {
                    initialWorkToViewReservations();
                    return true;
                } else if (i == R.id.action_end_of_day) {
                    initialWorkToEndOfDayReport();
                    return true;
                }else if (i == R.id.action_exit) {
                    showExitDialog("Are you sure want to exit now?", "EXIT", "CANCEL");
                    return true;
                } else {
                    return onMenuItemClick(item);
                }
            }
        });

        popup.show();
    }


    private void initialWorkToViewOrders() {
        if(UtilityMethod.isConnectedToInternet(DashBoardTodayActivityMain.this)) {
            showFilterOptionDialog(true);
        }
        else {
            showInternetDialogOrders("Please check your internet connection.", "RETRY", "CANCEL");
        }
    }


    private void initialWorkToViewReservations() {
        if(UtilityMethod.isConnectedToInternet(DashBoardTodayActivityMain.this)) {
            showFilterOptionDialog(false);
        }
        else {
            showInternetDialogReservations("Please check your internet connection.", "RETRY", "CANCEL");
        }
    }


    private void initialWorkToEndOfDayReport() {
        if(UtilityMethod.isConnectedToInternet(DashBoardTodayActivityMain.this)) {
            startActivity(new Intent(DashBoardTodayActivityMain.this, EndOfDayActivity.class));
        }
        else {
            showInternetDialogEndOfDay("Please check your internet connection.", "RETRY", "CANCEL");
        }
    }


    public void showInternetDialogOrders(String message, String okButton, String cancelButton) {
        final Dialog dialog = new Dialog(DashBoardTodayActivityMain.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.dialog_exit);
        TextView txtViewPopupMessage = (TextView) dialog.findViewById(R.id.txtViewPopupMessage);
        txtViewPopupMessage.setText(message);
        final TextView txtAccept = (TextView) dialog.findViewById(R.id.txtAccept);
        txtAccept.setText(okButton);

        // if button is clicked, close the custom dialog
        txtAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000){
                    mLastClickTime = SystemClock.elapsedRealtime();
                    return;
                }
                else {
                    mLastClickTime = SystemClock.elapsedRealtime();
                }
                dialog.dismiss();
                initialWorkToViewOrders();
            }

        });

        final TextView txtCancel = (TextView) dialog.findViewById(R.id.txtCancel);
        txtCancel.setText(cancelButton);
        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        dialog.show();
    }


    public void showInternetDialogReservations(String message, String okButton, String cancelButton) {
        final Dialog dialog = new Dialog(DashBoardTodayActivityMain.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.dialog_exit);
        TextView txtViewPopupMessage = (TextView) dialog.findViewById(R.id.txtViewPopupMessage);
        txtViewPopupMessage.setText(message);
        final TextView txtAccept = (TextView) dialog.findViewById(R.id.txtAccept);
        txtAccept.setText(okButton);

        // if button is clicked, close the custom dialog
        txtAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000){
                    mLastClickTime = SystemClock.elapsedRealtime();
                    return;
                }
                else {
                    mLastClickTime = SystemClock.elapsedRealtime();
                }
                dialog.dismiss();
                initialWorkToViewReservations();
            }

        });

        final TextView txtCancel = (TextView) dialog.findViewById(R.id.txtCancel);
        txtCancel.setText(cancelButton);
        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        dialog.show();
    }


    public void showInternetDialogEndOfDay(String message, String okButton, String cancelButton) {
        final Dialog dialog = new Dialog(DashBoardTodayActivityMain.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.dialog_exit);
        TextView txtViewPopupMessage = (TextView) dialog.findViewById(R.id.txtViewPopupMessage);
        txtViewPopupMessage.setText(message);
        final TextView txtAccept = (TextView) dialog.findViewById(R.id.txtAccept);
        txtAccept.setText(okButton);

        // if button is clicked, close the custom dialog
        txtAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000){
                    mLastClickTime = SystemClock.elapsedRealtime();
                    return;
                }
                else {
                    mLastClickTime = SystemClock.elapsedRealtime();
                }
                dialog.dismiss();
                initialWorkToEndOfDayReport();
            }

        });

        final TextView txtCancel = (TextView) dialog.findViewById(R.id.txtCancel);
        txtCancel.setText(cancelButton);
        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        dialog.show();
    }


    public void showExitDialog(String message, String okButton, String cancelButton) {
        final Dialog dialog = new Dialog(DashBoardTodayActivityMain.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.dialog_exit);
        TextView txtViewPopupMessage = (TextView) dialog.findViewById(R.id.txtViewPopupMessage);
        txtViewPopupMessage.setText(message);
        final TextView txtAccept = (TextView) dialog.findViewById(R.id.txtAccept);
        txtAccept.setText(okButton);

        // if button is clicked, close the custom dialog
        txtAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                finish();
            }

        });

        final TextView txtCancel = (TextView) dialog.findViewById(R.id.txtCancel);
        txtCancel.setText(cancelButton);
        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        dialog.show();
    }


    private void showFilterOptionDialog(final boolean isOrderSelected) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.dialog_filter);
        final TextView txtAccept = (TextView) dialog.findViewById(R.id.txtAccept);
        // if button is clicked, close the custom dialog
        txtAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    mLastClickTime = SystemClock.elapsedRealtime();
                    return;
                } else {
                    mLastClickTime = SystemClock.elapsedRealtime();
                }
                dialog.dismiss();
                if(isOrderSelected) {
                    Intent intent = new Intent(DashBoardTodayActivityMain.this, DashBoardActivity.class);
                    intent.putExtra("selected_pos", selectedPosition);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(DashBoardTodayActivityMain.this, AllReservationsActivity.class);
                    intent.putExtra("selected_pos", selectedPosition);
                    startActivity(intent);
                }
            }

        });

        final TextView txtCancel = (TextView) dialog.findViewById(R.id.txtCancel);
        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        RelativeLayout[] relativeOptions = new RelativeLayout[8];
        ImageView[] imgOptions = new ImageView[8];

        int[] relativeIds = {R.id.relativeToday, R.id.relativeYesterday, R.id.relativeLast7, R.id.relativeLast30, R.id.relativeThisMonth, R.id.relativeLastMonth, R.id.relativeCustomRange};
        int[] imgIds = {R.id.imgViewToday, R.id.imgViewYesterday, R.id.imgViewLast7, R.id.imgViewLast30, R.id.imgViewThisMonth, R.id.imgViewLastMonth, R.id.imgViewCustomRange};

        for(int i = 0; i < relativeIds.length; i++) {
            relativeOptions[i] = (RelativeLayout) dialog.findViewById(relativeIds[i]);
            imgOptions[i] = (ImageView) dialog.findViewById(imgIds[i]);
            relativeOptions[i].setOnClickListener(filterClickListener(i, imgOptions[i]));
        }

        selectedPosition = 4;
        prevSelectedImgView = imgOptions[selectedPosition];
        imgOptions[selectedPosition].setVisibility(View.VISIBLE);

        dialog.show();
    }
    
    View.OnClickListener filterClickListener(final int selectedPos, final ImageView imgViewSelected) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedPosition = selectedPos;
                prevSelectedImgView.setVisibility(View.INVISIBLE);
                imgViewSelected.setVisibility(View.VISIBLE);
                prevSelectedImgView = imgViewSelected;
            }
        };
    }



    Runnable awakeRunnable = new Runnable() {
        @Override
        public void run() {
            PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
            wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "WAKELOCK_MAIN");
            wakeLock.acquire();
            awakeHandler.postDelayed(releaseRunnable, 2000);
        }
    };

    Runnable releaseRunnable = new Runnable() {
        @Override
        public void run() {
            if(wakeLock != null && wakeLock.isHeld()) {
                wakeLock.release();
            }
            awakeHandler.postDelayed(awakeRunnable, 10800000);
        }
    };


    @Override
    public Context getClassContext() {
        return this;
    }

    @Override
    public void setReservationTab() {
        boolean isActive = preferenceUtil.getReservation();
        if(isActive) {
            adapter =  new ViewPagerAdapter(this, getSupportFragmentManager(), Titles);
        } else {
            adapter =  new ViewPagerAdapter(this, getSupportFragmentManager(), TitlesOne);
        }
        pager.setAdapter(adapter);
        // Setting the ViewPager For the SlidingTabsLayout
        tabs.setViewPager(pager);
    }

    @Override
    public void showProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait....");
        progressDialog.show();
    }

    @Override
    public void dismissDialog() {
        progressDialog.dismiss();
    }

    @Override
    public void showToast(String message, String subMessage, boolean isButtonOk) {
        new CustomToast(this, message, subMessage, isButtonOk);
    }

}
