package com.dashboard.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chefonline.managermobile.R;
import com.dashboard.customview.CustomToast;
import com.dashboard.presenter.SettingPresenter;
import com.dashboard.singleton.ApplicationVisibleState;
import com.dashboard.utility.PreferenceUtil;
import com.dashboard.view.SettingsView;

/**
 * Created by user on 1/6/2016.
 */
public class SettingsActivity extends AppCompatActivity implements SettingsView, View.OnClickListener {
    //  Button btnDeliveryTime,btnChangeOffers,btnCallbackSupport,btnEndOfDay,btnAdminSettings,btnLogout;
    RelativeLayout relativeChangeDeliveryTime, relativeChangeOffers, relativeCallbackSupport, relativeAdminSettings, relativeLogout;
    private ProgressDialog progressDialog;

    SettingPresenter settingPresenter;
    private long mLastClickTime = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings2);
        settingPresenter = new SettingPresenter(this);
        initView();
    }

    @Override
    protected void onPause() {
        ApplicationVisibleState.activityPaused();
        super.onPause();
    }

    @Override
    protected void onResume() {
        ApplicationVisibleState.activityResumed();
        super.onResume();
    }

    private void initView() {
        relativeChangeDeliveryTime = (RelativeLayout) findViewById(R.id.relativeChangeDeliveryTime);
        relativeChangeOffers = (RelativeLayout) findViewById(R.id.relativeChangeOffers);
        relativeCallbackSupport = (RelativeLayout) findViewById(R.id.relativeCallbackSupport);
        relativeAdminSettings = (RelativeLayout) findViewById(R.id.relativeAdminSettings);
        relativeLogout = (RelativeLayout) findViewById(R.id.relativeLogout);

        relativeChangeDeliveryTime.setOnClickListener(this);
        relativeChangeOffers.setOnClickListener(this);
        relativeCallbackSupport.setOnClickListener(this);
        relativeAdminSettings.setOnClickListener(this);
        relativeLogout.setOnClickListener(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Settings");
        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }
    }

    private void showSetPinDialog() {
        final Dialog setPinDialog = new Dialog(this);
        setPinDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setPinDialog.setContentView(R.layout.dialog_set_pin);

        WindowManager.LayoutParams lp = setPinDialog.getWindow().getAttributes();
        lp.dimAmount = 0.9f;

        final EditText edtPin = (EditText) setPinDialog.findViewById(R.id.edtPin);
        final EditText edtPinConfirm = (EditText) setPinDialog.findViewById(R.id.edtPinConfirm);
        Button btnSetPin = (Button) setPinDialog.findViewById(R.id.btnSetPin);
        Button imgBtnClose = (Button) setPinDialog.findViewById(R.id.imgBtnClose);


        imgBtnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setPinDialog.dismiss();
            }
        });

        btnSetPin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    mLastClickTime = SystemClock.elapsedRealtime();
                    return;
                } else {
                    mLastClickTime = SystemClock.elapsedRealtime();
                }
                String adminPin = "", confirmPin = "";
                adminPin = edtPin.getText().toString().trim();
                confirmPin = edtPinConfirm.getText().toString().trim();
                if (adminPin.equals("") || confirmPin.equals("")) {
                    hideKeyboard(setPinDialog);
                    new CustomToast(SettingsActivity.this, "Please fill all fields", "", false);
                } else if (adminPin.length() < 4) {
                    hideKeyboard(setPinDialog);
                    new CustomToast(SettingsActivity.this, "Minimum 4 character is required", "", false);
                } else if (!adminPin.equals(confirmPin)) {
                    hideKeyboard(setPinDialog);
                    new CustomToast(SettingsActivity.this, "Pincodes not matched", "", false);
                } else {
                    PreferenceUtil myUtil = new PreferenceUtil(SettingsActivity.this);
                    myUtil.setAdminPin(adminPin);
                    setPinDialog.dismiss();
                    startActivity(new Intent(SettingsActivity.this, AdminSettingsActivity.class));
                }
            }
        });

        edtPinConfirm.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    String adminPin = "", confirmPin = "";
                    adminPin = edtPin.getText().toString().trim();
                    confirmPin = edtPinConfirm.getText().toString().trim();
                    if (adminPin.equals("") || confirmPin.equals("")) {
                        hideKeyboard(setPinDialog);
                        new CustomToast(SettingsActivity.this, "Please fill all fields", "", false);
                    } else if (adminPin.length() < 4) {
                        hideKeyboard(setPinDialog);
                        new CustomToast(SettingsActivity.this, "Minimum 4 character is required", "", false);
                    } else if (!adminPin.equals(confirmPin)) {
                        hideKeyboard(setPinDialog);
                        new CustomToast(SettingsActivity.this, "Pincodes not matched", "", false);
                    } else {
                        PreferenceUtil myUtil = new PreferenceUtil(SettingsActivity.this);
                        myUtil.setAdminPin(adminPin);
                        setPinDialog.dismiss();
                        startActivity(new Intent(SettingsActivity.this, AdminSettingsActivity.class));
                    }
                    return true;
                }
                return false;
            }
        });

        setPinDialog.show();

    }


    private void showEnterPinDialog() {
        final Dialog enterPinDialog = new Dialog(this);
        enterPinDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        enterPinDialog.setContentView(R.layout.dialog_enter_pin);

        WindowManager.LayoutParams lp = enterPinDialog.getWindow().getAttributes();
        lp.dimAmount = 0.9f;

        final EditText edtPin = (EditText) enterPinDialog.findViewById(R.id.edtPin);
        Button btnSetPin = (Button) enterPinDialog.findViewById(R.id.btnSetPin);
        Button imgBtnClose = (Button) enterPinDialog.findViewById(R.id.imgBtnClose);
        LinearLayout linearForgotPin = (LinearLayout) enterPinDialog.findViewById(R.id.linearForgotPincode);

        linearForgotPin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enterPinDialog.dismiss();
                openDialogForgotPincode("Pincode will be sent to your mobile via SMS.", "SEND CODE", "CANCEL");
            }
        });

        imgBtnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    mLastClickTime = SystemClock.elapsedRealtime();
                    return;
                } else {
                    mLastClickTime = SystemClock.elapsedRealtime();
                }
                enterPinDialog.dismiss();
            }
        });

        btnSetPin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    mLastClickTime = SystemClock.elapsedRealtime();
                    return;
                } else {
                    mLastClickTime = SystemClock.elapsedRealtime();
                }
                hideKeyboard(enterPinDialog);
                String adminPin = "";
                adminPin = edtPin.getText().toString().trim();
                if (adminPin.equals("")) {
                    new CustomToast(SettingsActivity.this, "Please enter pincode", "", false);
                } else {
                    PreferenceUtil myUtil = new PreferenceUtil(SettingsActivity.this);
                    String realPin = myUtil.getAdminPin();
                    if (realPin.equals(adminPin)) {
                        enterPinDialog.dismiss();
                        startActivity(new Intent(SettingsActivity.this, AdminSettingsActivity.class));
                    } else {
                        new CustomToast(SettingsActivity.this, "Pincode not matched", "", false);
                    }
                }
            }
        });

        edtPin.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    hideKeyboard(enterPinDialog);
                    String adminPin = "";
                    adminPin = edtPin.getText().toString().trim();
                    if (adminPin.equals("")) {
                        new CustomToast(SettingsActivity.this, "Please enter pincode", "", false);
                    } else {
                        PreferenceUtil myUtil = new PreferenceUtil(SettingsActivity.this);
                        String realPin = myUtil.getAdminPin();
                        if (realPin.equals(adminPin)) {
                            enterPinDialog.dismiss();
                            startActivity(new Intent(SettingsActivity.this, AdminSettingsActivity.class));
                        } else {
                            new CustomToast(SettingsActivity.this, "Pincode not matched", "", false);
                        }
                    }
                    return true;
                }
                return false;
            }
        });

        enterPinDialog.show();

    }


    private void openDialogForgotPincode(String message, String okButton, String cancelButton) {
        final Dialog dialog = new Dialog(SettingsActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.dialog_exit);
        TextView txtViewPopupMessage = (TextView) dialog.findViewById(R.id.txtViewPopupMessage);
        txtViewPopupMessage.setText(message);
        final TextView txtAccept = (TextView) dialog.findViewById(R.id.txtAccept);
        txtAccept.setText(okButton);

        // if button is clicked, close the custom dialog
        txtAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    mLastClickTime = SystemClock.elapsedRealtime();
                    return;
                } else {
                    mLastClickTime = SystemClock.elapsedRealtime();
                }
                dialog.dismiss();
                PreferenceUtil myUtil = new PreferenceUtil(SettingsActivity.this);
                settingPresenter.onClickForgotPincode(myUtil.getUserMobile(), myUtil.getAdminPin());
            }

        });

        final TextView txtCancel = (TextView) dialog.findViewById(R.id.txtCancel);
        txtCancel.setText(cancelButton);
        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        dialog.show();
    }


    private void hideKeyboard(Dialog dialog) {
        View v = dialog.getCurrentFocus();
        if (v != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
        }

    }


    private void showLogoutDialog() {
        final Dialog dialog = new Dialog(SettingsActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.dialog_warning);
        TextView txtMessage = (TextView) dialog.findViewById(R.id.txtMessage);
        txtMessage.setText("Are you sure want to logout?");
        final TextView txtAccept = (TextView) dialog.findViewById(R.id.txtAccept);
        txtAccept.setText("LOGOUT");

        // if button is clicked, close the custom dialog
        txtAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                callLogoutAPI();
            }

        });

        final TextView txtCancel = (TextView) dialog.findViewById(R.id.txtCancel);
        txtCancel.setText("CANCEL");
        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        dialog.show();
    }


    private void callLogoutAPI() {
        PreferenceUtil preferenceUtil = new PreferenceUtil(this);
        settingPresenter.onClickLogout(preferenceUtil.getUserID(), preferenceUtil.getUUID());
    }


    @Override
    public void makeLogout() {
        PreferenceUtil preferenceUtil = new PreferenceUtil(SettingsActivity.this);
        preferenceUtil.setLogInStatus(0); //Logout
        preferenceUtil.setResID("");
        Intent intent = new Intent(SettingsActivity.this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }


    @Override
    public Context getClassContext() {
        return this.getApplicationContext();
    }


    @Override
    public void showProgressDialog() {
        progressDialog = new ProgressDialog(SettingsActivity.this);
        progressDialog.setMessage("Please wait....");
        progressDialog.show();
    }

    @Override
    public void dismissDialog() {
        progressDialog.dismiss();
    }

    @Override
    public void showToast(String message, String subMessage, boolean isButtonOk) {
        new CustomToast(SettingsActivity.this, message, subMessage, isButtonOk);
    }


    @Override
    public void onClick(View v) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            mLastClickTime = SystemClock.elapsedRealtime();
            return;
        } else {
            mLastClickTime = SystemClock.elapsedRealtime();
        }
        if (v.getId() == R.id.relativeChangeDeliveryTime) {
            new CustomToast(SettingsActivity.this, "Development on progress", "", false);
        } else if (v.getId() == R.id.relativeChangeOffers) {
            //startActivity(new Intent(SettingsActivity.this,OffersListActivity.class));
            new CustomToast(SettingsActivity.this, "Development on progress", "", false);
        } else if (v.getId() == R.id.relativeCallbackSupport) {
            startActivity(new Intent(SettingsActivity.this, CallbackSupportActivity.class));
        } else if (v.getId() == R.id.relativeAdminSettings) {
            PreferenceUtil myUtil = new PreferenceUtil(SettingsActivity.this);
            if (myUtil.getAdminPin().equals("")) showSetPinDialog();
            else showEnterPinDialog();
        } else if (v.getId() == R.id.relativeLogout) {
            showLogoutDialog();
        }

    }


}
