package com.dashboard.activity;


import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.chefonline.managermobile.R;
import com.dashboard.customview.CustomToast;
import com.dashboard.customview.MsmEditText;
import com.dashboard.singleton.ApplicationVisibleState;
import com.dashboard.utility.PreferenceUtil;

public class ChangePincodeActivity extends AppCompatActivity implements View.OnClickListener{
    private static String TAG = "ChangePincodeActivity";
    private MsmEditText editTextPrevPincode;
    private MsmEditText editTextNewPincode;
    private MsmEditText editTextConfirmPincode;
    private Button btnChange;
    PreferenceUtil preferenceUtil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pincode);
        initView();
        setUiListener();
    }

    @Override
    protected void onPause() {
        ApplicationVisibleState.activityPaused();
        super.onPause();
    }

    @Override
    protected void onResume() {
        ApplicationVisibleState.activityResumed();
        super.onResume();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }


    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        if(savedInstanceState != null) {
            editTextPrevPincode.setText(savedInstanceState.getString("prev_pin"));
            editTextNewPincode.setText(savedInstanceState.getString("new_pin"));
            editTextConfirmPincode.setText(savedInstanceState.getString("confirm_pin"));
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString("prev_pin", editTextPrevPincode.getText().toString());
        outState.putString("new_pin", editTextNewPincode.getText().toString());
        outState.putString("confirm_pin", editTextConfirmPincode.getText().toString());
    }

    private void setUiListener() {
        btnChange.setOnClickListener(this);
    }

    private void initView() {
        preferenceUtil = new PreferenceUtil(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Change Pincode");
        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

        editTextPrevPincode = (MsmEditText) findViewById(R.id.editTextPrevPincode);
        editTextNewPincode = (MsmEditText) findViewById(R.id.editTextNewPincode);
        editTextConfirmPincode = (MsmEditText) findViewById(R.id.editTextConfirmPincode);
        btnChange = (Button) findViewById(R.id.btnReset);

    }


    private void changePincode() {
        PreferenceUtil myUtil = new PreferenceUtil(ChangePincodeActivity.this);
        myUtil.setAdminPin(editTextNewPincode.getText().toString().trim());
        new CustomToast(ChangePincodeActivity.this, "Pincode changed successfully", "", true);
        onBackPressed();
    }


    private boolean inputValidation() {

        if (editTextPrevPincode.getText().toString().trim().equalsIgnoreCase("")) {
            editTextPrevPincode.setErrorTextVisible(true);
            editTextPrevPincode.setErrorMsg("Please enter valid pincode.");
            editTextPrevPincode.setTypingFocus();
            return false;
        }

        PreferenceUtil myUtil = new PreferenceUtil(ChangePincodeActivity.this);
        String prevPincode = myUtil.getAdminPin();
        if(!editTextPrevPincode.getText().toString().trim().equals(prevPincode)) {
            editTextPrevPincode.setErrorTextVisible(true);
            editTextPrevPincode.setErrorMsg("Wrong previous pincode.");
            editTextPrevPincode.setTypingFocus();
            return false;
        }

        if (editTextNewPincode.getText().toString().trim().equalsIgnoreCase("")) {
            editTextNewPincode.setErrorTextVisible(true);
            editTextNewPincode.setErrorMsg("Please enter valid pincode.");
            editTextNewPincode.setTypingFocus();
            return false;
        }

        if (editTextNewPincode.getText().toString().trim().length() < 4) {
            editTextNewPincode.setErrorTextVisible(true);
            editTextNewPincode.setErrorMsg("Minimum 4 character is required.");
            editTextNewPincode.setTypingFocus();
            return false;
        }

        if (editTextConfirmPincode.getText().trim().equalsIgnoreCase("")) {
            editTextConfirmPincode.setErrorTextVisible(true);
            editTextConfirmPincode.setErrorMsg("Confirm pincode is required.");
            editTextConfirmPincode.setTypingFocus();
            return false;
        }

        if (!editTextNewPincode.getText().trim().equalsIgnoreCase(editTextConfirmPincode.getText().toString().trim())) {
            editTextConfirmPincode.setErrorTextVisible(true);
            editTextConfirmPincode.setErrorMsg("Pincode mismatch.");
            editTextConfirmPincode.setTypingFocus();
            return false;
        }

        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnReset:
                if (inputValidation()) {
                    changePincode();
                }
                break;

        }
    }

}

