package com.dashboard.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.chefonline.managermobile.R;
import com.dashboard.customview.CustomToast;
import com.dashboard.presenter.AddOfferPresenter;
import com.dashboard.services.AddOfferService;
import com.dashboard.singleton.ApplicationVisibleState;
import com.dashboard.utility.ConstantValues;
import com.dashboard.view.AddOfferView;

/**
 * Created by user on 1/18/2016.
 */
public class AddChangeOffersActivity extends AppCompatActivity implements View.OnClickListener, AddOfferView{

    TextView txtCollection, txtDelivery, txtBoth, txtLeft, txtRight, txtYes, txtNo, txtDefault;
    ImageView imgOffer;
    Button btnGallery, btnSubmit, btnOfferDay;
    EditText edtOfferTitle, edtOfferDesc, edtSpent;
    private ProgressDialog progressDialog;

    private int offerFor = ConstantValues.OFFER_FOR_NONE;
    private int offerPosition = ConstantValues.OFFER_POSITION_NONE;
    private int firstOrderOffer = ConstantValues.FIRST_ORDER_OFFER_NO;
    private boolean defaultStatus = false;
    private String offerImageUrl = "";
    private String offerDay = "";

    private AddOfferPresenter addOfferPresenter;
    private long mLastClickTime = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_change_offers);
        initView();
        setUIClickHandler();
        addOfferPresenter = new AddOfferPresenter(this, new AddOfferService());

    }

    @Override
    protected void onPause() {
        ApplicationVisibleState.activityPaused();
        super.onPause();
    }

    @Override
    protected void onResume() {
        ApplicationVisibleState.activityResumed();
        super.onResume();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int) event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }


    private void initView() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Add or Change Offer");
        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

        txtCollection = (TextView)findViewById(R.id.txtCollection);
        txtDelivery = (TextView)findViewById(R.id.txtDelivery);
        txtBoth = (TextView)findViewById(R.id.txtBoth);
        txtLeft = (TextView)findViewById(R.id.txtLeft);
        txtRight = (TextView)findViewById(R.id.txtRight);
        txtYes = (TextView)findViewById(R.id.txtYes);
        txtNo = (TextView)findViewById(R.id.txtNo);
        txtDefault = (TextView)findViewById(R.id.txtDefault);
        btnGallery = (Button)findViewById(R.id.btnGallery);
        btnSubmit = (Button)findViewById(R.id.btnSubmit);
        btnOfferDay = (Button)findViewById(R.id.btnOfferDay);
        imgOffer = (ImageView)findViewById(R.id.imgOffer);
        edtOfferTitle = (EditText)findViewById(R.id.edtOfferTitle);
        edtOfferDesc = (EditText)findViewById(R.id.edtOfferDesc);
        edtSpent = (EditText)findViewById(R.id.edtSpent);

    }


    private void setUIClickHandler() {
        txtCollection.setOnClickListener(this);
        txtDelivery.setOnClickListener(this);
        txtBoth.setOnClickListener(this);
        txtLeft.setOnClickListener(this);
        txtRight.setOnClickListener(this);
        txtYes.setOnClickListener(this);
        txtNo.setOnClickListener(this);
        txtDefault.setOnClickListener(this);
        btnGallery.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);
        btnOfferDay.setOnClickListener(this);
    }


    @Override
    public String getOfferTitle() {
        return edtOfferTitle.getText().toString().trim();
    }

    @Override
    public String getOfferDesc() {
        return edtOfferDesc.getText().toString().trim();
    }

    @Override
    public String getSpentAmount() {
        return edtSpent.getText().toString().trim();
    }

    @Override
    public int getOfferFor() {
        return offerFor;
    }

    @Override
    public int getOfferPosition() {
        return offerPosition;
    }

    @Override
    public Context getClassContext() {
        return this;
    }

    @Override
    public void showProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait....");
        progressDialog.show();
    }

    @Override
    public void showToast(String message, String subMessage, boolean isButtonOk) {
        new CustomToast(this, message, subMessage, isButtonOk);
    }

    @Override
    public void dismissDialog() {
        progressDialog.dismiss();
    }

    @Override
    public String getOfferImage() {
        return offerImageUrl;
    }

    @Override
    public void setOfferImage(String url) {
        offerImageUrl = url;
    }

    @Override
    public void setOfferFor(int offerForNo) {
        offerFor=offerForNo;
        if(offerFor == ConstantValues.OFFER_FOR_COLLECTION) {
            txtCollection.setBackgroundResource(R.drawable.round_red_background);
            txtDelivery.setBackgroundResource(R.drawable.round_gray_background);
            txtBoth.setBackgroundResource(R.drawable.round_gray_background);
            txtCollection.setTextColor(getResources().getColor(R.color.white));
            txtDelivery.setTextColor(getResources().getColor(R.color.black));
            txtBoth.setTextColor(getResources().getColor(R.color.black));
        }
        else if(offerFor == ConstantValues.OFFER_FOR_DELIVERY) {
            txtDelivery.setBackgroundResource(R.drawable.round_red_background);
            txtCollection.setBackgroundResource(R.drawable.round_gray_background);
            txtBoth.setBackgroundResource(R.drawable.round_gray_background);
            txtDelivery.setTextColor(getResources().getColor(R.color.white));
            txtCollection.setTextColor(getResources().getColor(R.color.black));
            txtBoth.setTextColor(getResources().getColor(R.color.black));

        } else if(offerFor == ConstantValues.OFFER_FOR_BOTH) {
            txtBoth.setBackgroundResource(R.drawable.round_red_background);
            txtCollection.setBackgroundResource(R.drawable.round_gray_background);
            txtDelivery.setBackgroundResource(R.drawable.round_gray_background);
            txtBoth.setTextColor(getResources().getColor(R.color.white));
            txtCollection.setTextColor(getResources().getColor(R.color.black));
            txtDelivery.setTextColor(getResources().getColor(R.color.black));
        }
    }

    @Override
    public void setOfferPosition(int offerPosNo) {
        offerPosition=offerPosNo;
        if(offerPosition == ConstantValues.OFFER_POSITION_LEFT) {
            txtLeft.setBackgroundResource(R.drawable.round_red_background);
            txtRight.setBackgroundResource(R.drawable.round_gray_background);
            txtLeft.setTextColor(getResources().getColor(R.color.white));
            txtRight.setTextColor(getResources().getColor(R.color.black));
        }
        else if(offerPosition == ConstantValues.OFFER_POSITION_RIGHT) {
            txtRight.setBackgroundResource(R.drawable.round_red_background);
            txtLeft.setBackgroundResource(R.drawable.round_gray_background);
            txtRight.setTextColor(getResources().getColor(R.color.white));
            txtLeft.setTextColor(getResources().getColor(R.color.black));
        }
    }

    @Override
    public boolean getDefaultStatus() {
        return defaultStatus;
    }

    @Override
    public void setDefaultStatus(boolean status) {
        defaultStatus=status;
        if(defaultStatus) {
            txtDefault.setBackgroundResource(R.drawable.round_red_background);
            txtDefault.setTextColor(ContextCompat.getColor(AddChangeOffersActivity.this, R.color.white));
        }
        else {
            txtDefault.setBackgroundResource(R.drawable.round_gray_background);
            txtDefault.setTextColor(ContextCompat.getColor(AddChangeOffersActivity.this, R.color.black));
        }
    }

    @Override
    public String getOfferDay() {
        return offerDay;
    }

    @Override
    public void setOfferDay(String day) {
        offerDay = day;
    }

    @Override
    public void showDaysPopup(View v) {
        final PopupMenu popup = new PopupMenu(this, v);

        popup.getMenuInflater().inflate(R.menu.menu_offer_days, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                int i = item.getItemId();
                if (i == R.id.action_mon_day) {
                    //Toast.makeText(getApplicationContext(), "Monday selected", Toast.LENGTH_SHORT).show();
                    setOfferDay("Mon");
                    return true;
                } else if (i == R.id.action_tue_day) {
                    //Toast.makeText(getApplicationContext(), "Tuesday selected", Toast.LENGTH_SHORT).show();
                    setOfferDay("Tue");
                    return true;
                } else if (i == R.id.action_wed_day) {
                   // Toast.makeText(getApplicationContext(), "Wednesday selected", Toast.LENGTH_SHORT).show();
                    setOfferDay("Wed");
                    return true;
                } else if (i == R.id.action_thu_day) {
                   // Toast.makeText(getApplicationContext(), "Thursday selected", Toast.LENGTH_SHORT).show();
                    setOfferDay("Thu");
                    return true;
                } else if (i == R.id.action_fri_day) {
                   // Toast.makeText(getApplicationContext(), "Friday selected", Toast.LENGTH_SHORT).show();
                    setOfferDay("Fri");
                    return true;
                } else if (i == R.id.action_sat_day) {
                  //  Toast.makeText(getApplicationContext(), "Saturday selected", Toast.LENGTH_SHORT).show();
                    setOfferDay("Sat");
                    return true;
                } else if (i == R.id.action_sun_day) {
                   // Toast.makeText(getApplicationContext(), "Sunday selected", Toast.LENGTH_SHORT).show();
                    setOfferDay("Sun");
                    return true;
                } else if (i == R.id.action_all_day) {
                    //Toast.makeText(getApplicationContext(), "All days selected", Toast.LENGTH_SHORT).show();
                    setOfferDay("All");
                    return true;
                } else {
                    return false;
                }
            }
        });

        popup.show();
    }

    @Override
    public int getFirstOrderOfferStatus() {
        return firstOrderOffer;
    }

    @Override
    public void setFirstOrderOfferStatus(int status) {
        firstOrderOffer=status;
        if(firstOrderOffer == ConstantValues.FIRST_ORDER_OFFER_NO) {
            txtNo.setBackgroundResource(R.drawable.round_red_background);
            txtYes.setBackgroundResource(R.drawable.round_gray_background);
            txtNo.setTextColor(ContextCompat.getColor(AddChangeOffersActivity.this, R.color.white));
            txtYes.setTextColor(ContextCompat.getColor(AddChangeOffersActivity.this, R.color.black));
        }
        else {
            txtYes.setBackgroundResource(R.drawable.round_red_background);
            txtNo.setBackgroundResource(R.drawable.round_gray_background);
            txtYes.setTextColor(ContextCompat.getColor(AddChangeOffersActivity.this, R.color.white));
            txtNo.setTextColor(ContextCompat.getColor(AddChangeOffersActivity.this, R.color.black));
        }
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.txtCollection) {
            setOfferFor(ConstantValues.OFFER_FOR_COLLECTION);
        } else if(v.getId() == R.id.txtDelivery) {
            setOfferFor(ConstantValues.OFFER_FOR_DELIVERY);
        } else if(v.getId() == R.id.txtBoth) {
            setOfferFor(ConstantValues.OFFER_FOR_BOTH);
        } else if(v.getId() == R.id.txtLeft) {
            setOfferPosition(ConstantValues.OFFER_POSITION_LEFT);
        } else if(v.getId() == R.id.txtRight) {
            setOfferPosition(ConstantValues.OFFER_POSITION_RIGHT);
        } else if(v.getId() == R.id.txtDefault) {
            if(defaultStatus) setDefaultStatus(false);
            else setDefaultStatus(true);
        } else if(v.getId() == R.id.txtNo) {
            setFirstOrderOfferStatus(ConstantValues.FIRST_ORDER_OFFER_NO);
        } else if(v.getId() == R.id.txtYes) {
            setFirstOrderOfferStatus(ConstantValues.FIRST_ORDER_OFFER_YES);
        } else if(v.getId() == R.id.btnGallery) {
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000){
                mLastClickTime = SystemClock.elapsedRealtime();
                return;
            } else {
                mLastClickTime = SystemClock.elapsedRealtime();
            }
            addOfferPresenter.onClickGallery();
        } else if(v.getId() == R.id.btnSubmit) {
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000){
                mLastClickTime = SystemClock.elapsedRealtime();
                return;
            } else {
                mLastClickTime = SystemClock.elapsedRealtime();
            }
            addOfferPresenter.onClickSubmit();
        } else if(v.getId() == R.id.btnOfferDay) {
            showDaysPopup(v);
        }
    }


}
