package com.dashboard.activity;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chefonline.managermobile.R;
import com.dashboard.adapter.DashBoardDataAdapter;
import com.dashboard.adapter.DeleteOrderAdapter;
import com.dashboard.broadcastreceiver.ConnectivityChangeReceiver;
import com.dashboard.datamodel.OrderData;
import com.dashboard.datamodel.PaymentData;
import com.dashboard.datamodel.ReservationDataUnconfirmed;
import com.dashboard.datamodel.ScheduleData;
import com.dashboard.presenter.DashBoardPresenter;
import com.dashboard.services.DashBoardService;
import com.dashboard.singleton.ApplicationVisibleState;
import com.dashboard.utility.ConstantValues;
import com.dashboard.utility.DateFormatter;
import com.dashboard.utility.PreferenceUtil;
import com.dashboard.utility.UtilityMethod;
import com.dashboard.view.ConnectivityChangeInterface;
import com.dashboard.view.DashBoardView;

import java.util.ArrayList;

public class DashBoardActivity extends AppCompatActivity implements DashBoardView, View.OnClickListener, ConnectivityChangeInterface{
    public static String TAG = "DashBoardActivity";
    public static String fromDate = "";
    public static String toDate = "";
    TextView textViewShowMore;
    TextView textViewFilter;
    TextView textViewPayPal;
    TextView textViewTotal;
    TextView textViewTotalOrder;
    TextView textViewCollectionAmount;
    TextView textViewDeliveryAmount;
    TextView textViewCash;
    TextView textViewPaypalLabel;
    TextView textViewCashLabel;
    TextView textViewConnectivity;
    ImageButton btnCustomRange;
    LinearLayout linearMessage;
    RelativeLayout relativeContainer;
    FloatingActionButton floatBtnFilter;

    private  int selectedPosition = 4;
    private ImageView prevSelectedImgView;

    RecyclerView recyclerView;
    RelativeLayout relative_show_more;
    private RecyclerView.LayoutManager mLayoutManager;
    public static DashBoardPresenter dashBoardPresenter;

    boolean deleteEnabled;
    DashBoardDataAdapter myAdapter;
    private long mLastClickTime = 0;

    ArrayList<OrderData> orderDatas;

    Animation animSlideDown;
    Toolbar toolbar;
    ConnectivityChangeReceiver connectivityChangeReceiver;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);

        initView();
        initAnimation();
        dashBoardPresenter = new DashBoardPresenter(this, new DashBoardService());

        connectivityChangeReceiver = new ConnectivityChangeReceiver(this);
        IntentFilter connectivityChangeFilter = new IntentFilter(ConstantValues.ACTION_CONNECTIVITY_CHANGE);
        registerReceiver(connectivityChangeReceiver, connectivityChangeFilter);

        //Local broadcast receiver for updating non printed item
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter("on-print-completed"));

        if(savedInstanceState == null) {
            try {
                deleteEnabled = getIntent().getExtras().getBoolean("deleteEnabled");
            } catch (Exception e) {
                deleteEnabled = false;
            }

            if (deleteEnabled) {
                selectedPosition = 0;
            } else {
                selectedPosition = getIntent().getExtras().getInt("selected_pos");
            }
            ArrayList<OrderData> mOrderDatas = new ArrayList<>();
            setOrderData(mOrderDatas);
            filterOptionSelected();
        } else {
            deleteEnabled = savedInstanceState.getBoolean("deleteEnabled");
            selectedPosition = savedInstanceState.getInt("selectedPos");
            orderDatas = (ArrayList<OrderData>)savedInstanceState.getSerializable("orderData");
            setOrderData(orderDatas);
            textViewFilter.setText(savedInstanceState.getString("filter_text"));
            toolbar.setSubtitle(savedInstanceState.getString("sub_title"));
            setCollectionOrder(savedInstanceState.getString("collection_text"));
            setDeliveryOrder(savedInstanceState.getString("delivery_text"));
            setCash(savedInstanceState.getString("cash_text"));
            setPayPal(savedInstanceState.getString("paypal_text"));
            setTotal(savedInstanceState.getString("total_text"));
            setTotalOrder(savedInstanceState.getString("total_order_text"));
            String showMoreText = savedInstanceState.getString("show_more_text");
            textViewShowMore.setText(showMoreText);
            if(showMoreText.equalsIgnoreCase("Hide More")) {
                expand();
            } else {
                collapse();
            }
            if(orderDatas.size() > 0) {
                showEmptyView(false);
            } else {
                showEmptyView(true);
            }
        }

    }


    @Override
    protected void onPause() {
        ApplicationVisibleState.activityPaused();
        super.onPause();
    }

    @Override
    protected void onResume() {
        try {
            ApplicationVisibleState.activityResumed();
            myAdapter.notifyDataSetChanged();
        } catch (Exception e) {

        }
        super.onResume();
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int position = intent.getExtras().getInt("position");
            orderDatas.get(position).setPrintStatus("1");
            myAdapter.notifyDataSetChanged();

            Log.i("receiver", "Got Position: " + position);
        }
    };

    @Override
    protected void onDestroy() {
        // Unregister since the activity is about to be closed.
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        if(connectivityChangeReceiver != null) {
            unregisterReceiver(connectivityChangeReceiver);
        }
        super.onDestroy();
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("deleteEnabled", deleteEnabled);
        outState.putInt("selectedPos", selectedPosition);
        outState.putSerializable("orderData", orderDatas);
        outState.putString("filter_text", textViewFilter.getText().toString());
        String subTitle = "";
        if(toolbar.getSubtitle() != null) {
            subTitle = toolbar.getSubtitle().toString();
        }
        outState.putString("sub_title", subTitle);
        outState.putString("show_more_text", textViewShowMore.getText().toString());
        outState.putString("cash_text", textViewCash.getText().toString());
        outState.putString("paypal_text", textViewPayPal.getText().toString());
        outState.putString("total_text", textViewTotal.getText().toString());
        outState.putString("total_order_text", textViewTotalOrder.getText().toString());
        outState.putString("collection_text", textViewCollectionAmount.getText().toString());
        outState.putString("delivery_text", textViewDeliveryAmount.getText().toString());
    }

    private void initView() {
        recyclerView = (RecyclerView) findViewById(R.id.recycleViewOrderList);
        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(DashBoardActivity.this);

        recyclerView.setLayoutManager(mLayoutManager);

        floatBtnFilter = (FloatingActionButton) findViewById(R.id.floatBtnFilter);
        floatBtnFilter.setOnClickListener(this);

        textViewFilter = (TextView) findViewById(R.id.textViewFilter);
        textViewShowMore = (TextView) findViewById(R.id.textViewShowMore);
        textViewPayPal = (TextView) findViewById(R.id.textViewPaypal);
        textViewTotal = (TextView) findViewById(R.id.textViewTotal);
        textViewTotalOrder = (TextView) findViewById(R.id.textViewTotalOrder);
        textViewCollectionAmount = (TextView) findViewById(R.id.textViewCollectionAmount);
        textViewDeliveryAmount = (TextView) findViewById(R.id.textViewDeliveryAmount);
        textViewCash = (TextView) findViewById(R.id.textViewCash);
        textViewPaypalLabel = (TextView) findViewById(R.id.textViewPaypalLabel);
        textViewCashLabel = (TextView) findViewById(R.id.textViewCashLabel);
        textViewConnectivity = (TextView) findViewById(R.id.textViewConnectivity);
        btnCustomRange = (ImageButton) findViewById(R.id.btnCustomRange);
        linearMessage = (LinearLayout) findViewById(R.id.linearMessage);
        relativeContainer = (RelativeLayout) findViewById(R.id.relativeLayoutInfo);
        btnCustomRange.setOnClickListener(this);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (deleteEnabled) getSupportActionBar().setTitle("Delete Orders");
        else getSupportActionBar().setTitle("Orders");

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 100) {
            if (resultCode == RESULT_OK) {
                try {
                    fromDate = data.getStringExtra("fromDate");
                    toDate = data.getStringExtra("toDate");
                    String showingText1 = "" + DateFormatter.doFormat(fromDate, DateFormatter.DD_MM_YYYY, DateFormatter.MMM_dd_yyyy);
                    String showingText2 = "" + DateFormatter.doFormat(toDate, DateFormatter.DD_MM_YYYY, DateFormatter.MMM_dd_yyyy);
                    textViewFilter.setText(showingText1 + " - " + showingText2);
                    toolbar.setSubtitle(showingText1 + " - " + showingText2);

                    dashBoardPresenter.onClickGetOrder(ConstantValues.ORDER_ONLINE_LIST_API_ID, true);
                } catch (Exception e) {
                    Log.i(TAG, e.getMessage());
                }

            }

            if (resultCode == RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_dash_board, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_settings:
                //openSettings();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

        //return super.onOptionsItemSelected(item);
    }

    @Override
    public String getRestaurantId() {
        PreferenceUtil preferenceUtil = new PreferenceUtil(this);
        return preferenceUtil.getResID();
    }

    @Override
    public String getStartDate() {
        return fromDate;
    }

    @Override
    public String getEndDate() {
        return toDate;
    }

    @Override
    public ProgressDialog showProgressDialog() {
        ProgressDialog progress = new ProgressDialog(DashBoardActivity.this);
        progress.setMessage("Please wait....");
        progress.show();
        return progress;
    }

    @Override
    public void checkConnectivity() {

    }

    @Override
    public void showInternetDialog(String message, String okButton, String cancelButton) {

    }

    @Override
    public Context getClassContext() {
        return DashBoardActivity.this;
    }

    @Override
    public void setScheduleData(ArrayList<ScheduleData> scheduleData) {

    }

    @Override
    public void setOrderData(ArrayList<OrderData> orderDatas) {
        this.orderDatas = orderDatas;
        if (deleteEnabled) {
            DeleteOrderAdapter myAdapter = new DeleteOrderAdapter(orderDatas, getClassContext(), false);
            recyclerView.setAdapter(myAdapter);
        } else {
            myAdapter = new DashBoardDataAdapter(orderDatas, getClassContext(), false);
            recyclerView.setAdapter(myAdapter);
        }

    }

    @Override
    public void setReservationData(ArrayList<ReservationDataUnconfirmed> reservationDataUnconfirmed) {

    }

    @Override
    public void setPaymentData(ArrayList<PaymentData> paymentData) {
        if(paymentData != null && paymentData.size() > 0) {
            if(paymentData.get(0).getPaymentAmount() != null && !"".equalsIgnoreCase(paymentData.get(0).getPaymentAmount())) {
                textViewPayPal.setText("" + UtilityMethod.priceFormatter(Double.valueOf(paymentData.get(0).getPaymentAmount())));
            } else {
                textViewPayPal.setText("0.00");
            }
            textViewPaypalLabel.setText(paymentData.get(0).getPaymentMethod());
            if(paymentData.size() > 1) {
                if(paymentData.get(1).getPaymentAmount() != null && !"".equalsIgnoreCase(paymentData.get(1).getPaymentAmount())) {
                    textViewCash.setText("" + UtilityMethod.priceFormatter(Double.valueOf(paymentData.get(1).getPaymentAmount())));
                } else {
                    textViewCash.setText("0.00");
                }
                textViewCashLabel.setText(paymentData.get(1).getPaymentMethod());
            } else {
                textViewCash.setText("0.00");
                textViewCashLabel.setText("Others");
            }
        }
    }

    @Override
    public void setCash(String text) {
        textViewCash.setText(text);
    }

    @Override
    public void setPayPal(String text) {
        textViewPayPal.setText(text);
    }

    @Override
    public void setTotal(String text) {
        textViewTotal.setText(text);
    }

    @Override
    public void setTotalOrder(String text) {
        textViewTotalOrder.setText(text);
    }

    @Override
    public void setDeliveryOrder(String text) {
        textViewDeliveryAmount.setText(text);
    }

    @Override
    public void setCollectionOrder(String text) {
        textViewCollectionAmount.setText(text);
    }

    @Override
    public void setTotalOrderReservation(String text) {

    }

    @Override
    public void showNotPrintedOrder() {

    }

    @Override
    public void showEmptyView(boolean isVisible) {
        if (isVisible) {
            relativeContainer.setVisibility(View.GONE);
            linearMessage.setVisibility(View.VISIBLE);
            textViewShowMore.setVisibility(View.GONE);
        } else {
            linearMessage.setVisibility(View.GONE);
            relativeContainer.setVisibility(View.VISIBLE);
            textViewShowMore.setVisibility(View.VISIBLE);
        }


    }

    private void filterOptionSelected() {
        try {
            if (selectedPosition == 0) {
                fromDate = DateFormatter.getCurrentDate();
                toDate = DateFormatter.getCurrentDate();
                String showingText1 = "" + DateFormatter.doFormat(fromDate, DateFormatter.DD_MM_YYYY, DateFormatter.MMM_dd_yyyy);
                String showingText2 = "" + DateFormatter.doFormat(toDate, DateFormatter.DD_MM_YYYY, DateFormatter.MMM_dd_yyyy);
                textViewFilter.setText(showingText1 + " - " + showingText2);
                toolbar.setSubtitle(showingText1 + " - " + showingText2);
                dashBoardPresenter.onClickGetOrder(ConstantValues.ORDER_ONLINE_LIST_API_ID, true);
            }

            if (selectedPosition == 1) {
                fromDate = DateFormatter.getCustomDate(-1);
                toDate = DateFormatter.getCustomDate(-1);
                String showingText1 = "" + DateFormatter.doFormat(fromDate, DateFormatter.DD_MM_YYYY, DateFormatter.MMM_dd_yyyy);
                String showingText2 = "" + DateFormatter.doFormat(toDate, DateFormatter.DD_MM_YYYY, DateFormatter.MMM_dd_yyyy);
                textViewFilter.setText(showingText1 + " - " + showingText2);
                toolbar.setSubtitle(showingText1 + " - " + showingText2);
                dashBoardPresenter.onClickGetOrder(ConstantValues.ORDER_ONLINE_LIST_API_ID, true);
            }

            if (selectedPosition == 2) {
                fromDate = DateFormatter.getCustomDate(-6);
                toDate = DateFormatter.getCurrentDate();
                String showingText1 = "" + DateFormatter.doFormat(fromDate, DateFormatter.DD_MM_YYYY, DateFormatter.MMM_dd_yyyy);
                String showingText2 = "" + DateFormatter.doFormat(toDate, DateFormatter.DD_MM_YYYY, DateFormatter.MMM_dd_yyyy);
                textViewFilter.setText(showingText1 + " - " + showingText2);
                toolbar.setSubtitle(showingText1 + " - " + showingText2);
                dashBoardPresenter.onClickGetOrder(ConstantValues.ORDER_ONLINE_LIST_API_ID, true);
            }

            if (selectedPosition == 3) {
                fromDate = DateFormatter.getCustomDate(-29);
                toDate = DateFormatter.getCurrentDate();
                String showingText1 = "" + DateFormatter.doFormat(fromDate, DateFormatter.DD_MM_YYYY, DateFormatter.MMM_dd_yyyy);
                String showingText2 = "" + DateFormatter.doFormat(toDate, DateFormatter.DD_MM_YYYY, DateFormatter.MMM_dd_yyyy);
                textViewFilter.setText(showingText1 + " - " + showingText2);
                toolbar.setSubtitle(showingText1 + " - " + showingText2);
                dashBoardPresenter.onClickGetOrder(ConstantValues.ORDER_ONLINE_LIST_API_ID, true);
            }

            if (selectedPosition == 4) {
                int day = DateFormatter.getDayofMonth();
                fromDate = DateFormatter.getCustomDate(-(day - 1));
                toDate = DateFormatter.getCustomDate(DateFormatter.getTotalDaysOfMonth() - day);
                String showingText1 = "" + DateFormatter.doFormat(fromDate, DateFormatter.DD_MM_YYYY, DateFormatter.MMM_dd_yyyy);
                String showingText2 = "" + DateFormatter.doFormat(toDate, DateFormatter.DD_MM_YYYY, DateFormatter.MMM_dd_yyyy);
                textViewFilter.setText(showingText1 + " - " + showingText2);
                toolbar.setSubtitle(showingText1 + " - " + showingText2);
                dashBoardPresenter.onClickGetOrder(ConstantValues.ORDER_ONLINE_LIST_API_ID, true);
            }

            if (selectedPosition == 5) {
                int day = DateFormatter.getDayofMonth();
                fromDate = DateFormatter.getLastMonthFirstDate();
                toDate = DateFormatter.getLastMonthLastDate();
                String showingText1 = "" + DateFormatter.doFormat(fromDate, DateFormatter.DD_MM_YYYY, DateFormatter.MMM_dd_yyyy);
                String showingText2 = "" + DateFormatter.doFormat(toDate, DateFormatter.DD_MM_YYYY, DateFormatter.MMM_dd_yyyy);
                textViewFilter.setText(showingText1 + " - " + showingText2);
                toolbar.setSubtitle(showingText1 + " - " + showingText2);
                dashBoardPresenter.onClickGetOrder(ConstantValues.ORDER_ONLINE_LIST_API_ID, true);
            }

            if (selectedPosition == 6) {
                Intent intent = new Intent(DashBoardActivity.this, CustomRangeActivity.class);
                startActivityForResult(intent, 100);
            }
        } catch (Exception e) {

        }
    }


    View.OnClickListener filterClickListener(final int selectedPos, final ImageView imgViewSelected) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedPosition = selectedPos;
                prevSelectedImgView.setVisibility(View.INVISIBLE);
                imgViewSelected.setVisibility(View.VISIBLE);
                prevSelectedImgView = imgViewSelected;
            }
        };
    }


    private void showFilterOptionDialog() {
        final Dialog dialog = new Dialog(DashBoardActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.dialog_filter);
        final TextView txtAccept = (TextView) dialog.findViewById(R.id.txtAccept);
        // if button is clicked, close the custom dialog
        txtAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    mLastClickTime = SystemClock.elapsedRealtime();
                    return;
                } else {
                    mLastClickTime = SystemClock.elapsedRealtime();
                }
                dialog.dismiss();
                filterOptionSelected();
            }

        });

        final TextView txtCancel = (TextView) dialog.findViewById(R.id.txtCancel);
        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        RelativeLayout[] relativeOptions = new RelativeLayout[8];
        ImageView[] imgOptions = new ImageView[8];

        int[] relativeIds = {R.id.relativeToday, R.id.relativeYesterday, R.id.relativeLast7, R.id.relativeLast30, R.id.relativeThisMonth, R.id.relativeLastMonth, R.id.relativeCustomRange};
        int[] imgIds = {R.id.imgViewToday, R.id.imgViewYesterday, R.id.imgViewLast7, R.id.imgViewLast30, R.id.imgViewThisMonth, R.id.imgViewLastMonth, R.id.imgViewCustomRange};

        for(int i = 0; i < relativeIds.length; i++) {
            relativeOptions[i] = (RelativeLayout) dialog.findViewById(relativeIds[i]);
            imgOptions[i] = (ImageView) dialog.findViewById(imgIds[i]);
            relativeOptions[i].setOnClickListener(filterClickListener(i, imgOptions[i]));
        }

        prevSelectedImgView = imgOptions[selectedPosition];
        imgOptions[selectedPosition].setVisibility(View.VISIBLE);

        dialog.show();
    }


    private void initAnimation() {
        relative_show_more = (RelativeLayout) findViewById(R.id.relative_show_more);
        textViewShowMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (relative_show_more.getVisibility() == View.GONE){
                    textViewShowMore.setText("Hide More");
                    expand();
                }else{
                    collapse();
                    textViewShowMore.setText("Show More");
                }
            }
        });
    }

    private void expand() {
        //set Visible
        relative_show_more.setVisibility(View.VISIBLE);

        final int widthSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        final int heightSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        relative_show_more.measure(widthSpec, heightSpec);

        ValueAnimator mAnimator = slideAnimator(0, relative_show_more.getMeasuredHeight());
        mAnimator.start();
    }


    private void collapse() {
        int finalHeight = relative_show_more.getHeight();

        ValueAnimator mAnimator = slideAnimator(finalHeight, 0);

        mAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                //Height=0, but it set visibility to GONE
                relative_show_more.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        mAnimator.start();
    }

    private ValueAnimator slideAnimator(int start, int end) {

        ValueAnimator animator = ValueAnimator.ofInt(start, end);

        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                //Update Height
                int value = (Integer) valueAnimator.getAnimatedValue();
                ViewGroup.LayoutParams layoutParams = relative_show_more.getLayoutParams();
                layoutParams.height = value;
                relative_show_more.setLayoutParams(layoutParams);
            }
        });
        return animator;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnCustomRange:
                Intent intent = new Intent(DashBoardActivity.this, CustomRangeActivity.class);
                startActivityForResult(intent, 100);
                break;
            case R.id.floatBtnFilter:
                showFilterOptionDialog();
                break;


        }


    }


    @Override
    public void onConnectivityChanged() {
        if(UtilityMethod.isConnectedToInternet(this)){
            textViewConnectivity.setBackgroundColor(ContextCompat.getColor(this, R.color.green));
            textViewConnectivity.setText("Connected");
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    textViewConnectivity.setVisibility(View.GONE);
                }
            }, 1000);
        } else {
            textViewConnectivity.setBackgroundColor(ContextCompat.getColor(this, R.color.amber_dark));
            textViewConnectivity.setText("Waiting for network");
            textViewConnectivity.setVisibility(View.VISIBLE);
        }
    }
}
