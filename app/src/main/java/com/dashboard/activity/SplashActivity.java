package com.dashboard.activity;

import android.Manifest;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.chefonline.managermobile.R;
import com.dashboard.alarm.AlarmReceiver;
import com.dashboard.presenter.SplashPresenter;
import com.dashboard.utility.ConstantValues;
import com.dashboard.utility.PreferenceUtil;
import com.dashboard.utility.UtilityMethod;
import com.dashboard.view.SplashView;

import java.util.Calendar;

public class SplashActivity extends AppCompatActivity implements SplashView{

    private long mLastClickTime = 0;
    private final int MY_PERMISSIONS_REQUEST_READ_PHONE_STATE = 1;
    String szImei = "";
    boolean isEverythingOk = false;
    TextView textViewVersion;
    String playStoreVersion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        initView();
        checkDeviceId();

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isEverythingOk) {
            initialWork();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        isEverythingOk = true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_view_orders, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_PHONE_STATE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                    szImei = UtilityMethod.getDeviceImeiId(this); // Requires

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                setDeviceId();
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }


    private void initView() {
        textViewVersion = (TextView) findViewById(R.id.textViewVersion);
        String versionName = getVersionName();
        textViewVersion.setText("Manager V " + versionName);
    }

    private String getVersionName()
    {
        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;
            return version;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return "";
        }
    }


    private void showPermissionDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_exit);
        TextView txtViewPopupMessage = (TextView) dialog.findViewById(R.id.txtViewPopupMessage);
        txtViewPopupMessage.setText("Sorry! READ_PHONE_STATE permission must be needed to register for push notification. Please accept the permission.");
        final TextView txtAccept = (TextView) dialog.findViewById(R.id.txtAccept);
        txtAccept.setText("SHOW PERMISSION");

        // if button is clicked, close the custom dialog
        txtAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                checkReadPhoneStatePermission();
            }

        });

        final TextView txtCancel = (TextView) dialog.findViewById(R.id.txtCancel);
        txtCancel.setText("CANCEL");
        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                finish();
            }

        });

        dialog.show();

    }

    private void checkReadPhoneStatePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                ActivityCompat.requestPermissions(SplashActivity.this, new String[]{Manifest.permission.READ_PHONE_STATE}, MY_PERMISSIONS_REQUEST_READ_PHONE_STATE);
            } else {
                szImei = UtilityMethod.getDeviceImeiId(this); // Requires
                setDeviceId();
            }
        } else {
            szImei = UtilityMethod.getDeviceImeiId(this); // Requires
            setDeviceId();
        }
    }

    private void checkDeviceId() {
        PreferenceUtil preferenceUtil = new PreferenceUtil(SplashActivity.this);
        szImei = preferenceUtil.getUUID();
        if (szImei.equals("")) {
            checkReadPhoneStatePermission();
        } else {
            initialWork();
        }
    }

    private void setDeviceId() {

        if (szImei.equals("")) {
            showPermissionDialog();
        } else {
            PreferenceUtil preferenceUtil = new PreferenceUtil(this);
            preferenceUtil.setUUID(szImei);

            checkDeviceId();
        }

    }


    private void initialWork() {

        if (!UtilityMethod.isConnectedToInternet(getApplicationContext())) {
            //isEverythingOk = true;
            openDialog("Please check your internet connection.", "RETRY", "CANCEL");
            return;
        }

        if(playStoreVersion == null) {
            SplashPresenter splashPresenter = new SplashPresenter(this);
            splashPresenter.getPlayStoreVersion("7");
        } else {
            checkUpdate();
        }
      //  setAlarmAndCallNext();

    }


    private void checkUpdate() {
        Log.e("Version", getVersionName());
        setAlarmAndCallNext();
        /*if(playStoreVersion.equalsIgnoreCase(getVersionName()) || playStoreVersion.equalsIgnoreCase("-1")) {
            setAlarmAndCallNext();
        } else {
            showUpdateDialog();
            //setAlarmAndCallNext(); // when do not need version checking
        }*/
    }


    private void setAlarmAndCallNext() {
        setFixedTimeAlarm();

        Thread timer = new Thread() {
            public void run() {
                try {
                    sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    PreferenceUtil preferenceUtil = new PreferenceUtil(SplashActivity.this);
                    if (preferenceUtil.getLogInStatus() == 1) {
                        if(preferenceUtil.getUSER_GROUP_ID().equalsIgnoreCase("1")) {
                            Intent intent = new Intent(SplashActivity.this, RestaurantListActivity.class);
                          //  intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        } else {
                            Intent intent = new Intent(SplashActivity.this, DashBoardTodayActivityMain.class);
                           // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        }
                    } else {
                        Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                      //  intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }

                    finish();

                }
            }
        };
        timer.start();
    }


    private void showUpdateDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_exit);
        TextView txtViewPopupMessage = (TextView) dialog.findViewById(R.id.txtViewPopupMessage);
        txtViewPopupMessage.setText("An updated version of this application is available in Google Play Store. Please update this application.");
        final TextView txtAccept = (TextView) dialog.findViewById(R.id.txtAccept);
        txtAccept.setText("UPDATE");

        // if button is clicked, close the custom dialog
        txtAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Uri uri = Uri.parse("market://details?id=" + getPackageName());
                Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
                try {
                    startActivity(myAppLinkToMarket);
                    finish();
                } catch (ActivityNotFoundException e) {
                    setAlarmAndCallNext();
                }
            }

        });

        final TextView txtCancel = (TextView) dialog.findViewById(R.id.txtCancel);
        txtCancel.setText("CANCEL");
        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                finish();
            }

        });

        dialog.show();
    }


    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    @Override
    public void setPlayStoreVersion(String version) {
        playStoreVersion = version;
        ConstantValues.PLAY_STORE_VERSION = version;
        if(playStoreVersion == null) {
            setAlarmAndCallNext();
        } else {
            checkUpdate();
        }
    }


    private void openDialog(String message, String okButton, String cancelButton) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.dialog_exit);
        TextView txtViewPopupMessage = (TextView) dialog.findViewById(R.id.txtViewPopupMessage);
        txtViewPopupMessage.setText(message);
        final TextView txtAccept = (TextView) dialog.findViewById(R.id.txtAccept);
        txtAccept.setText(okButton);

        // if button is clicked, close the custom dialog
        txtAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    mLastClickTime = SystemClock.elapsedRealtime();
                    return;
                } else {
                    mLastClickTime = SystemClock.elapsedRealtime();
                }
                dialog.dismiss();
                initialWork();
            }

        });

        final TextView txtCancel = (TextView) dialog.findViewById(R.id.txtCancel);
        txtCancel.setText(cancelButton);
        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                onBackPressed();
            }

        });

        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                onBackPressed();
            }
        });

        dialog.show();
    }


    private void setFixedTimeAlarm() {
        Intent myIntent = new Intent(SplashActivity.this, AlarmReceiver.class);
        myIntent.setAction(ConstantValues.ALARM_ACTION);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, myIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);

        Calendar firingCal = Calendar.getInstance();
        Calendar currentCal = Calendar.getInstance();

        firingCal.set(Calendar.HOUR_OF_DAY, 5); // At the hour you wanna fire
        firingCal.set(Calendar.MINUTE, 0); // Particular minute
        firingCal.set(Calendar.SECOND, 0); // particular second

        long intendedTime = firingCal.getTimeInMillis();
        long currentTime = currentCal.getTimeInMillis();

        if (intendedTime >= currentTime) // you can add buffer time too here to ignore some small differences in milliseconds
        {
            //set from today
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, intendedTime, AlarmManager.INTERVAL_DAY, pendingIntent);

        } else {
            //set from next day
            //you might consider using calendar.add() for adding one day to the current day
            firingCal.add(Calendar.DAY_OF_MONTH, 1);
            intendedTime = firingCal.getTimeInMillis();
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, intendedTime, AlarmManager.INTERVAL_DAY, pendingIntent);
        }
    }

}
