package com.dashboard.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.chefonline.managermobile.R;
import com.dashboard.customview.CustomToast;
import com.dashboard.presenter.CallbackSupportPresenter;
import com.dashboard.singleton.ApplicationVisibleState;
import com.dashboard.utility.PreferenceUtil;
import com.dashboard.utility.UtilityMethod;
import com.dashboard.view.CallbackSuportView;

/**
 * Created by user on 2/4/2016.
 */
public class CallbackSupportActivity extends AppCompatActivity implements View.OnClickListener, CallbackSuportView{

    EditText edtDesc;
    Button btnSubmit;
    private ProgressDialog progressDialog;

    CallbackSupportPresenter presenter;
    private long mLastClickTime = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_callback_support);
        initView();
        setUiClickHandler();
        presenter = new CallbackSupportPresenter(this);
    }

    @Override
    protected void onPause() {
        ApplicationVisibleState.activityPaused();
        super.onPause();
    }

    @Override
    protected void onResume() {
        ApplicationVisibleState.activityResumed();
        super.onResume();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        if(savedInstanceState != null) {
            edtDesc.setText(savedInstanceState.getString("input_text"));
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString("input_text", edtDesc.getText().toString());
    }


    private void initView() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Callback Support");
        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }
        edtDesc = (EditText) findViewById(R.id.edtDesc);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
    }

    private void setUiClickHandler() {
        btnSubmit.setOnClickListener(this);
    }

    @Override
    public Context getClassContext() {
        return this.getApplicationContext();
    }

    @Override
    public void onSuccessSubmission() {
        onBackPressed();
    }

    @Override
    public void showProgressDialog() {
        progressDialog = new ProgressDialog(CallbackSupportActivity.this);
        progressDialog.setMessage("Please wait....");
        progressDialog.show();
    }

    @Override
    public void dismissDialog() {
        progressDialog.dismiss();
    }

    @Override
    public void showToast(String message, String subMessage, boolean isButtonOk) {
        new CustomToast(CallbackSupportActivity.this, message, subMessage, isButtonOk);
    }


    @Override
    public void onClick(View v) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000){
            mLastClickTime = SystemClock.elapsedRealtime();
            return;
        }
        else {
            mLastClickTime = SystemClock.elapsedRealtime();
        }
        if(v.getId() == R.id.btnSubmit) {
            if(edtDesc.getText().toString().trim().equals("")) {
                new CustomToast(CallbackSupportActivity.this, "Please type your text", "", false);
            }
            else {
                long upTime = UtilityMethod.getDeviceUpTime();
                long hour = upTime/3600;
                long remainingTime = upTime - (hour*3600);
                long minute = remainingTime/60;
                long second = remainingTime - (minute*60);
                String time = "";
                if(hour != 0) {
                    if(hour == 1) time += hour + " Hour ";
                    else time += hour + " Hours ";
                }
                if(minute != 0) {
                    if(minute == 1) time += minute + "Minute ";
                    else time += minute + "Minutes ";
                }
                if(second != 0) {
                    if(second == 1) time += second + " Second";
                    else time += second + " Seconds";
                }
                time = time.trim();
                PreferenceUtil myUtil = new PreferenceUtil(CallbackSupportActivity.this);
                presenter.onClickSubmitQuery(myUtil.getUUID(), UtilityMethod.getDeviceSerialNumber(this), UtilityMethod.getBatteryStatus(this), UtilityMethod.getBatteryLevel(this)+"%", UtilityMethod.getDeviceIpAddress(), time, UtilityMethod.getWifiMacAddress(this), myUtil.getResID(), myUtil.getRestaurantName(), myUtil.getUserEmail(), edtDesc.getText().toString().trim());
            }
        }
    }


}
