package com.dashboard.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.chefonline.managermobile.R;
import com.dashboard.presenter.PrinterSettingsPresenter;
import com.dashboard.singleton.ApplicationVisibleState;
import com.dashboard.view.PrinterSettingsView;


/**
 * Created by user on 1/6/2016.
 */
public class PrinterSettingsActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener, PrinterSettingsView {
   // ToggleButton toggleKitchenCopy;
   // ToggleButton toggleReservationCopy;
    CheckBox checkKitchenCopy;
    CheckBox checkReservationCopy;
    PrinterSettingsPresenter printerSettingsPresenter;
    private long mLastClickTime = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_printer_settings2);
        printerSettingsPresenter = new PrinterSettingsPresenter(this);
        initView();
        setUiClickHandler();
        loadData();
    }

    @Override
    protected void onPause() {
        ApplicationVisibleState.activityPaused();
        super.onPause();
    }

    @Override
    protected void onResume() {
        ApplicationVisibleState.activityResumed();
        super.onResume();
    }

    private void initView() {
        checkKitchenCopy = (CheckBox) findViewById(R.id.checkKitchenCopy);
        checkReservationCopy = (CheckBox) findViewById(R.id.checkReservationCopy);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Printer Settings");
        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

    }

    private void setUiClickHandler() {
        checkKitchenCopy.setOnCheckedChangeListener(this);
        checkReservationCopy.setOnCheckedChangeListener(this);
    }


    private void loadData() {
        printerSettingsPresenter.setIsKitchenCopyActive();
        printerSettingsPresenter.setIsReservationCopyActive();
    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(buttonView.getId() == R.id.checkKitchenCopy)  {
            printerSettingsPresenter.setKitchenPreference(isChecked);
        } else if(buttonView.getId() == R.id.checkReservationCopy) {
            printerSettingsPresenter.setReservationPreference(isChecked);
        }

    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    @Override
    public void setKitchenActive(boolean status) {
        checkKitchenCopy.setChecked(status);
    }

    @Override
    public void setReservationActive(boolean status) {
        checkReservationCopy.setChecked(status);
    }

}
