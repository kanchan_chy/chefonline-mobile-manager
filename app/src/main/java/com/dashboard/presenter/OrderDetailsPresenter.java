package com.dashboard.presenter;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

import com.dashboard.datamodel.CustomerDataSerialize;
import com.dashboard.datamodel.OrderData;
import com.dashboard.jsonparser.JsonParser;
import com.dashboard.datamodel.OrderDetailsData;
import com.dashboard.services.OrderDetailsService;
import com.dashboard.services.OrderDetailsApiCallBack;
import com.dashboard.utility.PreferenceUtil;
import com.dashboard.utility.UtilityMethod;
import com.dashboard.view.OnRequestComplete;
import com.dashboard.view.OrderDetailsView;

/**
 * Created by masum on 22/08/2015.
 */
public class OrderDetailsPresenter {
    OrderDetailsView orderDetailsView;
    OrderDetailsService dashBoardService;

    public OrderDetailsPresenter(OrderDetailsView dashBoardView, OrderDetailsService dashBoardService) {
        this.orderDetailsView = dashBoardView;
        this.dashBoardService = dashBoardService;
    }

    public void onClickGetOrder() {
        final ProgressDialog progressDialog = orderDetailsView.showProgressDialog();
        new OrderDetailsApiCallBack(orderDetailsView.getClassContext(), orderDetailsView.getOrderNo(), new OnRequestComplete() {
            @Override
            public void onRequestComplete(String response) {
                try {
                    progressDialog.dismiss();
                    Log.e("Order Details", "" + response);

                    OrderDetailsData dishDatas = JsonParser.parseOrderDetailsData(response);

                    if (dishDatas != null) {
                        if (dishDatas.getDishDatas().size() > 0) {
                            orderDetailsView.setOrderData(dishDatas);
                            orderDetailsView.setTotal("Total: £" + UtilityMethod.priceFormatterRound(Double.valueOf(dishDatas.getTotalAmount())));
                            orderDetailsView.setRestaurantName(dishDatas.getRestaurantName());
                            orderDetailsView.setPaymentMethod(dishDatas.getPaymentMethod());
                            orderDetailsView.setOrderQuantity(dishDatas.getOrderItemCount());
                        }

                    }

                } catch (Exception e) {

                }
            }
        });

    }

    public static void onClickGetOrderDetailsFromNotification(final Context context, final OrderData orderData) {
        //final ProgressDialog progressDialog = orderDetailsView.showProgressDialog();
        new OrderDetailsApiCallBack(context, orderData.getOrderNo(), new OnRequestComplete() {
            @Override
            public void onRequestComplete(String response) {
                //progressDialog.dismiss();
                Log.e("Order Details", "" + response);
                try {
                    OrderDetailsData dishDatas = JsonParser.parseOrderDetailsData(response);

                    if (dishDatas != null) {
                        if (dishDatas.getDishDatas().size() > 0) {
                            CustomerDataSerialize serialUserData = new CustomerDataSerialize(4, "Chef Online", orderData);
                            UtilityMethod.printBill(context, dishDatas, serialUserData);
                            if(new PreferenceUtil(context).getKitchenCopy()) {
                                UtilityMethod.printBillKitchen(context, dishDatas, serialUserData);
                            }

                            //DashBoardTodayFragment.dashBoardPresenter.postPrintStatus();
                        }

                    }

                } catch (Exception e) {
                    Log.e("Print Exception", "" + e.getMessage());
                }
            }
        });

    }


}
