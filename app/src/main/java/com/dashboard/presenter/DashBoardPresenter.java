package com.dashboard.presenter;

import android.app.ProgressDialog;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.dashboard.datamodel.OrderData;
import com.dashboard.datamodel.ReservationDataUnconfirmed;
import com.dashboard.datamodel.ScheduleData;
import com.dashboard.jsonparser.JsonParser;
import com.dashboard.services.DashBoardOrderApiCallBack;
import com.dashboard.services.DashBoardService;
import com.dashboard.services.ReservationApiCall;
import com.dashboard.services.RestaurantScheduleApiCall;
import com.dashboard.singleton.AppData;
import com.dashboard.utility.UtilityMethod;
import com.dashboard.view.DashBoardInterface;
import com.dashboard.view.DashBoardView;
import com.dashboard.view.OnRequestComplete;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by masum on 22/08/2015.
 */
public class DashBoardPresenter {
    public static String TAG = "DashBoardPresenter";
    DashBoardView dashBoardView;
    DashBoardService dashBoardService;
    ProgressDialog progressDialog;

    public DashBoardPresenter(DashBoardView dashBoardView, DashBoardService dashBoardService) {
        this.dashBoardView = dashBoardView;
        this.dashBoardService = dashBoardService;
    }

    public void onClickGetOrder(final String functionId, final boolean isProgressVisible) {
        if (isProgressVisible) {
            progressDialog = dashBoardView.showProgressDialog();
        }

        new DashBoardOrderApiCallBack(functionId, dashBoardView.getClassContext(), dashBoardView.getRestaurantId(),
                dashBoardView.getStartDate(), dashBoardView.getEndDate(), new DashBoardInterface() {
            @Override
            public void onRequestComplete(String response) {
                Log.i(TAG, "Dash Board Response" + response);
                ArrayList<OrderData> orderDatas = new ArrayList<>();
                try {
                    if (isProgressVisible) {
                        progressDialog.dismiss();
                    }

                    orderDatas = JsonParser.parseOrderData(response);

                    if (orderDatas.size() > 0) {
                        int deliveryOrder = 0, collectionOrder = 0;
                        for( int i = 0; i < orderDatas.size(); i++) {
                            if(orderDatas.get(i).getOrderType().equalsIgnoreCase("collection")) {
                                collectionOrder ++;
                            }
                            if(orderDatas.get(i).getOrderType().equalsIgnoreCase("delivery")) {
                                deliveryOrder ++;
                            }
                        }
                        dashBoardView.setPaymentData(AppData.getInstance().getPaymentDatas());
                      //  dashBoardView.setCash("" + UtilityMethod.priceFormatter(AppData.getInstance().getTotalCash()));
                     //   dashBoardView.setPayPal("" + UtilityMethod.priceFormatter(AppData.getInstance().getTotalPaypal()));
                        dashBoardView.setTotal("" + UtilityMethod.priceFormatter(AppData.getInstance().getGrandTotal()));
                        if (orderDatas.size() > 1) dashBoardView.setTotalOrder("" + orderDatas.size());
                        else dashBoardView.setTotalOrder("" + orderDatas.size());
                        dashBoardView.setDeliveryOrder("" + deliveryOrder);
                        dashBoardView.setCollectionOrder("" + collectionOrder);
                        dashBoardView.setOrderData(orderDatas);
                        dashBoardView.showEmptyView(false);

                    } else {
                        dashBoardView.setPaymentData(AppData.getInstance().getPaymentDatas());
                     //   dashBoardView.setCash("0.00");
                      //  dashBoardView.setPayPal("0.00");
                        dashBoardView.setTotal("0.00");
                        dashBoardView.setTotalOrder("0");
                        dashBoardView.setDeliveryOrder("0");
                        dashBoardView.setCollectionOrder("0");
                        dashBoardView.setOrderData(orderDatas);
                        dashBoardView.showEmptyView(true);

                    }

                } catch (Exception e) {
                    try {
                        dashBoardView.setPaymentData(AppData.getInstance().getPaymentDatas());
                        //   dashBoardView.setCash("0.00");
                        //  dashBoardView.setPayPal("0.00");
                        dashBoardView.setTotal("0.00");
                        dashBoardView.setTotalOrder("0");
                        dashBoardView.setDeliveryOrder("0");
                        dashBoardView.setCollectionOrder("0");
                        dashBoardView.setOrderData(orderDatas);
                        dashBoardView.showEmptyView(true);
                        Toast.makeText(dashBoardView.getClassContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
                    } catch (Exception e2) {

                    }
                }

            }

        });

    }

    public void onClickGetReservation(final String functionId, final String status, final boolean isProgressVisible) {
        if (isProgressVisible) {
            progressDialog = dashBoardView.showProgressDialog();
        }

        new ReservationApiCall().callReservationList(dashBoardView.getClassContext(), functionId, dashBoardView.getStartDate(), dashBoardView.getEndDate(), status, dashBoardView.getRestaurantId(), new OnRequestComplete() {
            @Override
            public void onRequestComplete(String response) {
                Log.i(TAG, "Reservation Response " + response);
                ArrayList<ReservationDataUnconfirmed> reservationDataUnconfirmeds = new ArrayList<>();
                try {
                    if (isProgressVisible) {
                        progressDialog.dismiss();
                    }

                    reservationDataUnconfirmeds = JsonParser.parseReservationData(response);
                    if(reservationDataUnconfirmeds.size() > 0) {
                        dashBoardView.setReservationData(reservationDataUnconfirmeds);
                        dashBoardView.showEmptyView(false);
                    } else {
                        dashBoardView.setReservationData(reservationDataUnconfirmeds);
                        dashBoardView.showEmptyView(true);
                    }

                } catch (Exception e) {
                    try {
                        dashBoardView.setReservationData(reservationDataUnconfirmeds);
                        dashBoardView.showEmptyView(true);
                        Toast.makeText(dashBoardView.getClassContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
                    }
                    catch (Exception e1) {

                    }
                }
            }
        });

        /*new DashBoardOrderApiCallBack(functionId, dashBoardView.getClassContext(), dashBoardView.getRestaurantId(), dashBoardView.getStartDate(), dashBoardView.getEndDate(), new DashBoardInterface() {
            @Override
            public void onRequestComplete(String response) {
                Log.i(TAG, "Reservation Response " + response);
                try {
                    if (isProgressVisible) {
                        progressDialog.dismiss();
                    }

                    ArrayList<ReservationData> reservationDatas = new ArrayList<>();
                    reservationDatas = JsonParser.parseReservationData(response);
                    if(reservationDatas.size() > 0) {
                        dashBoardView.setReservationData(reservationDatas);
                    } else {
                        dashBoardView.showEmptyView(true);
                    }

                } catch (Exception e) {
                    try {
                        dashBoardView.showEmptyView(true);
                        Toast.makeText(dashBoardView.getClassContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
                    }
                    catch (Exception e1) {

                    }
                }

            }

        });*/

    }

    public void onClickScheduleData() {
       final ProgressDialog progressDialog = dashBoardView.showProgressDialog();
        new RestaurantScheduleApiCall(dashBoardView.getClassContext(), dashBoardView.getRestaurantId(), new OnRequestComplete() {
            @Override
            public void onRequestComplete(String response) {
                //Log.e("Order Details", "" + response);
                ArrayList<ScheduleData> scheduleDatas = new ArrayList<>();
                try {
                    progressDialog.dismiss();
                    scheduleDatas = JsonParser.getScheduleData(response);

                    if (scheduleDatas != null) {
                        if (scheduleDatas.size() > 0) {
                            dashBoardView.setScheduleData(scheduleDatas);

                            SimpleDateFormat timeFormat = new SimpleDateFormat("h:mm a");
                            SimpleDateFormat sdfDay = new SimpleDateFormat("EEEE");
                            String currentDateTime = timeFormat.format(new Date());
                            String today = sdfDay.format(new Date());

                            //Compare before -1, after 1, equal 0
                            for (int i = 0; i < scheduleDatas.size(); i++) {
                                if (today.equalsIgnoreCase(scheduleDatas.get(i).getDayName())) {
                                    Date date1 = timeFormat.parse(scheduleDatas.get(i).getOpeningTime());
                                    if (date1.compareTo(timeFormat.parse(currentDateTime)) == 0
                                            || date1.compareTo(timeFormat.parse(currentDateTime)) == 1) {
                                        //Call change status API
                                    }
                                }
                            }

                        }

                    }

                } catch (Exception e) {

                }
            }
        });

    }

    public void checkingIfRestaurantOpen(final ArrayList<ScheduleData> scheduleDatas) {
        final Handler handler = new Handler();
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {

                try {
                    SimpleDateFormat timeFormatFull = new SimpleDateFormat("h:mm:ss a");
                    SimpleDateFormat timeFormat = new SimpleDateFormat("h:mm a");
                    SimpleDateFormat sdfDay = new SimpleDateFormat("EEEE");
                    String currentDateTime = timeFormat.format(new Date());
                    String currentDateTimeFull = timeFormatFull.format(new Date());
                    String today = sdfDay.format(new Date());

                    //Compare before -1, after 1, equal 0
                    for (int i = 0; i < scheduleDatas.size(); i++) {
                        if (today.equalsIgnoreCase(scheduleDatas.get(i).getDayName())) {
                            Date date1 = timeFormat.parse(scheduleDatas.get(i).getOpeningTime());
                            if (date1.compareTo(timeFormat.parse(currentDateTime)) == 0) {

                                Log.i("CALLING>>>", ".....");
                                handler.removeCallbacks(this);
                                //postRestaurantStatus();
                                //Call change status API
                            }
                        }
                    }

                    Log.i("CURRENT TIME", "* " + timeFormatFull.parse(currentDateTimeFull));


                    handler.postDelayed(this, 1000);

                } catch (Exception e) {
                    Log.i("EXCEPTION", "##" + e.getMessage());
                    handler.postDelayed(this, 1000);
                }
            }
        };

        //handler.postDelayed(runnable, 1000);
    }


}
