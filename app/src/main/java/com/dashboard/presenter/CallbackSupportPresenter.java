package com.dashboard.presenter;

import android.util.Log;

import com.android.volley.VolleyError;
import com.dashboard.services.CallbackSupportApiCallback;
import com.dashboard.view.CallbackSuportView;
import com.dashboard.view.VolleyApiInterface;

import org.json.JSONObject;


public class CallbackSupportPresenter {
    public static String TAG = "SettingPresenter";
    CallbackSuportView callbackSuportView;

    public CallbackSupportPresenter(CallbackSuportView callbackSuportView) {
        this.callbackSuportView = callbackSuportView;
    }

    public void onClickSubmitQuery(String deviceId, String deviceOsSerial, String batterySattus, String batteryLevel, String ipAddress,String upTime, String wifiMac, String restId, String restName, String email, String query) {
        callbackSuportView.showProgressDialog();
        new  CallbackSupportApiCallback(callbackSuportView.getClassContext(), deviceId, deviceOsSerial, batterySattus, batteryLevel, ipAddress, upTime, wifiMac, restId, restName, email, query, new VolleyApiInterface() {

            @Override
            public void onRequestSuccess(String response) {
                try {
                    Log.e("Callback Response", response);
                    callbackSuportView.dismissDialog();
                    JSONObject jsonObject = new JSONObject(response);

                    if ("Success".equalsIgnoreCase(jsonObject.getString("status"))) {
                        callbackSuportView.showToast(jsonObject.getString("msg"), "", true);
                        callbackSuportView.onSuccessSubmission();

                    } else {
                        callbackSuportView.showToast(jsonObject.getString("msg"), "", false);
                    }

                } catch (Exception e) {
                    try {
                        callbackSuportView.showToast("Something went wrong", "", false);
                    } catch (Exception e1) {

                    }
                }
            }

            @Override
            public void onRequestFailed(VolleyError error) {
                try {
                    callbackSuportView.dismissDialog();
                    callbackSuportView.showToast("Something went wrong", "", false);
                }catch (Exception e) {
                }
            }

        });

    }


}
