package com.dashboard.presenter;

import android.util.Log;

import com.android.volley.VolleyError;
import com.dashboard.services.ForgotPincodeApiCallback;
import com.dashboard.services.LogoutApiCallback;
import com.dashboard.view.SettingsView;
import com.dashboard.view.VolleyApiInterface;

import org.json.JSONObject;


public class SettingPresenter {
    public static String TAG = "SettingPresenter";
    SettingsView settingsView;

    public SettingPresenter(SettingsView settingsView) {
        this.settingsView = settingsView;
    }

    public void onClickForgotPincode(String mobile, String pincode) {
        settingsView.showProgressDialog();
        new ForgotPincodeApiCallback(settingsView.getClassContext(), mobile, pincode, new VolleyApiInterface() {

            @Override
            public void onRequestSuccess(String response) {
                try {
                    settingsView.dismissDialog();
                    JSONObject jsonObject = new JSONObject(response);
                    JSONObject jsonAppObject = jsonObject.getJSONObject("app");
                    Log.e(TAG, response);

                    if ("Success".equalsIgnoreCase(jsonAppObject.getString("status"))) {
                        settingsView.showToast(jsonAppObject.getString("msg"), "", true);

                    } else {
                        settingsView.showToast(jsonAppObject.getString("msg"), "", false);
                    }

                } catch (Exception e) {
                    try {
                        settingsView.showToast("Something went wrong", "", false);
                    }catch (Exception e1) {
                    }
                }
            }

            @Override
            public void onRequestFailed(VolleyError error) {
                try {
                    settingsView.showToast("Something went wrong", "", false);
                }catch (Exception e) {
                }
            }

        });

    }


    public void onClickLogout(String userId, String deviceId) {
        settingsView.showProgressDialog();
        new LogoutApiCallback(settingsView.getClassContext(), userId, deviceId, new VolleyApiInterface() {

            @Override
            public void onRequestSuccess(String response) {
                try {
                    Log.e(TAG, response);
                    settingsView.dismissDialog();
                    JSONObject jsonObject = new JSONObject(response);
                    if ("Success".equalsIgnoreCase(jsonObject.getString("status"))) {
                        settingsView.makeLogout();

                    } else {
                        settingsView.showToast("Sorry, logout failed", "", false);
                    }

                } catch (Exception e) {
                    try {
                        settingsView.showToast("Something went wrong", "", false);
                    }catch (Exception e1) {
                    }
                }
            }

            @Override
            public void onRequestFailed(VolleyError error) {
                try {
                    settingsView.showToast("Something went wrong", "", false);
                }catch (Exception e) {
                }
            }

        });

    }


}
