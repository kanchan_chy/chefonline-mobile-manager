package com.dashboard.presenter;

import com.dashboard.datamodel.OrderOnlineStatusData;
import com.dashboard.jsonparser.JsonParser;
import com.dashboard.services.AdminSettingsApiCall;
import com.dashboard.view.AdminSettingsActivityView;
import com.dashboard.view.OnRequestComplete;

/**
 * Created by masum on 24/01/2016.
 */
public class AdminSettingsPresenter {
    AdminSettingsActivityView adminSettingsActivityView;
    public AdminSettingsPresenter(AdminSettingsActivityView adminSettingsActivityView) {
        this.adminSettingsActivityView = adminSettingsActivityView;
    }

    public void getRestaurantOrderOnlineStatus(String restaurantId) {
        adminSettingsActivityView.showLoading();
        new AdminSettingsApiCall().callRestaurantOrderOnlineStatusAPI(adminSettingsActivityView.getAppContext(), restaurantId, new OnRequestComplete() {
            @Override
            public void onRequestComplete(String response) {
                //Toast.makeText(adminSettingsActivityView.getAppContext(), "" + response, Toast.LENGTH_SHORT).show();
                try {
                    OrderOnlineStatusData orderOnlineStatusData = JsonParser.getRestaurantOnlineOrderStatus(response);

                    if ("Success".equalsIgnoreCase(orderOnlineStatusData.getSuccess())) {
                        adminSettingsActivityView.setOrderOnlineStatus(orderOnlineStatusData.getIsComingSoon());
                    } else {
                        adminSettingsActivityView.setEnableOrderOnline(false);
                    }
                    adminSettingsActivityView.visibleUI();
                    adminSettingsActivityView.hideLoading();
                } catch (Exception e) {

                }
            }
        });
    }

    public void postRestaurantOrderOnlineStatus(String restaurantId, String status) {
        adminSettingsActivityView.showLoading();
        new AdminSettingsApiCall().callChangeOrderOnlineStatusAPI(adminSettingsActivityView.getAppContext(), restaurantId, status, new OnRequestComplete() {
            @Override
            public void onRequestComplete(String response) {
                try {
                    OrderOnlineStatusData orderOnlineStatusData = JsonParser.getRestaurantOnlineOrderStatus(response);
                    adminSettingsActivityView.setOrderOnlineStatus(orderOnlineStatusData.getIsComingSoon());

                    if ("Error".equalsIgnoreCase(orderOnlineStatusData.getSuccess())) {
                        adminSettingsActivityView.setEnableOrderOnline(false);
                    } else {
                        adminSettingsActivityView.setEnableOrderOnline(true);
                    }
                    adminSettingsActivityView.visibleUI();
                    adminSettingsActivityView.hideLoading();
                } catch (Exception e) {

                }
            }
        });
    }
}
