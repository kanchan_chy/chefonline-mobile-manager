package com.dashboard.presenter;

import android.util.Log;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.dashboard.datamodel.EndOfDayData;
import com.dashboard.datamodel.EndOfDayPayment;
import com.dashboard.datamodel.EndOfDayUser;
import com.dashboard.jsonparser.JsonParser;
import com.dashboard.services.EndOfDayReportApiCallBack;
import com.dashboard.utility.UtilityMethod;
import com.dashboard.view.EndOfDayView;
import com.dashboard.view.VolleyApiInterface;

import java.util.ArrayList;

/**
 * Created by masum on 22/08/2015.
 */
public class EndOfDayPresenter {
    public static String TAG = "EndOfDayPresenter";
    EndOfDayView endOfDayView;

    public EndOfDayPresenter(EndOfDayView endOdDayView) {
        this.endOfDayView = endOdDayView;
    }

    public void loadListData() {
        endOfDayView.showProgressDialog();
        new EndOfDayReportApiCallBack(endOfDayView.getClassContext(), endOfDayView.getRestaurantId(), endOfDayView.getCurrentDateTime(), new VolleyApiInterface() {
            @Override
            public void onRequestSuccess(String response) {
                try {
                    Log.i(TAG, response);
                    boolean isEmpty = true;
                    EndOfDayData endOfDayData = JsonParser.parseEndOfDayData(response);
                    endOfDayView.setTotalAmount(UtilityMethod.priceFormatter(endOfDayData.getTotalAmount()));
                    endOfDayView.setTotalItem(endOfDayData.getTotalItem());
                    ArrayList<EndOfDayUser> endOfDayUsers= new ArrayList<>();
                    endOfDayUsers = endOfDayData.getEndOfDayUsers();
                    if(endOfDayUsers != null && endOfDayUsers.size() > 0) {
                        endOfDayView.loadUserData(endOfDayUsers);
                        isEmpty = false;
                    }
                    ArrayList<EndOfDayPayment> endOfDayPayments = new ArrayList<>();
                    endOfDayPayments = endOfDayData.getEndOfDayPayments();
                    if(endOfDayPayments != null && endOfDayPayments.size() > 0) {
                        endOfDayView.loadPaymentData(endOfDayPayments);
                        isEmpty = false;
                    }
                    if(isEmpty) {
                        endOfDayView.setEmptyView();
                    }
                    endOfDayView.dismissProgressDialog();
                }
                catch (Exception e) {
                    try {
                        endOfDayView.setEmptyView();
                        endOfDayView.dismissProgressDialog();
                        Toast.makeText(endOfDayView.getClassContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
                    }catch (Exception e1) {
                    }
                }
            }

            @Override
            public void onRequestFailed(VolleyError error) {
                try {
                    endOfDayView.setEmptyView();
                    endOfDayView.dismissProgressDialog();
                    Toast.makeText(endOfDayView.getClassContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
                }catch (Exception e) {
                }
            }
        });
    }


}
