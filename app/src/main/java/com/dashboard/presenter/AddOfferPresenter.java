package com.dashboard.presenter;

import com.android.volley.VolleyError;
import com.dashboard.services.AddOfferService;
import com.dashboard.view.AddOfferView;
import com.dashboard.view.AddOffersResponseInterface;


public class AddOfferPresenter implements AddOffersResponseInterface{

    public static String TAG = "AddOffersPresenter";
    AddOfferView addOfferView;
    AddOfferService addOfferService;

    public AddOfferPresenter(AddOfferView addOfferView, AddOfferService addOfferService) {
        this.addOfferView = addOfferView;
        this.addOfferService = addOfferService;
    }

    public void onClickGallery() {
        addOfferView.showProgressDialog();
        addOfferService.callOfferGalleryApi(addOfferView.getClassContext(), addOfferView.getOfferTitle(), addOfferView.getOfferDesc(), addOfferView.getSpentAmount(), addOfferView.getOfferImage(), String.valueOf(addOfferView.getOfferFor()), String.valueOf(addOfferView.getOfferPosition()), String.valueOf(addOfferView.getFirstOrderOfferStatus()), addOfferView.getDefaultStatus(), addOfferView.getOfferDay());
    }


    public void onClickSubmit() {
        addOfferView.showProgressDialog();
        addOfferService.callOfferSubmitApi(addOfferView.getClassContext(), addOfferView.getOfferTitle(), addOfferView.getOfferDesc(), addOfferView.getSpentAmount(), addOfferView.getOfferImage(), String.valueOf(addOfferView.getOfferFor()), String.valueOf(addOfferView.getOfferPosition()));
    }


    @Override
    public void onSubmitRequestSuccess(String response) {

    }

    @Override
    public void onSubmitRequestError(VolleyError error) {

    }

    @Override
    public void onGalleryRequestSuccess(String response) {

    }

    @Override
    public void onGalleryRequestError(VolleyError error) {

    }
}
