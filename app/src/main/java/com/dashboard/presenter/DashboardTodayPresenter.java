package com.dashboard.presenter;

import android.util.Log;

import com.android.volley.VolleyError;
import com.dashboard.services.ReservationSettingsApiCallback;
import com.dashboard.utility.PreferenceUtil;
import com.dashboard.view.DashBoardTodayView;
import com.dashboard.view.VolleyApiInterface;

import org.json.JSONObject;


public class DashboardTodayPresenter {
    public static String TAG = "DashboardTodayPresenter";
    DashBoardTodayView dashBoardTodayView;
    PreferenceUtil preferenceUtil;

    public DashboardTodayPresenter(DashBoardTodayView dashBoardTodayView) {
        this.dashBoardTodayView = dashBoardTodayView;
        preferenceUtil = new PreferenceUtil(dashBoardTodayView.getClassContext());
    }


    public void getReservationSettings() {
        dashBoardTodayView.showProgressDialog();
        new ReservationSettingsApiCallback().callReservationStatusApi(dashBoardTodayView.getClassContext(), preferenceUtil.getResID(), new VolleyApiInterface() {
            @Override
            public void onRequestSuccess(String response) {
                try {
                    Log.e("Reservation_response", response);
                    dashBoardTodayView.dismissDialog();
                    JSONObject jsonObject = new JSONObject(response);
                    if ("Success".equalsIgnoreCase(jsonObject.getString("status"))) {
                        JSONObject jsonList = jsonObject.getJSONObject("list");
                        String reservationStatus = jsonList.getString("accept_reservation");
                        String autoReservationStatus = jsonList.getString("is_auto_reservation");
                        if("1".equalsIgnoreCase(reservationStatus)) {
                            preferenceUtil.setReservation(true);
                        } else {
                            preferenceUtil.setReservation(false);
                        }
                        if("1".equalsIgnoreCase(autoReservationStatus)) {
                            preferenceUtil.setAutoReservation(true);
                        } else {
                            preferenceUtil.setAutoReservation(false);
                        }
                    }

                } catch (Exception e) {
                    Log.e("Parse Error", "" + e.getMessage());
                }
                try {
                    dashBoardTodayView.setReservationTab();
                } catch (Exception e1) {

                }
            }

            @Override
            public void onRequestFailed(VolleyError error) {
                try {
                    dashBoardTodayView.dismissDialog();
                    preferenceUtil.setReservation(true);
                    dashBoardTodayView.setReservationTab();
                } catch (Exception e) {
                }
            }
        });
    }



}
