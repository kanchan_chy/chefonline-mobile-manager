package com.dashboard.presenter;

import com.dashboard.view.PrinterSettingsView;
import com.dashboard.utility.PreferenceUtil;

/**
 * Created by masum on 06/01/2016.
 */
public class PrinterSettingsPresenter {
    PrinterSettingsView printerSettingsView;
    PreferenceUtil preference;

    public PrinterSettingsPresenter(PrinterSettingsView printerSettingsView) {
        this.printerSettingsView = printerSettingsView;
        preference = new PreferenceUtil(printerSettingsView.getAppContext());
    }

    public void setKitchenPreference(boolean status) {
        preference.setKitchenCopy(status);
    }

    public void setReservationPreference(boolean status) {
        preference.setReservationCopy(status);
    }

    public void setIsKitchenCopyActive() {
        printerSettingsView.setKitchenActive(preference.getKitchenCopy());
    }

    public void setIsReservationCopyActive() {
        printerSettingsView.setReservationActive(preference.getReservationCopy());
    }

}
