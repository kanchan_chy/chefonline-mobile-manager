package com.dashboard.presenter;

import android.util.Log;

import com.android.volley.VolleyError;
import com.dashboard.services.ReservationSettingsApiCallback;
import com.dashboard.utility.PreferenceUtil;
import com.dashboard.view.ReservationSettingsView;
import com.dashboard.view.VolleyApiInterface;

import org.json.JSONObject;

/**
 * Created by masum on 06/01/2016.
 */
public class ReservationSettingsPresenter {
    public static String TAG = "ReservationSettingsPresenter";
    ReservationSettingsView reservationSettingsView;
    PreferenceUtil preferenceUtil;

    public ReservationSettingsPresenter(ReservationSettingsView reservationSettingsView) {
        this.reservationSettingsView = reservationSettingsView;
        preferenceUtil = new PreferenceUtil(reservationSettingsView.getAppContext());
    }


    public void getReservationSettings() {
        reservationSettingsView.showProgressDialog();
        new ReservationSettingsApiCallback().callReservationStatusApi(reservationSettingsView.getAppContext(), preferenceUtil.getResID(), new VolleyApiInterface() {
            @Override
            public void onRequestSuccess(String response) {
                try {
                    Log.e("Reservation_response", response);
                    reservationSettingsView.dismissProgressDialog();
                    JSONObject jsonObject = new org.json.JSONObject(response);
                    if ("Success".equalsIgnoreCase(jsonObject.getString("status"))) {
                        JSONObject jsonList = jsonObject.getJSONObject("list");
                        String reservationStatus = jsonList.getString("accept_reservation");
                        String autoReservationStatus = jsonList.getString("is_auto_reservation");
                        if("1".equalsIgnoreCase(reservationStatus)) {
                            preferenceUtil.setReservation(true);
                        } else {
                            preferenceUtil.setReservation(false);
                        }
                        if("1".equalsIgnoreCase(autoReservationStatus)) {
                            preferenceUtil.setAutoReservation(true);
                        } else {
                            preferenceUtil.setAutoReservation(false);
                        }
                    }

                } catch (Exception e) {
                    Log.e("Parse Error", "" + e.getMessage());
                }
                try {
                    reservationSettingsView.loadReservationSettings();
                } catch (Exception e1) {

                }
            }

            @Override
            public void onRequestFailed(VolleyError error) {
                try {
                    reservationSettingsView.dismissProgressDialog();
                    reservationSettingsView.showToast("Something went wrong", "", false);
                } catch (Exception e) {
                }
            }
        });
    }


    public void setIsAutoReservationActive(String restId, String isAutoReservation) {
        reservationSettingsView.showProgressDialog();
        new ReservationSettingsApiCallback().callAutoReservationSettingsApi(reservationSettingsView.getAppContext(), restId, isAutoReservation, new VolleyApiInterface() {
            @Override
            public void onRequestSuccess(String response) {
                try {
                    Log.i(TAG, response);
                    reservationSettingsView.dismissProgressDialog();
                    JSONObject jsonObject = new JSONObject(response);
                    String msg = jsonObject.getString("msg");
                    if("Success".equalsIgnoreCase(jsonObject.getString("status"))) {
                        reservationSettingsView.setAutoReservationActive(true);
                        reservationSettingsView.showToast(msg, "", true);
                    }
                    else {
                        reservationSettingsView.setAutoReservationActive(false);
                        reservationSettingsView.showToast(msg, "", false);
                    }
                }
                catch (Exception e) {

                }
            }

            @Override
            public void onRequestFailed(VolleyError error) {
                try {
                    reservationSettingsView.dismissProgressDialog();
                    reservationSettingsView.showToast("Something went wrong", "", false);
                }catch (Exception e) {
                }
            }
        });

    }



    public void setIsReservationActive(String restId, String isReservationOn) {
        reservationSettingsView.showProgressDialog();
        new ReservationSettingsApiCallback().callReservationSettingsApi(reservationSettingsView.getAppContext(), restId, isReservationOn, new VolleyApiInterface() {
            @Override
            public void onRequestSuccess(String response) {
                try {
                    Log.i(TAG, response);
                    reservationSettingsView.dismissProgressDialog();
                    JSONObject jsonObject = new JSONObject(response);
                    String msg = jsonObject.getString("msg");
                    if("Success".equalsIgnoreCase(jsonObject.getString("status"))) {
                        reservationSettingsView.setReservationActive(true);
                        reservationSettingsView.showToast(msg, "", true);
                    }
                    else {
                        reservationSettingsView.setReservationActive(false);
                        reservationSettingsView.showToast(msg, "", false);
                    }
                }
                catch (Exception e) {

                }
            }

            @Override
            public void onRequestFailed(VolleyError error) {
                try {
                    reservationSettingsView.dismissProgressDialog();
                    reservationSettingsView.showToast("Something went wrong", "", false);
                }catch (Exception e) {
                }
            }
        });

    }


}
