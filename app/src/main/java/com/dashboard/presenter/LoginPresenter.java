package com.dashboard.presenter;

import android.app.ProgressDialog;
import android.util.Log;

import com.dashboard.datamodel.UserData;
import com.dashboard.jsonparser.JsonParser;
import com.dashboard.services.ForgotPassApiCallback;
import com.dashboard.services.LoginApiCallback;
import com.dashboard.services.LoginService;
import com.dashboard.singleton.AppData;
import com.dashboard.utility.PreferenceUtil;
import com.dashboard.view.LoginApiInterface;
import com.dashboard.view.LoginView;

import org.json.JSONObject;


public class LoginPresenter {
    public static String TAG = "LoginPresenter";
    LoginView loginView;
    LoginService loginService;
    ProgressDialog progressDialog;
    PreferenceUtil preferenceUtil;

    public LoginPresenter(LoginView loginView, LoginService loginService) {
        this.loginView = loginView;
        this.loginService = loginService;
        preferenceUtil = new PreferenceUtil(loginView.getClassContext());
    }

    public void onClickLogin() {
        progressDialog = loginView.showProgressDialog();
        new LoginApiCallback(loginView.getClassContext(), loginView.getLoginUser(), loginView.getLoginPassword(),
                loginView.getDeviceId(),
                loginView.getRegKey(), loginView.getPlatformNo(), new LoginApiInterface() {

            @Override
            public void onRequestComplete(String response) {
                Log.e("LOGIN RESPONSE", "" + response);
                try {
                    UserData userData = JsonParser.parseUserDate(response);
                    if (userData.getStatus() == 1) {
                        AppData.getInstance().setUserDatas(userData);
                        preferenceUtil.setLogInStatus(1);
                        preferenceUtil.setUserGroupId(userData.getUserGroupId());
                        if("4".equalsIgnoreCase(userData.getUserGroupId())) {
                            preferenceUtil.setUserID(userData.getUserId());
                            preferenceUtil.setUserName(userData.getUserName());
                            preferenceUtil.setResID(userData.getRestaurantId());
                            preferenceUtil.setRestaurantName(userData.getRestaurantName());
                            preferenceUtil.setUserMobile(userData.getMobileNumber());
                            preferenceUtil.setUserEmail(userData.getUserEmail());
                            preferenceUtil.setUserPassword(loginView.getLoginPassword());
                            preferenceUtil.setGCMRegId(loginView.getRegKey());
                            loginView.openDashboardActivity();
                        } else if("1".equalsIgnoreCase(userData.getUserGroupId())) {
                            preferenceUtil.setUserID(userData.getUserId());
                            preferenceUtil.setUserName(userData.getUserName());
                            preferenceUtil.setUserMobile(userData.getMobileNumber());
                            preferenceUtil.setUserEmail(userData.getUserEmail());
                            preferenceUtil.setUserPassword(loginView.getLoginPassword());
                            preferenceUtil.setGCMRegId(loginView.getRegKey());
                            progressDialog.dismiss();
                            loginView.openRestaurantListActivity();
                        }

                    } else {
                        progressDialog.dismiss();
                        loginView.showToast(userData.getMessage(), "Login failed. Please try again." , false);
                    }

                } catch (Exception e) {
                }
            }
        });


    }



    public void onClickForgotPassword() {
        progressDialog = loginView.showProgressDialog();
        new ForgotPassApiCallback(loginView.getClassContext(), "", new LoginApiInterface() {

            @Override
            public void onRequestComplete(String response) {
                try {
                    progressDialog.dismiss();
                    JSONObject jsonObject = new org.json.JSONObject(response);
                    JSONObject jsonAppObject = jsonObject.getJSONObject("app");
                    Log.e(TAG, jsonAppObject.toString());
                    jsonAppObject.getString("status");

                    if ("Success".equalsIgnoreCase(jsonAppObject.getString("status"))) {
                        loginView.showToast(jsonAppObject.getString("msg"), "", true);
                        loginView.dismissDialog();

                    } else {
                        loginView.showToast(jsonAppObject.getString("msg"), "", false);
                    }

                } catch (Exception e) {
                    try {
                        loginView.showToast(e.getMessage(), "", false);
                    } catch (Exception e2) {

                    }
                }
            }
        });

    }


}
