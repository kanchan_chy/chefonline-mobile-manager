package com.dashboard.presenter;

import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.chefonline.managermobile.R;
import com.dashboard.activity.DashBoardTodayActivityMain;
import com.dashboard.datamodel.RecentData;
import com.dashboard.datamodel.RestaurantListData;
import com.dashboard.db.DataBaseUtil;
import com.dashboard.jsonparser.JsonParser;
import com.dashboard.services.RestaurantListApiCallback;
import com.dashboard.utility.PreferenceUtil;
import com.dashboard.view.RestaurantListView;
import com.dashboard.view.VolleyApiInterface;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


public class RestaurantListPresenter {
    public static String TAG = "RestaurantListPresenter";
    RestaurantListView restaurantListView;
    PreferenceUtil preferenceUtil;

    public RestaurantListPresenter(RestaurantListView restaurantListView) {
        this.restaurantListView = restaurantListView;
        preferenceUtil = new PreferenceUtil(restaurantListView.getClassContext());
    }

    public void onClickGetRestaurantList(final boolean showProgress, String paginationId, String offset, String searchText) {
        if(showProgress) {
            restaurantListView.showProgressDialog();
        }
        new RestaurantListApiCallback(restaurantListView.getClassContext(), paginationId, offset, searchText, new VolleyApiInterface() {

            @Override
            public void onRequestSuccess(String response) {
                try {
                    Log.e("Rest_List Response", response);
                    if(showProgress) {
                        restaurantListView.dismissDialog();
                    }
                    ArrayList<RestaurantListData> restaurantListDatas = JsonParser.parseRestaurantList(response);
                    if (restaurantListDatas != null) {
                        restaurantListView.setRestaurantData(restaurantListDatas);
                    } else {
                        restaurantListDatas = new ArrayList<>();
                        restaurantListView.setRestaurantData(restaurantListDatas);
                    }

                } catch (Exception e) {
                    try {
                        ArrayList<RestaurantListData> restaurantListDatas = new ArrayList<>();
                        restaurantListView.setRestaurantData(restaurantListDatas);
                    } catch (Exception e1) {

                    }
                }
            }

            @Override
            public void onRequestFailed(VolleyError error) {
                try {
                    if(showProgress) {
                        restaurantListView.dismissDialog();
                    }
                    ArrayList<RestaurantListData> restaurantListDatas = new ArrayList<>();
                    restaurantListView.setRestaurantData(restaurantListDatas);
                }catch (Exception e) {
                }
            }

        });

    }




    public void addRecentRestaurants(RelativeLayout relativeRecent, LinearLayout linearHolder) {
        try {

            ArrayList<RecentData>recentDatas = new ArrayList<>();
            DataBaseUtil dataBaseUtil = new DataBaseUtil(restaurantListView.getClassContext());
            dataBaseUtil.open();
            recentDatas = dataBaseUtil.fetchRecentData(String.valueOf(20));
            dataBaseUtil.close();

            if(recentDatas == null || recentDatas.size() == 0) {
                relativeRecent.setVisibility(View.GONE);
            } else {
                relativeRecent.setVisibility(View.VISIBLE);
                int size = recentDatas.size();
                View[] views = new View[size];
                ImageView[] imgViewLogos = new ImageView[size];
                TextView[] textViewNames = new TextView[size];
                LayoutInflater inflater = LayoutInflater.from(restaurantListView.getClassContext());
               // View v = inflater.inflate(R.layout.column_recent_restaurant, null);
                linearHolder.removeAllViews();
                for (int i = 0; i < size; i++) {
                    views[i] = inflater.inflate(R.layout.column_recent_restaurant, null);
                    textViewNames[i] = (TextView) views[i].findViewById(R.id.textViewRestaurantName);
                    imgViewLogos[i] = (ImageView) views[i].findViewById(R.id.imgViewLogo);
                    textViewNames[i].setText(recentDatas.get(i).getRestaurantName());
                    if(recentDatas.get(i).getLogoUrl() != null && !"".equalsIgnoreCase(recentDatas.get(i).getLogoUrl().trim())) {
                        try {
                            Picasso.with(restaurantListView.getClassContext()).load(recentDatas.get(i).getLogoUrl().trim()).placeholder(R.drawable.placeholder).error(R.drawable.placeholder).into(imgViewLogos[i]);
                        } catch (Exception e) {
                        }
                    }
                    views[i].setOnClickListener(recentClickListener(recentDatas.get(i).getRestaurantId(), recentDatas.get(i).getRestaurantName(), recentDatas.get(i).getLogoUrl()));
                    linearHolder.addView(views[i]);
                }
            }
        } catch (Exception e) {
            Log.e("Db_error", "" + e);
            Toast.makeText(restaurantListView.getClassContext(), "" + e, Toast.LENGTH_LONG).show();
        }


    }




    View.OnClickListener recentClickListener(final String restId, final String restName, final String restLogo) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                insertOrUpdateRecentData(restId, restName, restLogo);
                preferenceUtil.setResID(restId);
                preferenceUtil.setRestaurantName(restName);
                Intent intent = new Intent(restaurantListView.getClassContext(), DashBoardTodayActivityMain.class);
                restaurantListView.getClassContext().startActivity(intent);
            }
        };
    }


    private void insertOrUpdateRecentData(String restId, String restName, String restLogo) {
        try {

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String viewDate = sdf.format(new Date());

            DataBaseUtil dataBaseUtil = new DataBaseUtil(restaurantListView.getClassContext());
            dataBaseUtil.open();
            ArrayList<String>recentRestIds = dataBaseUtil.fetchRecentRestIds();
            if(recentRestIds != null && recentRestIds.contains(restId)) {
                dataBaseUtil.updateRecentData(restId, restName, restLogo, viewDate);
            } else {
                dataBaseUtil.insertRecentData(restId, restName, restLogo, viewDate);
            }
            dataBaseUtil.close();
        } catch (Exception e) {
            Log.e("Db_error", "" + e);
            Toast.makeText(restaurantListView.getClassContext(), "" + e, Toast.LENGTH_LONG).show();
        }
    }


}
