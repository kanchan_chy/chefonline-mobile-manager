package com.dashboard.broadcastreceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.dashboard.utility.ConstantValues;
import com.dashboard.view.ConnectivityChangeInterface;

/**
 * Created by user on 6/16/2016.
 */
public class ConnectivityChangeReceiver extends BroadcastReceiver{

    ConnectivityChangeInterface connectivityChangeInterface;

    public ConnectivityChangeReceiver(ConnectivityChangeInterface connectivityChangeInterface) {
        this.connectivityChangeInterface = connectivityChangeInterface;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent.getAction().equals(ConstantValues.ACTION_CONNECTIVITY_CHANGE)){
            connectivityChangeInterface.onConnectivityChanged();
        }
    }
}
