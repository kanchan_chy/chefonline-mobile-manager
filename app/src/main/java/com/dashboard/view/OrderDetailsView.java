package com.dashboard.view;

import android.app.ProgressDialog;
import android.content.Context;

import com.dashboard.datamodel.OrderDetailsData;

public interface OrderDetailsView{
    Context getClassContext();
    void setOrderData(OrderDetailsData dishDatas);
    void setTotal(String total);
    void setRestaurantName(String name);
    void setOrderQuantity(int amount);
    void setPaymentMethod(String method);
    String getOrderNo();
    ProgressDialog showProgressDialog();
}
