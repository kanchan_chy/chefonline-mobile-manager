package com.dashboard.view;

import android.content.Context;

/**
 * Created by masum on 19/08/2015.
 */
public interface SettingsView {
    Context getClassContext();
    void showProgressDialog();
    void dismissDialog();
    void showToast(String message, String subMessage, boolean isButtonOk);
    void makeLogout();
}
