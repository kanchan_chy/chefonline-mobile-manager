package com.dashboard.view;

import android.content.Context;

/**
 * Created by user on 1/6/2016.
 */
public interface PrinterSettingsView {
    Context getAppContext();
    void setKitchenActive(boolean status);
    void setReservationActive(boolean status);
}
