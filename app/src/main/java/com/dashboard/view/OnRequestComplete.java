package com.dashboard.view;

/**
 * Created by masum on 22/08/2015.
 */
public interface OnRequestComplete {
    void onRequestComplete(String response);
}
