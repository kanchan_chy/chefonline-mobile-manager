package com.dashboard.view;

import android.content.Context;

/**
 * Created by masum on 19/08/2015.
 */
public interface CallbackSuportView {
    Context getClassContext();
    void onSuccessSubmission();
    void showProgressDialog();
    void dismissDialog();
    void showToast(String message, String subMessage, boolean isButtonOk);
}
