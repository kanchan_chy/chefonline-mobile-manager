package com.dashboard.view;

import android.content.Context;

/**
 * Created by masum on 31/08/2015.
 */
public interface DashBoardTodayView {
    Context getClassContext();
    void setReservationTab();
    void showProgressDialog();
    void dismissDialog();
    void showToast(String message, String subMessage, boolean isButtonOk);
}
