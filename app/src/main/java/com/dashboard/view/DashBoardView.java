package com.dashboard.view;

import android.app.ProgressDialog;
import android.content.Context;

import com.dashboard.datamodel.OrderData;
import com.dashboard.datamodel.PaymentData;
import com.dashboard.datamodel.ReservationDataUnconfirmed;
import com.dashboard.datamodel.ScheduleData;

import java.util.ArrayList;

/**
 * Created by masum on 22/08/2015.
 */
public interface DashBoardView {
    String getRestaurantId();
    String getStartDate();
    String getEndDate();
    Context getClassContext();
    ProgressDialog showProgressDialog();
    void setScheduleData(ArrayList<ScheduleData> scheduleData);
    void setOrderData(ArrayList<OrderData> orderData);
    void setReservationData(ArrayList<ReservationDataUnconfirmed> reservationDataUnconfirmed);
    void setPaymentData(ArrayList<PaymentData> paymentData);
    void setCash(String text);
    void setPayPal(String text);
    void setTotal(String text);
    void setTotalOrder(String text);
    void setDeliveryOrder(String text);
    void setCollectionOrder(String text);
    void setTotalOrderReservation(String text);
    void showNotPrintedOrder();
    void showEmptyView(boolean isVisible);
    void checkConnectivity();
    void showInternetDialog(String message, String okButton, String cancelButton);
}
