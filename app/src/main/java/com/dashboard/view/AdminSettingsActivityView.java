package com.dashboard.view;

import android.content.Context;

/**
 * Created by masum on 24/01/2016.
 */
public interface AdminSettingsActivityView {
    void setOrderOnlineStatus(String value);
    Context getAppContext();
    void setEnableOrderOnline(boolean value);
    void showLoading();
    void hideLoading();
    void visibleUI();

}
