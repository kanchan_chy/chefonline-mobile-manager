package com.dashboard.view;

import android.content.Context;

/**
 * Created by masum on 19/08/2015.
 */
public interface ReservationView {
    Context getClassContext();
    String getRestaurantId();
    String getStartDate();
    String getEndDate();
    void loadTodayReservationFragment();
    void loadUnconfirmedReservationFragment();
    void showProgressDialog();
    void dismissDialog();
    void showToast(String message, String subMessage, boolean isButtonOk);
}
