package com.dashboard.view;

import android.content.Context;

import com.dashboard.datamodel.RestaurantListData;

import java.util.ArrayList;

/**
 * Created by masum on 19/08/2015.
 */
public interface RestaurantListView {
    Context getClassContext();
    void setRestaurantData(ArrayList<RestaurantListData> restaurantListDatas);
    void openDashboardActivity();
    void showEmptyView(boolean show);
    void showProgressDialog();
    void dismissDialog();
    void showToast(String message, String subMessage, boolean isButtonOk);
}
