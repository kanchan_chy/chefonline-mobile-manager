package com.dashboard.view;

import android.content.Context;

/**
 * Created by masum on 19/08/2015.
 */
public interface SplashView {
    Context getAppContext();
    void setPlayStoreVersion(String version);
}
