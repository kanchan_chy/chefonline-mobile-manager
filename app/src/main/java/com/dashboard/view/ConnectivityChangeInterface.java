package com.dashboard.view;

/**
 * Created by user on 6/16/2016.
 */
public interface ConnectivityChangeInterface {
    void onConnectivityChanged();
}
