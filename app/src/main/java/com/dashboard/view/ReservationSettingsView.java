package com.dashboard.view;

import android.content.Context;


/**
 * Created by user on 1/6/2016.
 */
public interface ReservationSettingsView {
    Context getAppContext();
    String getRestaurantId();
    void loadReservationSettings();
    void setAutoReservationActive(boolean status);
    void setReservationActive(boolean status);
    void showProgressDialog();
    void dismissProgressDialog();
    void showToast(String message, String subMessage, boolean isButtonOk);
}
