package com.dashboard.view;

import android.content.Context;
import android.view.View;

/**
 * Created by masum on 19/08/2015.
 */

public interface AddOfferView {
    String getOfferTitle();
    String getOfferDesc();
    String getSpentAmount();
    int getOfferFor();
    void setOfferFor(int offerForNo);
    int getOfferPosition();
    void setOfferPosition(int offerPosNo);
    boolean getDefaultStatus();
    void setDefaultStatus(boolean status);
    String getOfferDay();
    void setOfferDay(String day);
    int getFirstOrderOfferStatus();
    void setFirstOrderOfferStatus(int status);
    Context getClassContext();
    void showProgressDialog();
    void showToast(String message, String subMessage, boolean isButtonOk);
    void dismissDialog();
    String getOfferImage();
    void setOfferImage(String url);
    void showDaysPopup(View v);
}
