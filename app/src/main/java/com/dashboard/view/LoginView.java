package com.dashboard.view;

import android.app.ProgressDialog;
import android.content.Context;

/**
 * Created by masum on 19/08/2015.
 */
public interface LoginView {
    String getLoginUser();
    String getLoginPassword();
    String getRegKey();
    String getPlatformNo();
    String getDeviceId();
    void openDashboardActivity();
    void openRestaurantListActivity();
    Context getClassContext();
    ProgressDialog showProgressDialog();
    void showToast(String message, String subMessage, boolean isButtonOk);
    void dismissDialog();
}
