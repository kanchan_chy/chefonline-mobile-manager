package com.dashboard.view;

import com.android.volley.VolleyError;

/**
 * Created by masum on 22/08/2015.
 */

public interface AddOffersResponseInterface {
    void onSubmitRequestSuccess(String response);
    void onSubmitRequestError(VolleyError error);
    void onGalleryRequestSuccess(String response);
    void onGalleryRequestError(VolleyError error);
}
