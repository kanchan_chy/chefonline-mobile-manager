package com.dashboard.view;

import android.content.Context;

import com.dashboard.datamodel.EndOfDayPayment;
import com.dashboard.datamodel.EndOfDayUser;

import java.util.ArrayList;

/**
 * Created by masum on 19/08/2015.
 */

public interface EndOfDayView {
    Context getClassContext();
    void loadPaymentData(ArrayList<EndOfDayPayment> endOfDayPayments);
    void loadUserData(ArrayList<EndOfDayUser> endOfDayUsers);
    void setEmptyView();
    void setTotalAmount(String totalAmount);
    void setTotalItem(String totalItem);
    void showProgressDialog();
    void dismissProgressDialog();
    void showToast(String message, String subMessage, boolean isButtonOk);
    String getRestaurantId();
    String getCurrentDateTime();
}
