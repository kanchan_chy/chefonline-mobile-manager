package com.dashboard.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.dashboard.datamodel.RecentData;
import com.dashboard.datamodel.RestaurantListData;

import java.util.ArrayList;
import java.util.HashMap;

public class DataBaseUtil {

	//Database Info
	private static final String TAG = "DataBaseUtil";
	private static final String DATABASE_NAME = "db_chefonline_manager";
	private static final String DATABASE_TABLE = "tb_recent_rest";
	private static final int DATABASE_VERSION = 1;

	// Table Columns
	public static final String KEY_ROWID = "_id";
	public static final String KEY_REST_ID = "rest_id";
    public static final String KEY_REST_NAME = "rest_name";
    public static final String KEY_REST_LOGO = "rest_logo";
    public static final String KEY_VIEW_DATE = "view_date";

    //Column
	public String[] columns = {KEY_ROWID, KEY_REST_ID, KEY_REST_NAME, KEY_REST_LOGO, KEY_VIEW_DATE};

	private static final String CREATE_TABLE_RECENT_REST = "create table "
			+ DATABASE_TABLE + " (" + KEY_ROWID + " integer primary key autoincrement, "
            + KEY_REST_ID + " text not null, "
            + KEY_REST_NAME + " text not null, "
            + KEY_REST_LOGO + " text not null, "
			+ KEY_VIEW_DATE + " date);";

	private final Context context;

	private DatabaseHelper dbHelper;
	private SQLiteDatabase sqlDb;

	public DataBaseUtil(Context ctx) {
		this.context = ctx;
	}

	public DataBaseUtil open() throws SQLException {
		dbHelper = new DatabaseHelper(context);
		sqlDb = dbHelper.getWritableDatabase();
		// Log.i("DB WORK","YES");
		return this;
	}

	public void close() {
		sqlDb.close();
	}

    /** Insert Order data */
    public long insertRecentData(String restaurantId, String restaurantName, String restaurantLogo, String viewDate) {
        ContentValues cv = new ContentValues();
        cv.put(KEY_REST_ID, restaurantId);
        cv.put(KEY_REST_NAME, restaurantName);
        cv.put(KEY_REST_LOGO, restaurantLogo);
        cv.put(KEY_VIEW_DATE, viewDate);

        return sqlDb.insert(DATABASE_TABLE, null, cv);
    }

    /** Delete Order data */
    public boolean deleteRecentData(String restId) {
        return sqlDb.delete(DATABASE_TABLE, KEY_REST_ID + "=" + restId, null) > 0;
    }

    public boolean emptyRecentData() {
        // query(table, column, where, selection, groupby, having, order)
        return sqlDb.delete(DATABASE_TABLE, null, null) > 0;
    }

    /** Update cart data */
    public long updateRecentData(String restId, String restName, String restLogo, String viewDate) {
        ContentValues cv = new ContentValues();
        cv.put(KEY_REST_NAME, restName);
        cv.put(KEY_REST_LOGO, restLogo);
        cv.put(KEY_VIEW_DATE, viewDate);
        return sqlDb.update(DATABASE_TABLE, cv, KEY_REST_ID + "= ?", new String[] {restId});
    }


    /** Fetch all recent data */
    public ArrayList<RecentData> fetchRecentData(String limit) {
        ArrayList<RecentData> recentDatas = new ArrayList<>();
        // query(table, column, where, selection, groupby, having, order)
       // Cursor cursor = sqlDb.query(DATABASE_TABLE, columns, null, null, null, null, "datetime(" + KEY_VIEW_DATE + ") DESC", limit);
        Cursor cursor = sqlDb.query(DATABASE_TABLE, columns, null, null, null, null, KEY_VIEW_DATE + " DESC", limit);

        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
           RecentData recentData = new RecentData();
            Log.i("REST_ID", "* " + cursor.getString(cursor.getColumnIndex(KEY_REST_ID)));
            recentData.setRestaurantId(cursor.getString(cursor.getColumnIndex(KEY_REST_ID)));
            recentData.setRestaurantName(cursor.getString(cursor.getColumnIndex(KEY_REST_NAME)));
            recentData.setLogoUrl(cursor.getString(cursor.getColumnIndex(KEY_REST_LOGO)));
            recentData.setViewDate(cursor.getString(cursor.getColumnIndex(KEY_VIEW_DATE)));
            recentDatas.add(recentData);
           cursor.moveToNext();
        }

        cursor.close();
        return recentDatas;
    }


    //  Fetch recent restaurant names
    public ArrayList<String> fetchRecentRestIds() {
        ArrayList<String>restIds = new ArrayList<>();
        String[] columnsTemp = {KEY_REST_ID};
        // query(table, column, where, selection, groupby, having, order)
        Cursor cursor = sqlDb.query(DATABASE_TABLE, columnsTemp, null, null, null, null, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            restIds.add(cursor.getString(cursor.getColumnIndex(KEY_REST_ID)));
            cursor.moveToNext();
        }

        cursor.close();
        return restIds;
    }


    /** Database Helper class for creating table */
	private class DatabaseHelper extends SQLiteOpenHelper {

		public DatabaseHelper(Context context) {
			// TODO Auto-generated constructor stub
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			// TODO Auto-generated method stub
			db.execSQL(CREATE_TABLE_RECENT_REST);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// TODO Auto-generated method stub
			Log.w(TAG, "Upgrading Database from version " + oldVersion + " to " + newVersion + " version.");
		}
	}
}
