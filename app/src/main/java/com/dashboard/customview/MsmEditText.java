package com.dashboard.customview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chefonline.managermobile.R;


/**
 * Created by masum on 21/06/2015.
 */
public class MsmEditText extends LinearLayout {
    private View mValue;
    private ImageView mImage;
    EditText editText;
    private TextView txtViewSetError;


    public MsmEditText(Context context) {
        this(context, null);
    }

    public MsmEditText(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.Options, 0, 0);
        String titleText = a.getString(R.styleable.Options_titleText);
        String hintText = a.getString(R.styleable.Options_hintText);
        int inputMethod = a.getInt(R.styleable.Options_android_inputType, EditorInfo.TYPE_TEXT_VARIATION_NORMAL);
        int textColor = a.getColor(R.styleable.Options_textColor, context.getResources().getColor(R.color.black));
        int errorTextColor = a.getColor(R.styleable.Options_errorTextColor, context.getResources().getColor(R.color.black));
        int hintColor = a.getColor(R.styleable.Options_hintColor, context.getResources().getColor(R.color.white_darken));

        TypedArray aFont = context.obtainStyledAttributes(attrs, R.styleable.ChefOnlineTextView, 0, 0);
        String fontName = aFont.getString(R.styleable.ChefOnlineTextView_fontName);
        Typeface myTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/" + fontName);


        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.msm_edit_text, this, true);
        editText = (EditText) getChildAt(0);
        txtViewSetError = (TextView) getChildAt(1);
        setErrorMsgColor(errorTextColor);

        if (fontName!=null) {
            editText.setTypeface(myTypeface);
        }
        a.recycle();

        setOrientation(LinearLayout.VERTICAL);
        setGravity(Gravity.CENTER_VERTICAL);

        editText.setTextColor(textColor);
        editText.setHintTextColor(hintColor);
        //editText.setHintTextColor(context.getResources().getColor(R.color.colorPrimaryDark));
        editText.setHint(hintText);
        editText.setInputType(inputMethod);

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() != 0) {
                    txtViewSetError.setText("");
                    txtViewSetError.setVisibility(GONE);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        //txtViewSetError.setText(titleText);

       /* mValue = getChildAt(1);
        mValue.setBackgroundColor(valueColor);
        mImage = (ImageView) getChildAt(2);*/
    }

    public String getText() {
        return editText.getText().toString();
    }

    public void setText(String value) {
        editText.setText(value);
    }

    public void setErrorMsgColor(int color) {
        txtViewSetError.setTextColor(color);
    }

    public void setErrorMsg(String string) {
        txtViewSetError.setText(string);
    }

    public void setImageVisible(boolean visible) {
        mImage.setVisibility(visible ? View.VISIBLE : View.GONE);
    }

    public void setTypingFocus() {
        editText.setFocusableInTouchMode(true);
        editText.requestFocus();
    }

    public void setEnabled(boolean enabled) {
        editText.setEnabled(enabled);
    }

    public void setErrorTextVisible(boolean visible) {
        txtViewSetError.setVisibility(visible ? View.VISIBLE : View.GONE);
    }
}
