package com.dashboard.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chefonline.managermobile.R;
import com.dashboard.activity.DashBoardTodayActivityMain;
import com.dashboard.adapter.OnlineOrderDataAdapter;
import com.dashboard.broadcastreceiver.ConnectivityChangeReceiver;
import com.dashboard.datamodel.OrderData;
import com.dashboard.datamodel.PaymentData;
import com.dashboard.datamodel.ReservationDataUnconfirmed;
import com.dashboard.datamodel.ScheduleData;
import com.dashboard.presenter.DashBoardPresenter;
import com.dashboard.services.DashBoardService;
import com.dashboard.singleton.AppData;
import com.dashboard.utility.ConstantValues;
import com.dashboard.utility.DateFormatter;
import com.dashboard.utility.PreferenceUtil;
import com.dashboard.utility.UtilityMethod;
import com.dashboard.view.ConnectivityChangeInterface;
import com.dashboard.view.DashBoardView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class DashBoardTodayFragment extends Fragment implements View.OnClickListener, DashBoardView, SwipeRefreshLayout.OnRefreshListener, ConnectivityChangeInterface{
    public static String fromDate = "";
    public static String toDate = "";
    RelativeLayout relativeMessage;
    RelativeLayout relativeUpdate;
    //RelativeLayout relativeInfo;
    SwipeRefreshLayout swipeRefreshLayout;
    TextView textViewConnectivity;
    TextView textViewUpdate;
    Button btnUpdate;
    Context context;

    RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    public static DashBoardPresenter dashBoardPresenter;

    PreferenceUtil preferenceUtil;
    OnlineOrderDataAdapter myAdapter;

    boolean isBlankAdapter = false;
    boolean isRefreshed = false;
    ArrayList<OrderData> orderDatas = new ArrayList<>();
    String totalValue = "0.00";

    Handler timerHandler;
    private long mLastClickTime = 0;
    private boolean isPortrait = false, isInfoVisible = true;
    View v;
    ConnectivityChangeReceiver connectivityChangeReceiver;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        v = inflater.inflate(R.layout.fragment_dash_board_today, container, false);
        setHasOptionsMenu(true);
        initView(v);

        context = v.getContext();
        connectivityChangeReceiver = new ConnectivityChangeReceiver(this);
        IntentFilter connectivityChangeFilter = new IntentFilter(ConstantValues.ACTION_CONNECTIVITY_CHANGE);
        v.getContext().registerReceiver(connectivityChangeReceiver, connectivityChangeFilter);

        if (savedInstanceState == null) {
            ArrayList<OrderData> mOrderDatas = new ArrayList<>();
            isBlankAdapter = true;
            setOrderData(mOrderDatas);
            loadData();
        } else {
            isBlankAdapter = savedInstanceState.getBoolean("blank_adapter");
            setTotal(savedInstanceState.getString("total_value"));
            orderDatas = (ArrayList<OrderData>) savedInstanceState.getSerializable("order_data");
            setOrderData(orderDatas);
        }

        return v;
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("blank_adapter", isBlankAdapter);
        outState.putString("total_value", totalValue);
        outState.putSerializable("order_data", orderDatas);
    }

    @Override
    public void onConfigurationChanged(Configuration _newConfig) {
        relativeMessage = (RelativeLayout) v.findViewById(R.id.relativeMessage);
        if (_newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            relativeMessage.setVisibility(View.GONE);
        }

        if (_newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            relativeMessage.setVisibility(View.GONE);
        }
        super.onConfigurationChanged(_newConfig);
    }


    @Override
    public void onDestroyView() {
        if(connectivityChangeReceiver != null) {
            v.getContext().unregisterReceiver(connectivityChangeReceiver);
        }
        super.onDestroyView();
    }


    @Override
    public void onResume() {
        super.onResume();
        checkUpdate();
    }


    Runnable timerRunnable = new Runnable() {
        @Override
        public void run() {
            myAdapter.notifyDataSetChanged();
            timerHandler.postDelayed(this, 1000); //run every minute
        }
    };

    private void initView(View view) {
        int orientation = getActivity().getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            isPortrait = true;
        } else {
            isPortrait = false;
        }

        isInfoVisible = true;

        timerHandler = new Handler();
        preferenceUtil = new PreferenceUtil(getActivity());
        dashBoardPresenter = new DashBoardPresenter(this, new DashBoardService());

        recyclerView = (RecyclerView) view.findViewById(R.id.recycleViewOrderList);
        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        relativeMessage = (RelativeLayout) view.findViewById(R.id.relativeMessage);
        relativeUpdate = (RelativeLayout) view.findViewById(R.id.relativeUpdate);
        textViewConnectivity = (TextView) view.findViewById(R.id.textViewConnectivity);
        textViewUpdate = (TextView) view.findViewById(R.id.textViewUpdate);
        btnUpdate = (Button) view.findViewById(R.id.btnUpdate);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);

        btnUpdate.setOnClickListener(this);
        swipeRefreshLayout.setOnRefreshListener(this);


    }


    private String getVersionName()
    {
        PackageInfo pInfo = null;
        try {
            pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
            String version = pInfo.versionName;
            return version;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return "";
        }
    }

    private void checkUpdate() {
        if(ConstantValues.PLAY_STORE_VERSION != null && !"".equalsIgnoreCase(ConstantValues.PLAY_STORE_VERSION)) {

            if(!ConstantValues.PLAY_STORE_VERSION.equalsIgnoreCase(getVersionName()) && !ConstantValues.PLAY_STORE_VERSION.equalsIgnoreCase("-1")) {
                relativeUpdate.setVisibility(View.VISIBLE);
            } else {
                relativeUpdate.setVisibility(View.GONE);
            }
        } else {
            relativeUpdate.setVisibility(View.GONE);
        }
    }

    private void loadData() {
        fromDate = DateFormatter.getCurrentDate();
        toDate = DateFormatter.getCurrentDate();

        try {
            String showingText1 = "" + DateFormatter.doFormat(fromDate, DateFormatter.DD_MM_YYYY, DateFormatter.MMM_dd_yyyy);
            String showingText2 = "" + DateFormatter.doFormat(toDate, DateFormatter.DD_MM_YYYY, DateFormatter.MMM_dd_yyyy);
            //textViewFilter.setText(showingText1 + " - " + showingText2);
          //  textViewFilter.setText("Today's Order");
            checkConnectivity();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getRestaurantId() {
        return preferenceUtil.getResID();
    }

    @Override
    public String getStartDate() {
        return fromDate;
    }

    @Override
    public String getEndDate() {
        return toDate;
    }

    @Override
    public ProgressDialog showProgressDialog() {
        ProgressDialog progress = new ProgressDialog(getActivity());
        progress.setMessage("Please wait....");
        progress.show();
        return progress;
    }

    @Override
    public void checkConnectivity() {
        if (!UtilityMethod.isConnectedToInternet(getActivity())) {
            showInternetDialog("Please check your internet connection.", "RETRY", "CANCEL");
        } else {
            dashBoardPresenter.onClickGetOrder(ConstantValues.ORDER_ONLINE_LIST_API_ID, true);
            //dashBoardPresenter.onClickScheduleData();
        }
    }

    @Override
    public void showInternetDialog(String message, String okButton, String cancelButton) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.dialog_exit);
        TextView txtViewPopupMessage = (TextView) dialog.findViewById(R.id.txtViewPopupMessage);
        txtViewPopupMessage.setText(message);

        final TextView txtAccept = (TextView) dialog.findViewById(R.id.txtAccept);
        txtAccept.setText(okButton);

        // if button is clicked, close the custom dialog
        txtAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    mLastClickTime = SystemClock.elapsedRealtime();
                    return;
                } else {
                    mLastClickTime = SystemClock.elapsedRealtime();
                }
                dialog.dismiss();
                checkConnectivity();
            }

        });

        final TextView txtCancel = (TextView) dialog.findViewById(R.id.txtCancel);
        txtCancel.setText(cancelButton);
        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                getActivity().finish();
            }

        });

        dialog.show();
    }

    @Override
    public Context getClassContext() {
        return getContext();
    }

    @Override
    public void setScheduleData(ArrayList<ScheduleData> scheduleDatas) {
        dashBoardPresenter.checkingIfRestaurantOpen(scheduleDatas);

    }

    @Override
    public void setOrderData(ArrayList<OrderData> orderDatas) {
        AppData.getInstance().setOrderDatas(orderDatas);

        this.orderDatas = orderDatas;

        if (isBlankAdapter) {
            isBlankAdapter = false;
        } else {
            DashBoardTodayActivityMain.changeBubbleText(0, orderDatas.size());
        }


        myAdapter = new OnlineOrderDataAdapter(orderDatas, totalValue, getClassContext(), true);
        recyclerView.setAdapter(myAdapter);

        timerHandler.removeCallbacks(timerRunnable);
        timerHandler = new Handler();
        timerHandler.postDelayed(timerRunnable, 500);

        if (orderDatas.size() > 0) {
            relativeMessage.setVisibility(View.GONE);
        } else {
            relativeMessage.setVisibility(View.VISIBLE);
        }

        if(isRefreshed) {
            isRefreshed = false;
            swipeRefreshLayout.setRefreshing(false);
        }

    }

    @Override
    public void setReservationData(ArrayList<ReservationDataUnconfirmed> reservationDataUnconfirmed) {

    }

    @Override
    public void setPaymentData(ArrayList<PaymentData> paymentData) {

    }

    @Override
    public void onPause() {
        //timerHandler.removeCallbacks(timerRunnable);
        super.onPause();
    }

    @Override
    public void setCash(String text) {
      //  textViewCash.setText(text);
    }

    @Override
    public void setPayPal(String text) {
     //   textViewPayPal.setText(text);
    }

    @Override
    public void setTotal(String text) {
      //  textViewTotal.setText(text);
        totalValue = text;
    }

    @Override
    public void setTotalOrder(String text) {

    }

    @Override
    public void setDeliveryOrder(String text) {

    }

    @Override
    public void setCollectionOrder(String text) {

    }

    @Override
    public void setTotalOrderReservation(String text) {

    }

    @Override
    public void showNotPrintedOrder() {

    }

    @Override
    public void showEmptyView(boolean isVisible) {
        if (isVisible) {
            DashBoardTodayActivityMain.changeBubbleText(0, 0);
            relativeMessage.setVisibility(View.VISIBLE);
        } else {
            relativeMessage.setVisibility(View.GONE);
        }
    }

    private void schedulePool() {
        ScheduledExecutorService scheduleTaskExecutor;
        scheduleTaskExecutor = Executors.newScheduledThreadPool(5);

        // This schedule a task to run every 10 minutes:
        scheduleTaskExecutor.scheduleAtFixedRate(new Runnable() {
            public void run() {
                // Parsing RSS feed:
                //myFeedParser.doSomething();

                // If you need update UI, simply do this:
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        // update your UI component here.
                        fromDate = DateFormatter.getCurrentDate();
                        toDate = DateFormatter.getCurrentDate();
                        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
                        String currentDateandTime = sdf.format(new Date());
                        Log.i("****", "Current time " + currentDateandTime);
                        if (currentDateandTime.equalsIgnoreCase("05:00:00")) {
                            checkConnectivity();
                        }

                    }
                });
            }
        }, 0, 1, TimeUnit.SECONDS);
    }


    @Override
    public void onRefresh() {
        isRefreshed = true;
        dashBoardPresenter.onClickGetOrder(ConstantValues.ORDER_ONLINE_LIST_API_ID, false);
    }

    @Override
    public void onConnectivityChanged() {
        if(UtilityMethod.isConnectedToInternet(context)){
            textViewConnectivity.setBackgroundColor(ContextCompat.getColor(context, R.color.green));
            textViewConnectivity.setText("Connected");
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    textViewConnectivity.setVisibility(View.GONE);
                }
            }, 1000);
        } else {
            textViewConnectivity.setBackgroundColor(ContextCompat.getColor(context, R.color.amber_dark));
            textViewConnectivity.setText("Waiting for network");
            textViewConnectivity.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btnUpdate) {
            try {
                Uri uri = Uri.parse("market://details?id=" + getActivity().getPackageName());
                Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(myAppLinkToMarket);
            } catch (ActivityNotFoundException e) {
            }
        }
    }
}
