package com.dashboard.fragments;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.chefonline.managermobile.R;
import com.dashboard.activity.DashBoardTodayActivityMain;
import com.dashboard.adapter.ReservationAdapter;
import com.dashboard.broadcastreceiver.ConnectivityChangeReceiver;
import com.dashboard.datamodel.OrderData;
import com.dashboard.datamodel.PaymentData;
import com.dashboard.datamodel.ReservationDataUnconfirmed;
import com.dashboard.datamodel.ScheduleData;
import com.dashboard.presenter.DashBoardPresenter;
import com.dashboard.services.DashBoardService;
import com.dashboard.utility.ConstantValues;
import com.dashboard.utility.DateFormatter;
import com.dashboard.utility.PreferenceUtil;
import com.dashboard.utility.UtilityMethod;
import com.dashboard.view.ConnectivityChangeInterface;
import com.dashboard.view.DashBoardView;

import java.util.ArrayList;

/**
 * Created by Edwin on 15/02/2015.
 */
public class UnconfirmedReservationFragment extends Fragment implements DashBoardView, ConnectivityChangeInterface{

    RecyclerView recyclerViewReservation;
    TextView textViewMessage;
    TextView textViewConnectivity;
    RecyclerView.LayoutManager layoutManager;
    Context context;

    ReservationAdapter adapter;
    public static DashBoardPresenter dashBoardPresenter;
    PreferenceUtil preferenceUtil;
    public static String fromDate = "";
    public static String toDate = "";

    ArrayList<ReservationDataUnconfirmed> reservationDataUnconfirmed = new ArrayList<>();
    ArrayList<String> reservationIds = new ArrayList<>();
    boolean isBlankAdapter = false, isPortrait = false;
    ConnectivityChangeReceiver connectivityChangeReceiver;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View v = inflater.inflate(R.layout.fragment_reservation_unconfirmed, container,false);
        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);

        context = view.getContext();
        connectivityChangeReceiver = new ConnectivityChangeReceiver(this);
        IntentFilter connectivityChangeFilter = new IntentFilter(ConstantValues.ACTION_CONNECTIVITY_CHANGE);
        context.registerReceiver(connectivityChangeReceiver, connectivityChangeFilter);

        if(savedInstanceState == null) {
            reservationDataUnconfirmed = new ArrayList<>();
            isBlankAdapter = true;
            setReservationData(reservationDataUnconfirmed);
            dashBoardPresenter.onClickGetReservation(ConstantValues.ORDER_RESERVATION_LIST_API_ID, "3", true);
            LocalBroadcastManager.getInstance(getActivity()).registerReceiver(broadcastReceiver, new IntentFilter("on-update-auto-reservation"));

        } else {
            isBlankAdapter = savedInstanceState.getBoolean("blank_adapter");
            reservationDataUnconfirmed = (ArrayList<ReservationDataUnconfirmed>)savedInstanceState.getSerializable("unconfirmed_data");
            setReservationData(reservationDataUnconfirmed);
            if(reservationDataUnconfirmed.size() > 0) {
                showEmptyView(false);
            } else {
                showEmptyView(true);
            }
        }
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                dashBoardPresenter.onClickGetReservation(ConstantValues.ORDER_RESERVATION_LIST_API_ID, "3", false);
            } catch (Exception e) {
                Log.e("Unconfirm Broadcast", "Error =" + e.getMessage());
            }

        }
    };

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("blank_adapter", isBlankAdapter);
        outState.putSerializable("unconfirmed_data", reservationDataUnconfirmed);
    }


    @Override
    public void onDestroyView() {
        if(connectivityChangeReceiver != null) {
            context.unregisterReceiver(connectivityChangeReceiver);
        }
        super.onDestroyView();
    }


    private void initView(View view) {
        int orientation = getActivity().getResources().getConfiguration().orientation;
        if(orientation == Configuration.ORIENTATION_PORTRAIT) {
            isPortrait = true;
        } else {
            isPortrait = false;
        }
        textViewMessage = (TextView) view.findViewById(R.id.textViewMessage);
        textViewConnectivity = (TextView) view.findViewById(R.id.textViewConnectivity);
        recyclerViewReservation = (RecyclerView)view.findViewById(R.id.recyclerViewReservation);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerViewReservation.setLayoutManager(layoutManager);

        dashBoardPresenter = new DashBoardPresenter(this, new DashBoardService());
        preferenceUtil = new PreferenceUtil(getActivity());
        fromDate = DateFormatter.getCurrentDate();
        toDate = DateFormatter.getCurrentDate();

        recyclerViewReservation.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (!isPortrait) {
                    if (dy > 0) {
                        if (ReservationFragment.isBottomTabVisible) {
                            ReservationFragment.hideBottomTab();
                        }
                    } else {
                        if (!ReservationFragment.isBottomTabVisible) {
                            ReservationFragment.showBottomTab();
                        }
                    }
                }
            }
        });

    }



    private void doAutoScroll() {
        int autoIndex = -1;
        for (int i = ConstantValues.reservationNewIds.size() - 1; i >= 0; i--) {
            String tempId = ConstantValues.reservationNewIds.get(i);
            if(reservationIds.contains(tempId)) {
                autoIndex = reservationIds.indexOf(tempId);
                break;
            }
        }
        if(autoIndex != -1) {
            layoutManager.scrollToPosition(autoIndex);
        }
    }


    private void clearNewData() {
        reservationIds = new ArrayList<>();
        for(int i = 0; i < reservationDataUnconfirmed.size(); i++) {
            reservationIds.add(reservationDataUnconfirmed.get(i).getReservationId());
        }
        if(reservationIds.size() > 0) {
            if(ConstantValues.isClearable) {
                ArrayList<String> newReservationIds = new ArrayList<>();
                for(int i = 0; i < ConstantValues.reservationNewIds.size(); i++) {
                    if(!reservationIds.contains(ConstantValues.reservationNewIds.get(i))) {
                        newReservationIds.add(ConstantValues.reservationNewIds.get(i));
                    }
                }
                ConstantValues.reservationNewIds = newReservationIds;
                ConstantValues.isClearable = false;
            }
        }
    }



    @Override
    public String getRestaurantId() {
        return preferenceUtil.getResID();
    }

    @Override
    public String getStartDate() {
        return fromDate;
    }

    @Override
    public String getEndDate() {
        return "";
    }

    @Override
    public Context getClassContext() {
        return getContext();
    }

    @Override
    public void setScheduleData(ArrayList<ScheduleData> scheduleData) {

    }

    @Override
    public void setOrderData(ArrayList<OrderData> orderData) {

    }

    @Override
    public void setReservationData(ArrayList<ReservationDataUnconfirmed> reservationDataUnconfirmed) {
        if(isBlankAdapter) {
            isBlankAdapter = false;
        }
        else {
            if(!preferenceUtil.getAutoReservation()) {
                DashBoardTodayActivityMain.changeBubbleText(1, reservationDataUnconfirmed.size());
                ReservationFragment.countReservBubble = reservationDataUnconfirmed.size();
            }
            ReservationFragment.changeTabText(1, "Unconfirmed (" + reservationDataUnconfirmed.size() + ")");
        }
        this.reservationDataUnconfirmed = reservationDataUnconfirmed;
        clearNewData();
        adapter = new ReservationAdapter(getActivity(), preferenceUtil.getResID(), preferenceUtil.getRestaurantName(), reservationDataUnconfirmed, true);
        recyclerViewReservation.setAdapter(adapter);
        doAutoScroll();
    }

    @Override
    public void setPaymentData(ArrayList<PaymentData> paymentData) {

    }

    @Override
    public void setCash(String text) {

    }

    @Override
    public void setPayPal(String text) {

    }

    @Override
    public void setTotal(String text) {

    }

    @Override
    public void setTotalOrder(String text) {

    }

    @Override
    public void setDeliveryOrder(String text) {

    }

    @Override
    public void setCollectionOrder(String text) {

    }

    @Override
    public void setTotalOrderReservation(String text) {

    }

    @Override
    public void showNotPrintedOrder() {

    }

    @Override
    public void showEmptyView(boolean isVisible) {
        if (isVisible) {
            if(!preferenceUtil.getAutoReservation()) {
                DashBoardTodayActivityMain.changeBubbleText(1, 0);
                ReservationFragment.countReservBubble = 0;
            }
           ReservationFragment.changeTabText(1, "Unconfirmed (0)");
            textViewMessage.setVisibility(View.VISIBLE);
        } else {
            textViewMessage.setVisibility(View.GONE);
        }
    }

    @Override
    public ProgressDialog showProgressDialog() {
        ProgressDialog progress = new ProgressDialog(getActivity());
        progress.setMessage("Please wait....");
        progress.show();
        return progress;
    }

    @Override
    public void checkConnectivity() {

    }

    @Override
    public void showInternetDialog(String message, String okButton, String cancelButton) {

    }

    @Override
    public void onConnectivityChanged() {
        if(UtilityMethod.isConnectedToInternet(context)){
            textViewConnectivity.setBackgroundColor(ContextCompat.getColor(context, R.color.green));
            textViewConnectivity.setText("Connected");
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    textViewConnectivity.setVisibility(View.GONE);
                }
            }, 1000);
        } else {
            textViewConnectivity.setBackgroundColor(ContextCompat.getColor(context, R.color.amber_dark));
            textViewConnectivity.setText("Waiting for network");
            textViewConnectivity.setVisibility(View.VISIBLE);
        }
    }
}