package com.dashboard.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chefonline.managermobile.R;
import com.dashboard.activity.DashBoardTodayActivityMain;
import com.dashboard.utility.ConstantValues;
import com.dashboard.utility.PreferenceUtil;

public class ReservationFragment extends Fragment implements View.OnClickListener{

    static TextView[] tabs = new TextView[2];
    RelativeLayout relativeToday;
    RelativeLayout relativeUnconfirmed;
    static LinearLayout linearTabs;
    TextView textViewToday;
    TextView textViewUnconfirmed;
    static TextView textViewReservationStatus;
    ImageView imgViewToday;
    ImageView imgViewUnconfirmed;

    static PreferenceUtil preferenceUtil;
    private long mLastClickTime = 0;
    private static int dp_1, dp_56;
    public static boolean isBottomTabVisible = true;
    public static int countReservBubble = 0;
    boolean justCreated = false;
    private boolean isUnconfirmedLoaded;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // attachBottomBar(savedInstanceState);
        View v = inflater.inflate(R.layout.fragment_reservation, container,false);
        //LocalBroadcastManager.getInstance(getActivity()).registerReceiver(broadcastReceiver, new IntentFilter("on-update-auto-reservation"));
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(broadcastReceiverLoadTodays, new IntentFilter("on-update-completed"));
     /*  Intent intent = new Intent("on-update-completed");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);  */
        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        justCreated = false;
        preferenceUtil = new PreferenceUtil(getActivity());
        initView(view);
        setUiClickHandler();
        if(savedInstanceState == null) {
            loadFragment();
        } else {
            if(preferenceUtil.getAutoReservation()) {
                textViewReservationStatus.setVisibility(View.VISIBLE);
            } else {
                textViewReservationStatus.setVisibility(View.GONE);
            }
            countReservBubble = savedInstanceState.getInt("count_reserv_bubble");
            DashBoardTodayActivityMain.changeBubbleText(1, countReservBubble);
            textViewToday.setText(savedInstanceState.getString("today_text"));
            textViewUnconfirmed.setText(savedInstanceState.getString("unconfirmed_text"));
            isUnconfirmedLoaded = savedInstanceState.getBoolean("unconfirmed_loaded");
            if(isUnconfirmedLoaded) {
               // loadTodaysFragment();
               // loadUnconfirmedFragment();
                imgViewUnconfirmed.setColorFilter(ContextCompat.getColor(getActivity(), R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
                textViewUnconfirmed.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
                textViewUnconfirmed.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
                imgViewToday.setColorFilter(ContextCompat.getColor(getActivity(), R.color.black_unselected), PorterDuff.Mode.SRC_IN);
                textViewToday.setTextColor(ContextCompat.getColor(getActivity(), R.color.black_unselected));
                textViewToday.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
            } else {
               // loadUnconfirmedFragment();
               // loadTodaysFragment();
                imgViewToday.setColorFilter(ContextCompat.getColor(getActivity(), R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
                textViewToday.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
                textViewToday.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
                textViewUnconfirmed.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
                imgViewUnconfirmed.setColorFilter(ContextCompat.getColor(getActivity(), R.color.black_unselected), PorterDuff.Mode.SRC_IN);
                textViewUnconfirmed.setTextColor(ContextCompat.getColor(getActivity(), R.color.black_unselected));
                textViewUnconfirmed.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
            }
        }
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("unconfirmed_loaded", isUnconfirmedLoaded);
        outState.putString("unconfirmed_text", textViewUnconfirmed.getText().toString());
        outState.putString("today_text", textViewToday.getText().toString());
        outState.putInt("count_reserv_bubble", countReservBubble);
    }


    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                justCreated = false;
                if(preferenceUtil.getAutoReservation()) {
                    loadTodaysFragment();
                } else {
                    loadUnconfirmedFragment();
                }
            } catch (Exception e) {

            }

        }
    };

    private BroadcastReceiver broadcastReceiverLoadTodays = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            loadTodaysFragment();
        }
    };

    /*
    @Override
    public void onResume() {
        super.onResume();
        if(!preferenceUtil.getAutoReservation()) {
            textViewReservationStatus.setVisibility(View.GONE);
            loadUnconfirmedFragment();
        } else {
            textViewReservationStatus.setVisibility(View.VISIBLE);
            loadTodaysFragment();
        }
    }  */


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser) {
            if(justCreated) {
                if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                    showBottomTab();
                }
                if(!preferenceUtil.getAutoReservation()) {
                    textViewReservationStatus.setVisibility(View.GONE);
                    loadUnconfirmedFragment();
                } else {
                    textViewReservationStatus.setVisibility(View.VISIBLE);
                    loadTodaysFragment();
                }
            } else {
                justCreated = true;
            }
        }
    }

    private void loadFragment() {
        if(preferenceUtil.getAutoReservation()) {
            textViewReservationStatus.setVisibility(View.VISIBLE);
            loadUnconfirmedFragment();
            loadTodaysFragment();
        }
        else {
            textViewReservationStatus.setVisibility(View.GONE);
            loadTodaysFragment();
            loadUnconfirmedFragment();
        }
    }

    public static void changeTabText(int pos, String text) {
        tabs[pos].setText(text);
    }

    public static void hideBottomTab() {
        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) linearTabs.getLayoutParams();
        lp.width = RelativeLayout.LayoutParams.MATCH_PARENT;
        lp.height = dp_1;
        linearTabs.setLayoutParams(lp);
        if(preferenceUtil.getAutoReservation()) {
            textViewReservationStatus.setVisibility(View.GONE);
        }
        isBottomTabVisible = false;
    }

    public static void showBottomTab() {
        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) linearTabs.getLayoutParams();
        lp.width = RelativeLayout.LayoutParams.MATCH_PARENT;
        lp.height = dp_56;
        linearTabs.setLayoutParams(lp);
        if(preferenceUtil.getAutoReservation()) {
            textViewReservationStatus.setVisibility(View.VISIBLE);
        }
        isBottomTabVisible = true;
    }

    private void initView(View view) {
        textViewReservationStatus = (TextView) view.findViewById(R.id.textViewReservationStatus);
        relativeToday = (RelativeLayout) view.findViewById(R.id.relativeToday);
        relativeUnconfirmed = (RelativeLayout) view.findViewById(R.id.relativeUnconfirmed);
        linearTabs = (LinearLayout) view.findViewById(R.id.linearTabs);
        textViewToday = (TextView) view.findViewById(R.id.textViewToday);
        textViewUnconfirmed = (TextView) view.findViewById(R.id.textViewUnconfirmed);
        imgViewToday = (ImageView) view.findViewById(R.id.imgViewToday);
        imgViewUnconfirmed = (ImageView) view.findViewById(R.id.imgViewUnconconfirmed);
        tabs[0] = textViewToday;
        tabs[1] = textViewUnconfirmed;

        float scale = getResources().getDisplayMetrics().density;
        dp_1 = (int) (1 * scale + 0.5f);
        dp_56 = (int) (56 * scale + 0.5f);
    }

    private void setUiClickHandler() {
        relativeToday.setOnClickListener(this);
        relativeUnconfirmed.setOnClickListener(this);
    }

    private void loadUnconfirmedFragment() {
        try {
            FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
            transaction.replace(R.id.frameReservation, new UnconfirmedReservationFragment()).commit();
            imgViewUnconfirmed.setColorFilter(ContextCompat.getColor(getActivity(), R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
            textViewUnconfirmed.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
            textViewUnconfirmed.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
            imgViewToday.setColorFilter(ContextCompat.getColor(getActivity(), R.color.black_unselected), PorterDuff.Mode.SRC_IN);
            textViewToday.setTextColor(ContextCompat.getColor(getActivity(), R.color.black_unselected));
            textViewToday.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
            isUnconfirmedLoaded = true;
        } catch (Exception e) {

        }
    }

    private void loadTodaysFragment() {
        try {
            FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
            transaction.replace(R.id.frameReservation, new TodayReservationFragment()).commit();
            imgViewToday.setColorFilter(ContextCompat.getColor(getActivity(), R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
            textViewToday.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
            textViewToday.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
            textViewUnconfirmed.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
            imgViewUnconfirmed.setColorFilter(ContextCompat.getColor(getActivity(), R.color.black_unselected), PorterDuff.Mode.SRC_IN);
            textViewUnconfirmed.setTextColor(ContextCompat.getColor(getActivity(), R.color.black_unselected));
            textViewUnconfirmed.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
            isUnconfirmedLoaded = false;
        } catch (Exception e) {

        }
    }


    @Override
    public void onClick(View v) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000){
            mLastClickTime = SystemClock.elapsedRealtime();
            return;

        } else {
            mLastClickTime = SystemClock.elapsedRealtime();
        }
        if(v.getId() == R.id.relativeToday) {
            ConstantValues.isClearable = true;
            loadTodaysFragment();
        } else if (v.getId() == R.id.relativeUnconfirmed) {
            ConstantValues.isClearable = true;
            loadUnconfirmedFragment();
        }
    }

}