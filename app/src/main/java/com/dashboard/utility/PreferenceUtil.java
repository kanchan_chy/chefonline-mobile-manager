package com.dashboard.utility;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class PreferenceUtil {
	
	Context mContext;
	private SharedPreferences sharedPreferences;
	private SharedPreferences.Editor spEditor;

    private final String LOGIN_STATUS = "login_status";
    private final String USER_ID = "user_id";
    private final String GCM_REG_ID = "gcm_reg_id";
    private final String RES_NAME = "res_name";

    private final String USER_GROUP_ID = "user_group_id";
    private final String USER_EMAIL = "email";
    private final String USER_PASSWORD = "password";
    private final String REMEMBER_USER = "remember_user";
    private final String FIRST_NAME = "first_name";
    private final String USER_SUR_NAME = "sur_name";
    private final String USER_LAST_NAME = "last_name";
    private final String USER_POSTCODE = "postcode";
    private final String USER_MOB = "mobile_no";
    private final String USER_NAME = "name";
    private final String USER_ADDRESS = "address1";
    private final String USER_ADDRESS2 = "address2";
    private final String USER_TEL = "telephone_no";
    private final String USER_COUNTRY = "user_country";

    private final String KITCHEN_COPY = "kitchen_copy";
    private final String RESERVATION_COPY = "reservation_copy";
    private final String AUTO_RESERVATION = "auto_reservation";
    private final String RESERVATION = "reservation";

    private final String UUID = "uuid";
    private final String REST_ID = "res_id";

    private final String ADMIN_PIN = "admin_pin";


	public PreferenceUtil(Context mContext)
	{
		super();
		this.mContext = mContext;
		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
	}


    public void setResID (String user_id) {
        spEditor = sharedPreferences.edit();
        spEditor.putString(REST_ID, user_id);
        spEditor.commit();

    }

    public String getResID () {
        return sharedPreferences.getString(REST_ID, "");

    }

    public void setAdminPin(String pin) {
        spEditor = sharedPreferences.edit();
        spEditor.putString(ADMIN_PIN, pin);
        spEditor.commit();
    }

    public String getAdminPin() {
        return sharedPreferences.getString(ADMIN_PIN, "");
    }

    public void setRestaurantName (String name) {
        spEditor = sharedPreferences.edit();
        spEditor.putString(RES_NAME, name);
        spEditor.commit();

    }

    public String getRestaurantName () {
        return sharedPreferences.getString(RES_NAME, "");

    }

    public void setUserID (String user_id) {
        spEditor = sharedPreferences.edit();
        spEditor.putString(USER_ID, user_id);
        spEditor.commit();

    }

    public String getUserID () {
        return sharedPreferences.getString(USER_ID, "");

    }


    public void setUserName (String user_name) {
        spEditor = sharedPreferences.edit();
        spEditor.putString(USER_NAME, user_name);
        spEditor.commit();

    }

    public String getUserName () {
        return sharedPreferences.getString(USER_NAME, "");

    }


    public void setLogInStatus (int status) {
        spEditor = sharedPreferences.edit();
        spEditor.putInt(LOGIN_STATUS, status);
        spEditor.commit();

    }

    public int getLogInStatus () {
        return sharedPreferences.getInt(LOGIN_STATUS, 0);
    }


    public void setGCMRegId (String regid) {
        spEditor = sharedPreferences.edit();
        spEditor.putString(GCM_REG_ID, regid);
        spEditor.commit();

    }

    public String getGCMRegId () {
        return sharedPreferences.getString(GCM_REG_ID, "");
    }

    public boolean getKitchenCopy() {
        return sharedPreferences.getBoolean(KITCHEN_COPY, false);
    }

    public void setKitchenCopy(boolean status) {
        spEditor = sharedPreferences.edit();
        spEditor.putBoolean(KITCHEN_COPY, status);
        spEditor.commit();
    }


    public boolean getReservationCopy() {
        return sharedPreferences.getBoolean(RESERVATION_COPY, false);
    }

    public void setReservationCopy(boolean status) {
        spEditor = sharedPreferences.edit();
        spEditor.putBoolean(RESERVATION_COPY, status);
        spEditor.commit();
    }

    public boolean getAutoReservation() {
        return sharedPreferences.getBoolean(AUTO_RESERVATION, false);
    }

    public void setAutoReservation(boolean status) {
        spEditor = sharedPreferences.edit();
        spEditor.putBoolean(AUTO_RESERVATION, status);
        spEditor.commit();
    }


    public boolean getReservation() {
        return sharedPreferences.getBoolean(RESERVATION, false);
    }

    public void setReservation(boolean status) {
        spEditor = sharedPreferences.edit();
        spEditor.putBoolean(RESERVATION, status);
        spEditor.commit();
    }


    public void setUserGroupId(String userGroupId) {
        spEditor = sharedPreferences.edit();
        spEditor.putString(USER_GROUP_ID, userGroupId);
        spEditor.commit();
    }

    public String getUSER_GROUP_ID() {
        return sharedPreferences.getString(USER_GROUP_ID, "");
    }

    public void setUserEmail(String userGroupId) {
        spEditor = sharedPreferences.edit();
        spEditor.putString(USER_EMAIL, userGroupId);
        spEditor.commit();
    }

    public String getUserEmail() {
        return sharedPreferences.getString(USER_EMAIL, "");
    }

    public void setUserPassword(String userPassword) {
        spEditor = sharedPreferences.edit();
        spEditor.putString(USER_PASSWORD, userPassword);
        spEditor.commit();
    }

    public String getUserPassword() {
        return sharedPreferences.getString(USER_PASSWORD, "");
    }

    public void setRememberUser(boolean rememberUser) {
        spEditor = sharedPreferences.edit();
        spEditor.putBoolean(REMEMBER_USER, rememberUser);
        spEditor.commit();
    }

    public boolean getRememberUser() {
        return sharedPreferences.getBoolean(REMEMBER_USER, false);
    }

    public void setUserFirstName(String value) {
        spEditor = sharedPreferences.edit();
        spEditor.putString(FIRST_NAME, value);
        spEditor.commit();
    }

    public String getUserFirstName() {
        return sharedPreferences.getString(FIRST_NAME, "");
    }

    public void setUserSurName(String value) {
        spEditor = sharedPreferences.edit();
        spEditor.putString(USER_SUR_NAME, value);
        spEditor.commit();
    }

    public String getUserSurName() {
        return sharedPreferences.getString(USER_SUR_NAME, "");
    }

    public void setUserLastName(String value) {
        spEditor = sharedPreferences.edit();
        spEditor.putString(USER_LAST_NAME, value);
        spEditor.commit();
    }

    public String getUserLastName() {
        return sharedPreferences.getString(USER_LAST_NAME, "");
    }

    public void setUserPostCode(String value) {
        spEditor = sharedPreferences.edit();
        spEditor.putString(USER_POSTCODE, value);
        spEditor.commit();
    }

    public String getUserPostCode() {
        return sharedPreferences.getString(USER_POSTCODE, "");
    }

    public void setUserMobile(String value) {
        spEditor = sharedPreferences.edit();
        spEditor.putString(USER_MOB, value);
        spEditor.commit();
    }

    public String getUserMobile() {
        return sharedPreferences.getString(USER_MOB, "");
    }

    public void setUserAddress1(String value) {
        spEditor = sharedPreferences.edit();
        spEditor.putString(USER_ADDRESS, value);
        spEditor.commit();
    }

    public String getUserAddress() {
        return sharedPreferences.getString(USER_ADDRESS, "");
    }

    public void setUserAddress2(String value) {
        spEditor = sharedPreferences.edit();
        spEditor.putString(USER_ADDRESS2, value);
        spEditor.commit();
    }

    public String getUserAddress2() {
        return sharedPreferences.getString(USER_ADDRESS2, "");
    }

    public void setUserTel(String value) {
        spEditor = sharedPreferences.edit();
        spEditor.putString(USER_TEL, value);
        spEditor.commit();
    }

    public String getUserTel() {
        return sharedPreferences.getString(USER_TEL, "");
    }

    public void setUUID(String value) {
        spEditor = sharedPreferences.edit();
        spEditor.putString(UUID, value);
        spEditor.commit();
    }

    public String getUserCountry() {
        return sharedPreferences.getString(USER_COUNTRY, "");
    }

    public void setUserCountry(String value) {
        spEditor = sharedPreferences.edit();
        spEditor.putString(USER_COUNTRY, value);
        spEditor.commit();
    }

    public String getUUID() {
        return sharedPreferences.getString(UUID, "");
    }


}