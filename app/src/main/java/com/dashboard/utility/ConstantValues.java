package com.dashboard.utility;

import java.util.ArrayList;

/**
 * Created by masum on 22/08/2015.
 */
public class ConstantValues {

    //public static String BASE_API_URL = "http://testing.smartrestaurantsolutions.com/mobileapi-v1/Tigger.php";
    public static String BASE_API_URL = "http://smartrestaurantsolutions.com/mobileapi-test/Tigger.php";   // test
    //public static String BASE_API_URL = "http://smartrestaurantsolutions.com/mobileapi-v2/Tigger.php";
  //  public static String BASE_API_URL = "http://smartrestaurantsolutions.com/mobileapi-v2/v2/Tigger.php"; // new main
    //public static String BASE_API_URL = "http://10.0.0.231/api/mobileapi-test/Tigger.php";
 //   public static String BASE_API_URL = "http://10.0.0.144/mobileapi-v2/Tigger.php"; //Local Host Raji Vai

    public static String PLATFORM_ID = "2";
    public static boolean IS_NEW_ORDER = false;
    public static boolean IS_ACTIVITY_RUNNING = true;
    public static int OFFER_FOR_NONE = 0;
    public static int OFFER_FOR_COLLECTION = 1;
    public static int OFFER_FOR_DELIVERY = 2;
    public static int OFFER_FOR_BOTH = 3;
    public static int OFFER_POSITION_NONE = 0;
    public static int OFFER_POSITION_LEFT = 1;
    public static int OFFER_POSITION_RIGHT = 2;
    public static int FIRST_ORDER_OFFER_NO = 0;
    public static int FIRST_ORDER_OFFER_YES = 1;
    public static String ORDER_ONLINE_LIST_API_ID = "33";
    public static String ORDER_RESERVATION_LIST_API_ID = "55";
    public static String ALARM_ACTION = "com.chefonline.srsdashboard.ALARM";
    public static String BATTERY_STATUS_CHARGING = "Battery charging";
    public static String BATTERY_STATUS_DISCHARGING = "Battery discharging";
    public static String BATTERY_STATUS_FULL = "Battery full";
    public static String BATTERY_STATUS_NOT_CHARGING = "Battery not charging";
    public static String BATTERY_STATUS_UNKNOWN = "Battery status unknown";
    public static ArrayList<String>reservationNewIds = new ArrayList<>();
    public static boolean isClearable = false;

    public static String UUID = "";
    public static String RESTAURANT_ID = "";
    public static String ACTION_CONNECTIVITY_CHANGE="android.net.conn.CONNECTIVITY_CHANGE";
    public static String PLAY_STORE_VERSION = "";
}
