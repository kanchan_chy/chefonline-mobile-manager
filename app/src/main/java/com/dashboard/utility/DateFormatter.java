package com.dashboard.utility;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by masum on 06/05/2015.
 */
public class DateFormatter {
    public static String YYYY_MM_DD = "yyyy-MM-dd";
    public String YYYY_MMM_DD_EEEE = "yyyy-MMM-dd, EEEE";
    public static String DD_MMM_YYYY = "DD-MMM-YYYY"; // 05-Mar-2014
    public static String DD_MM_YYYY = "dd-MM-yyyy"; //05-03-2014

    public static String MMM_dd_yyyy = "MMM d, yyyy"; //july 5, 2015

    public static String yyyy_mm_dd_hh_mm_ss = "yyyy-MM-dd hh:mm:ss";
    public static String EEE_MMM_dd_yy = "EEE, MMM d, ''yyyy";
    public static String EEE_dd_MMM_yy = "EEEE, d MMM yyyy";
    public static String EE_dd_MMM_yy = "EEE, d MMM yyyy"; // wed, 5 july 2014

    public static String doFormat(String getDate, String fromDate, String toDate){
        Date date = null;
        String date_s = getDate;
        SimpleDateFormat dt = new SimpleDateFormat(fromDate);
        SimpleDateFormat dt1 = null;
        // *** note that it's "yyyy-MM-dd hh:mm:ss" not "yyyy-mm-dd hh:mm:ss"
        try {
            date = dt.parse(date_s);
            dt1 = new SimpleDateFormat(toDate);
            //System.out.println(dt1.format(date));
        } catch (Exception e) {
            return "00:00:00";
        }

        return dt1.format(date);
    }

    private static String getYesterdayFormatted() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, -1);
        Date tomorrow = calendar.getTime();
        SimpleDateFormat dateFormat = new SimpleDateFormat(DateFormatter.MMM_dd_yyyy);
        return dateFormat.format(tomorrow);

    }

    private static String getYesterdayMain() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, -1);
        Date tomorrow = calendar.getTime();
        SimpleDateFormat dateFormat = new SimpleDateFormat(DateFormatter.DD_MM_YYYY);
        return dateFormat.format(tomorrow);

    }

    private static String getLastWeekFormatted() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, -6);
        Date tomorrow = calendar.getTime();
        SimpleDateFormat dateFormat = new SimpleDateFormat(DateFormatter.MMM_dd_yyyy);
        return dateFormat.format(tomorrow);

    }

    private static String getLastWeekMain() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, -6);
        Date tomorrow = calendar.getTime();
        SimpleDateFormat dateFormat = new SimpleDateFormat(DateFormatter.DD_MM_YYYY);
        return dateFormat.format(tomorrow);

    }

    private static String getLastThirtyDaysMain() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, -29);
        Date tomorrow = calendar.getTime();
        SimpleDateFormat dateFormat = new SimpleDateFormat(DateFormatter.DD_MM_YYYY);
        return dateFormat.format(tomorrow);

    }

    private static String getLastThirtyDaysFormatted() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, -29);
        Date tomorrow = calendar.getTime();
        SimpleDateFormat dateFormat = new SimpleDateFormat(DateFormatter.MMM_dd_yyyy);
        return dateFormat.format(tomorrow);

    }

    private static String getThisMonth() {
        Calendar calendar = Calendar.getInstance();
        int days = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        calendar.add(Calendar.DAY_OF_YEAR, -29);
        Date tomorrow = calendar.getTime();
        SimpleDateFormat dateFormat = new SimpleDateFormat(DateFormatter.DD_MM_YYYY);
        return dateFormat.format(tomorrow);

    }

    private static String getThisMonthFormatted() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, -29);
        Date tomorrow = calendar.getTime();
        SimpleDateFormat dateFormat = new SimpleDateFormat(DateFormatter.MMM_dd_yyyy);
        return dateFormat.format(tomorrow);

    }

    /*test case */

    public static String getCurrentDate() {
        Calendar calendar = Calendar.getInstance();
        Date today = calendar.getTime();
        SimpleDateFormat dateFormat = new SimpleDateFormat(DD_MM_YYYY);
        String todayAsString = dateFormat.format(today);
        return todayAsString;

    }

    public static String getCurrentDateTime() {
        Calendar calendar = Calendar.getInstance(Locale.getDefault());
        Date today = calendar.getTime();
        SimpleDateFormat dateFormat = new SimpleDateFormat(yyyy_mm_dd_hh_mm_ss);
        String todayAsString = dateFormat.format(today);
        return todayAsString;

    }

    public static int getTotalDaysOfMonth() {
        Calendar calendar = Calendar.getInstance();
        int days = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        calendar.add(Calendar.DAY_OF_YEAR, days);
        Date tomorrow = calendar.getTime();
        SimpleDateFormat dateFormat = new SimpleDateFormat(DateFormatter.MMM_dd_yyyy);
        return days;

    }

    public static String getCustomDate(int days) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, days);
        Date tomorrow = calendar.getTime();
        SimpleDateFormat dateFormat = new SimpleDateFormat(DateFormatter.DD_MM_YYYY);
        return dateFormat.format(tomorrow);

    }

    public static String getLastMonthLastDate() {
        Calendar aCalendar = Calendar.getInstance();
        aCalendar.set(Calendar.DATE, 1);
        aCalendar.add(Calendar.DAY_OF_MONTH, -1);
        Date lastDateOfPreviousMonth = aCalendar.getTime();
        SimpleDateFormat dateFormat = new SimpleDateFormat(DateFormatter.DD_MM_YYYY);
        return dateFormat.format(lastDateOfPreviousMonth);

    }
    public static String getLastMonthFirstDate() {
        Calendar aCalendar = Calendar.getInstance();
        aCalendar.add(Calendar.MONTH, -1);
        aCalendar.set(Calendar.DATE, 1);
        Date firstDateOfPreviousMonth = aCalendar.getTime();

        SimpleDateFormat dateFormat = new SimpleDateFormat(DateFormatter.DD_MM_YYYY);
        return dateFormat.format(firstDateOfPreviousMonth);
    }

    public static int getDayofMonth() {
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        return day;
    }

    public boolean isToday(String selectedDateStr) {
        Calendar calendar = Calendar.getInstance();
        Date todayTime = calendar.getTime();
        SimpleDateFormat dateFormat = new SimpleDateFormat(DD_MM_YYYY);
        String todayStr = dateFormat.format(todayTime);
        try {
            Date todayDate = dateFormat.parse(todayStr);
            Date selectedDate = dateFormat.parse(selectedDateStr);
            if(selectedDate.equals(todayDate)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {

        }
        return false;
    }


    public boolean isTomorrow(String selectedDateStr) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, 1);
        Date tomorrowTime = calendar.getTime();
        SimpleDateFormat dateFormat = new SimpleDateFormat(DD_MM_YYYY);
        String tomorrowStr = dateFormat.format(tomorrowTime);
        try {
            Date tomorrowDate = dateFormat.parse(tomorrowStr);
            Date selectedDate = dateFormat.parse(selectedDateStr);
            if(selectedDate.equals(tomorrowDate)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {

        }
        return false;
    }


}
