package com.dashboard.utility;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.dashboard.customview.CustomToast;

public class ActivityUtil {
  private Context context;
  private Activity activity;

  public ActivityUtil(Context context, Activity activity) {
    this.context = context;
    this.activity = activity;
  }

  public void startMainActivity() {
    //context.startActivity(new Intent(context, MainActivity.class));
  }

  public void openToast(String message, String subMessage, boolean iconOk) {
    new CustomToast(activity, message, subMessage, iconOk);
  }
}
