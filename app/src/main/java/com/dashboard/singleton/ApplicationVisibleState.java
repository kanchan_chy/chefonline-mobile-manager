package com.dashboard.singleton;

import android.app.Application;

/**
 * Created by masum on 15/05/2016.
 */
public class ApplicationVisibleState extends Application {
    public static boolean isActivityVisible() {
        return activityVisible;
    }

    public static void activityResumed() {
        activityVisible = true;
    }

    public static void activityPaused() {
        activityVisible = false;
    }

    private static boolean activityVisible = false;
}
