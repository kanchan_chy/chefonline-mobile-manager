package com.dashboard.singleton;

import com.dashboard.datamodel.OrderData;
import com.dashboard.datamodel.PaymentData;
import com.dashboard.datamodel.UserData;

import java.util.ArrayList;

/**
 * Created by masum on 22/08/2015.
 */
public class AppData {
    private static AppData ourInstance = new AppData();
    private AppData() {
    }

    public static AppData getInstance() {
        return ourInstance;
    }

    public static void destroyInstance() {
        ourInstance = null;
    }

    private UserData userDatas;
    private Double totalCash;
    private Double totalPaypal;
    private Double grandTotal;

    private ArrayList<OrderData> orderDatas;
    private ArrayList<PaymentData> paymentDatas;

    public UserData getUserDatas() {
        return userDatas;
    }

    public void setUserDatas(UserData userDatas) {
        this.userDatas = userDatas;
    }


    public Double getTotalCash() {
        return totalCash;
    }

    public void setTotalCash(Double totalCash) {
        this.totalCash = totalCash;
    }

    public Double getTotalPaypal() {
        return totalPaypal;
    }

    public void setTotalPaypal(Double totalPaypal) {
        this.totalPaypal = totalPaypal;
    }

    public Double getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(Double grandTotal) {
        this.grandTotal = grandTotal;
    }

    public ArrayList<OrderData> getOrderDatas() {
        return orderDatas;
    }

    public void setOrderDatas(ArrayList<OrderData> orderDatas) {
        this.orderDatas = orderDatas;
    }

    public ArrayList<PaymentData> getPaymentDatas() {
        return paymentDatas;
    }

    public void setPaymentDatas(ArrayList<PaymentData> paymentDatas) {
        this.paymentDatas = paymentDatas;
    }


}
