package com.dashboard.alarm;

/**
 * Created by user on 2/7/2016.
 */
import android.app.KeyguardManager;
import android.content.Context;
import android.os.PowerManager;

public abstract class WakeLocker {
    private static PowerManager.WakeLock wakeLock;
    private static KeyguardManager.KeyguardLock mKeyguardLock;
    private static int acquiredBy = 0;
    private static int releasedBy = 0;


    public static void acquire(Context ctx) {
     /*   if (wakeLock != null) wakeLock.release();

        PowerManager pm = (PowerManager) ctx.getSystemService(Context.POWER_SERVICE);
        wakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK |
                PowerManager.ACQUIRE_CAUSES_WAKEUP |
                PowerManager.ON_AFTER_RELEASE, "TAG");
        wakeLock.acquire();  */
        if(wakeLock == null) {
            PowerManager pm = (PowerManager) ctx.getSystemService(Context.POWER_SERVICE);
            wakeLock = pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "WAKELOCK");
            KeyguardManager mKeyguardManager=(KeyguardManager)ctx.getSystemService(Context.KEYGUARD_SERVICE);
            mKeyguardLock=mKeyguardManager.newKeyguardLock("KEYGUARDLOCK");
        }
        if(!wakeLock.isHeld()) {
            acquiredBy = 1;
            releasedBy = 0;
            wakeLock.acquire();
            mKeyguardLock.disableKeyguard();
        } else {
            acquiredBy++;
        }
    }

    public static void release() {
     /*   if (wakeLock != null) wakeLock.release(); wakeLock = null;  */
        releasedBy++;
        if(releasedBy == acquiredBy) {
            if(wakeLock != null) {
                wakeLock.release();
                mKeyguardLock.reenableKeyguard();
                mKeyguardLock = null;
                wakeLock = null;
            }
        }
    }
}
