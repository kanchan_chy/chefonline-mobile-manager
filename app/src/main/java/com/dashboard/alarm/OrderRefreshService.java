package com.dashboard.alarm;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.dashboard.fragments.DashBoardTodayFragment;
import com.dashboard.utility.ConstantValues;
import com.dashboard.utility.DateFormatter;

/**
 * Created by user on 1/27/2016.
 */
public class OrderRefreshService extends IntentService {

    public OrderRefreshService() {
        super("OrderRefreshService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {
            Log.e("ALARM_STATUS", "Alarm received");
            DashBoardTodayFragment.fromDate = DateFormatter.getCurrentDate();
            DashBoardTodayFragment.toDate = DateFormatter.getCurrentDate();
            DashBoardTodayFragment.dashBoardPresenter.onClickGetOrder(ConstantValues.ORDER_ONLINE_LIST_API_ID, false);
        } catch (Exception e5) {
        }
        WakeLocker.release();
        //  WakefulBroadcastReceiver.completeWakefulIntent(intent);
    }
}
