package com.dashboard.alarm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.dashboard.utility.ConstantValues;

/**
 * Created by user on 1/27/2016.
 */
public class AlarmReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent.getAction().equals(ConstantValues.ALARM_ACTION)) {
            WakeLocker.acquire(context);
            Intent service = new Intent(context, OrderRefreshService.class);
            context.startService(service);
        /*    ComponentName comp = new ComponentName(context.getPackageName(), OrderRefreshService.class.getName());
            startWakefulService(context, (intent.setComponent(comp)));
           // setResultCode(Activity.RESULT_OK);  */
        }
    }
}
