package com.dashboard.adapter;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintJob;
import android.print.PrintManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chefonline.managermobile.R;
import com.dashboard.activity.MapActivity;
import com.dashboard.customview.CustomToast;
import com.dashboard.datamodel.CustomerDataSerialize;
import com.dashboard.datamodel.DishData;
import com.dashboard.datamodel.OrderAmountData;
import com.dashboard.datamodel.OrderDetailsData;
import com.dashboard.datamodel.PizzaItemData;
import com.dashboard.utility.UtilityMethod;

import java.util.ArrayList;

public class OrderDetailsAdapter extends RecyclerView.Adapter<OrderDetailsAdapter.ViewHolder>{
    public static final int TYPE_HEADER = 0;
    public static final int TYPE_ITEM = 1;
    public static final int TYPE_FOOTER = 2;
    private ArrayList<DishData> mDataset = new ArrayList<>();
    private Activity context;
    private CustomerDataSerialize customerDataSerialize;
    private OrderDetailsData orderDetailsData;
    public static final int MY_PERMISSIONS_REQUEST_CALL_PHONE = 1;
    public static String phNumber = "";

    private String printingText = "";
    private WebView mWebView;


    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView textViewName;
        public TextView textViewPizzaItem;
        public TextView textViewQuantity;
        public TextView textViewPrice;

        public LinearLayout linearLayoutViewOffer;
        public LinearLayout linearAmountHolder;

        public TextView textViewPaymentType;
        public TextView textViewOrderType;
        public TextView textViewOfferText;
        public TextView textViewTotalItemsCount;

        public TextView textViewRestaurantName;
        public TextView textViewCustomerName;
        public TextView textViewAddress;
        public TextView textViewPostcode;
        public TextView textViewCity;
        public TextView textViewMobile;
        public TextView textViewInTime;
        public TextView textViewOutTime;
        public TextView textViewComments;
        public Button btnCall;
        public Button btnMap;
        public Button btnPrint;

        public Button btnMenu;
        public int holderId;
        public boolean addHeader;

        public ViewHolder(View v, int viewType) {
            super(v);

            if (viewType == TYPE_ITEM) {
                textViewName = (TextView) v.findViewById(R.id.textViewName);
                textViewPizzaItem = (TextView) v.findViewById(R.id.textViewPizzaItem);
                textViewQuantity = (TextView) v.findViewById(R.id.textViewQuantity);
                textViewPrice = (TextView) v.findViewById(R.id.textViewPrice);
                holderId = TYPE_ITEM;

            } else if (viewType == TYPE_FOOTER) {
                textViewTotalItemsCount = (TextView) v.findViewById(R.id.textViewTotalItemsCount);
                textViewPaymentType = (TextView) v.findViewById(R.id.textViewPaymentType);
                textViewOrderType = (TextView) v.findViewById(R.id.textViewOrderType);

                textViewOfferText = (TextView) v.findViewById(R.id.textViewOfferText);
                linearLayoutViewOffer = (LinearLayout) v.findViewById(R.id.linearLayoutViewOffer);
                linearAmountHolder = (LinearLayout) v.findViewById(R.id.linearAmountHolder);

                holderId = TYPE_FOOTER;

            } else {
                textViewRestaurantName = (TextView) v.findViewById(R.id.textViewRestaurantName);
                textViewCustomerName = (TextView) v.findViewById(R.id.textViewCustomerName);
                textViewAddress = (TextView) v.findViewById(R.id.textViewAddress);
                textViewPostcode = (TextView) v.findViewById(R.id.textViewPostcode);
                textViewCity = (TextView) v.findViewById(R.id.textViewCity);
                textViewMobile = (TextView) v.findViewById(R.id.textViewMobile);
                textViewInTime = (TextView) v.findViewById(R.id.textViewInTime);
                textViewOutTime = (TextView) v.findViewById(R.id.textViewOutTime);
                textViewComments = (TextView) v.findViewById(R.id.textViewComments);
                btnCall = (Button) v.findViewById(R.id.btnCall);
                btnMap = (Button) v.findViewById(R.id.btnMap);
                btnPrint = (Button) v.findViewById(R.id.btnPrint);
                holderId = TYPE_HEADER;
            }

        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public OrderDetailsAdapter(OrderDetailsData orderDetailsData, Activity context, CustomerDataSerialize customerDataSerialize, boolean addHeader) {
        mDataset = orderDetailsData.getDishDatas();
        this.orderDetailsData = orderDetailsData;
        if(addHeader) {
            DishData dishData2 = new DishData();
            mDataset.add(0, dishData2); // set header in 0 index
        }
        this.context = context;
        this.customerDataSerialize = customerDataSerialize;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public OrderDetailsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v;
        if (viewType == TYPE_ITEM) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_list, parent, false);

        } else if (viewType == TYPE_FOOTER) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.footer_order_summary, parent, false);

        } else {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.header_customer_info, parent, false);
        }


        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v, viewType);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        if (holder.holderId == TYPE_ITEM) {
            holder.textViewName.setText(mDataset.get(position).getDishName());
            holder.textViewQuantity.setText(mDataset.get(position).getDishQty() + "  X");

            String pizzaItems= "";
            double totalPrice = 0.0;
            if(mDataset.get(position).isPizzaMenu()) {
                ArrayList<PizzaItemData> pizzaItemList = mDataset.get(position).getPizzaItemList();
                if(pizzaItemList != null && pizzaItemList.size() > 0) {
                    for (int i = 0; i < pizzaItemList.size(); i++) {
                        if("".equalsIgnoreCase(pizzaItems)) {
                            pizzaItems += pizzaItemList.get(i).getItemName();
                        } else {
                            pizzaItems += ", " + pizzaItemList.get(i).getItemName();
                        }
                        if(pizzaItemList.get(i).getItemPrice() != null && !"".equalsIgnoreCase(pizzaItemList.get(i).getItemPrice()) && !"null".equalsIgnoreCase(pizzaItemList.get(i).getItemPrice())) {
                            totalPrice += Double.valueOf(pizzaItemList.get(i).getItemPrice());
                        }
                    }
                }
            }
            if("".equalsIgnoreCase(pizzaItems)) {
                holder.textViewPizzaItem.setVisibility(View.GONE);
                if(mDataset.get(position).getDishPrice() != null && !"".equalsIgnoreCase(mDataset.get(position).getDishPrice()) && !"null".equalsIgnoreCase(mDataset.get(position).getDishPrice())) {
                    holder.textViewPrice.setText("£" + UtilityMethod.priceFormatter(Double.valueOf(mDataset.get(position).getDishPrice())));
                }
            } else {
                holder.textViewPizzaItem.setText(pizzaItems);
                holder.textViewPizzaItem.setVisibility(View.VISIBLE);

                double qty = 1.0;
                if(mDataset.get(position).getDishQty() != null && !"".equalsIgnoreCase(mDataset.get(position).getDishQty()) && !"null".equalsIgnoreCase(mDataset.get(position).getDishQty())) {
                    qty = Double.parseDouble(mDataset.get(position).getDishQty());
                }
                double realPrice = (qty * totalPrice);
                holder.textViewPrice.setText("£" + UtilityMethod.priceFormatter(realPrice));
            }

        } else if (holder.holderId == TYPE_FOOTER)  {
            if (orderDetailsData.getOffer() != null && !"".equals(orderDetailsData.getOffer()) && !"null".equals(orderDetailsData.getOffer()) && !"Offers available for you.".equalsIgnoreCase(orderDetailsData.getOffer())) {
                holder.linearLayoutViewOffer.setVisibility(View.VISIBLE);
                holder.textViewOfferText.setText("*** " + orderDetailsData.getOffer() + " ***");

            } else {
                holder.linearLayoutViewOffer.setVisibility(View.GONE);
            }

            addAllAmounts(holder);

            holder.textViewPaymentType.setText(orderDetailsData.getPaymentMethod().toUpperCase());
            holder.textViewTotalItemsCount.setText("" + orderDetailsData.getOrderItemCount());
            holder.textViewOrderType.setText(customerDataSerialize.getOrderType().toUpperCase());

            //For Header control
        } else if (holder.holderId == TYPE_HEADER) {
            holder.textViewRestaurantName.setText(orderDetailsData.getRestaurantName());
            holder.textViewCustomerName.setText(customerDataSerialize.getCustomerName().trim().toUpperCase());
            if(customerDataSerialize.getCustomerAddress() != null && !"".equalsIgnoreCase(customerDataSerialize.getCustomerAddress()) && !"null".equalsIgnoreCase(customerDataSerialize.getCustomerAddress())) {
                holder.textViewAddress.setText("" + customerDataSerialize.getCustomerAddress().trim());
            } else {
                holder.textViewAddress.setText("Address not available");
            }
            if(customerDataSerialize.getPostCode() != null && !"".equalsIgnoreCase(customerDataSerialize.getPostCode()) && !"null".equalsIgnoreCase(customerDataSerialize.getPostCode())) {
                holder.textViewPostcode.setText("" + customerDataSerialize.getPostCode().trim().toUpperCase());
            } else {
                holder.textViewPostcode.setText("Postcode not available");
            }
            if(customerDataSerialize.getCustomerCity() != null && !"".equalsIgnoreCase(customerDataSerialize.getCustomerCity()) && !"null".equalsIgnoreCase(customerDataSerialize.getCustomerCity())) {
                holder.textViewCity.setText("" + customerDataSerialize.getCustomerCity().trim().toUpperCase());
            } else {
                holder.textViewCity.setVisibility(View.GONE);
            }
            if(customerDataSerialize.getCustomerMobile() != null && !"".equalsIgnoreCase(customerDataSerialize.getCustomerMobile()) && !"null".equalsIgnoreCase(customerDataSerialize.getCustomerMobile())) {
                holder.textViewMobile.setText(customerDataSerialize.getCustomerMobile());
            } else {
                holder.textViewMobile.setText("Mobile number not available");
            }
            holder.textViewInTime.setText("IN: " + customerDataSerialize.getOrderIn());
            holder.textViewOutTime.setText("OUT: " + customerDataSerialize.getOrderOut());
            if (customerDataSerialize.getComments() == null || customerDataSerialize.getComments().equalsIgnoreCase("") || customerDataSerialize.getComments().equalsIgnoreCase("null")) {
                holder.textViewComments.setVisibility(View.GONE);
            } else {
                holder.textViewComments.setText(customerDataSerialize.getComments());
            }

            holder.btnCall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(customerDataSerialize.getCustomerMobile() != null && !"".equalsIgnoreCase(customerDataSerialize.getCustomerMobile())) {
                        phNumber = customerDataSerialize.getCustomerMobile();
                        makePhoneCall();
                    } else {
                        showWarningDialog("Sorry! Mobile number not available");
                    }
                }
            });

            holder.btnMap.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(orderDetailsData.getRestLatitude() != null && orderDetailsData.getRestLongitude() != null && orderDetailsData.getCustomerLatitude() != null && orderDetailsData.getCustomerLongitude() != null) {
                        if(!"".equalsIgnoreCase(orderDetailsData.getRestLatitude()) && !"".equalsIgnoreCase(orderDetailsData.getRestLongitude()) && !"".equalsIgnoreCase(orderDetailsData.getCustomerLatitude()) && !"".equalsIgnoreCase(orderDetailsData.getCustomerLongitude())) {
                            if(!"null".equalsIgnoreCase(orderDetailsData.getRestLatitude()) && !"null".equalsIgnoreCase(orderDetailsData.getRestLongitude()) && !"null".equalsIgnoreCase(orderDetailsData.getCustomerLatitude()) && !"null".equalsIgnoreCase(orderDetailsData.getCustomerLongitude())) {
                                Intent intent = new Intent(context, MapActivity.class);
                                intent.putExtra("customer_lat", orderDetailsData.getCustomerLatitude());
                                intent.putExtra("customer_lon", orderDetailsData.getCustomerLongitude());
                                intent.putExtra("rest_lat", orderDetailsData.getRestLatitude());
                                intent.putExtra("rest_lon", orderDetailsData.getRestLongitude());
                                intent.putExtra("rest_name", orderDetailsData.getRestaurantName());
                                intent.putExtra("customer_name", orderDetailsData.getCustomerName());
                                context.startActivity(intent);
                            } else {
                                showWarningDialog("Sorry! User Location not found");
                            }
                        } else {
                            showWarningDialog("Sorry! User Location not found");
                        }
                    } else {
                        showWarningDialog("Sorry! User Location not found");
                    }
                }
            });


            holder.btnPrint.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    arrangePrintData();
                    printOrderDetailsData();
                }
            });


        }


    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position) == 0) {
            return TYPE_HEADER;
        } else if (isPositionHeader(position) == 2) {
            return TYPE_FOOTER;
        }

        return TYPE_ITEM;
    }

    private int isPositionHeader(int position) {
        if (position == 0) {
            return 0;

        } else if (position == mDataset.size()) {
            return 2;
        }

        return 1;
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size() + 1;
    }


    private void addAllAmounts(ViewHolder holder) {
        ArrayList<OrderAmountData> orderAmountDatas = orderDetailsData.getOrderAmountDatas();
        int size = orderAmountDatas.size();
        TextView[] textViewNames = new TextView[size];
        TextView[] textViewAmounts = new TextView[size];
        View[] views = new View[size];
        holder.linearAmountHolder.removeAllViews();
        LayoutInflater inflater = LayoutInflater.from(context);
        for (int i = 0; i < size; i++) {
            views[i] = inflater.inflate(R.layout.row_footer_order_summary, null);
            textViewNames[i] = (TextView)views[i].findViewById(R.id.textViewName);
            textViewAmounts[i] = (TextView)views[i].findViewById(R.id.textViewAmount);
            textViewNames[i].setText(orderAmountDatas.get(i).getName());
            if(orderAmountDatas.get(i).getName().equalsIgnoreCase("Discount Amount")) {
                textViewAmounts[i].setText("-£" + orderAmountDatas.get(i).getAmount());
            } else {
                textViewAmounts[i].setText("£" + orderAmountDatas.get(i).getAmount());
            }
            holder.linearAmountHolder.addView(views[i]);
        }
    }


    private void checkCallPhonePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (context.checkSelfPermission(Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                ActivityCompat.requestPermissions(context, new String[]{Manifest.permission.CALL_PHONE}, MY_PERMISSIONS_REQUEST_CALL_PHONE);
            } else {
                try{
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + phNumber));
                    context.startActivity(callIntent);
                }
                catch (Exception e){
                    //  showWarningDialog("Unable to deliver the call. "+ e.getMessage());
                }
            }
        } else {
            try{
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + phNumber));
                context.startActivity(callIntent);
            }
            catch (Exception e){
                //  showWarningDialog("Unable to deliver the call. "+ e.getMessage());
            }
        }
    }


    private void makePhoneCall() {
        boolean noProblem = false;
        TelephonyManager telMgr = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
        int simState = telMgr.getSimState();
        switch (simState) {
            case TelephonyManager.SIM_STATE_ABSENT:
                noProblem=false;
                showWarningDialog("SIM card not found. Please insert SIM card.");
                break;
            case TelephonyManager.SIM_STATE_NETWORK_LOCKED:
                noProblem=false;
                showWarningDialog("Network problem encountered. Please check network settings.");
                break;
            case TelephonyManager.SIM_STATE_UNKNOWN:
                noProblem=false;
                showWarningDialog("Sorry! Call is not possible");
                break;
            case TelephonyManager.SIM_STATE_READY:
                noProblem = true;
                break;
        }
        if(noProblem){
            checkCallPhonePermission();
        }
    }


    private void showWarningDialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(true);
        builder.setTitle("Warning");
        builder.setMessage(message);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();

    }




    private void arrangePrintData() {
        printingText = "";
        printingText += "<b>" + orderDetailsData.getRestaurantName() + "</b><br>";

        printingText += "<hr>";
        printingText += customerDataSerialize.getOrderType().toUpperCase() + "<br>";
        printingText += "IN: &nbsp;" + customerDataSerialize.getOrderIn() + " &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; OUT: &nbsp;" + customerDataSerialize.getOrderOut() + "<br>";

        printingText += "<hr>";
        printingText += customerDataSerialize.getCustomerName().trim().toUpperCase() + "<br>";
        if(customerDataSerialize.getCustomerAddress() != null && !"".equalsIgnoreCase(customerDataSerialize.getCustomerAddress()) && !"null".equalsIgnoreCase(customerDataSerialize.getCustomerAddress())) {
            printingText += "" + customerDataSerialize.getCustomerAddress().trim() + "<br>";
        } else {
            printingText += "Address not available" + "<br>";
        }
        if(customerDataSerialize.getCustomerCity() != null && !"".equalsIgnoreCase(customerDataSerialize.getCustomerCity()) && !"null".equalsIgnoreCase(customerDataSerialize.getCustomerCity())) {
            printingText += "" + customerDataSerialize.getCustomerCity().trim().toUpperCase() + "<br>";
        }
        if(customerDataSerialize.getPostCode() != null && !"".equalsIgnoreCase(customerDataSerialize.getPostCode()) && !"null".equalsIgnoreCase(customerDataSerialize.getPostCode())) {
            printingText += "" + customerDataSerialize.getPostCode().trim().toUpperCase() + "<br>";
        } else {
            printingText += "Postcode not available" + "<br>";
        }
        if(customerDataSerialize.getCustomerMobile() != null && !"".equalsIgnoreCase(customerDataSerialize.getCustomerMobile()) && !"null".equalsIgnoreCase(customerDataSerialize.getCustomerMobile())) {
            printingText += "" + customerDataSerialize.getCustomerMobile() + "<br>";
        } else {
            printingText += "Mobile number not available" + "<br>";
        }

        if (customerDataSerialize.getComments() != null && !customerDataSerialize.getComments().equalsIgnoreCase("") && !customerDataSerialize.getComments().equalsIgnoreCase("null")) {
            printingText += "<hr>Notes:<br>";
            printingText += "" + customerDataSerialize.getComments() + "<br>";
        }

        printingText += "<hr>";
        printingText += "<Table>";

        for (int i = 1; i < mDataset.size(); i++) {

            String pizzaItems= "";
            double totalPrice = 0.0;
            if(mDataset.get(i).isPizzaMenu()) {
                ArrayList<PizzaItemData> pizzaItemList = mDataset.get(i).getPizzaItemList();
                if(pizzaItemList != null && pizzaItemList.size() > 0) {
                    for (int j = 0; j < pizzaItemList.size(); j++) {
                        if("".equalsIgnoreCase(pizzaItems)) {
                            pizzaItems += pizzaItemList.get(j).getItemName();
                        } else {
                            pizzaItems += ", " + pizzaItemList.get(j).getItemName();
                        }
                        if(pizzaItemList.get(j).getItemPrice() != null && !"".equalsIgnoreCase(pizzaItemList.get(j).getItemPrice()) && !"null".equalsIgnoreCase(pizzaItemList.get(j).getItemPrice())) {
                            totalPrice += Double.valueOf(pizzaItemList.get(j).getItemPrice());
                        }
                    }
                }
            }

            if("".equalsIgnoreCase(pizzaItems)) {
                printingText += "<tr><td>" + mDataset.get(i).getDishQty() + " X " + mDataset.get(i).getDishName() + "</td><td>&nbsp;&nbsp;£" + UtilityMethod.priceFormatter(Double.valueOf(mDataset.get(i).getDishPrice())) + "</td></tr>";
            } else {
                double qty = 1.0;
                if(mDataset.get(i).getDishQty() != null && !"".equalsIgnoreCase(mDataset.get(i).getDishQty()) && !"null".equalsIgnoreCase(mDataset.get(i).getDishQty())) {
                    qty = Double.parseDouble(mDataset.get(i).getDishQty());
                }
                double realPrice = (qty * totalPrice);
                printingText += "<tr><td>" + mDataset.get(i).getDishQty() + " X " + mDataset.get(i).getDishName() + "</td><td>&nbsp;&nbsp;£" + UtilityMethod.priceFormatter(realPrice) + "</td></tr>";
                if(qty < 10.0) {
                    printingText += "<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + pizzaItems+ "</td></tr>";
                } else {
                    printingText += "<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + pizzaItems+ "</td></tr>";
                }
            }
        }

        printingText += "</Table>";

        boolean hasGrandTotal = false;
        printingText += "<hr>";
        printingText += "<Table>";
        printingText += "<tr><td>ORDER TOTAL</td><td>&nbsp; &nbsp;£" + UtilityMethod.priceFormatterRound(Double.valueOf(orderDetailsData.getTotalAmount())) + "</td></tr>";
        if (orderDetailsData.getDiscountAmount() != null && !orderDetailsData.getDiscountAmount().equalsIgnoreCase("") && !orderDetailsData.getDiscountAmount().equalsIgnoreCase("null")) {
            if (orderDetailsData.getDiscountAmount().equalsIgnoreCase("0.00")) {
                printingText += "<tr><td>DISCOUNT</td><td>&nbsp; &nbsp; £" + UtilityMethod.priceFormatterRound(Double.valueOf(orderDetailsData.getDiscountAmount())) + "</td></tr>";
            } else {
                printingText += "<tr><td>DISCOUNT</td><td>&nbsp; &nbsp; -£" + UtilityMethod.priceFormatterRound(Double.valueOf(orderDetailsData.getDiscountAmount())) + "</td></tr>";
            }
            hasGrandTotal = true;
        }
        if(orderDetailsData.getDeliveryCharge() != null && !orderDetailsData.getDeliveryCharge().equalsIgnoreCase("") && !orderDetailsData.getDeliveryCharge().equalsIgnoreCase("null")) {
            double deliveryCharge = Double.valueOf(orderDetailsData.getDeliveryCharge());
            if(deliveryCharge > 0.0) {
                printingText += "<tr><td>DELIVERY CHARGE></td><td>&nbsp; &nbsp; £" + UtilityMethod.priceFormatterRound(deliveryCharge) + "</td></tr>";
                hasGrandTotal = true;
            }
        }
        if(hasGrandTotal) {
            printingText += "<tr><td>GRAND TOTAL</td><td>&nbsp; &nbsp; £" + UtilityMethod.priceFormatterRound(Double.valueOf(orderDetailsData.getGrandTotal())) + "</td></tr>";
        }

        printingText += "</Table>";

        printingText += "<hr>";
        printingText += "TOTAL ITEM(S) &nbsp; &nbsp; &nbsp;" + orderDetailsData.getOrderItemCount() + "<br>";
        printingText += "PAYMENT METHOD: &nbsp;" + orderDetailsData.getPaymentMethod().toUpperCase() + "<br>";

        if (orderDetailsData.getOffer() != null && !"".equals(orderDetailsData.getOffer()) && !"null".equals(orderDetailsData.getOffer()) && !"Offers available for you.".equalsIgnoreCase(orderDetailsData.getOffer())) {
            printingText += "<hr>";
            printingText += "*** " + orderDetailsData.getOffer() + " ***";
        }
       // Log.e("Print", printingText);
    }


    private void createWebPrintJob(WebView webView) {

        // Get a PrintManager instance
        PrintManager printManager = (PrintManager) context.getSystemService(Context.PRINT_SERVICE);

        // Get a print adapter instance
        PrintDocumentAdapter printAdapter = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            printAdapter = webView.createPrintDocumentAdapter();
            // Create a print job with name and adapter instance
            String jobName = context.getString(R.string.app_name) + " Document";
            PrintJob printJob = printManager.print(jobName, printAdapter, new PrintAttributes.Builder().build());

            // Save the job object for later status checking
            // mPrintJobs.add(printJob);
        } else {
            new CustomToast(context, "This feature is not supported in your device", "", false);
        }

    }


    private void printOrderDetailsData() {
        WebView webView = new WebView(context);
        webView.setWebViewClient(new WebViewClient() {

            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                // Log.i(TAG, "page finished loading " + url);
                createWebPrintJob(view);
                mWebView = null;
            }
        });

        // Generate an HTML document on the fly:
        String htmlDocument = "<html><body>" + printingText + "</body></html>";
        webView.loadDataWithBaseURL(null, htmlDocument, "text/HTML", "UTF-8", null);

        // Keep a reference to WebView object until you pass the PrintDocumentAdapter
        // to the PrintManager
        mWebView = webView;
    }




}