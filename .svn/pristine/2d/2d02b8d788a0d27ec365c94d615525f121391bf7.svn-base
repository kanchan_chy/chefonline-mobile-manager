package com.dashboard.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chefonline.managermobile.R;
import com.dashboard.customview.CustomToast;
import com.dashboard.presenter.AdminSettingsPresenter;
import com.dashboard.singleton.ApplicationVisibleState;
import com.dashboard.utility.PreferenceUtil;
import com.dashboard.utility.UtilityMethod;
import com.dashboard.view.AdminSettingsActivityView;

/**
 * Created by user on 1/6/2016.
 */
public class AdminSettingsActivity extends AppCompatActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener, AdminSettingsActivityView {
    int onlineOrderStatus = 0;
    AdminSettingsPresenter adminSettingsPresenter;
    ProgressDialog progress;
    //Button btnDeleteOrders, btnChangePassword, btnChangePinCode, btnTurnOffOnOrderOnline, btnPaymentSetting, btnReservationSettings, btnClose;
    RelativeLayout relativeDeleteOrders, relativeChangePassword, relativeChangePincode, relativePaymentSettings, relativeReservationSettings;
    CheckBox checkOnlineOrder;
    CheckBox checkRememberPassword;
    LinearLayout linearContainer;
    private long mLastClickTime = 0;
    boolean callDialog = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_settings2);
        initView();
        if(savedInstanceState == null) {
            callDialog = true;
            progress = new ProgressDialog(AdminSettingsActivity.this);
            adminSettingsPresenter = new AdminSettingsPresenter(this);
            adminSettingsPresenter.getRestaurantOrderOnlineStatus(new PreferenceUtil(this).getResID());
        } else {
            callDialog = false;
            boolean tempChecked = savedInstanceState.getBoolean("isChecked");
            checkOnlineOrder.setChecked(tempChecked);
            visibleUI();
        }

    }

    @Override
    protected void onPause() {
        ApplicationVisibleState.activityPaused();
        super.onPause();
    }

    @Override
    protected void onResume() {
        ApplicationVisibleState.activityResumed();
        super.onResume();
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("isChecked", checkOnlineOrder.isChecked());
    }

    private void initView() {
        relativeDeleteOrders = (RelativeLayout) findViewById(R.id.relativeDeleteOrder);
        relativeChangePassword = (RelativeLayout) findViewById(R.id.relativeChangePassword);
        relativeChangePincode = (RelativeLayout) findViewById(R.id.relativeChangePin);
        relativeReservationSettings = (RelativeLayout) findViewById(R.id.relativeReservationSettings);
        relativePaymentSettings = (RelativeLayout) findViewById(R.id.relativePaymentSettings);
        linearContainer = (LinearLayout) findViewById(R.id.linearContainer);
        checkOnlineOrder = (CheckBox) findViewById(R.id.checkOnlineOrder);
        checkRememberPassword = (CheckBox) findViewById(R.id.checkRememberPassword);

        relativeDeleteOrders.setOnClickListener(this);
        relativeChangePassword.setOnClickListener(this);
        relativeChangePincode.setOnClickListener(this);
        relativeReservationSettings.setOnClickListener(this);
        relativePaymentSettings.setOnClickListener(this);
        checkOnlineOrder.setOnCheckedChangeListener(this);
        checkRememberPassword.setOnCheckedChangeListener(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Admin Settings");
        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

        PreferenceUtil preferenceUtil = new PreferenceUtil(this);
        checkRememberPassword.setChecked(preferenceUtil.getRememberUser());

    }

    private void initialWorkToDeleteOrders() {
        if (UtilityMethod.isConnectedToInternet(AdminSettingsActivity.this)) {
            Intent intent = new Intent(AdminSettingsActivity.this, DashBoardActivity.class);
            intent.putExtra("deleteEnabled", true);
            startActivity(intent);
        } else {
            showInternetDialog("Please check your internet connection.", "RETRY", "CANCEL");
        }
    }

    public void showInternetDialog(String message, String okButton, String cancelButton) {
        final Dialog dialog = new Dialog(AdminSettingsActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.dialog_exit);
        TextView txtViewPopupMessage = (TextView) dialog.findViewById(R.id.txtViewPopupMessage);
        txtViewPopupMessage.setText(message);
        final TextView txtAccept = (TextView) dialog.findViewById(R.id.txtAccept);
        txtAccept.setText(okButton);

        // if button is clicked, close the custom dialog
        txtAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000){
                    mLastClickTime = SystemClock.elapsedRealtime();
                    return;
                }
                else {
                    mLastClickTime = SystemClock.elapsedRealtime();
                }
                dialog.dismiss();
                initialWorkToDeleteOrders();
            }

        });

        final TextView txtCancel = (TextView) dialog.findViewById(R.id.txtCancel);
        txtCancel.setText(cancelButton);
        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        dialog.show();
    }

    public void showTurnOffConfirmationDialog() {
        final Dialog dialog = new Dialog(AdminSettingsActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.dialog_warning);
        TextView txtMessage = (TextView) dialog.findViewById(R.id.txtMessage);
        if(onlineOrderStatus == 1) txtMessage.setText("Are you sure want to turn on online ordering?");
        else txtMessage.setText("Are you sure want to turn off online ordering?");
        final TextView txtAccept = (TextView) dialog.findViewById(R.id.txtAccept);
        txtAccept.setText("YES");

        // if button is clicked, close the custom dialog
        txtAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    mLastClickTime = SystemClock.elapsedRealtime();
                    return;
                } else {
                    mLastClickTime = SystemClock.elapsedRealtime();
                }
                dialog.dismiss();
                if (onlineOrderStatus == 1) {
                    adminSettingsPresenter.postRestaurantOrderOnlineStatus(new PreferenceUtil(AdminSettingsActivity.this).getResID(), "0");
                } else {
                    showTurnOffMessageDialog();
                }
            }

        });

        final TextView txtCancel = (TextView) dialog.findViewById(R.id.txtCancel);
        txtCancel.setText("NO");
        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onlineOrderStatus == 1) {
                    callDialog = false;
                    checkOnlineOrder.setChecked(false);
                } else {
                    callDialog = false;
                    checkOnlineOrder.setChecked(true);
                }
                dialog.dismiss();
            }

        });

        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                if (onlineOrderStatus == 1) {
                    callDialog = false;
                    checkOnlineOrder.setChecked(false);
                } else {
                    callDialog = false;
                    checkOnlineOrder.setChecked(true);
                }
            }
        });

        dialog.show();
    }

    public void showTurnOffMessageDialog() {
        final Dialog dialog = new Dialog(AdminSettingsActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.dialog_warning);
        TextView txtWarning = (TextView) dialog.findViewById(R.id.txtWarning);
        txtWarning.setText("Message");
        TextView txtMessage = (TextView) dialog.findViewById(R.id.txtMessage);
        txtMessage.setText("Turning online ordering off will restrict customers from making any order. Restaurant will be shown as a coming soon restaurant");
        final TextView txtAccept = (TextView) dialog.findViewById(R.id.txtAccept);
        txtAccept.setText("ACCEPT");

        // if button is clicked, close the custom dialog
        txtAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    mLastClickTime = SystemClock.elapsedRealtime();
                    return;
                } else {
                    mLastClickTime = SystemClock.elapsedRealtime();
                }
                dialog.dismiss();
                adminSettingsPresenter.postRestaurantOrderOnlineStatus(new PreferenceUtil(AdminSettingsActivity.this).getResID(), "1");
            }

        });

        final TextView txtCancel = (TextView) dialog.findViewById(R.id.txtCancel);
        txtCancel.setText("CANCEL");
        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onlineOrderStatus == 1) {
                    callDialog = false;
                    checkOnlineOrder.setChecked(false);
                } else {
                    callDialog = false;
                    checkOnlineOrder.setChecked(true);
                }
                dialog.dismiss();
            }

        });

        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                if (onlineOrderStatus == 1) {
                    callDialog = false;
                    checkOnlineOrder.setChecked(false);
                } else {
                    callDialog = false;
                    checkOnlineOrder.setChecked(true);
                }
            }
        });

        dialog.show();
    }



    public void showPasswordWarning(String message, String okButton, String cancelButton) {
        final Dialog dialog = new Dialog(AdminSettingsActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_exit);
        TextView txtViewPopupMessage = (TextView) dialog.findViewById(R.id.txtViewPopupMessage);
        txtViewPopupMessage.setText(message);
        final TextView txtAccept = (TextView) dialog.findViewById(R.id.txtAccept);
        txtAccept.setText(okButton);

        // if button is clicked, close the custom dialog
        txtAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                makeLogout();
            }

        });

        TextView txtCancel = (TextView) dialog.findViewById(R.id.txtCancel);
        txtCancel.setVisibility(View.GONE);
      /*  txtCancel.setText(cancelButton);
        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });  */

        dialog.show();
    }


    private void makeLogout() {
        PreferenceUtil preferenceUtil = new PreferenceUtil(AdminSettingsActivity.this);
        preferenceUtil.setLogInStatus(0); //Logout
        preferenceUtil.setResID("");
        Intent intent = new Intent(AdminSettingsActivity.this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }


    @Override
    public void setOrderOnlineStatus(String value) {
        if ("1".equalsIgnoreCase(value)) {
            onlineOrderStatus = 1;
            if(checkOnlineOrder.isChecked()) {
                callDialog = false;
                checkOnlineOrder.setChecked(false);
            }
        } else {
            onlineOrderStatus = 0;
            if(!checkOnlineOrder.isChecked()) {
                callDialog = false;
                checkOnlineOrder.setChecked(true);
            }
        }
    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    @Override
    public void setEnableOrderOnline(boolean value) {
        checkOnlineOrder.setEnabled(value);
    }

    @Override
    public void showLoading() {
        progress.setMessage("Please wait....");
        progress.setCancelable(false);
        progress.setCanceledOnTouchOutside(false);
        progress.show();
    }

    @Override
    public void hideLoading() {
        progress.dismiss();
    }

    @Override
    public void visibleUI() {
        if(linearContainer.getVisibility() == View.INVISIBLE) {
            linearContainer.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (buttonView.getId() == R.id.checkOnlineOrder) {
            if(callDialog) {
                showTurnOffConfirmationDialog();
            } else {
                callDialog = true;
            }
        } else if(buttonView.getId() == R.id.checkRememberPassword) {
            PreferenceUtil preferenceUtil = new PreferenceUtil(AdminSettingsActivity.this);
            preferenceUtil.setRememberUser(isChecked);
            if(isChecked) {
                if(preferenceUtil.getUserPassword() == null || "".equalsIgnoreCase(preferenceUtil.getUserPassword())) {
                    showPasswordWarning("You have to provide your password at least once. Please login again.", "LOGIN", "CANCEL");
                }
            }
        }
    }


    @Override
    public void onClick(View v) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000){
            mLastClickTime = SystemClock.elapsedRealtime();
            return;
        }
        else {
            mLastClickTime = SystemClock.elapsedRealtime();
        }
        if (v.getId() == R.id.relativeDeleteOrder) {
            initialWorkToDeleteOrders();
        } else if (v.getId() == R.id.relativeChangePassword) {
            startActivity(new Intent(AdminSettingsActivity.this, ResetPasswordActivity.class));
        }else if (v.getId() == R.id.relativeChangePin) {
            startActivity(new Intent(AdminSettingsActivity.this,ChangePincodeActivity.class));
        } else if (v.getId() == R.id.relativePaymentSettings) {
            new CustomToast(AdminSettingsActivity.this, "Development on progress", "", false);
        } else if (v.getId() == R.id.relativeReservationSettings) {
            startActivity(new Intent(AdminSettingsActivity.this,ReservationSettingsActivity.class));
        }

    }


}
