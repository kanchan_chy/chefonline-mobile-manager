package com.dashboard.jsonparser;

import android.util.Log;

import com.dashboard.datamodel.DishData;
import com.dashboard.datamodel.EndOfDayData;
import com.dashboard.datamodel.EndOfDayPayment;
import com.dashboard.datamodel.EndOfDayUser;
import com.dashboard.datamodel.OrderAmountData;
import com.dashboard.datamodel.OrderData;
import com.dashboard.datamodel.OrderOnlineStatusData;
import com.dashboard.datamodel.PaymentData;
import com.dashboard.datamodel.ReservationDataConfirmed;
import com.dashboard.datamodel.ReservationDataUnconfirmed;
import com.dashboard.datamodel.RestaurantListData;
import com.dashboard.datamodel.ScheduleData;
import com.dashboard.datamodel.OrderDetailsData;
import com.dashboard.datamodel.UserData;
import com.dashboard.singleton.AppData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by masum on 10/03/2015.
 */
public class JsonParser {
    public static String TAG = "JsonParser";

    public static String getPostCode (String response){
        try {
            JSONObject jsonObject = new org.json.JSONObject(response);
            JSONArray jsonArray = jsonObject.getJSONArray("result");

            int length = jsonArray.length();
            for (int i = 0; i < 1; i++) {
                return jsonArray.getJSONObject(i).getString("postcode");
            }

        } catch (Exception e) {
            return "";
        }

        return "";
    }

    //API 32
    public static UserData parseUserDate(String response) {
        UserData userData = new UserData();
        try {
            JSONObject jsonObject = new org.json.JSONObject(response);

            if ( 1 == jsonObject.getInt("status")) {
                userData.setMessage(jsonObject.getString("msg"));
                userData.setStatus(jsonObject.getInt("status"));
                userData.setUserGroupId(jsonObject.getString("user_group_id"));
                if("4".equalsIgnoreCase(userData.getUserGroupId())) {
                    userData.setUserId(jsonObject.getJSONObject("UserDetails").getString("user_id"));
                    userData.setRestaurantId(jsonObject.getJSONObject("UserDetails").getString("rest_id"));
                    userData.setRestaurantName(jsonObject.getJSONObject("UserDetails").getString("restaurant_name"));
                    userData.setMobileNumber(jsonObject.getJSONObject("UserDetails").getString("mobile"));
                    userData.setUserEmail(jsonObject.getJSONObject("UserDetails").getString("email"));
                    userData.setUserName(jsonObject.getJSONObject("UserDetails").getString("name"));
                } else if("1".equalsIgnoreCase(userData.getUserGroupId())) {
                    userData.setUserId(jsonObject.getJSONObject("UserDetails").getString("user_id"));
                    userData.setMobileNumber(jsonObject.getJSONObject("UserDetails").getString("mobile"));
                    userData.setUserEmail(jsonObject.getJSONObject("UserDetails").getString("email"));
                    userData.setUserName(jsonObject.getJSONObject("UserDetails").getString("name"));
                }

            } else if (0 == jsonObject.getInt("status")) {
                userData.setMessage(jsonObject.getString("msg"));
                userData.setStatus(jsonObject.getInt("status"));
            }

        } catch (Exception e) {
            Log.e("Parse Error", "" + e.getMessage());
            return userData;
        }

        return userData;
    }

    //Api 33
    public static ArrayList<OrderData> parseOrderData(String response) {

        ArrayList<OrderData> orderHistoryDatas = new ArrayList<>();
        try {
            JSONObject jsonObject = new org.json.JSONObject(response);

            if ("1".equalsIgnoreCase(jsonObject.getString("status"))) {
                AppData.getInstance().setTotalCash(jsonObject.getDouble("cash"));
                AppData.getInstance().setTotalPaypal(jsonObject.getDouble("paypal"));
                AppData.getInstance().setGrandTotal(jsonObject.getDouble("total"));

                ArrayList<PaymentData> paymentDatas = new ArrayList<>();
                JSONArray jPayments = jsonObject.getJSONArray("payment");
                for (int i = 0; i < jPayments.length(); i++) {
                    PaymentData paymentData = new PaymentData();
                    paymentData.setPaymentMethod(jPayments.getJSONObject(i).getString("payment_method"));
                    paymentData.setPaymentAmount(jPayments.getJSONObject(i).getString("amount"));
                    paymentDatas.add(paymentData);
                }
                AppData.getInstance().setPaymentDatas(paymentDatas);

                JSONArray jsonArray = jsonObject.getJSONArray("orders");

                for (int i = 0; i < jsonArray.length(); i ++) {
                    OrderData orderData = new OrderData();
                    orderData.setOrderNo(jsonArray.getJSONObject(i).getString("order_no"));
                    orderData.setCustomerName(jsonArray.getJSONObject(i).getString("customer_name"));
                    orderData.setCustomerCity(jsonArray.getJSONObject(i).getString("city"));
                    orderData.setCustomerAddress(jsonArray.getJSONObject(i).getString("address"));
                    orderData.setCustomerMobile(jsonArray.getJSONObject(i).getString("customer_mobile"));

                    orderData.setOrderType(jsonArray.getJSONObject(i).getString("order_type"));
                    orderData.setPostCode(jsonArray.getJSONObject(i).getString("postcode"));
                    orderData.setGrandTotal(jsonArray.getJSONObject(i).getString("grand_total"));

                    orderData.setOrderDate(jsonArray.getJSONObject(i).getString("order_date"));
                    orderData.setOrderIn(jsonArray.getJSONObject(i).getString("order_in"));
                    orderData.setOrderOut(jsonArray.getJSONObject(i).getString("order_out"));
                    orderData.setPaymentMethod(jsonArray.getJSONObject(i).getString("payment_method"));
                    orderData.setPaymentStatus(jsonArray.getJSONObject(i).getString("payment_status"));
                    orderData.setPrintStatus(jsonArray.getJSONObject(i).getString("printing_status"));
                    orderData.setComments(jsonArray.getJSONObject(i).getString("comments"));
                    orderData.setPaymentTransactionId(jsonArray.getJSONObject(i).getString("paypal_transaction_id"));
                    orderHistoryDatas.add(orderData);
                    // orderHistoryData.setAcceptReservation(jsonArray.getJSONObject(i).getString("accept_reservation"));
                }
            }

        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            return orderHistoryDatas;
        }

        return orderHistoryDatas;
    }

    //Api 55
    public static ArrayList<ReservationDataUnconfirmed> parseReservationData(String response) {

        ArrayList<ReservationDataUnconfirmed> reservationHistoryDatas = new ArrayList<>();
        try {
            JSONObject jsonObject = new org.json.JSONObject(response);

            if ("Success".equalsIgnoreCase(jsonObject.getString("status"))) {

                JSONArray jsonArray = jsonObject.getJSONArray("reservation_list");

                for (int i = 0; i < jsonArray.length(); i ++) {
                    ReservationDataUnconfirmed reservationDataUnconfirmed = new ReservationDataUnconfirmed();
                    reservationDataUnconfirmed.setReservationId(jsonArray.getJSONObject(i).getString("id"));
                    reservationDataUnconfirmed.setCustomerFirstName(jsonArray.getJSONObject(i).getString("first_name"));
                    reservationDataUnconfirmed.setCustomerLastName(jsonArray.getJSONObject(i).getString("last_name"));
                    reservationDataUnconfirmed.setCustomerMObileNo(jsonArray.getJSONObject(i).getString("mobile"));
                    reservationDataUnconfirmed.setCustomerEmail(jsonArray.getJSONObject(i).getString("email"));
                    reservationDataUnconfirmed.setReservedDate(jsonArray.getJSONObject(i).getString("reservation_date"));
                    reservationDataUnconfirmed.setReservedTime(jsonArray.getJSONObject(i).getString("reservation_time"));
                    reservationDataUnconfirmed.setCreatedDate(jsonArray.getJSONObject(i).getString("created_date"));
                    reservationDataUnconfirmed.setCreatedTime(jsonArray.getJSONObject(i).getString("created_time"));
                    reservationDataUnconfirmed.setSpecialRequest(jsonArray.getJSONObject(i).getString("special_request"));
                    reservationDataUnconfirmed.setGuestNumber(jsonArray.getJSONObject(i).getString("no_of_guest"));
                    reservationDataUnconfirmed.setReservationStatus(jsonArray.getJSONObject(i).getString("status"));
                    reservationHistoryDatas.add(reservationDataUnconfirmed);
                }
            }

        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            return reservationHistoryDatas;
        }

        return reservationHistoryDatas;
    }

    //Api 26
    public static ReservationDataUnconfirmed parseSingleReservationData(String response) {
        ReservationDataUnconfirmed reservationDataUnconfirmed = new ReservationDataUnconfirmed();
        try {
            JSONObject jsonObject = new org.json.JSONObject(response);

            if ("Success".equalsIgnoreCase(jsonObject.getString("status"))) {

                JSONObject jsonObject1 = jsonObject.getJSONObject("reservation");

                //for (int i = 0; i < jsonArray.length(); i ++) {

                    reservationDataUnconfirmed.setReservationId(jsonObject1.getString("id"));
                    reservationDataUnconfirmed.setCustomerFirstName(jsonObject1.getString("first_name"));
                    reservationDataUnconfirmed.setCustomerLastName(jsonObject1.getString("last_name"));
                    reservationDataUnconfirmed.setCustomerMObileNo(jsonObject1.getString("mobile"));
                    reservationDataUnconfirmed.setCustomerEmail(jsonObject1.getString("email"));
                    reservationDataUnconfirmed.setReservedDate(jsonObject1.getString("reservation_date"));
                    reservationDataUnconfirmed.setReservedTime(jsonObject1.getString("reservation_time"));
                    reservationDataUnconfirmed.setSpecialRequest(jsonObject1.getString("special_request"));
                    reservationDataUnconfirmed.setCreatedDate(jsonObject1.getString("created_date"));
                    reservationDataUnconfirmed.setCreatedTime(jsonObject1.getString("created_time"));
                    reservationDataUnconfirmed.setGuestNumber(jsonObject1.getString("no_of_guest"));
                    //reservationHistoryDatas.add(reservationDataUnconfirmed);
                //}
            }

        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            return reservationDataUnconfirmed;
        }

        return reservationDataUnconfirmed;
    }

    // API 51
    public static EndOfDayData parseEndOfDayData(String response) {

        EndOfDayData endOfDayData = new EndOfDayData();
        ArrayList<EndOfDayPayment> endOfDayPayments = new ArrayList<>();
        ArrayList<EndOfDayUser> endOfDayUsers = new ArrayList<>();
        try {
            JSONObject jsonObject = new org.json.JSONObject(response);

            if ("Success".equalsIgnoreCase(jsonObject.getString("status"))) {
                endOfDayData.setTotalAmount(Double.valueOf(jsonObject.getJSONObject("total").getString("total_amount")));
                endOfDayData.setTotalItem(jsonObject.getJSONObject("total").getString("total_item_sold"));

                JSONArray jsonArray = jsonObject.getJSONArray("payment");
                for (int i = 0; i < jsonArray.length(); i ++) {
                    EndOfDayPayment endOfDayPayment = new EndOfDayPayment();
                    endOfDayPayment.setGrandTotal(Double.valueOf(jsonArray.getJSONObject(i).getString("grand_total")));
                    endOfDayPayment.setPaymentMethod(jsonArray.getJSONObject(i).getString("payment_method"));
                    endOfDayPayments.add(endOfDayPayment);
                }
                endOfDayData.setEndOfDayPayments(endOfDayPayments);

                JSONArray jsonArrayUsers = jsonObject.getJSONArray("users");
                for (int i=0; i < jsonArrayUsers.length(); i++) {
                    EndOfDayUser endOfDayUser = new EndOfDayUser();
                    endOfDayUser.setUserId(jsonArrayUsers.getJSONObject(i).getString("user_id"));
                    endOfDayUser.setUserName(jsonArrayUsers.getJSONObject(i).getString("username"));
                    endOfDayUser.setOrderPrice(jsonArrayUsers.getJSONObject(i).getString("price"));
                    endOfDayUser.setOrderedDish(jsonArrayUsers.getJSONObject(i).getString("total_ordered_dish"));
                    endOfDayUsers.add(endOfDayUser);
                }
                endOfDayData.setEndOfDayUsers(endOfDayUsers);
            }

        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            return endOfDayData;
        }

        return endOfDayData;
    }

    public static OrderDetailsData parseOrderDetailsData(String response) throws JSONException {
        OrderDetailsData orderDetailsData = new OrderDetailsData();
        ArrayList<DishData> dishDatas = new ArrayList<>();

        try {
            JSONObject jsonObject = new org.json.JSONObject(response);
            orderDetailsData.setTotalAmount(jsonObject.getString("total"));
            orderDetailsData.setGrandTotal(jsonObject.getString("grand_total"));
            orderDetailsData.setDiscountAmount(jsonObject.getString("discount_amount"));
            orderDetailsData.setPaymentMethod(jsonObject.getString("payment_method"));
            orderDetailsData.setDeliveryCharge(jsonObject.getString("delivery_charge"));
            orderDetailsData.setRestaurantName(jsonObject.getString("restaurant_name"));
            orderDetailsData.setOrderItemCount(jsonObject.getInt("total_ordered_quantity"));
            orderDetailsData.setOffer(jsonObject.getString("offer"));
            orderDetailsData.setOrderIn(jsonObject.getString("order_time"));
            orderDetailsData.setOrderOut(jsonObject.getString("delivery_time"));
            orderDetailsData.setOrderType(jsonObject.getString("order_type"));
            orderDetailsData.setComments(jsonObject.getString("comments"));
            orderDetailsData.setRestLatitude(jsonObject.getString("restaurant_latitude"));
            orderDetailsData.setRestLongitude(jsonObject.getString("restaurant_longitude"));

            orderDetailsData.setCustomerName(jsonObject.getJSONObject("user_details").getString("full_name"));
            orderDetailsData.setCustomerAddress(jsonObject.getJSONObject("user_details").getString("address1") + ", " + jsonObject.getJSONObject("user_details").getString("address2"));
            orderDetailsData.setCustomerMobile(jsonObject.getJSONObject("user_details").getString("mobile_no"));
            orderDetailsData.setCustomerCity(jsonObject.getJSONObject("user_details").getString("town"));
            orderDetailsData.setPostCode(jsonObject.getJSONObject("user_details").getString("postcode"));
            orderDetailsData.setCustomerEmail(jsonObject.getJSONObject("user_details").getString("email"));
            orderDetailsData.setCustomerLatitude(jsonObject.getJSONObject("user_details").getString("user_latitude"));
            orderDetailsData.setCustomerLongitude(jsonObject.getJSONObject("user_details").getString("user_longitude"));

            JSONArray jsonArray = jsonObject.getJSONArray("item_list");
            for (int i = 0; i < jsonArray.length(); i ++) {
                DishData dishData = new DishData();
                dishData.setDishName(jsonArray.getJSONObject(i).getString("dish_name"));
                dishData.setDishId(jsonArray.getJSONObject(i).getString("dish_id"));
                dishData.setDishPrice(jsonArray.getJSONObject(i).getString("dish_price"));
                dishData.setDishUnitPrice(jsonArray.getJSONObject(i).getString("unit_price"));
                dishData.setDishQty(jsonArray.getJSONObject(i).getString("quantity"));
                dishDatas.add(dishData);

               // orderHistoryData.setDishDatas(dishDatas);
            }
            orderDetailsData.setDishDatas(dishDatas);

            ArrayList<OrderAmountData> orderAmountDatas = new ArrayList<>();
            JSONArray jAmounts = jsonObject.getJSONArray("order_amount_details");
            for(int i = 0; i < jAmounts.length(); i++) {
                OrderAmountData orderAmountData = new OrderAmountData();
                orderAmountData.setName(jAmounts.getJSONObject(i).getString("name"));
                orderAmountData.setAmount(jAmounts.getJSONObject(i).getString("amount"));
                orderAmountDatas.add(orderAmountData);
            }
            orderDetailsData.setOrderAmountDatas(orderAmountDatas);

        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            return orderDetailsData;
        }

        return orderDetailsData;
    }

    //API 35
    public static String parseDeleteOrder(String response) {
        try {
            JSONObject jsonObject = new org.json.JSONObject(response);

            if ( 1 == jsonObject.getInt("status")) {
                return "1";

            } else if (0 == jsonObject.getInt("status")) {
                return "0";
            }

        } catch (Exception e) {
            Log.e("Parse Error", "" + e.getMessage());
            return "0";
        }

        return "0";
    }

    public static ArrayList<ScheduleData> getScheduleData(String response) {
        ArrayList<ScheduleData> scheduleDatas = new ArrayList<>();
        try {
            JSONObject jsonObject = new org.json.JSONObject(response);
            JSONArray jsonArray = jsonObject.getJSONArray("schedule");
            int length = jsonArray.length();

            for (int j = 0; j < length; j++) {
                ScheduleData scheduleData = new ScheduleData();
                scheduleData.setWeekDayId(jsonArray.getJSONObject(j).getString("weekday_id"));
                scheduleData.setDayName(jsonArray.getJSONObject(j).getString("weekday_name"));
                scheduleData.setOpeningTime(jsonArray.getJSONObject(j).getString("opening_time_formated"));
                scheduleData.setClosingTime(jsonArray.getJSONObject(j).getString("closing_time_formated"));
                scheduleDatas.add(scheduleData);
            }
        } catch (Exception e) {
            return scheduleDatas;
        }

     return scheduleDatas;
    }

    public static OrderOnlineStatusData getRestaurantOnlineOrderStatus(String response) {
        Log.i("Resp", "****" + response);
        OrderOnlineStatusData restaurantOnlineOrderStatusData = new OrderOnlineStatusData();
        try {
            JSONObject jsonObject = new org.json.JSONObject(response);
            JSONObject jsonObject1 = jsonObject.getJSONObject("data");
            restaurantOnlineOrderStatusData.setMessage(jsonObject.getString("msg"));
            restaurantOnlineOrderStatusData.setSuccess(jsonObject.getString("status"));
            restaurantOnlineOrderStatusData.setIsComingSoon(jsonObject1.getString("coming_soon"));

        } catch (Exception e) {
            Log.e("Error",e +" ");
            restaurantOnlineOrderStatusData.setIsComingSoon("0");
            restaurantOnlineOrderStatusData.setSuccess("Error");
            restaurantOnlineOrderStatusData.setMessage("Update problem encountered");
            return restaurantOnlineOrderStatusData;
        }

        return restaurantOnlineOrderStatusData;
    }


    //API 35
    public static String getReservationUpdateData(String response) {
        try {
            JSONObject jsonObject = new org.json.JSONObject(response);

            if ("Success".equalsIgnoreCase(jsonObject.getString("status"))) {
                return jsonObject.getString("msg");

            } else if("Failed".equalsIgnoreCase(jsonObject.getString("status"))) {
                return jsonObject.getString("msg");
            }

        } catch (Exception e) {
            Log.e("Parse Error", "" + e.getMessage());
            return "Something went wrong.";
        }

        return "0";
    }

    //API 66
    public static ArrayList<RestaurantListData> parseRestaurantList(String response) {
        ArrayList<RestaurantListData> restaurantListDatas = new ArrayList<>();
        try {
            JSONObject jsonObject = new org.json.JSONObject(response);
            if("Success".equalsIgnoreCase(jsonObject.getString("status"))) {
                JSONArray jLists = jsonObject.getJSONArray("list");
                for (int i = 0; i < jLists.length(); i++) {
                    RestaurantListData restaurantListData = new RestaurantListData();
                    JSONObject jRest = jLists.getJSONObject(i);
                    restaurantListData.setRestaurantId(jRest.getString("rest_id"));
                    restaurantListData.setRestaurantName(jRest.getString("restaurant_name"));
                    restaurantListData.setLogoUrl(jRest.getString("logo"));
                    restaurantListData.setCuisineName(jRest.getString("cuisine"));
                    restaurantListDatas.add(restaurantListData);
                }
                return restaurantListDatas;
            } else {
                return restaurantListDatas;
            }
        } catch (Exception e) {

        }
        return restaurantListDatas;
    }

}
