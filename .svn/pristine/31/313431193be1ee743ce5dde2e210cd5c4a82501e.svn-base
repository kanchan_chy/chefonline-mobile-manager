package com.dashboard.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chefonline.managermobile.R;
import com.dashboard.customview.CustomToast;
import com.dashboard.datamodel.ReservationDataSerialize;
import com.dashboard.presenter.ReservationDetailsPresenter;
import com.dashboard.singleton.ApplicationVisibleState;
import com.dashboard.utility.DateFormatter;
import com.dashboard.view.ReservationDetailsView;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ReservationDetailsActivity extends AppCompatActivity implements ReservationDetailsView, View.OnClickListener{

    ReservationDetailsPresenter reservationDetailsPresenter;
    ReservationDataSerialize reservationDataSerialize;

    TextView textViewRestaurantName;
    TextView textViewCustomerName;
    TextView textViewEmail;
    TextView textViewMobile;
    TextView textViewGuestNo;
    TextView textViewCreatedDate;
    TextView textViewCreatedTime;
    TextView textViewReservedDate;
    TextView textViewReservedTime;
    TextView textViewSpecialLevel;
    TextView textViewSpecialInstruction;
    Button btnAccept;
    Button btnDecline;
    LinearLayout linearButtons;
    ProgressDialog progress;

    boolean fromUnconfirmed;
    private long mLastClickTime = 0;
    private boolean isPressedAccept = false;
    public static final int MY_PERMISSIONS_REQUEST_CALL_PHONE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reservation_details);
        initView();

    }

    @Override
    protected void onPause() {
        ApplicationVisibleState.activityPaused();
        super.onPause();
    }

    @Override
    protected void onResume() {
        ApplicationVisibleState.activityResumed();
        super.onResume();
    }

    private void initView() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Reservation Details");
        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

        progress = new ProgressDialog(this);
        textViewRestaurantName = (TextView) findViewById(R.id.textViewRestaurantName);
        textViewCustomerName = (TextView) findViewById(R.id.textViewCustomerName);
        textViewEmail = (TextView) findViewById(R.id.textViewEmail);
        textViewMobile = (TextView) findViewById(R.id.textViewMobile);
        textViewGuestNo = (TextView) findViewById(R.id.textViewGuestNo);
        textViewCreatedDate = (TextView) findViewById(R.id.textViewCreatedDate);
        textViewCreatedTime = (TextView) findViewById(R.id.textViewCreatedTime);
        textViewReservedDate = (TextView) findViewById(R.id.textViewReservationDate);
        textViewReservedTime = (TextView) findViewById(R.id.textViewReservationTime);
        textViewSpecialLevel = (TextView) findViewById(R.id.textViewSpecialLevel);
        textViewSpecialInstruction = (TextView) findViewById(R.id.textViewSpecialInstruction);
        linearButtons = (LinearLayout) findViewById(R.id.linearButtons);

        textViewMobile.setOnClickListener(this);

        btnAccept = (Button) findViewById(R.id.btnAccept);
        btnAccept.setOnClickListener(this);
        btnDecline = (Button) findViewById(R.id.btnDecline);
        btnDecline.setOnClickListener(this);

        try {
            fromUnconfirmed = getIntent().getExtras().getBoolean("fromUnconfirmed");
            reservationDataSerialize = (ReservationDataSerialize) getIntent().getSerializableExtra("sampleObject");
        }
        catch (Exception e) {
            fromUnconfirmed = false;
        }

        if(!fromUnconfirmed) {
            linearButtons.setVisibility(View.GONE);
        } else {
            if(isReservationTimeOver()) {
                linearButtons.setVisibility(View.GONE);
            }
        }

        reservationDetailsPresenter  = new ReservationDetailsPresenter(this);
        reservationDetailsPresenter.showReservationDetails(reservationDataSerialize);
    }

    /*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_order_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.home) {
            onBackPressed();
            return true;
        } else if (id == R.id.action_print) {
            UtilityMethod.printReservationReport(getApplicationContext(), reservationDataSerialize);

        } else if (id == R.id.action_print_text) {
            UtilityMethod.printReservationReport(getApplicationContext(), reservationDataSerialize);
        }

        return super.onOptionsItemSelected(item);
    }  */

    private boolean isReservationTimeOver() {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            String currentDateStr = sdf.format(new Date());
            Date currentDate = sdf.parse(currentDateStr);
            String reservDateStr = reservationDataSerialize.getReservedDate();
            Date reservDate = sdf.parse(reservDateStr);
            if(reservDate.before(currentDate)) {
                return true;
            } else if(currentDate.before(reservDate)) {
                return false;
            } else {
                SimpleDateFormat sdfTime = new SimpleDateFormat("hh:mm a");
                String currentTimeStr = sdfTime.format(new Date());
                Date currentTime = sdfTime.parse(currentTimeStr);
                String reservTimeStr = reservationDataSerialize.getReservedTime();
                Date reservTime = sdfTime.parse(reservTimeStr);
                if(currentTime.before(reservTime)) {
                    return false;
                } else {
                    return true;
                }
            }
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "" + e, Toast.LENGTH_LONG).show();
        }

        return false;
    }

    @Override
    public Context getClassContext() {
        return ReservationDetailsActivity.this;
    }

    @Override
    public void setReservationData(ReservationDataSerialize reservationDataSerialize) {
        textViewRestaurantName.setText(reservationDataSerialize.getRestaurantName());
        String customerName = "";
        if(reservationDataSerialize.getCustomerTitle() != null && !reservationDataSerialize.getCustomerTitle().trim().equalsIgnoreCase("") && !reservationDataSerialize.getCustomerTitle().trim().equalsIgnoreCase("null")) {
            customerName = customerName + reservationDataSerialize.getCustomerTitle().trim().toUpperCase() + " ";
        }
        if(reservationDataSerialize.getCustomerFirstName() != null && !reservationDataSerialize.getCustomerFirstName().trim().equalsIgnoreCase("") && !reservationDataSerialize.getCustomerFirstName().trim().equalsIgnoreCase("null")) {
            customerName += reservationDataSerialize.getCustomerFirstName().trim().toUpperCase();
        }
        if(reservationDataSerialize.getCustomerLastName() != null && !reservationDataSerialize.getCustomerLastName().trim().equalsIgnoreCase("") && !reservationDataSerialize.getCustomerLastName().trim().equalsIgnoreCase("null")) {
            customerName += " " + reservationDataSerialize.getCustomerLastName().trim().toUpperCase();
        }
        textViewCustomerName.setText(customerName);
        if(reservationDataSerialize.getCustomerEmail() != null && !reservationDataSerialize.getCustomerEmail().equalsIgnoreCase("") && !reservationDataSerialize.getCustomerEmail().equalsIgnoreCase("null")) {
            textViewEmail.setText("" + reservationDataSerialize.getCustomerEmail().trim());
        } else {
            textViewEmail.setVisibility(View.GONE);
        }
        if(reservationDataSerialize.getCustomerMObileNo() != null && !"".equalsIgnoreCase(reservationDataSerialize.getCustomerMObileNo()) && !"null".equalsIgnoreCase(reservationDataSerialize.getCustomerMObileNo())) {
            textViewMobile.setText(reservationDataSerialize.getCustomerMObileNo().trim());
        } else {
            textViewMobile.setVisibility(View.GONE);
        }
        textViewGuestNo.setText(reservationDataSerialize.getGuestNumber());
        textViewCreatedDate.setText(reservationDataSerialize.getCreatedDate());
        textViewCreatedTime.setText(reservationDataSerialize.getCreatedTime());
        textViewReservedDate.setText(reservationDataSerialize.getReservedDate());
        textViewReservedTime.setText(reservationDataSerialize.getReservedTime());
        if(reservationDataSerialize.getSpecialRequest() == null || reservationDataSerialize.getSpecialRequest().trim().equalsIgnoreCase("") || reservationDataSerialize.getSpecialRequest().trim().equalsIgnoreCase("null")) {
            textViewSpecialLevel.setVisibility(View.GONE);
            textViewSpecialInstruction.setVisibility(View.GONE);
        }
        else {
            textViewSpecialInstruction.setText(reservationDataSerialize.getSpecialRequest().trim());
        }
    }

    @Override
    public String getRestaurantId() {
        return reservationDataSerialize.getRestaurantId();
    }

    @Override
    public String getRestaurantName() {
        return reservationDataSerialize.getRestaurantName();
    }

    @Override
    public String getReservationId() {
        return reservationDataSerialize.getReservationId();
    }

    @Override
    public void setMessage(String message, boolean success) {
        new CustomToast(this, "" + message, "", success);
    }

    @Override
    public void setFinishActivity() {
        Intent intent = new Intent("on-update-completed");
        LocalBroadcastManager.getInstance(ReservationDetailsActivity.this).sendBroadcast(intent);
        finish();
    }

    @Override
    public void showLoading() {
        progress.setMessage("Please wait....");
        progress.show();
    }

    @Override
    public void hideLoading() {
        progress.dismiss();
    }

    @Override
    protected void onDestroy() {
        if (isPressedAccept) {
            if(DateFormatter.getCurrentDate().equalsIgnoreCase(reservationDataSerialize.getReservedDate())) {
                Intent intent = new Intent("on-update-completed");
                LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
            }
        }
        super.onDestroy();
    }


    private void checkCallPhonePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, MY_PERMISSIONS_REQUEST_CALL_PHONE);
            } else {
                try{
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + reservationDataSerialize.getCustomerMObileNo()));
                    startActivity(callIntent);
                }
                catch (Exception e){
                    //  showWarningDialog("Unable to deliver the call. "+ e.getMessage());
                }
            }
        } else {
            try{
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + reservationDataSerialize.getCustomerMObileNo()));
                startActivity(callIntent);
            }
            catch (Exception e){
                //  showWarningDialog("Unable to deliver the call. "+ e.getMessage());
            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL_PHONE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (checkSelfPermission(Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            try{
                                Intent callIntent = new Intent(Intent.ACTION_CALL);
                                callIntent.setData(Uri.parse("tel:" + reservationDataSerialize.getCustomerMObileNo()));
                                startActivity(callIntent);
                            }
                            catch (Exception e){
                                //  showWarningDialog("Unable to deliver the call. "+ e.getMessage());
                            }
                        }
                    }
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
        }
    }

    private void showWarningDialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle("Warning");
        builder.setMessage(message);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();

    }

    private void makePhoneCall() {
        boolean noProblem = false;
        TelephonyManager telMgr = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        int simState = telMgr.getSimState();
        switch (simState) {
            case TelephonyManager.SIM_STATE_ABSENT:
                noProblem=false;
                  showWarningDialog("SIM card not found. Please insert SIM card.");
                break;
            case TelephonyManager.SIM_STATE_NETWORK_LOCKED:
                noProblem=false;
                 showWarningDialog("Network problem encountered. Please check network settings.");
                break;
            case TelephonyManager.SIM_STATE_UNKNOWN:
                noProblem=false;
                showWarningDialog("Sorry! Call is not possible");
                break;
            case TelephonyManager.SIM_STATE_READY:
                noProblem = true;
                break;
        }
        if(noProblem){
            checkCallPhonePermission();
        }
    }


    @Override
    public void onClick(View v) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000){
            mLastClickTime = SystemClock.elapsedRealtime();
            return;
        } else {
            mLastClickTime = SystemClock.elapsedRealtime();
        }
        switch (v.getId()) {
            case R.id.btnAccept:
                isPressedAccept = true;
                reservationDetailsPresenter.acceptDeclineReservation("27", "1");
                break;
            case R.id.btnDecline:
                isPressedAccept = false;
                reservationDetailsPresenter.acceptDeclineReservation("27", "-1"); // -1 for decline 3 for unconfirmed
                break;
            case R.id.textViewMobile:
                if(reservationDataSerialize.getCustomerMObileNo() != null && !"".equalsIgnoreCase(reservationDataSerialize.getCustomerMObileNo())) {
                    makePhoneCall();
                }
        }

    }


}
